[Setup]
AppId={{018D5B15-9AB3-4915-A23E-E2143F6B93A1}
AppName=5SZian Payroll System
AppVersion=2.0.17.410
;AppVerName=5SZian Payroll System 2.0.17.410
AppPublisher=Mach 21 Studios
DefaultDirName={pf}\5SZian Payroll System
DisableProgramGroupPage=yes
OutputBaseFilename=5SZPayroll_2.0.17.410
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\ClosedXML.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\ClosedXML.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\CsvHelper.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\CsvHelper.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\CsvHelper.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\database.sdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\DocumentFormat.OpenXml.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\DocumentFormat.OpenXml.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\Microsoft.ReportViewer.WinForms.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.vshost.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.vshost.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\PayrollSystem.vshost.exe.manifest"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\Release Notes.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\System.Data.SqlServerCe.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\hkri\PayrollSystem\Inno Setup Files\PayrollSystem v.2.0.17.410\Resources\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{commonprograms}\5SZian Payroll System"; Filename: "{app}\PayrollSystem.exe"
Name: "{commondesktop}\5SZian Payroll System"; Filename: "{app}\PayrollSystem.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\PayrollSystem.exe"; Description: "{cm:LaunchProgram,5SZian Payroll System}"; Flags: nowait postinstall skipifsilent

[Dirs]
Name: "{app}"; Permissions: everyone-modify;