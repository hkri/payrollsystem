﻿using System;
using ClosedXML;
using System.Text;
using System.Linq;
using ClosedXML.Excel;
using System.Diagnostics;
using System.Collections.Generic;

namespace PayrollSystem {

    /*
     * ClosedXMLEditor
     * A class to help simplify ClosedXML spreadsheet editing.
     * 
     * John Espiritu
     * Created: July 21, 2016 
     * Updated: July 23, 2016
     * 
     */

    /// <summary>
    /// A class to help simplify ClosedXML spreadsheet editing.
    /// </summary>
    public class ClosedXMLEditor : IDisposable {
        
        #region constructor

        /// <summary>
        /// Initializes new instance of ClosedXMLEditor and prepare necessary stuff.
        /// A single instance of ClosedXMLEditor creates new XLWorkbook object.
        /// </summary>
        public ClosedXMLEditor() {
            Workbook = new XLWorkbook();
        }

        /// <summary>
        /// Initializes new instance of ClosedXMLEditor and prepare necessary stuff.
        /// </summary>
        /// <param name="workbook">Existing workbook to work with.</param>
        public ClosedXMLEditor(XLWorkbook workbook) {
            //If ever user provided a null workbook, create new instance instead.
            if (workbook == null)
                Workbook = new XLWorkbook(XLEventTracking.Disabled);
            else
                Workbook = workbook;
        }

        #endregion

        #region Enums

        /// <summary>
        /// Common cell formatting. Used by ClosedXMLFormatter class.
        /// </summary>
        public enum CellFormats : byte {
            Bold = 1,
            Italic = 2,
            Underlined = 4,
            Strikethrough = 8,
            None = 16
        }

        /// <summary>
        /// Text alignment types.
        /// </summary>
        public enum TextAlignments : int {
            /*
             *   First 3 bits: Top, Middle, Bottom
             *   Last 3 bits:  Left, Center, Right
             *              32 16 8   4 2 1
             *   (vertical) 1  1  1 | 0 0 0 (Horizontal)
             */
            //Group 32
            TopLeft =       36,
            TopCenter =     34,
            TopRight =      33,

            //Group 16
            MiddleLeft =    20,
            MiddleCenter =  18,
            MiddleRight =   17,

            //Group 8
            BottomLeft =    12,
            BottomCenter =  10,
            BottomRight =   9
        }

        /// <summary>
        /// Cell border styles.
        /// </summary>
        public enum CellBorders : int {
            None    =    1,
            Left    =    2,
            Top     =    4,
            Right   =    8,
            Bottom  =    16,
            All     =    32,
            Outside =    64
        }

        #endregion

        #region Properties

        XLWorkbook _workbook = null;
        /// <summary>
        /// Gets or sets the workbook associated with this editor.
        /// </summary>
        public XLWorkbook Workbook {
            get { return _workbook; }
            set { _workbook = value; }
        }

        IXLWorksheet _activeWorksheet = null;
        /// <summary>
        /// Gets or sets the active worksheet.
        /// </summary>
        public IXLWorksheet ActiveWorksheet {
            get {
                return _activeWorksheet;
            }
        }
        
        CellBorders _borders = CellBorders.None;
        /// <summary>
        /// Sets or gets the borders of the cell.
        /// </summary>
        public CellBorders Borders {
            get { return _borders; }
            set { _borders = value; }
        }

        XLBorderStyleValues _borderStyle = XLBorderStyleValues.None;
        /// <summary>
        /// Get or sets the border style of the cell.
        /// </summary>
        public XLBorderStyleValues BorderStyle {
            get { return _borderStyle; }
            set { _borderStyle = value; }
        }

        XLColor _borderColor = XLColor.Black;
        /// <summary>
        /// Gets or sets the border colors.
        /// </summary>
        public XLColor BorderColor {
            get { return _borderColor; }
            set { _borderColor = value; }
        }

        String _fontName = "Calibri";
        /// <summary>
        /// Gets or sets the font name of the cell.
        /// </summary>
        public String FontName {
            get { return _fontName; }
            set { _fontName = value; }
        }

        XLColor _backColor = XLColor.NoColor;
        /// <summary>
        /// Gets or sets the back color of the cell.
        /// </summary>
        public XLColor BackColor {
            get { return _backColor; }
            set { _backColor = value; }
        }

        TextAlignments _textAlignment = TextAlignments.MiddleLeft;
        /// <summary>
        /// Sets or gets the text alignment of the text in the cell.
        /// </summary>
        public TextAlignments TextAlignment {
            get { return _textAlignment; }
            set { _textAlignment = value; }
        }
        
        XLColor _fontColor = XLColor.Black;
        /// <summary>
        /// Sets or gets the primary font color for cell formatting.
        /// </summary>
        public XLColor FontColor {
            get { return _fontColor; }
            set { _fontColor = value; }
        }

        CellFormats _formatting = CellFormats.None;
        /// <summary>
        /// The basic text formatting (bold, italic, underlined) to be used on formatting functions.
        /// </summary>
        public CellFormats Formatting {
            get { return _formatting; }
            set { _formatting = value; }
        }

        int _fontSize = 11;
        /// <summary>
        /// The font size to be used on formatting functions.
        /// </summary>
        public int FontSize {
            get { return _fontSize; }
            set { _fontSize = value; }
        }

        IXLStyle _cellStyle;
        /// <summary>
        /// IXLStyle of the setting.
        /// </summary>
        public IXLStyle CellStyle {
            get { return _cellStyle; }
            set { _cellStyle = value; }
        }

        String _numberFormat = "0";
        /// <summary>
        /// Gets or sets the number format of the cell.
        /// </summary>
        public String NumberFormat {
            get { return _numberFormat; }
            set { _numberFormat = value; }
        }

        String _dateFormat = "mm/dd/yyyy";
        /// <summary>
        /// Gets or sets the date format of the cell.
        /// </summary>
        public String DateFormat {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        String _fileName = "";
        /// <summary>
        /// Gets or sets the filename of the workbook file.
        /// </summary>
        public String FileName {
            get { return _fileName; }
            set { _fileName = value; }
        }

        bool _forceRightAlignNumbers = true;
        /// <summary>
        /// Forces right alignment on numeric cells.
        /// </summary>
        public bool ForceRightAlignNumbers {
            get { return _forceRightAlignNumbers; }
            set { _forceRightAlignNumbers = value; }
        }

                
        #endregion

        #region Disposable Implementation

        /// <summary>
        /// Dispose worksheets and workbook upon ClosedXMLEditor dispose.
        /// </summary>
        public void Dispose() {
            if (ActiveWorksheet != null)
                ActiveWorksheet.Dispose();
            if (Workbook != null)
                Workbook.Dispose();
        }

        #endregion

        #region Workbook Functions

        /// <summary>
        /// Saves the workbook on the specified filename.
        /// </summary>
        /// <param name="filename">Filename of the workbook.</param>
        public bool Save(string filename) {
            try {
                FileName = filename;
                Workbook.SaveAs(filename);
                return true;
            } catch (Exception ex) {
                Trace.WriteLine("ClosedXMLEditor@Save(): " + ex.Message);
                return false;
            }
        }

        #endregion

        #region Worksheet Functions

        /// <summary>
        /// Sets the active worksheet. If sheet does not exist, create it.
        /// </summary>
        /// <param name="sheetName">Name of the worksheet to set.</param>
        /// <returns></returns>
        public IXLWorksheet SetWorksheet(string sheetName) {
            IXLWorksheet sheet;
            if (Workbook.TryGetWorksheet(sheetName, out sheet)) {
                _activeWorksheet = sheet;
                return sheet;
            } else {
                _activeWorksheet = Workbook.Worksheets.Add(sheetName);
                return ActiveWorksheet;
            }
        }

        /// <summary>
        /// Deletes the worksheet of the specified name.
        /// </summary>
        /// <param name="sheetName">Name of worksheet to delete.</param>
        public void DeleteWorksheet(string sheetName) {
            try {
                Workbook.Worksheets.Delete(sheetName);  //Delete worksheet.
            } catch (Exception ex) {
                Trace.WriteLine(ex.Message);    //Log error message, if there is any.
            }
        }

        #endregion

        #region Cell Retrieval Functions

        /// <summary>
        /// Retrieves the cell of the active worksheet based on specified address.
        /// </summary>
        /// <param name="irow">Row index.</param>
        /// <param name="icol">Column index.</param>
        /// <returns>Cell at the specified row-column index.</returns>
        public IXLCell Cell(int irow, int icol) {
            if (ActiveWorksheet != null)
                return ActiveWorksheet.Cell(irow, icol);
            else
                throw new Exception("Active worksheet is null");
        }

        /// <summary>
        /// Gets spreadsheet cell at the specified address.
        /// </summary>
        /// <param name="address">Cell address.</param>
        /// <returns>Cell at the specified address.</returns>
        public IXLCell Cell(string address) {
            if (ActiveWorksheet == null) throw new Exception("Active worksheet is null.");
            return ActiveWorksheet.Cell(address);
        }

        /// <summary>
        /// Returns the column at the specified index.
        /// </summary>
        /// <param name="index">Column index.</param>
        /// <returns>Column at the specified index.</returns>
        public IXLColumn Column(int index) {
            if (ActiveWorksheet == null) throw new Exception("Active worksheet is null.");
            return ActiveWorksheet.Column(index);
        }

        /// <summary>
        /// Returns column at the specified letter.
        /// </summary>
        /// <param name="address">Letter of the column.</param>
        /// <returns>Column at specified letter.</returns>
        public IXLColumn Column(string address) {
            if (ActiveWorksheet == null) throw new Exception("Active worksheet is null.");
            return ActiveWorksheet.Column(address);
        }

        /// <summary>
        /// Gets collection of columns.
        /// </summary>
        /// <param name="startCol">Starting column index.</param>
        /// <param name="endCol">Last column index.</param>
        /// <returns>Collection of columns within specified indices.</returns>
        public IXLColumns Columns(int startCol, int endCol) {
            if (ActiveWorksheet == null) throw new Exception("Active worksheet is null.");
            return ActiveWorksheet.Columns(startCol, endCol);
        }

        #endregion

        #region Cell Write Functions

        /// <summary>
        /// Selects range of cells, merge it, then inserts value. Based on active worksheet.
        /// </summary>
        /// <param name="rowStart">Address of starting row cell.</param>
        /// <param name="colStart">Address of starting column cell.</param>
        /// <param name="rowLast">Address of last row cell.</param>
        /// <param name="colLast">Address of last column cell.</param>
        /// <param name="value">Value and data type to insert.</param>
        /// <returns>Affected cell range.</returns>
        public IXLRange SetMerge(int rowStart, int colStart, int rowLast, int colLast, XLCellData value) {
            if (value == null)
                throw new Exception("Value cannot be null or empty.");
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet is null (no active worksheet).");
            IXLRange c = ActiveWorksheet.Range(rowStart, colStart, rowLast, colLast).Merge();
            c.Value = value.Value;
            c.DataType = value.DataType;
            Format(c);
            return c;
        }

        /// <summary>
        /// Selects range of cells, merge it, then inserts value. Based on Active worksheet.
        /// </summary>
        /// <param name="rangeAddress">Range address.</param>
        /// <param name="value">Value and data type to insert.</param>
        /// <returns>Affected range.</returns>
        public IXLRange SetMerge(string rangeAddress, XLCellData value) {
            if (value == null)
                throw new Exception("Value cannot be null or empty.");
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet is null (no active worksheet).");
            IXLRange c = ActiveWorksheet.Range(rangeAddress).Merge();
            c.Value = value.Value;
            c.DataType = value.DataType;
            Format(c);
            return c;
        }

        /// <summary>
        /// Inserts specified value continuously starting on the specified row.
        /// </summary>
        /// <param name="startRow">Starting row.</param>
        /// <param name="col">Column index. </param>
        /// <param name="values">Values to insert.</param>
        /// <returns>Affected cells range.</returns>
        public IXLRange SetRows(int startRow, int col, XLCellDataCollection values) {
            if (values == null || values.Count <= 0)
                throw new Exception("Values cannot be null or empty.");
            if(ActiveWorksheet == null)
                throw new Exception("Active worksheet is null.");
            
            for (int i = 0; i < values.Count; i++) {
                IXLCell cell = ActiveWorksheet.Cell(startRow + i, col);
                Set(cell, values[i].Value, values[i].DataType);
            }

            return Format(ActiveWorksheet.Range(startRow, col, startRow + (values.Count - 1), col));
        }

        /// <summary>
        /// Inserts the collection of data as contiguous columns.
        /// </summary>
        /// <param name="startCol">Starting column.</param>
        /// <param name="row">Base row index.</param>
        /// <param name="values">Values to insert.</param>
        /// <returns>Affected cells range.</returns>
        public IXLRange SetCols(int startCol, int row, XLCellDataCollection values){
            if (values == null || values.Count <= 0)
                throw new Exception("Values cannot be null or empty.");
            if(ActiveWorksheet == null)
                throw new Exception("Active worksheet is null."); 
            
            for (int i = 0; i < values.Count; i++) {
                IXLCell cell = ActiveWorksheet.Cell(row, startCol + i);
                cell.SetDataType(values[i].DataType);
                Set(cell, values[i].Value, values[i].DataType);
            }

            return ActiveWorksheet.Range(row, startCol, row, startCol + (values.Count - 1));
        }

        /// <summary>
        /// Inserts collection of data into the spreadsheet, with cell type defined by object type.
        /// </summary>
        /// <param name="startCol">Starting column index.</param>
        /// <param name="row">Starting row index.</param>
        /// <param name="values">Collection of values to be inserted.</param>
        /// <returns></returns>
        public IXLRange SetColsUntyped(int startCol, int row, XLCellDataCollection values) {
            if (values == null || values.Count <= 0)
                throw new Exception("Values cannot be null or empty.");
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet is null.");

            for (int i = 0; i < values.Count; i++) {
                IXLCell cell = ActiveWorksheet.Cell(row, startCol + i);
                Set(cell, values[i].Value);
            }

            return Format(ActiveWorksheet.Range(row, startCol, row, startCol + (values.Count - 1)));
        }

        /// <summary>
        /// Sets value on the specified cell and formats it.
        /// </summary>
        /// <param name="address">Cell address.</param>
        /// <param name="value">Value to be inserted.</param>
        /// <param name="dataType">Datatype of value.</param>
        /// <returns>Affected cell.</returns>
        public IXLCell Set(string address, object value, XLCellValues dataType = XLCellValues.Text) {
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet cannot be null or empty.");
            return Format(Set(ActiveWorksheet.Cell(address), value, dataType));
        }

        /// <summary>
        /// Sets value to the specified cell then formats it.
        /// </summary>
        /// <param name="row">Index of row.</param>
        /// <param name="col">Index of cell column.</param>
        /// <param name="value">Value to be inserted.</param>
        /// <param name="dataType">Data type of value to be inserted.</param>
        /// <returns></returns>
        public IXLCell Set(int row, int col, object value, XLCellValues dataType = XLCellValues.Text) {
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet not set.");
            IXLCell c = ActiveWorksheet.Cell(row, col);
            return Set(c, value, dataType);
        }

        /// <summary>
        /// Sets value to the specified cell.
        /// </summary>
        /// <param name="cell">Cell destination.</param>
        /// <param name="value">Value to insert.</param>
        /// <param name="dataType">Data type of object. Defaulted to Text data type.</param>
        /// <returns>Affected cell.</returns>
        public IXLCell Set(IXLCell cell, object value, XLCellValues dataType = XLCellValues.Text) {
            Format(cell.SetValue(value).SetDataType(dataType));
            return cell;
        }
              
        /// <summary>
        /// Sets the value of the specified cells.
        /// </summary>
        /// <param name="cells">Destination cells.</param>
        /// <param name="values">Values to insert. Must be equal with the number of destination cells.</param>
        /// <param name="dataTypes">Data type of each values. Must be equal with the number of values to be inserted.</param>
        /// <returns>Affected cells.</returns>
        public IXLCells Set(IXLCells cells, ICollection<Object> values, ICollection<XLCellValues> dataTypes = null) {
            if (cells.Count() != values.Count()) throw new Exception("Destination cells and values must be of same count.");
            if (dataTypes != null) {
                if (values.Count != dataTypes.Count)
                    throw new Exception("Values and its corresponding data types must be of same number.");
            }
            for (int i = 0; i < cells.Count(); i++) {
                IXLCell cell = cells.ElementAt(i);
                Object value = values.ElementAt(i);
                XLCellValues dataType = XLCellValues.Text;
                if (dataTypes != null)
                    dataType = dataTypes.ElementAt(i);
                Set(cell, value, dataType);
            }
            return cells;
        }

        /// <summary>
        /// Insert values to the specified cells.
        /// </summary>
        /// <param name="cells">Destination cells.</param>
        /// <param name="values">Values to be inserted.</param>
        /// <returns>Affected cells.</returns>
        public IXLCells Set(IXLCells cells, XLCellDataCollection values) {
            if (cells.Count() != values.Count)
                throw new Exception("Destination cells and values shall have the same quantity.");
            for (int i = 0; i < cells.Count(); i++) {
                IXLCell cell = cells.ElementAt(i);
                Object value = values[i].Value;
                XLCellValues dataType = values[i].DataType;
                Set(cell, value, dataType);
            }
            return Format(cells);
        }

        /// <summary>
        /// Sets data to specified cell.
        /// </summary>
        /// <param name="cell">Destination cell.</param>
        /// <param name="value">Value to insert.</param>
        /// <returns></returns>
        public IXLCell Set(IXLCell cell, object value) {
            Format(cell).SetValue(value);
            return cell;
        }
        
        #endregion

        #region Cell Format Functions

        /// <summary>
        /// Applies the number formatting to specififed cell.
        /// </summary>
        /// <param name="cell">Target cell.</param>
        /// <returns></returns>
        public IXLCell ApplyNumberFormat(IXLCell cell) {
            //Set number format.
            cell.Style.NumberFormat.SetFormat(NumberFormat);
            return cell;
        }

        /// <summary>
        /// Applies the date formatting to the specified cell.
        /// </summary>
        /// <param name="cell">Targe cell.</param>
        /// <returns></returns>
        public IXLCell ApplyDateFormat(IXLCell cell) {
            //Set date format.
            cell.Style.DateFormat.SetFormat(DateFormat);
            return cell;
        }

        /// <summary>
        /// Sets the formatting of the specified cell based on the set formatting properties of this class.
        /// <param name="cell">The cell to format.</param>
        /// </summary>
        public IXLCell Format(IXLCell cell) {
            /* Font Name */
            cell.Style.Font.SetFontName(FontName);

            /* Set font size */
            cell.Style.Font.SetFontSize(FontSize);

            /* Set font color */
            cell.Style.Font.SetFontColor(FontColor);

            /* Set cell background color */
            cell.Style.Fill.SetBackgroundColor(BackColor);

            /* Perform basic formatting (Bold, italic, underline, strikethrough) */
            if ((Formatting & CellFormats.None) != CellFormats.None) {
                cell.Style.Font.SetStrikethrough((Formatting & CellFormats.Strikethrough) == CellFormats.Strikethrough);
                cell.Style.Font.SetBold((Formatting & CellFormats.Bold) == CellFormats.Bold);
                cell.Style.Font.SetItalic((Formatting & CellFormats.Italic) == CellFormats.Italic);
                if ((Formatting & CellFormats.Underlined) == CellFormats.Underlined)
                    cell.Style.Font.SetUnderline(XLFontUnderlineValues.Single);
                else
                    cell.Style.Font.SetUnderline(XLFontUnderlineValues.None);
            } else {
                /* Reset all Formatting if CellFormats = none */
                cell.Style.Font.SetStrikethrough(false).Font.SetBold(false).Font.SetItalic(false);
                cell.Style.Font.SetUnderline(XLFontUnderlineValues.None);
            }

            /* Setup text alignment. */
            /* Vertical Alignment */
            if (((int)TextAlignment & 32) == 32)
                cell.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
            if (((int)TextAlignment & 16) == 16)
                cell.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            if (((int)TextAlignment & 8) == 8)
                cell.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);

            /* Horizontal Alignment */
            if (((int)TextAlignment & 4) == 4)
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            if (((int)TextAlignment & 2) == 2)
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            if (((int)TextAlignment & 1) == 1)
                cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            /* Set the cell borders. */
            if ((Borders & CellBorders.None) == 0) {
                if ((Borders & CellBorders.Left) == CellBorders.Left || (Borders & CellBorders.All) == CellBorders.All)
                    cell.Style.Border.SetLeftBorder(BorderStyle).Border.SetLeftBorderColor(BorderColor);
                if ((Borders & CellBorders.Top) == CellBorders.Top || (Borders & CellBorders.All) == CellBorders.All)
                    cell.Style.Border.SetTopBorder(BorderStyle).Border.SetTopBorderColor(BorderColor);
                if ((Borders & CellBorders.Right) == CellBorders.Right || (Borders & CellBorders.All) == CellBorders.All)
                    cell.Style.Border.SetRightBorder(BorderStyle).Border.SetRightBorderColor(BorderColor);
                if ((Borders & CellBorders.Bottom) == CellBorders.Bottom || (Borders & CellBorders.All) == CellBorders.All)
                    cell.Style.Border.SetBottomBorder(BorderStyle).Border.SetBottomBorderColor(BorderColor);
                if ((Borders & CellBorders.Outside) == CellBorders.Outside)
                    cell.Style.Border.SetOutsideBorder(BorderStyle).Border.SetOutsideBorderColor(BorderColor);
            }

            /* Apply number format */
            if ((cell.DataType) == XLCellValues.Number) {
                //Set numeric format on numeric cell data type only.
                cell.Style.NumberFormat.SetFormat(NumberFormat);

                /* Force right align on numeric cell data type */
                if (ForceRightAlignNumbers)
                    cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
            } else if ((cell.DataType) == XLCellValues.DateTime)
                cell.Style.DateFormat.SetFormat(DateFormat);
            else
                cell.Style.NumberFormat.SetFormat("@");

            
            /* Returns the specified cell to allow chain formatting or whatever. */
            return cell;
        }

        /// <summary>
        /// Sets the formatting of the specified cell range based on the set formatting properties of this class.
        /// <param name="cellRange">The cell range to format.</param>
        /// </summary>
        public IXLRange Format(IXLRange cellRange) {
            /* Font Name */
            cellRange.Style.Font.SetFontName(FontName);

            /* Set font size */
            cellRange.Style.Font.SetFontSize(FontSize);

            /* Set font color */
            cellRange.Style.Font.SetFontColor(FontColor);

            /* Set cell background color */
            cellRange.Style.Fill.SetBackgroundColor(BackColor);

            /* Perform basic formatting (Bold, italic, underline, strikethrough) */
            if ((Formatting & CellFormats.None) != CellFormats.None) {
                cellRange.Style.Font.SetStrikethrough((Formatting & CellFormats.Strikethrough) == CellFormats.Strikethrough);
                cellRange.Style.Font.SetBold((Formatting & CellFormats.Bold) == CellFormats.Bold);
                cellRange.Style.Font.SetItalic((Formatting & CellFormats.Italic) == CellFormats.Italic);
                if ((Formatting & CellFormats.Underlined) == CellFormats.Underlined)
                    cellRange.Style.Font.SetUnderline(XLFontUnderlineValues.Single);
                else
                    cellRange.Style.Font.SetUnderline(XLFontUnderlineValues.None);
            } else {
                /* Reset all Formatting if CellFormats = none */
                cellRange.Style.Font.SetStrikethrough(false).Font.SetBold(false).Font.SetItalic(false);
                cellRange.Style.Font.SetUnderline(XLFontUnderlineValues.None);
            }

            /* Setup text alignment. */
            /* Vertical Alignment */
            if (((int)TextAlignment & 32) == 32)
                cellRange.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
            if (((int)TextAlignment & 16) == 16)
                cellRange.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            if (((int)TextAlignment & 8) == 8)
                cellRange.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);

            /* Horizontal Alignment */
            if (((int)TextAlignment & 4) == 4)
                cellRange.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            if (((int)TextAlignment & 2) == 2)
                cellRange.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            if (((int)TextAlignment & 1) == 1)
                cellRange.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            /* Set the cell borders. */
            if ((Borders & CellBorders.None) == 0) {
                if ((Borders & CellBorders.Left) == CellBorders.Left || (Borders & CellBorders.All) == CellBorders.All)
                    cellRange.Style.Border.SetLeftBorder(BorderStyle).Border.SetLeftBorderColor(BorderColor);
                if ((Borders & CellBorders.Top) == CellBorders.Top || (Borders & CellBorders.All) == CellBorders.All)
                    cellRange.Style.Border.SetTopBorder(BorderStyle).Border.SetTopBorderColor(BorderColor);
                if ((Borders & CellBorders.Right) == CellBorders.Right || (Borders & CellBorders.All) == CellBorders.All)
                    cellRange.Style.Border.SetRightBorder(BorderStyle).Border.SetRightBorderColor(BorderColor);
                if ((Borders & CellBorders.Bottom) == CellBorders.Bottom || (Borders & CellBorders.All) == CellBorders.All)
                    cellRange.Style.Border.SetBottomBorder(BorderStyle).Border.SetBottomBorderColor(BorderColor);
                if ((Borders & CellBorders.Outside) == CellBorders.Outside)
                    cellRange.Style.Border.SetOutsideBorder(BorderStyle).Border.SetOutsideBorderColor(BorderColor);
            }
            
            /* Apply number format */
            foreach (IXLCell cell in cellRange.CellsUsed()) {
                if ((cell.DataType) == XLCellValues.Number) {
                    /* Cell numeric data type format */
                    cell.Style.NumberFormat.SetFormat(NumberFormat);

                    /* Force right align on numeric cell data type */
                    if (ForceRightAlignNumbers)
                        cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                } else if ((cell.DataType) == XLCellValues.DateTime)
                    cell.Style.DateFormat.SetFormat(DateFormat);
                else
                    cell.Style.NumberFormat.SetFormat("@");
            }
            /* Returns the specified cell to allow chain formatting or whatever. */
            return cellRange;
        }
        
        /// <summary>
        /// Sets the formatting of the specified cell array based on the set formatting properties of this class.
        /// <param name="cells">The cell array to format.</param>
        /// </summary>
        public IXLCell[] Format(IXLCell[] cells) {
            //Loops cells array and format it individually.
            foreach (IXLCell cell in cells)
                Format(cell);   //Format using the individual cell formatter.

            /* Returns the specified cell to allow chain formatting or whatever. */
            return cells;
        }
        
        /// <summary>
        /// Sets the formatting of the specified cells based on the set formatting properties of this class.
        /// <param name="cells">The cells to format.</param>
        /// </summary>
        public IXLCells Format(IXLCells cells) {
            //Iterate through IXLCells.
            foreach (IXLCell cell in cells)
                Format(cell);   //Format using the individual cell formatter.

            /* Returns the specified cell to allow chain formatting or whatever. */
            return cells;
        }

        #endregion

        #region Property Set Function

        //Function property set to allow set chaining.

        /// <summary>
        /// Sets the font.
        /// </summary>
        /// <param name="fontName">Font name.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetFont(string fontName){
            FontName = fontName;
            return this;
        }

        /// <summary>
        /// Sets font size of content.
        /// </summary>
        /// <param name="fontSize">Font size.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetFontSize(int fontSize) {
            FontSize = fontSize;
            return this;
        }

        /// <summary>
        /// Sets the formatting flags
        /// </summary>
        /// <param name="formatting">Formatting flags.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetFormatting(CellFormats formatting) {
            Formatting = formatting;
            return this;
        }

        /// <summary>
        /// Sets the cell background color fill.
        /// </summary>
        /// <param name="fill">Color.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetBG(XLColor fill) {
            BackColor = fill;
            return this;
        }

        /// <summary>
        /// Sets the foreground color / text color.
        /// </summary>
        /// <param name="foreColor">Color.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetFG(XLColor foreColor) {
            FontColor = foreColor;
            return this;
        }

        /// <summary>
        /// Sets the alignment of the cell content.
        /// </summary>
        /// <param name="alignment">Alignment value.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetAlignment(TextAlignments alignment){
            TextAlignment = alignment;
            return this;
        }

        /// <summary>
        /// Sets the cell border appearance.
        /// </summary>
        /// <param name="borders">Border visibility flags.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetBorders(CellBorders borders) {
            Borders = borders;
            return this;
        }

        /// <summary>
        /// Sets the cell border color.
        /// </summary>
        /// <param name="borderColor">Color.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetBorderColor(XLColor borderColor) {
            BorderColor = borderColor;
            return this;
        }

        /// <summary>
        /// Sets the cell border style.
        /// </summary>
        /// <param name="border">Border style flags.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetBorderStyle(XLBorderStyleValues border) {
            BorderStyle = border;
            return this;
        }

        /// <summary>
        /// Sets the cell number format.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetNumberFormat(string format) {
            NumberFormat = format;
            return this;
        }

        /// <summary>
        /// Sets the date format of date cells.
        /// </summary>
        /// <param name="format">String format.</param>
        /// <returns></returns>
        public ClosedXMLEditor SetDateFormat(string format) {
            DateFormat = format;
            return this;
        }

        #endregion

        #region Column/Row Format Functions

        /// <summary>
        /// Resizes all columns in the collection.
        /// </summary>
        /// <param name="columns">Target columns.</param>
        /// <param name="width">Width of all columns.</param>
        public void SetColsWidth(string colsAddress, double width) {
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet is null or empty.");
            IXLColumns columns = ActiveWorksheet.Columns(colsAddress);
            foreach (IXLColumn col in columns)
                col.Width = width;
        }

        /// <summary>
        /// Resizes all rows in the collection.
        /// </summary>
        /// <param name="rows">Target rows.</param>
        /// <param name="height">Height of all rows.</param>
        public void SetRowsHeight(string rowsAddress, double height) {
            if (ActiveWorksheet == null)
                throw new Exception("Active worksheet is null or empty.");
            IXLRows rows = ActiveWorksheet.Rows(rowsAddress);
            foreach (IXLRow row in rows)
                row.Height = height;
        }

        #endregion

    }

}
