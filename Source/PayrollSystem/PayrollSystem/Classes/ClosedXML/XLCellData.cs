﻿using System;
using ClosedXML;
using System.Text;
using System.Linq;
using ClosedXML.Excel;
using System.Diagnostics;
using System.Collections.Generic;

namespace PayrollSystem {

    // XLCellData
    // A class for encapsulating cell value and data type.
    // To be used alongside ClosedXML
    //
    // John Espiritu
    // July 21, 2016

    /// <summary>
    /// Cell data using ClosedXML.
    /// </summary>
    public class XLCellData {

        #region Constructor

        /// <summary>
        /// Creates a new instance of ClosedXML cell data object.
        /// </summary>
        /// <param name="value">Value of the cell.</param>
        /// <param name="dataType">Value data type.</param>
        public XLCellData(object value, XLCellValues dataType = XLCellValues.Text) {
            Value = value;
            DataType = dataType;
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the value of the cell.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the data type of the cell value.
        /// </summary>
        public XLCellValues DataType { get; set; }

        #endregion

    }

}
