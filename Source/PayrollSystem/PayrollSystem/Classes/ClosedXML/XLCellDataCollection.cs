﻿using System;
using ClosedXML;
using System.Text;
using System.Linq;
using ClosedXML.Excel;
using System.Diagnostics;
using System.Collections.Generic;

namespace PayrollSystem {

    // XLCellDataCollection
    // A collection class for encapsulated cell value and data type (XLCellData).
    // To be used alongside ClosedXML
    //
    // John Espiritu
    // July 21, 2016

    /// <summary>
    /// Creates a new instance of XLCellData collection.
    /// Must be used alongside ClosedXML.
    /// </summary>
    public class XLCellDataCollection : List<XLCellData> {

        #region Constructor

        /// <summary>
        /// Creates new instance of XLCellData collection.
        /// </summary>
        public XLCellDataCollection() { }

        /// <summary>
        /// Creates new instance of XLCellData collection from an IEnumerable collection.
        /// </summary>
        /// <param name="dataArray"></param>
        public XLCellDataCollection(ICollection<XLCellData> dataArray) {
            AddRange(dataArray);
        }

        #endregion

    }

}
