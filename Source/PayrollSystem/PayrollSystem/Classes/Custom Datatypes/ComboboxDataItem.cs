﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    public class ComboboxDataItem {

        private string id = "";

        public string ID {
            get { return id; }
            set { id = value; }
        }

        string value = "";

        public string Value {
            get { return this.value; }
            set { this.value = value; }
        }

        public ComboboxDataItem(string id, string val) {
            ID = id;
            Value = val;
        }

    }

}
