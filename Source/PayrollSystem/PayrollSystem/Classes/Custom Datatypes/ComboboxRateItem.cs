﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    public class ComboboxRateItem {

        #region properties

        private string _id = "";

        public string ID {
            get { return _id; }
            set { _id = value; }
        }

        private string _dest = "";

        public string Destination {
            get { return _dest; }
            set { _dest = value; }
        }

        private double _rate = 0.00d;

        public double Rate {
            get { return _rate; }
            set { _rate = value; }
        }

        private string _rategroup = "";

        public string RateGroup {
            get { return _rategroup; }
            set { _rategroup = value; }
        }

        double _driverrate = 0.00;

        public double DriverRate {
            get { return _driverrate; }
            set { _driverrate = value; }
        }

        double _helperrate = 0.00;

        public double HelperRate {
            get { return _helperrate; }
            set { _helperrate = value; }
        }

        #endregion

        #region constructor

        public ComboboxRateItem(string nID, string nDest, double nRate, string nRateGroup, double nDriverRate, double nHelperRate) {
            ID = nID; Destination = nDest; Rate = nRate; RateGroup = nRateGroup;
            DriverRate = nDriverRate; HelperRate = nHelperRate;
        }

        #endregion

    }

}
