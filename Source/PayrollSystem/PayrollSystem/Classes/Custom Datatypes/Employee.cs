﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    public class Employee {

        #region Constructor

        public Employee(string _firstname, string _middlename, string _lastname, string _position, string _empID) {
            FirstName = _firstname;
            MiddleName = _middlename;
            LastName = _lastname;
            Position = _position;
            EmployeeID = _empID;
        }

        #endregion

        #region Properties

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string EmployeeID { get; set; }

        //F M L
        public string FMLName {
            get {
                string str = FirstName + " ";
                if (MiddleName.Trim() != "") str += MiddleName + " ";
                str += LastName;
                return str;
            }
        }

        //F L
        public string FLName {
            get {
                string str = FirstName + " " + LastName;
                return str;
            }
        }

        // L, F M
        public string LFMName {
            get {
                string str = LastName + ", ";
                str += FirstName;
                if (MiddleName.Trim() != "") str += " " + MiddleName;
                return str;
            }
        }

        // L, F
        public string LFName {
            get {
                string str = LastName + ", ";
                str += FirstName;
                return str;
            }
        }

        #endregion

        #region public function

        /// <summary>
        /// Function to use to check if the given string corresponds to the employee's name in any format.
        /// </summary>
        /// <param name="name_string">String with employee name to check.</param>
        /// <returns></returns>
        public bool CompareEmployeeNameString(string name_string) {
            string ss = name_string.Trim();
            if (ss.Equals(FMLName.Trim(), StringComparison.InvariantCultureIgnoreCase) || ss.Equals(FLName.Trim(), StringComparison.InvariantCultureIgnoreCase) ||
                ss.Equals(LFMName.Trim(), StringComparison.InvariantCultureIgnoreCase) || ss.Equals(LFName.Trim(), StringComparison.InvariantCultureIgnoreCase)) {
                    return true;
            }
            return false;
        }

        public string CompareEmployeeNameString_B(string name_string) {
            string ss = name_string.Trim();
            if (ss.Equals(FMLName.Trim(), StringComparison.InvariantCultureIgnoreCase) || ss.Equals(FLName.Trim(), StringComparison.InvariantCultureIgnoreCase) ||
                ss.Equals(LFMName.Trim(), StringComparison.InvariantCultureIgnoreCase) || ss.Equals(LFName.Trim(), StringComparison.InvariantCultureIgnoreCase)) {
                return EmployeeID;
            }
            return String.Empty;
        }

        #endregion

    }

}
