﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    //Class for Cash Advance items.
    public class PayrollCAItem {

        #region Constructor

        //CA Item contructor.
        public PayrollCAItem(string _date, string _plateNo, string _position, string _employeeID, string _employeeName, double _ca, string _particular) {
            CA = _ca;
            Date = DateTime.Parse(_date).ToString("MMM dd, yyyy");
            PlateNo = _plateNo;
            Position = _position;
            EmployeeID = _employeeID;
            Particular = _particular;
            EmployeeName = _employeeName;
        }

        #endregion

        #region Members

        public double CA { get; set; }
        public string Date { set; get; }
        public string PlateNo { get; set; }
        public string Position { get; set; }
        public string EmployeeID { get; set; }
        public string Particular { get; set; }
        public string EmployeeName { get; set; }

        #endregion

    }

}
