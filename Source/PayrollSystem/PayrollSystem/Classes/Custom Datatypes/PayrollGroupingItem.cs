﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    //Object for creating sums of rate per employee per plate no.
    public class PayrollGroupingItem {


        #region Constructor


        public PayrollGroupingItem(string plateNo, string employeeID, string position, double rate) {
            PlateNo = plateNo;
            EmployeeID = employeeID;
            Rate = rate;
            Position = position;
        }

        public PayrollGroupingItem(string plateNo, string employeeID, string employeeName, string position, double rate) {
            PlateNo = plateNo;
            EmployeeID = employeeID;
            Rate = rate;
            Position = position;
            EmployeeName = employeeName;
        }

        #endregion

        #region properties

        public string EmployeeName { get; set; }

        string _position = "";

        public string Position {
            get { return _position; }
            set { _position = value; }
        }


        string _plateNo = "";

        public string PlateNo {
            get { return _plateNo; }
            set { _plateNo = value; }
        }

        string _employeeID = "";

        public string EmployeeID {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

        double _rate = 0.0;

        public double Rate {
            get { return _rate; }
            set { _rate = value; }
        }

        #endregion

    }

}
