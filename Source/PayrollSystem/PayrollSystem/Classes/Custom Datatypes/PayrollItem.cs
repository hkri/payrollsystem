﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    //Class for creating payroll entry.
    public class PayrollItem {

        #region Constructor

        //Constructor of the payroll item.
        //Must give all required fields.
        public PayrollItem(string employeeID, string employee, string position, string plateno, double rate,  double sa) {
            EmployeeID = employeeID;
            Employee = employee;
            Position = position;
            PlateNo = plateno;
            Rate = rate;
            Tax = rate * 0.02;  //2% Tax from rate
            CF = rate * 0.05;   //5% CF from rate
            SA = sa;        
            TotalDeductions = Tax + CF + SA;    //Deductions = Tax + CF + SA
            GrossPay = Math.Round(Rate - TotalDeductions);  //Gross = Rate - Total Deductions
        }

        #endregion

        #region Properties

        public string Position { get; set; }

        public string EmployeeID { get; set; }

        string _employee = "";

        public string Employee {
            get { return _employee; }
            set { _employee = value; }
        }

        string _plateno = "";

        public string PlateNo {
            get { return _plateno; }
            set { _plateno = value; }
        }

        double _rate = 0.0;

        public double Rate {
            get { return _rate; }
            set { _rate = value; }
        }

        double _tax = 0.0;

        public double Tax {
            get { return _tax; }
            set { _tax = value; }
        }

        double _cf = 0.0;

        public double CF {
            get { return _cf; }
            set { _cf = value; }
        }

        double _sa = 0.0;

        public double SA {
            get { return _sa; }
            set { _sa = value; }
        }

        double _totaldeductions = 0.0;

        public double TotalDeductions {
            get { return _totaldeductions; }
            set { _totaldeductions = value; }
        }

        double _grosspay = 0.0;

        public double GrossPay {
            get { return _grosspay; }
            set { _grosspay = value; }
        }

        #endregion

    }

}
