﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    class PayrollItemComparator : IEqualityComparer<PayrollGroupingItem> {

        public bool Equals(PayrollGroupingItem x, PayrollGroupingItem y) {
            return (x.EmployeeID == y.EmployeeID) && (x.PlateNo.Trim().Equals(y.PlateNo.ToString(), StringComparison.CurrentCultureIgnoreCase));
        }

        public int GetHashCode(PayrollGroupingItem item) {
            return item.EmployeeID.GetHashCode();
        }

    }
}
