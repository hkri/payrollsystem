﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    class PayrollSummaryItem {

        #region Constructor

        public PayrollSummaryItem(string employeeID, string employeeName, double grossPay, double ca, double sa, double uni) {
            EmployeeID = employeeID;
            EmployeeName = employeeName;
            GrossPay = grossPay;
            CA = ca;
            SA = sa;
            Uniform = uni;
            NetPay = GrossPay - (CA + Uniform);
        }

        #endregion

        #region Properties

        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public double GrossPay { get; set; }
        public double SA {get; set;}
        public double Uniform { get; set; }
        public double CA { get; set; }
        public double NetPay { get; set; }

        #endregion

    }

}
