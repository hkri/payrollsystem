﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    //This class will be used for holding values for totals
    //DataGridView on SystemPage.
    public class TotalsItem {

        #region Constructor

        //Constructor.
        public TotalsItem(string item_name, double val) {
            Item = item_name;
            Value = val;
        }

        #endregion

        #region Properties

        string _item = "";
        //Hols the value/name of the item.
        public string Item {
            get { return _item; }
            set { _item = value; }
        }

        double _value = 0.0;
        //Holds the actual value of the item.
        public double Value {
            get { return _value; }
            set { _value = value; }
        }

        #endregion

    }

}
