﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    /// <summary>
    /// Creates a new instance of double-buffered DataGridView, because flickers.
    /// </summary>
    public class BufferedDataGridView : DataGridView {

        //Constructor
        public BufferedDataGridView() {
            DoubleBuffered = true;  //Enable double-buffering
        }

    }

}
