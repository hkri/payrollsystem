﻿/*
 * Buffered Panel Class
 * WinForms panel with double-buffering enabled.
 * 
 */

using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

namespace PayrollSystem {

    public class BufferedPanel : Panel {

        #region Constructor

        //Constructor
        public BufferedPanel() {
            DoubleBuffered = true;
            ResizeRedraw = true;
        }

        #endregion

        #region Properties

        AnchorStyles _borders = AnchorStyles.None;

        public AnchorStyles Borders {
            get { return _borders; }
            set { _borders = value; Invalidate(); }
        }

        Color _bordercolor = Color.Black;

        public Color BorderColor {
            get { return _bordercolor; }
            set { _bordercolor = value; Invalidate(); }
        }


        #endregion

        #region Overrides

        //Draw borders
        Brush b;
        Pen p;
        GraphicsPath gp;
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);    //Call base onpaint event.

            //Render borders.
            gp = new GraphicsPath();
            if ((Borders & AnchorStyles.Top) == AnchorStyles.Top) {
                gp.AddLine(new Point(0, 0), new Point(Width, 0));
                gp.CloseFigure();
            }
            if ((Borders & AnchorStyles.Right) == AnchorStyles.Right) {
                gp.AddLine(new Point(Width - 1, 0), new Point(Width - 1, Height - 1));
                gp.CloseFigure();
            }
            if ((Borders & AnchorStyles.Bottom) == AnchorStyles.Bottom) {
                gp.AddLine(new Point(Width - 1, Height - 1), new Point(0, Height - 1));
                gp.CloseFigure();
            }
            if ((Borders & AnchorStyles.Left) == AnchorStyles.Left) {
                gp.AddLine(new Point(0, Height - 1), new Point(0, 0));
                gp.CloseFigure();
            }

            b = new SolidBrush(BorderColor);
            p = new Pen(b);
            e.Graphics.DrawPath(p, gp);
            gp.Dispose();
            b.Dispose();
            p.Dispose();
        }

        #endregion

    }

}
