﻿/*
 * A WinForms panel with edge effect
 * designed for dialog windows.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace PayrollSystem {
    
    class DialogButtonPanel : Panel {

        #region Constructor

        public DialogButtonPanel() {
            DoubleBuffered = true;
        }

        #endregion

        #region Overrides - Rendering

        SolidBrush bru;
        Pen pn;
        protected override void OnPaint(PaintEventArgs e) {
            //base.OnPaint(e);
            //Draw the top border
            //Black first
            bru = new SolidBrush(Color.FromArgb(70, Color.Black));
            pn = new Pen(bru);
            e.Graphics.DrawLine(pn, Point.Empty, new Point(Width, 0));

            //White edge.
            bru = new SolidBrush(Color.FromArgb(240, Color.White));
            pn = new Pen(bru);
            e.Graphics.DrawLine(pn, new Point(0, 1), new Point(Width, 1));
        }

        #endregion

    }

}
