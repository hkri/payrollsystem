﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    public class DropDownToggleButton : CheckBox {

        #region Cache Image

        Bitmap img_expand = Properties.Resources.dropdown;
        Bitmap img_collapse = Properties.Resources.dropdown_rev;

        #endregion

        #region Constructor

        public DropDownToggleButton() {
            base.AutoSize = false;
            base.Size = new Size(25, 25);
            DoubleBuffered = true;
            Appearance = System.Windows.Forms.Appearance.Button;
            FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            FlatAppearance.BorderSize = 0;
            if (Checked) {
                Image = img_expand;
            } else {
                Image = img_collapse;
            }
        }

        #endregion

        #region Properties

        //Add collapsed property so we will not be confused with
        //checked = collapsed
        public bool Collapsed {
            get { return Checked; }
            set { Checked = value; }
        }

        #endregion

        #region Overrides

        protected override bool ShowFocusCues {
            get {
                return false;
            }
        }

        protected override void OnCheckedChanged(EventArgs e) {
            //Render the image.
            if (Checked) {
                Image = img_expand;
            } else {
                Image = img_collapse;
            }

            //Handle the base checked changed.
            base.OnCheckedChanged(e);
        }

        #endregion

    }

}
