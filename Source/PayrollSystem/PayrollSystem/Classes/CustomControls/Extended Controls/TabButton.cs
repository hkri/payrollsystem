﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollSystem {

    internal class TabButton : ToolStripButton {

        string _key = "";

        public string Key {
            get { return _key; }
            set { _key = value; }
        }

        public TabButton(string DataKey, string Content) {
            Key = DataKey;
            Text = Content;
        }

    }

}
