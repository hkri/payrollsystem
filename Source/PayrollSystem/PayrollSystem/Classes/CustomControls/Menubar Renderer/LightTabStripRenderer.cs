﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

namespace PayrollSystem {

    internal class LightTabStripRenderer : ToolStripProfessionalRenderer {

        #region objects

        Bitmap overflow_arrow = Properties.Resources.overflow_arrow;
        Brush b;
        Pen p;

        #endregion

        #region Constructor

        public LightTabStripRenderer() {
            this.RoundedEdges = false;
        }

        #endregion

        #region Overrides

        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e) {
            base.OnRenderToolStripBackground(e);
            if (!(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip) && e.ToolStrip is ToolStrip) {
                b = new SolidBrush(ColorTranslator.FromHtml("#eeeeee"));
                e.Graphics.FillRectangle(b, e.AffectedBounds);

                b = new SolidBrush(Color.FromArgb(50, 0, 0, 0));
                p = new Pen(b);

                e.Graphics.DrawLine(p, new Point(0, e.AffectedBounds.Height - 1), new Point(e.ToolStrip.Width, e.AffectedBounds.Height - 1));

            }
        }

        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e) {
            if (!(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip) && e.ToolStrip is ToolStrip) {
                if (e.Item is ToolStripButton && !e.Item.IsOnOverflow) {
                    ToolStripButton btn = (ToolStripButton)e.Item;
                    if (btn.Checked) {
                        b = new LinearGradientBrush(Point.Empty, new Point(0, e.Item.Height), ColorTranslator.FromHtml("#fcfcfc"), ColorTranslator.FromHtml("#f5f6f7"));
                        p = new Pen(b);
                        e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height));
                        b = new SolidBrush(Color.FromArgb(50, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawRectangle(p, new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height));
                    } else if (btn.Selected) {
                        b = new SolidBrush(ColorTranslator.FromHtml("#f3f3f3"));
                        e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height - 1));
                        b = new SolidBrush(Color.FromArgb(50, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawRectangle(p, new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height));
                    }
                } else if (e.Item is ToolStripButton && e.Item.IsOnOverflow) {
                    if (e.Item.Selected) {
                        b = new SolidBrush(ColorTranslator.FromHtml("#a6d6f1"));
                        e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height - 1));
                    }
                }
            } else {
                base.OnRenderButtonBackground(e);
            }
        }

        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e) {
            if (!(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip) && e.ToolStrip is ToolStrip) {
                /*
                b = new SolidBrush(Color.FromArgb(50, 0, 0, 0));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, e.AffectedBounds.Height - 1), new Point(e.ToolStrip.Width, e.AffectedBounds.Height - 1));
                */
            } else {
                base.OnRenderToolStripBorder(e); 

            }
        }

        protected override void OnRenderOverflowButtonBackground(ToolStripItemRenderEventArgs e) {
            if (!(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip) && e.ToolStrip is ToolStrip) {
                e.Graphics.DrawImage(overflow_arrow, new Rectangle((e.Item.Width / 2) - (overflow_arrow.Width / 2), e.Item.Height - 14, 9, 4));
                if (e.Item.Selected || e.Item.Pressed) {
                    b = new SolidBrush(Color.FromArgb(20, 0, 0, 0));
                    e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height));
                }
            } else {
                base.OnRenderOverflowButtonBackground(e);
            }
        }

        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e) {
            if (e.ToolStrip is ToolStrip && !(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip)) {
                if (e.Item is ToolStripButton) {
                    ToolStripButton b = (ToolStripButton)e.Item;
                    if (b.Checked == false) {
                        e.TextColor = Color.FromArgb(67,67,67);
                    } else {
                        e.TextColor = Color.FromArgb(67, 67, 67);
                    }
                    if (b.IsOnOverflow) {
                        e.TextColor = Color.FromArgb(67, 67, 67);
                    }
                }
            }
            base.OnRenderItemText(e);
        }

        #endregion

    }

}
