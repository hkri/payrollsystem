﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

namespace PayrollSystem {

    public class LightToolstripRenderer : ToolStripProfessionalRenderer {

        #region Graphics objects

        Brush b;
        Pen p;

        #endregion

        #region local variables
        
        #endregion

        #region Constructor

        public LightToolstripRenderer() {
            RoundedEdges = false;
        }

        #endregion

        #region overrides

        /* Modifies the Toolstrip Background Styles */
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e) {
            base.OnRenderToolStripBackground(e);

            /* Render conditions if toolstrip is a menubar */
            if (e.ToolStrip is MenuStrip) {

                /* Background Fill */
                b = new SolidBrush(Color.FromArgb(250, 250, 250));
                e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height));

                /* Draw dark border line below */
                b = new SolidBrush(Color.FromArgb(20, 0, 0, 0));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, e.ToolStrip.Height - 1), new Point(e.ToolStrip.Width, e.ToolStrip.Height - 1));


                /* light border line below */
                b = new SolidBrush(Color.FromArgb(255, 255, 255));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, e.ToolStrip.Height - 2), new Point(e.ToolStrip.Width, e.ToolStrip.Height - 2));

            } else if (e.ToolStrip is ToolStripDropDown) {
                /* Background Fill */
                b = new SolidBrush(Color.FromArgb(250, 250, 250));
                e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height));
                
            } else if (e.ToolStrip is ToolStrip && !(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip)) {
                /* Gradient Version */ /*
                b = new LinearGradientBrush(Point.Empty, new Point(0, e.ToolStrip.Height), Color.FromArgb(253, 253, 253), Color.FromArgb(237, 237, 237));
                e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height));
                */

                /* Flat Fill */
                b = new SolidBrush(Color.FromArgb(247, 247, 247));
                e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height));
            }

        }

        /* Modifies the Toolstrip Menu item background styles. */
        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e) {
            if (e.ToolStrip is MenuStrip) {
                if (e.Item is ToolStripMenuItem) {
                    if (e.Item.Pressed) {
                        b = new SolidBrush(Color.FromArgb(97, 97, 97));
                        e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height));
                        b = new SolidBrush(Color.FromArgb(40, 0, 0, 0));
                        p = new Pen(b, 2);
                        e.Graphics.DrawLine(p, new Point(0, e.Item.Height - 1), new Point(e.Item.Width, e.Item.Height - 1));
                        return;
                    }
                    if (e.Item.Selected) {
                        b = new SolidBrush(Color.FromArgb(19, 147, 255));
                        e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height));
                        b = new SolidBrush(Color.FromArgb(40, 0, 0, 0));
                        p = new Pen(b, 1);
                        e.Graphics.DrawLine(p, new Point(0, e.Item.Height - 1), new Point(e.Item.Width, e.Item.Height - 1));
                    }
                }
            } else if (e.ToolStrip is ToolStripDropDown) {
                if (e.Item.Selected && e.Item.Enabled) {
                    b = new LinearGradientBrush(Point.Empty, new Point(0, e.Item.Height), Color.FromArgb(19, 147, 255), Color.FromArgb(11, 133, 236));
                    e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.Item.Width, e.Item.Height));
                    b = new SolidBrush(Color.FromArgb(35, 0, 0, 0));
                    p = new Pen(b, 1);
                    e.Graphics.DrawLine(p, new Point(0, e.Item.Height - 1), new Point(e.Item.Width, e.Item.Height - 1));
                    e.Graphics.DrawLine(p, new Point(0, 0), new Point(e.Item.Width, 0));
                }
            } else {
                base.OnRenderMenuItemBackground(e);
            }
        }

        /* Modifies button background */
        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e) {
            if (e.ToolStrip is ToolStrip && !(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip)) {
                if (e.Item is ToolStripButton) {
                    if (e.Item.Pressed) {
                        b = new SolidBrush(Color.FromArgb(90, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawLine(p, new Point(1, 0), new Point(e.Item.Width - 2, 0));
                        e.Graphics.DrawLine(p, new Point(e.Item.Width - 1, 1), new Point(e.Item.Width - 1, e.Item.Height - 2));
                        e.Graphics.DrawLine(p, new Point(e.Item.Width - 2, e.Item.Height - 1), new Point(1, e.Item.Height - 1));
                        e.Graphics.DrawLine(p, new Point(0, e.Item.Height - 2), new Point(0, 1));
                        b = new SolidBrush(Color.FromArgb(25, 0, 0, 0));
                        e.Graphics.FillRectangle(b, new Rectangle(1, 1, e.Item.Width - 2, e.Item.Height - 2));
                        b = new SolidBrush(Color.FromArgb(40, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawRectangle(p, new Rectangle(1, 1, e.Item.Width + 3, e.Item.Height + 3));
                        b = new SolidBrush(Color.FromArgb(20, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawRectangle(p, new Rectangle(2, 2, e.Item.Width + 5, e.Item.Height + 5));
                        b = new SolidBrush(Color.FromArgb(10, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawRectangle(p, new Rectangle(3, 3, e.Item.Width + 7, e.Item.Height + 7));

                    } else if (e.Item.Selected) {
                        b = new SolidBrush(Color.FromArgb(50, 0, 0, 0));
                        p = new Pen(b);
                        e.Graphics.DrawLine(p, new Point(1, 0), new Point(e.Item.Width - 2, 0));
                        e.Graphics.DrawLine(p, new Point(e.Item.Width - 1, 1), new Point(e.Item.Width - 1, e.Item.Height - 2));
                        e.Graphics.DrawLine(p, new Point(e.Item.Width - 2, e.Item.Height - 1), new Point(1, e.Item.Height - 1));
                        e.Graphics.DrawLine(p, new Point(0, e.Item.Height - 2), new Point(0, 1));
                        b = new LinearGradientBrush(Point.Empty, new Point(0, e.Item.Height / 2), Color.FromArgb(240, 255, 255, 255), Color.Transparent);
                        e.Graphics.FillRectangle(b, new Rectangle(1, 1, e.Item.Width - 2, e.Item.Height / 2));
                    }
                }
            } else
                base.OnRenderButtonBackground(e);
        }

        /* Modified the dropdown menu borders */
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e) {
            if (e.ToolStrip is ToolStripDropDown) {
                b = new SolidBrush(Color.FromArgb(200, 200, 200));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, 0), new Point(0, e.AffectedBounds.Height));
                e.Graphics.DrawLine(p, new Point(0, e.AffectedBounds.Height - 1), new Point(e.AffectedBounds.Width - 1, e.AffectedBounds.Height - 1));
                e.Graphics.DrawLine(p, new Point(e.AffectedBounds.Width - 1, 0), new Point(e.AffectedBounds.Width - 1, e.AffectedBounds.Height - 1));

                b = new SolidBrush(Color.FromArgb(230, 230, 230));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(e.ConnectedArea.Width, 0), new Point(e.AffectedBounds.Width - 1, 0));
            } else if (e.ToolStrip is ToolStrip && !(e.ToolStrip is MenuStrip) && !(e.ToolStrip is StatusStrip)) {
                b = new SolidBrush(Color.FromArgb(30, 0, 0, 0));
                p = new Pen(b);
                if(e.ToolStrip.Dock == DockStyle.Top)
                    e.Graphics.DrawLine(p, new Point(0, e.ToolStrip.Height - 1), new Point(e.ToolStrip.Width, e.ToolStrip.Height - 1));
                else if(e.ToolStrip.Dock == DockStyle.Bottom)
                    e.Graphics.DrawLine(p, new Point(0, 0), new Point(e.ToolStrip.Width, 0));
            } else if (e.ToolStrip is ToolStrip && !(e.ToolStrip is MenuStrip) && (e.ToolStrip is StatusStrip)) {
                b = new SolidBrush(Color.FromArgb(40, 0, 0, 0));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, 0), new Point(e.ToolStrip.Width, 0));
            } else {
                base.OnRenderToolStripBorder(e);
            }
        }

        /* Modified the separator */
        protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e) {
            if (e.ToolStrip is ToolStripDropDown) {
                b = new SolidBrush(Color.FromArgb(220, 220, 220));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, 3), new Point(e.Item.Width, 3));
            } else
                base.OnRenderSeparator(e);
        }

        /* Modified the item check */
        protected override void OnRenderItemCheck(ToolStripItemImageRenderEventArgs e) {
            if (e.ToolStrip is ToolStripDropDown) {
                if (e.Item is ToolStripMenuItem) {
                    e.Graphics.DrawImage(e.Image, e.ImageRectangle);
                }
            } else
                base.OnRenderItemCheck(e);
        }

        /* Modified the dropdown menu image margine style. */
        protected override void OnRenderImageMargin(ToolStripRenderEventArgs e) {
            //base.OnRenderImageMargin(e);
            b = new SolidBrush(Color.FromArgb(250, 250, 250));
            e.Graphics.FillRectangle(b, new Rectangle(0, 0, e.AffectedBounds.Width, e.AffectedBounds.Height));
        }

        /* Modified the item text style */
        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e) {
            if (e.Item is ToolStripMenuItem && (e.ToolStrip is MenuStrip || e.ToolStrip is ToolStripDropDown)) {
                if (e.Item.Selected || e.Item.Pressed) {
                    e.TextColor = Color.White;
                }
            }
            base.OnRenderItemText(e);
        }

        #endregion


    }

}
