﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class TruckingLogPickerDialog : Form {

        #region Controls

        //Designer generated controls
        private Button btnCancel;
        private Button btnOK;
        private Label label1;
        protected internal DataGridView SelectionDataGrid;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private BufferedPanel bufferedPanel1;
        private BufferedPanel bufferedPanel2;
        private BufferedPanel bufferedPanel3;
        private BufferedPanel bufferedPanel4;
        private DateTimePicker dpYear;
        private Label lblYear;
        private Label label3;
        private Label lblSelectedCounter;
        private Button btnDeselectAll;
        private DialogButtonPanel dialogButtonPanel1;
        private CheckBox chkSelectAll;

        #endregion

        #region Local Variables

        DataTable dt;
        int SelectedCount = 0;

        //Date range
        DateTime StartDate, EndDate;

        #endregion

        #region Properties

        bool _multiselect = true;

        //Determines if the cash report selection will select multiple selection.
        public bool Multiselect {
            get { return _multiselect; }
            set { _multiselect = value; }
        }


        #endregion

        #region Constructor

        //Setup and initialization of controls.
        public TruckingLogPickerDialog() {
            InitializeComponent();
        }

        //Form designer generated initialization.
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TruckingLogPickerDialog));
            this.SelectionDataGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.bufferedPanel3 = new PayrollSystem.BufferedPanel();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.lblSelectedCounter = new System.Windows.Forms.Label();
            this.dpYear = new System.Windows.Forms.DateTimePicker();
            this.lblYear = new System.Windows.Forms.Label();
            this.bufferedPanel4 = new PayrollSystem.BufferedPanel();
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bufferedPanel2 = new PayrollSystem.BufferedPanel();
            this.dialogButtonPanel1 = new PayrollSystem.DialogButtonPanel();
            this.btnDeselectAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SelectionDataGrid)).BeginInit();
            this.bufferedPanel3.SuspendLayout();
            this.bufferedPanel1.SuspendLayout();
            this.dialogButtonPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SelectionDataGrid
            // 
            this.SelectionDataGrid.AllowUserToAddRows = false;
            this.SelectionDataGrid.AllowUserToDeleteRows = false;
            this.SelectionDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.SelectionDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SelectionDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.SelectionDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SelectionDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.SelectionDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SelectionDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SelectionDataGrid.ColumnHeadersHeight = 28;
            this.SelectionDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SelectionDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SelectionDataGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.SelectionDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectionDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.SelectionDataGrid.Location = new System.Drawing.Point(0, 127);
            this.SelectionDataGrid.MultiSelect = false;
            this.SelectionDataGrid.Name = "SelectionDataGrid";
            this.SelectionDataGrid.RowHeadersVisible = false;
            this.SelectionDataGrid.RowTemplate.Height = 28;
            this.SelectionDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SelectionDataGrid.ShowCellErrors = false;
            this.SelectionDataGrid.ShowEditingIcon = false;
            this.SelectionDataGrid.Size = new System.Drawing.Size(592, 254);
            this.SelectionDataGrid.TabIndex = 0;
            this.SelectionDataGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.SelectionDataGrid_CurrentCellDirtyStateChanged);
            this.SelectionDataGrid.SelectionChanged += new System.EventHandler(this.SelectionDataGrid_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.btnOK.Location = new System.Drawing.Point(357, 11);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(108, 28);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.btnCancel.Location = new System.Drawing.Point(472, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(108, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // bufferedPanel3
            // 
            this.bufferedPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bufferedPanel3.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel3.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel3.Controls.Add(this.chkSelectAll);
            this.bufferedPanel3.Controls.Add(this.lblSelectedCounter);
            this.bufferedPanel3.Controls.Add(this.dpYear);
            this.bufferedPanel3.Controls.Add(this.lblYear);
            this.bufferedPanel3.Controls.Add(this.bufferedPanel4);
            this.bufferedPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel3.Location = new System.Drawing.Point(0, 76);
            this.bufferedPanel3.Name = "bufferedPanel3";
            this.bufferedPanel3.Size = new System.Drawing.Size(592, 51);
            this.bufferedPanel3.TabIndex = 1;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(15, 28);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(79, 17);
            this.chkSelectAll.TabIndex = 7;
            this.chkSelectAll.Text = "Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // lblSelectedCounter
            // 
            this.lblSelectedCounter.AutoEllipsis = true;
            this.lblSelectedCounter.Location = new System.Drawing.Point(12, 2);
            this.lblSelectedCounter.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblSelectedCounter.Name = "lblSelectedCounter";
            this.lblSelectedCounter.Size = new System.Drawing.Size(285, 25);
            this.lblSelectedCounter.TabIndex = 6;
            this.lblSelectedCounter.Text = "No records selected.";
            this.lblSelectedCounter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dpYear
            // 
            this.dpYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpYear.CustomFormat = "yyyy";
            this.dpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpYear.Location = new System.Drawing.Point(498, 4);
            this.dpYear.Name = "dpYear";
            this.dpYear.ShowUpDown = true;
            this.dpYear.Size = new System.Drawing.Size(82, 21);
            this.dpYear.TabIndex = 0;
            this.dpYear.Visible = false;
            this.dpYear.ValueChanged += new System.EventHandler(this.dpYear_ValueChanged);
            // 
            // lblYear
            // 
            this.lblYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblYear.Location = new System.Drawing.Point(437, 6);
            this.lblYear.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(55, 19);
            this.lblYear.TabIndex = 4;
            this.lblYear.Text = "Year:";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblYear.Visible = false;
            // 
            // bufferedPanel4
            // 
            this.bufferedPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.bufferedPanel4.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel4.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel4.Location = new System.Drawing.Point(0, 50);
            this.bufferedPanel4.Name = "bufferedPanel4";
            this.bufferedPanel4.Size = new System.Drawing.Size(592, 1);
            this.bufferedPanel4.TabIndex = 3;
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Controls.Add(this.label1);
            this.bufferedPanel1.Controls.Add(this.label3);
            this.bufferedPanel1.Controls.Add(this.bufferedPanel2);
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 0);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(592, 76);
            this.bufferedPanel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(568, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Showing trucking logs only within the payroll\'s date period. Please select the tr" +
    "ucking log(s) you need from the list below.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(374, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Note: only trucking logs marked as \"final\" will be shown here.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bufferedPanel2
            // 
            this.bufferedPanel2.BackColor = System.Drawing.Color.Gainsboro;
            this.bufferedPanel2.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel2.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel2.Location = new System.Drawing.Point(0, 75);
            this.bufferedPanel2.Name = "bufferedPanel2";
            this.bufferedPanel2.Size = new System.Drawing.Size(592, 1);
            this.bufferedPanel2.TabIndex = 2;
            // 
            // dialogButtonPanel1
            // 
            this.dialogButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.dialogButtonPanel1.Controls.Add(this.btnDeselectAll);
            this.dialogButtonPanel1.Controls.Add(this.btnCancel);
            this.dialogButtonPanel1.Controls.Add(this.btnOK);
            this.dialogButtonPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dialogButtonPanel1.Location = new System.Drawing.Point(0, 381);
            this.dialogButtonPanel1.Name = "dialogButtonPanel1";
            this.dialogButtonPanel1.Size = new System.Drawing.Size(592, 50);
            this.dialogButtonPanel1.TabIndex = 2;
            // 
            // btnDeselectAll
            // 
            this.btnDeselectAll.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeselectAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.btnDeselectAll.Location = new System.Drawing.Point(15, 11);
            this.btnDeselectAll.Name = "btnDeselectAll";
            this.btnDeselectAll.Size = new System.Drawing.Size(108, 28);
            this.btnDeselectAll.TabIndex = 4;
            this.btnDeselectAll.Text = "&Deselect All";
            this.btnDeselectAll.UseVisualStyleBackColor = true;
            this.btnDeselectAll.Click += new System.EventHandler(this.btnDeselectAll_Click);
            // 
            // TruckingLogPickerDialog
            // 
            this.AcceptButton = this.btnOK;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(592, 431);
            this.Controls.Add(this.SelectionDataGrid);
            this.Controls.Add(this.bufferedPanel3);
            this.Controls.Add(this.bufferedPanel1);
            this.Controls.Add(this.dialogButtonPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "TruckingLogPickerDialog";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Trucking Log";
            this.Load += new System.EventHandler(this.CashReportPickerDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SelectionDataGrid)).EndInit();
            this.bufferedPanel3.ResumeLayout(false);
            this.bufferedPanel3.PerformLayout();
            this.bufferedPanel1.ResumeLayout(false);
            this.dialogButtonPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        #region Public Functions

        //Returns all of the selected Cash Reports in Dictionary form
        //Key:   Cash Report ID
        //Value: Cash Report Title
        public Dictionary<String, String> GetSelectedTruckingLogs() {
            Dictionary<String, String> selected = new Dictionary<String, String>();
            if (Multiselect) {
                foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                    if (r.Cells["Selected"].Value != null) {
                        if (((DataGridViewCheckBoxCell)r.Cells["Selected"]).Value.Equals(true))
                            selected.Add(r.Cells["ID"].Value.ToString(), r.Cells["Destination"].Value.ToString());
                    }
                }
            } else {
                selected.Add(SelectionDataGrid.SelectedRows[0].Cells["ID"].Value.ToString(), SelectionDataGrid.SelectedRows[0].Cells["Destination"].Value.ToString());
            }
            return selected;
        }

        //If period range is set, disable the selection.
        public void SetPeriodRange(string startdate, string enddate) {
            StartDate = DateTime.Parse(startdate);
            EndDate = DateTime.Parse(enddate);
            dpYear.Visible = lblYear.Visible = false;
        }

        #endregion

        #region Local Functions

        //Sets the header text of the specified column.
        void SetColHeader(string colname, string header) {
            SelectionDataGrid.Columns[colname].HeaderText = header;
        }

        //Loads all cash reports from database.
        void LoadTruckingLogs() {

            //QUERY REQUEST
            string cols = "A.ID, A.TransportDate, A.PlateNo, (A.Destination + IIF(A.IsBackLoad = true, ' (Backload)', '')) AS [Destination], A.Rate, A.DRNo, ";
            cols += "(B.LastName + ', ' + B.FirstName) AS [Driver], A.DriverRate, ";
            cols += "(C.LastName + ', ' + C.FirstName) AS [Helper], A.HelperRate, A.IsArchived, A.IsFinal, A.IsBackLoad ";
            string query = "SELECT " + cols + "FROM (TransportLog AS A ";
            query += "LEFT JOIN Employee AS B ON B.EmployeeID = A.Driver) ";
            query += "LEFT JOIN Employee AS C ON C.EmployeeID = A.Helper ";
            query += "WHERE A.IsFinal = true AND A.IsArchived = false AND ";
            query += "(A.TransportDate >= @startdate AND A.TransportDate <= @enddate)";
            
            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@startdate", StartDate);
            p[1] = new OleDbParameter("@enddate", EndDate);


            if (dt != null) dt.Dispose();
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

            //Move the title column to first.
            //dt.Columns["Title"].SetOrdinal(0);

            //SET DATA SOURCE
            SelectionDataGrid.Columns.Clear();  //Clear first all columns.
            SelectionDataGrid.DataSource = dt;

            //Autofit all columns to its contents.
            if (SelectionDataGrid.Columns.Count > 0) {
                foreach (DataGridViewColumn col in SelectionDataGrid.Columns) {
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    int nwidt = col.Width;
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    col.Width = nwidt;
                }
            }

            //Hide unnecessary columns.
            SelectionDataGrid.Columns["ID"].Visible = false;
            SelectionDataGrid.Columns["IsFinal"].Visible = false;
            SelectionDataGrid.Columns["IsBackLoad"].Visible = false;
            SelectionDataGrid.Columns["IsArchived"].Visible = false;
            
            //Rename some columns to user-friendly header texts.
            
            SetColHeader("PlateNo", "Plate Number");
            SetColHeader("TransportDate", "Date");
            SetColHeader("DRNo", "DR Number");
            SetColHeader("DriverRate", "Driver Rate");
            SetColHeader("HelperRate", "Helper Rate");
             

            //Lock all columns.
            foreach (DataGridViewColumn c in SelectionDataGrid.Columns)
                c.ReadOnly = true;

            //Add a checkbox column at index 0.
            if (Multiselect) {
                DataGridViewCheckBoxColumn col = new DataGridViewCheckBoxColumn();
                col.Name = "Selected";
                col.HeaderText = "Selected";
                col.Width = 30;
                col.ReadOnly = false;
                col.Resizable = DataGridViewTriState.False;
                SelectionDataGrid.Columns.Insert(0, col);
            }
            SetColHeader("Selected", "");
        }

        //Filter reports to be included.
        void FilterReports() {
            //Hide all items first.
            if (SelectionDataGrid.DataSource != null) {
                if (dpYear.Visible) {
                    //If date picker is visible, there is no date period binded.
                    SelectedCount = 0;  //Reset the selected item counter.
                    CurrencyManager cm = (CurrencyManager)SelectionDataGrid.BindingContext[SelectionDataGrid.DataSource];
                    cm.SuspendBinding();
                    foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                        if (DateTime.Parse(r.Cells["TransportDate"].Value.ToString()).Year != dpYear.Value.Year)
                            r.Visible = false;
                        else
                            r.Visible = true;
                    }
                    cm.ResumeBinding();
                    CountSelected();    //Display the number of checked cash report.
                } else {
                    //Filter by the given date period.   
                    SelectedCount = 0;  //Reset the selected item counter.
                    CurrencyManager cm = (CurrencyManager)SelectionDataGrid.BindingContext[SelectionDataGrid.DataSource];
                    cm.SuspendBinding();
                    foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                        if (DateTime.Parse(r.Cells["TransportDate"].Value.ToString()) >= StartDate &&
                            DateTime.Parse(r.Cells["TransportDate"].Value.ToString()) <= EndDate)
                            r.Visible = true;
                        else
                            r.Visible = false;
                    }
                    cm.ResumeBinding();
                    CountSelected();    //Display the number of checked cash report.
                }
            }
        }

        //Function to count selected items.
        void CountSelected() {
            if (Multiselect) {
                SelectedCount = 0;
                foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                    if (SelectionDataGrid.Columns["Selected"] != null) {
                        if (r.Cells["Selected"].Value != null) {
                            if (((DataGridViewCheckBoxCell)r.Cells["Selected"]).Value.Equals(true))
                                SelectedCount++;    //Increment selected item counter if item is checked.
                        }
                    } else
                        break;
                }
                trigger_check = false;  //Turn off checkbox event handler.
                if (SelectedCount == SelectionDataGrid.Rows.Count)
                    chkSelectAll.Checked = true;
                else
                    chkSelectAll.Checked = false;
                trigger_check = true;   //Turn on checkbox event handler.
            }
            ShowSelectedCount();    //Display the number of checked cash report.
        }

        //Show the number of selected cash reports.
        void ShowSelectedCount() {
            if (Multiselect) {
                if (SelectedCount == 0) {
                    lblSelectedCounter.Text = "No records selected.";
                } else if (SelectedCount == 1) {
                    lblSelectedCounter.Text = "Trucking log selected: 1";
                } else if (SelectedCount > 1) {
                    lblSelectedCounter.Text = "Trucking logs selected: " + SelectedCount;
                }
            } else {
                if (SelectionDataGrid.SelectedRows.Count > 0) {
                    lblSelectedCounter.Text = "Selected: " + SelectionDataGrid.SelectedRows[0].Cells["Destination"].Value.ToString();
                } else
                    lblSelectedCounter.Text = "No trucking log selected.";
            }
        }

        //Deselects all cash reports.
        void UncheckAll() {
            if (Multiselect) {
                foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                    ((DataGridViewCheckBoxCell)r.Cells["Selected"]).Value = false;
                }
                CountSelected();
            }
        }

        #endregion

        #region Window Event handlers

        //Handle the window load event.
        private void CashReportPickerDialog_Load(object sender, EventArgs e) {
            //Hide the Deselect All button when multiselection is disabled.
            if (Multiselect == false) btnDeselectAll.Visible = false;

            //Perform loading of cash reports.       
            LoadTruckingLogs();
            FilterReports();    //Filter by year.
        }

        #endregion

        #region Control Event handlers

        //Checks all items.
        bool trigger_check = true;  //Turn this flag off to when changing check state of checkbox without triggering
                                    //CheckChanged eventhandler.
        private void chkSelectAll_CheckedChanged(object sender, EventArgs e) {
            if (trigger_check) {
                if (SelectionDataGrid.Rows.Count > 0) {
                    if (Multiselect) {
                        foreach (DataGridViewRow r in SelectionDataGrid.Rows) {
                            ((DataGridViewCheckBoxCell)r.Cells["Selected"]).Value = chkSelectAll.Checked;
                        }
                        CountSelected();
                    }
                }
            }
        }

        //Handle the event when the cash reports datagrid changed selected rows.
        private void SelectionDataGrid_SelectionChanged(object sender, EventArgs e) {
            CountSelected();
        }

        //Filter data visible when year value is updated.
        private void dpYear_ValueChanged(object sender, EventArgs e) {
            FilterReports();
        }

        //Recount selected items when cell value is changed.
        private void SelectionDataGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e) {
            if (SelectionDataGrid.CurrentCell is DataGridViewCheckBoxCell) {
                SelectionDataGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                CountSelected();
            }
        }

        //Handle the event when the Deselect All button is clicked.
        private void btnDeselectAll_Click(object sender, EventArgs e) {
            UncheckAll();
        }

        #endregion

        #region Validation

        //When OK is clicked. Return dialog result OK
        private void btnOK_Click(object sender, EventArgs e) {
            if (Multiselect == false) {
                if (SelectionDataGrid.SelectedRows.Count > 0)
                    DialogResult = System.Windows.Forms.DialogResult.OK;
            } else {
                if (SelectedCount <= 0) {
                    Notifier.ShowWarning("Please select the trucking log record you need.", "No Selection");
                    return;
                }
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        #endregion

    }

}