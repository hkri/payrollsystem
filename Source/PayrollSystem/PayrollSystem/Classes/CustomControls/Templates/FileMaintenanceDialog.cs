﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PayrollSystem.Eventhandlers;

namespace PayrollSystem {

    public partial class FileMaintenanceDialog : Form {


        #region Constructor

        public FileMaintenanceDialog() {
            InitializeComponent();
        }

        #endregion

        #region public enum

        public enum FileMaintenanceDialogMode {
            Add,
            Editing
        }

        #endregion

        #region public properties

        private FileMaintenanceDialogMode _mode = FileMaintenanceDialogMode.Add;

        public FileMaintenanceDialogMode FileMaintenanceMode {
            get { return _mode; }
            set { _mode = value; }
        }
        
        #endregion

        #region Public Events

        public event FileMaintenanceDialogEventHandler OKButtonClicked;

        #endregion

        #region Public Functions

        public void SetOKButtonText(string caption) {
            btnOK.Text = caption;
        }

        public void SetTitle(string title) {
            Text = title;
        }

        public void TriggerOKButton() {
            btnOK.PerformClick();
        }

        public void SetOKButtonEnabled(bool enabled) {
            btnOK.Enabled = enabled;
        }

        #endregion

        #region Custom Eventhandlers

        protected virtual void OnOKButtonClicked(DialogEventArgs e) {
            if (OKButtonClicked != null) {
                OKButtonClicked(this, e);
            }
        }

        #endregion

        #region Button Eventhandlers

        private void btnOK_Click(object sender, EventArgs e) {
            DialogEventArgs arg = new DialogEventArgs();
            arg.Cancel = true;
            OnOKButtonClicked(arg);
            if (arg.Cancel == false) {
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        #endregion

        #region Window Event Handlers

        private void FileMaintenanceDialog_Load(object sender, EventArgs e) {
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Add) {
                Text = "New " + Text;
            } else if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                Text = "Edit " + Text;
            }
        }

        #endregion

    }

}
