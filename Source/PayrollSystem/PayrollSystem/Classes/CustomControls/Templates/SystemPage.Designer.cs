﻿namespace PayrollSystem {
    public partial class SystemPage {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemPage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnArchive = new System.Windows.Forms.ToolStripButton();
            this.btnImport = new System.Windows.Forms.ToolStripButton();
            this.btnExport = new System.Windows.Forms.ToolStripButton();
            this.btnMarkFinal = new System.Windows.Forms.ToolStripButton();
            this.btnRestore = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.btnView = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btnSearch = new System.Windows.Forms.ToolStripButton();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.lblSearch = new System.Windows.Forms.ToolStripLabel();
            this.btnToggleTotals = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markFinalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SplitContainerMain = new System.Windows.Forms.SplitContainer();
            this.RecordsDataGrid = new PayrollSystem.BufferedDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeaderPanel = new PayrollSystem.BufferedPanel();
            this.bufferedPanel3 = new PayrollSystem.BufferedPanel();
            this.lblHeaderText = new System.Windows.Forms.Label();
            this.TotalsDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bufferedPanel5 = new PayrollSystem.BufferedPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.MainToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SplitContainerSectionView = new System.Windows.Forms.SplitContainer();
            this.SectionsDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SectionsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.SectionsToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnSectionAdd = new System.Windows.Forms.ToolStripButton();
            this.btnSectionDelete = new System.Windows.Forms.ToolStripButton();
            this.FiltersPanel = new PayrollSystem.BufferedPanel();
            this.FiltersCheckPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.chkShowFinal = new System.Windows.Forms.CheckBox();
            this.DateFilterPanel = new PayrollSystem.BufferedPanel();
            this.btnSet = new System.Windows.Forms.Button();
            this.dpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dpStart = new System.Windows.Forms.DateTimePicker();
            this.lblDatePeriod = new System.Windows.Forms.Label();
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.MainToolStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMain)).BeginInit();
            this.SplitContainerMain.Panel1.SuspendLayout();
            this.SplitContainerMain.Panel2.SuspendLayout();
            this.SplitContainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).BeginInit();
            this.HeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerSectionView)).BeginInit();
            this.SplitContainerSectionView.Panel1.SuspendLayout();
            this.SplitContainerSectionView.Panel2.SuspendLayout();
            this.SplitContainerSectionView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SectionsDataGrid)).BeginInit();
            this.SectionsContextMenuStrip.SuspendLayout();
            this.SectionsToolStrip.SuspendLayout();
            this.FiltersPanel.SuspendLayout();
            this.FiltersCheckPanel.SuspendLayout();
            this.DateFilterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnDelete,
            this.btnArchive,
            this.btnImport,
            this.btnExport,
            this.btnMarkFinal,
            this.btnRestore,
            this.btnRefresh,
            this.btnView,
            this.toolStripButton1,
            this.btnSearch,
            this.txtSearch,
            this.lblSearch,
            this.btnToggleTotals});
            this.MainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Padding = new System.Windows.Forms.Padding(5, 2, 1, 2);
            this.MainToolStrip.Size = new System.Drawing.Size(875, 37);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "MainToolStrip";
            // 
            // btnAdd
            // 
            this.btnAdd.AutoToolTip = false;
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnAdd.Image = global::PayrollSystem.Properties.Resources.add;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(48, 30);
            this.btnAdd.Text = "New";
            this.btnAdd.ToolTipText = "New (Ctrl + N)";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnAdd.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnEdit
            // 
            this.btnEdit.AutoToolTip = false;
            this.btnEdit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnEdit.Image = global::PayrollSystem.Properties.Resources.edit;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(45, 30);
            this.btnEdit.Text = "Edit";
            this.btnEdit.ToolTipText = "Edit (F2)";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            this.btnEdit.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnEdit.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnDelete
            // 
            this.btnDelete.AutoToolTip = false;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnDelete.Image = global::PayrollSystem.Properties.Resources.delete;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(58, 30);
            this.btnDelete.Text = "Delete";
            this.btnDelete.ToolTipText = "Delete (Del)";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnDelete.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnDelete.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnArchive
            // 
            this.btnArchive.AutoToolTip = false;
            this.btnArchive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnArchive.Image = global::PayrollSystem.Properties.Resources.archive;
            this.btnArchive.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnArchive.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnArchive.Name = "btnArchive";
            this.btnArchive.Size = new System.Drawing.Size(63, 30);
            this.btnArchive.Text = "Archive";
            this.btnArchive.ToolTipText = "Archive (Shift + Del)";
            this.btnArchive.Click += new System.EventHandler(this.btnArchive_Click);
            this.btnArchive.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnArchive.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnImport
            // 
            this.btnImport.Image = global::PayrollSystem.Properties.Resources.import;
            this.btnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImport.Margin = new System.Windows.Forms.Padding(0, 1, 2, 2);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(63, 30);
            this.btnImport.Text = "Import";
            this.btnImport.ToolTipText = "Import (Ctrl + I)";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.AutoToolTip = false;
            this.btnExport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnExport.Image = global::PayrollSystem.Properties.Resources.save;
            this.btnExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExport.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(59, 30);
            this.btnExport.Text = "Export";
            this.btnExport.ToolTipText = "Export (Ctrl + S)";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.btnExport.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnExport.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnMarkFinal
            // 
            this.btnMarkFinal.AutoToolTip = false;
            this.btnMarkFinal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarkFinal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnMarkFinal.Image = global::PayrollSystem.Properties.Resources._lock;
            this.btnMarkFinal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMarkFinal.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnMarkFinal.Name = "btnMarkFinal";
            this.btnMarkFinal.Size = new System.Drawing.Size(75, 30);
            this.btnMarkFinal.Text = "Mark Final";
            this.btnMarkFinal.ToolTipText = "Mark Final (F4)";
            this.btnMarkFinal.Click += new System.EventHandler(this.btnMarkFinal_Click);
            this.btnMarkFinal.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnMarkFinal.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnRestore
            // 
            this.btnRestore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnRestore.Image = global::PayrollSystem.Properties.Resources.restore;
            this.btnRestore.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnRestore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(66, 30);
            this.btnRestore.Text = "Restore";
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AutoToolTip = false;
            this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnRefresh.Image = global::PayrollSystem.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 30);
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.ToolTipText = "Refresh (F5 or Shift + F5)";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            this.btnRefresh.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnRefresh.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // btnView
            // 
            this.btnView.Image = global::PayrollSystem.Properties.Resources.view;
            this.btnView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(79, 30);
            this.btnView.Text = "View Item";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            this.btnView.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnView.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripButton1.Enabled = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(1, 30);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // btnSearch
            // 
            this.btnSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSearch.AutoSize = false;
            this.btnSearch.AutoToolTip = false;
            this.btnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSearch.Image = global::PayrollSystem.Properties.Resources.search;
            this.btnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(23, 28);
            this.btnSearch.Text = "Search";
            this.btnSearch.ToolTipText = "Search (Ctrl + F)";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnSearch.MouseLeave += new System.EventHandler(this.ToolStripButtons_MouseLeave);
            this.btnSearch.MouseHover += new System.EventHandler(this.ToolStripButtons_MouseHover);
            // 
            // txtSearch
            // 
            this.txtSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.txtSearch.AutoSize = false;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtSearch.Margin = new System.Windows.Forms.Padding(0);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.txtSearch.Padding = new System.Windows.Forms.Padding(2);
            this.txtSearch.Size = new System.Drawing.Size(108, 21);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            this.txtSearch.Leave += new System.EventHandler(this.txtSearch_Leave);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblSearch.AutoSize = false;
            this.lblSearch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.lblSearch.Size = new System.Drawing.Size(52, 24);
            this.lblSearch.Text = "Search:";
            this.lblSearch.ToolTipText = "Search (Ctrl + F)";
            // 
            // btnToggleTotals
            // 
            this.btnToggleTotals.Image = global::PayrollSystem.Properties.Resources.sigma;
            this.btnToggleTotals.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToggleTotals.Name = "btnToggleTotals";
            this.btnToggleTotals.Size = new System.Drawing.Size(90, 20);
            this.btnToggleTotals.Text = "Show Totals";
            this.btnToggleTotals.Visible = false;
            this.btnToggleTotals.Click += new System.EventHandler(this.btnToggleTotals_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(875, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.archiveToolStripMenuItem,
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.markFinalToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.resetSearchToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.addToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.editToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // archiveToolStripMenuItem
            // 
            this.archiveToolStripMenuItem.Name = "archiveToolStripMenuItem";
            this.archiveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Delete)));
            this.archiveToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.archiveToolStripMenuItem.Text = "Archive";
            this.archiveToolStripMenuItem.Click += new System.EventHandler(this.btnArchive_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.importToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // markFinalToolStripMenuItem
            // 
            this.markFinalToolStripMenuItem.Name = "markFinalToolStripMenuItem";
            this.markFinalToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.markFinalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.markFinalToolStripMenuItem.Text = "Mark Final";
            this.markFinalToolStripMenuItem.Click += new System.EventHandler(this.btnMarkFinal_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // resetSearchToolStripMenuItem
            // 
            this.resetSearchToolStripMenuItem.Name = "resetSearchToolStripMenuItem";
            this.resetSearchToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
            this.resetSearchToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.resetSearchToolStripMenuItem.Text = "Reset Search";
            this.resetSearchToolStripMenuItem.Click += new System.EventHandler(this.resetSearchToolStripMenuItem_Click);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // SplitContainerMain
            // 
            this.SplitContainerMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.SplitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.SplitContainerMain.Location = new System.Drawing.Point(0, 0);
            this.SplitContainerMain.Name = "SplitContainerMain";
            // 
            // SplitContainerMain.Panel1
            // 
            this.SplitContainerMain.Panel1.Controls.Add(this.RecordsDataGrid);
            this.SplitContainerMain.Panel1.Controls.Add(this.HeaderPanel);
            this.SplitContainerMain.Panel1MinSize = 100;
            // 
            // SplitContainerMain.Panel2
            // 
            this.SplitContainerMain.Panel2.Controls.Add(this.TotalsDataGrid);
            this.SplitContainerMain.Panel2.Controls.Add(this.bufferedPanel5);
            this.SplitContainerMain.Panel2.Controls.Add(this.label2);
            this.SplitContainerMain.Panel2MinSize = 120;
            this.SplitContainerMain.Size = new System.Drawing.Size(693, 379);
            this.SplitContainerMain.SplitterDistance = 460;
            this.SplitContainerMain.SplitterWidth = 3;
            this.SplitContainerMain.TabIndex = 4;
            // 
            // RecordsDataGrid
            // 
            this.RecordsDataGrid.AllowUserToAddRows = false;
            this.RecordsDataGrid.AllowUserToDeleteRows = false;
            this.RecordsDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.RecordsDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.RecordsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.RecordsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RecordsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.RecordsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.RecordsDataGrid.ColumnHeadersHeight = 28;
            this.RecordsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.RecordsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.RecordsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RecordsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.RecordsDataGrid.Location = new System.Drawing.Point(0, 35);
            this.RecordsDataGrid.MultiSelect = false;
            this.RecordsDataGrid.Name = "RecordsDataGrid";
            this.RecordsDataGrid.ReadOnly = true;
            this.RecordsDataGrid.RowHeadersVisible = false;
            this.RecordsDataGrid.RowTemplate.Height = 28;
            this.RecordsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RecordsDataGrid.ShowCellErrors = false;
            this.RecordsDataGrid.ShowEditingIcon = false;
            this.RecordsDataGrid.Size = new System.Drawing.Size(460, 344);
            this.RecordsDataGrid.TabIndex = 1;
            this.RecordsDataGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.RecordsDataGrid_CellFormatting);
            this.RecordsDataGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.RecordsDataGrid_CellMouseDoubleClick);
            this.RecordsDataGrid.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.RecordsDataGrid_DataBindingComplete);
            this.RecordsDataGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.RecordsDataGrid_RowsAdded);
            this.RecordsDataGrid.SelectionChanged += new System.EventHandler(this.RecordsDataGrid_SelectionChanged);
            this.RecordsDataGrid.Sorted += new System.EventHandler(this.RecordsDataGrid_Sorted);
            this.RecordsDataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RecordsDataGrid_KeyDown);
            this.RecordsDataGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RecordsDataGrid_MouseDown);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.HeaderPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.HeaderPanel.Controls.Add(this.bufferedPanel3);
            this.HeaderPanel.Controls.Add(this.lblHeaderText);
            this.HeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(460, 35);
            this.HeaderPanel.TabIndex = 6;
            this.HeaderPanel.Visible = false;
            // 
            // bufferedPanel3
            // 
            this.bufferedPanel3.BackColor = System.Drawing.Color.Gainsboro;
            this.bufferedPanel3.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel3.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel3.Location = new System.Drawing.Point(0, 34);
            this.bufferedPanel3.Name = "bufferedPanel3";
            this.bufferedPanel3.Size = new System.Drawing.Size(460, 1);
            this.bufferedPanel3.TabIndex = 1;
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeaderText.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblHeaderText.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.Size = new System.Drawing.Size(460, 35);
            this.lblHeaderText.TabIndex = 0;
            this.lblHeaderText.Text = "Cash On Hand with Ann (Nov 25, 2016)";
            this.lblHeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TotalsDataGrid
            // 
            this.TotalsDataGrid.AllowUserToAddRows = false;
            this.TotalsDataGrid.AllowUserToDeleteRows = false;
            this.TotalsDataGrid.AllowUserToResizeRows = false;
            this.TotalsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.TotalsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TotalsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.TotalsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TotalsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.TotalsDataGrid.ColumnHeadersHeight = 28;
            this.TotalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TotalsDataGrid.ColumnHeadersVisible = false;
            this.TotalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TotalsDataGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this.TotalsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TotalsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.TotalsDataGrid.Location = new System.Drawing.Point(0, 31);
            this.TotalsDataGrid.MultiSelect = false;
            this.TotalsDataGrid.Name = "TotalsDataGrid";
            this.TotalsDataGrid.ReadOnly = true;
            this.TotalsDataGrid.RowHeadersVisible = false;
            this.TotalsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TotalsDataGrid.ShowCellErrors = false;
            this.TotalsDataGrid.ShowEditingIcon = false;
            this.TotalsDataGrid.Size = new System.Drawing.Size(230, 348);
            this.TotalsDataGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Totals";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // bufferedPanel5
            // 
            this.bufferedPanel5.BackColor = System.Drawing.Color.Gainsboro;
            this.bufferedPanel5.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel5.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel5.Location = new System.Drawing.Point(0, 30);
            this.bufferedPanel5.Name = "bufferedPanel5";
            this.bufferedPanel5.Size = new System.Drawing.Size(230, 1);
            this.bufferedPanel5.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(230, 30);
            this.label2.TabIndex = 3;
            this.label2.Text = "Totals";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainToolTip
            // 
            this.MainToolTip.AutoPopDelay = 5000;
            this.MainToolTip.InitialDelay = 500;
            this.MainToolTip.IsBalloon = true;
            this.MainToolTip.ReshowDelay = 100;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(57, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filters:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MainToolTip.SetToolTip(this.label1, "Cannot see a record?\r\nTry adjusting the filters.");
            // 
            // SplitContainerSectionView
            // 
            this.SplitContainerSectionView.BackColor = System.Drawing.Color.Gainsboro;
            this.SplitContainerSectionView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerSectionView.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainerSectionView.Location = new System.Drawing.Point(0, 69);
            this.SplitContainerSectionView.Name = "SplitContainerSectionView";
            // 
            // SplitContainerSectionView.Panel1
            // 
            this.SplitContainerSectionView.Panel1.Controls.Add(this.SectionsDataGrid);
            this.SplitContainerSectionView.Panel1.Controls.Add(this.label3);
            this.SplitContainerSectionView.Panel1.Controls.Add(this.SectionsToolStrip);
            this.SplitContainerSectionView.Panel1MinSize = 150;
            // 
            // SplitContainerSectionView.Panel2
            // 
            this.SplitContainerSectionView.Panel2.Controls.Add(this.SplitContainerMain);
            this.SplitContainerSectionView.Size = new System.Drawing.Size(875, 379);
            this.SplitContainerSectionView.SplitterDistance = 181;
            this.SplitContainerSectionView.SplitterWidth = 1;
            this.SplitContainerSectionView.TabIndex = 5;
            // 
            // SectionsDataGrid
            // 
            this.SectionsDataGrid.AllowUserToAddRows = false;
            this.SectionsDataGrid.AllowUserToDeleteRows = false;
            this.SectionsDataGrid.AllowUserToResizeRows = false;
            this.SectionsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.SectionsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SectionsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.SectionsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SectionsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.SectionsDataGrid.ColumnHeadersHeight = 28;
            this.SectionsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SectionsDataGrid.ColumnHeadersVisible = false;
            this.SectionsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            this.SectionsDataGrid.ContextMenuStrip = this.SectionsContextMenuStrip;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle7.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SectionsDataGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.SectionsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SectionsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.SectionsDataGrid.Location = new System.Drawing.Point(0, 30);
            this.SectionsDataGrid.MultiSelect = false;
            this.SectionsDataGrid.Name = "SectionsDataGrid";
            this.SectionsDataGrid.ReadOnly = true;
            this.SectionsDataGrid.RowHeadersVisible = false;
            this.SectionsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SectionsDataGrid.ShowCellErrors = false;
            this.SectionsDataGrid.ShowEditingIcon = false;
            this.SectionsDataGrid.Size = new System.Drawing.Size(181, 324);
            this.SectionsDataGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Totals";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // SectionsContextMenuStrip
            // 
            this.SectionsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSectionToolStripMenuItem,
            this.deleteSectionToolStripMenuItem});
            this.SectionsContextMenuStrip.Name = "SectionsContextMenuStrip";
            this.SectionsContextMenuStrip.Size = new System.Drawing.Size(150, 48);
            // 
            // addSectionToolStripMenuItem
            // 
            this.addSectionToolStripMenuItem.Name = "addSectionToolStripMenuItem";
            this.addSectionToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.addSectionToolStripMenuItem.Text = "Add Section...";
            this.addSectionToolStripMenuItem.Click += new System.EventHandler(this.btnSectionAdd_Click);
            // 
            // deleteSectionToolStripMenuItem
            // 
            this.deleteSectionToolStripMenuItem.Name = "deleteSectionToolStripMenuItem";
            this.deleteSectionToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteSectionToolStripMenuItem.Text = "Delete Section";
            this.deleteSectionToolStripMenuItem.Click += new System.EventHandler(this.btnSectionDelete_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(181, 30);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sections";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SectionsToolStrip
            // 
            this.SectionsToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SectionsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.SectionsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSectionAdd,
            this.btnSectionDelete});
            this.SectionsToolStrip.Location = new System.Drawing.Point(0, 354);
            this.SectionsToolStrip.Name = "SectionsToolStrip";
            this.SectionsToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 1, 0);
            this.SectionsToolStrip.Size = new System.Drawing.Size(181, 25);
            this.SectionsToolStrip.TabIndex = 6;
            this.SectionsToolStrip.Text = "toolStrip1";
            // 
            // btnSectionAdd
            // 
            this.btnSectionAdd.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSectionAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSectionAdd.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSectionAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSectionAdd.Image = global::PayrollSystem.Properties.Resources.add02;
            this.btnSectionAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSectionAdd.Name = "btnSectionAdd";
            this.btnSectionAdd.Size = new System.Drawing.Size(23, 22);
            this.btnSectionAdd.Text = "Add Section";
            this.btnSectionAdd.Click += new System.EventHandler(this.btnSectionAdd_Click);
            // 
            // btnSectionDelete
            // 
            this.btnSectionDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSectionDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSectionDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSectionDelete.Image = global::PayrollSystem.Properties.Resources.delete02;
            this.btnSectionDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSectionDelete.Name = "btnSectionDelete";
            this.btnSectionDelete.Size = new System.Drawing.Size(23, 22);
            this.btnSectionDelete.Text = "Delete Section";
            this.btnSectionDelete.Click += new System.EventHandler(this.btnSectionDelete_Click);
            // 
            // FiltersPanel
            // 
            this.FiltersPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.FiltersPanel.BorderColor = System.Drawing.Color.Black;
            this.FiltersPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.FiltersPanel.Controls.Add(this.FiltersCheckPanel);
            this.FiltersPanel.Controls.Add(this.DateFilterPanel);
            this.FiltersPanel.Controls.Add(this.label1);
            this.FiltersPanel.Controls.Add(this.bufferedPanel1);
            this.FiltersPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.FiltersPanel.Location = new System.Drawing.Point(0, 37);
            this.FiltersPanel.Name = "FiltersPanel";
            this.FiltersPanel.Size = new System.Drawing.Size(875, 32);
            this.FiltersPanel.TabIndex = 3;
            // 
            // FiltersCheckPanel
            // 
            this.FiltersCheckPanel.Controls.Add(this.chkShowFinal);
            this.FiltersCheckPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.FiltersCheckPanel.Location = new System.Drawing.Point(57, 0);
            this.FiltersCheckPanel.MaximumSize = new System.Drawing.Size(256, 9999);
            this.FiltersCheckPanel.MinimumSize = new System.Drawing.Size(120, 0);
            this.FiltersCheckPanel.Name = "FiltersCheckPanel";
            this.FiltersCheckPanel.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.FiltersCheckPanel.Size = new System.Drawing.Size(131, 31);
            this.FiltersCheckPanel.TabIndex = 3;
            this.FiltersCheckPanel.WrapContents = false;
            // 
            // chkShowFinal
            // 
            this.chkShowFinal.AutoSize = true;
            this.chkShowFinal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkShowFinal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.chkShowFinal.Location = new System.Drawing.Point(3, 7);
            this.chkShowFinal.Name = "chkShowFinal";
            this.chkShowFinal.Size = new System.Drawing.Size(119, 17);
            this.chkShowFinal.TabIndex = 1;
            this.chkShowFinal.Text = "Show Final Records";
            this.chkShowFinal.UseVisualStyleBackColor = true;
            this.chkShowFinal.CheckedChanged += new System.EventHandler(this.chkShowFinal_CheckedChanged);
            // 
            // DateFilterPanel
            // 
            this.DateFilterPanel.BorderColor = System.Drawing.Color.Black;
            this.DateFilterPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.DateFilterPanel.Controls.Add(this.btnSet);
            this.DateFilterPanel.Controls.Add(this.dpEnd);
            this.DateFilterPanel.Controls.Add(this.lblTo);
            this.DateFilterPanel.Controls.Add(this.dpStart);
            this.DateFilterPanel.Controls.Add(this.lblDatePeriod);
            this.DateFilterPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.DateFilterPanel.Location = new System.Drawing.Point(532, 0);
            this.DateFilterPanel.Name = "DateFilterPanel";
            this.DateFilterPanel.Size = new System.Drawing.Size(343, 31);
            this.DateFilterPanel.TabIndex = 2;
            // 
            // btnSet
            // 
            this.btnSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSet.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.btnSet.Location = new System.Drawing.Point(297, 4);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(43, 22);
            this.btnSet.TabIndex = 5;
            this.btnSet.Text = "SET";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // dpEnd
            // 
            this.dpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpEnd.Location = new System.Drawing.Point(181, 5);
            this.dpEnd.Name = "dpEnd";
            this.dpEnd.Size = new System.Drawing.Size(110, 20);
            this.dpEnd.TabIndex = 4;
            this.dpEnd.ValueChanged += new System.EventHandler(this.dpEnd_ValueChanged);
            // 
            // lblTo
            // 
            this.lblTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblTo.Location = new System.Drawing.Point(161, 8);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 15);
            this.lblTo.TabIndex = 3;
            this.lblTo.Text = "to";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dpStart
            // 
            this.dpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpStart.Location = new System.Drawing.Point(48, 5);
            this.dpStart.Name = "dpStart";
            this.dpStart.Size = new System.Drawing.Size(110, 20);
            this.dpStart.TabIndex = 2;
            this.dpStart.ValueChanged += new System.EventHandler(this.dpStart_ValueChanged);
            // 
            // lblDatePeriod
            // 
            this.lblDatePeriod.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatePeriod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblDatePeriod.Location = new System.Drawing.Point(1, 8);
            this.lblDatePeriod.Name = "lblDatePeriod";
            this.lblDatePeriod.Size = new System.Drawing.Size(44, 15);
            this.lblDatePeriod.TabIndex = 1;
            this.lblDatePeriod.Text = "Period:";
            this.lblDatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 31);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(875, 1);
            this.bufferedPanel1.TabIndex = 4;
            // 
            // SystemPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.ControlBox = false;
            this.Controls.Add(this.SplitContainerSectionView);
            this.Controls.Add(this.FiltersPanel);
            this.Controls.Add(this.MainToolStrip);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SystemPage";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "SystemPage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemPage_FormClosing);
            this.Load += new System.EventHandler(this.SystemPage_Load);
            this.VisibleChanged += new System.EventHandler(this.SystemPage_VisibleChanged);
            this.Resize += new System.EventHandler(this.SystemPage_Resize);
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.SplitContainerMain.Panel1.ResumeLayout(false);
            this.SplitContainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMain)).EndInit();
            this.SplitContainerMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).EndInit();
            this.HeaderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TotalsDataGrid)).EndInit();
            this.SplitContainerSectionView.Panel1.ResumeLayout(false);
            this.SplitContainerSectionView.Panel1.PerformLayout();
            this.SplitContainerSectionView.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerSectionView)).EndInit();
            this.SplitContainerSectionView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SectionsDataGrid)).EndInit();
            this.SectionsContextMenuStrip.ResumeLayout(false);
            this.SectionsToolStrip.ResumeLayout(false);
            this.SectionsToolStrip.PerformLayout();
            this.FiltersPanel.ResumeLayout(false);
            this.FiltersCheckPanel.ResumeLayout(false);
            this.FiltersCheckPanel.PerformLayout();
            this.DateFilterPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripButton btnExport;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripButton btnMarkFinal;
        private System.Windows.Forms.ToolStripButton btnSearch;
        private System.Windows.Forms.ToolStripLabel lblSearch;
        private System.Windows.Forms.ToolStripButton btnArchive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markFinalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private BufferedPanel DateFilterPanel;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblDatePeriod;
        protected internal System.Windows.Forms.ToolStripTextBox txtSearch;
        private System.Windows.Forms.SplitContainer SplitContainerMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridView TotalsDataGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton btnView;
        protected internal System.Windows.Forms.DateTimePicker dpEnd;
        protected internal System.Windows.Forms.DateTimePicker dpStart;
        protected internal BufferedDataGridView RecordsDataGrid;
        private System.Windows.Forms.ToolStripMenuItem resetSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        public System.Windows.Forms.ToolStrip MainToolStrip;
        public System.Windows.Forms.FlowLayoutPanel FiltersCheckPanel;
        private BufferedPanel FiltersPanel;
        private BufferedPanel bufferedPanel1;
        private System.Windows.Forms.ToolTip MainToolTip;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ToolStripButton btnRestore;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.SplitContainer SplitContainerSectionView;
        internal System.Windows.Forms.DataGridView SectionsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label3;
        private BufferedPanel HeaderPanel;
        private System.Windows.Forms.Label lblHeaderText;
        private BufferedPanel bufferedPanel3;
        private System.Windows.Forms.ToolStrip SectionsToolStrip;
        private System.Windows.Forms.ToolStripButton btnSectionAdd;
        private System.Windows.Forms.ToolStripButton btnSectionDelete;
        private System.Windows.Forms.ContextMenuStrip SectionsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addSectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSectionToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkShowFinal;
        private System.Windows.Forms.ToolStripButton btnImport;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private BufferedPanel bufferedPanel5;
        private System.Windows.Forms.ToolStripButton btnToggleTotals;
        private System.Windows.Forms.Button btnSet;
    }
}