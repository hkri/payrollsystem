﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    // The template for each individual pages of the system.
    public partial class SystemPage : Form {

        #region Constructor

        public SystemPage() {
            //Run component initialization.
            InitializeComponent();

            //Set the visible buttons to what is default (to trigger the hide functions)
            VisibleToolbarButtons = VisibleToolbarButtons;
            VisibleFilterControls = VisibleFilterControls;

            //Clear datagrid contents (design time elements)
            ClearRecordsDatagrid();
            ClearTotalsDatagrid();
            SetRecordsDataGridMultiselect(true);

            //Set custom toolbar renderer.
            MainToolStrip.Renderer = SectionsToolStrip.Renderer = SectionsContextMenuStrip.Renderer = GlobalVariables.renderer;

            //Set initial dates
            SetStartDate(GlobalVariables.StartDate.ToShortDateString());
            SetEndDate(GlobalVariables.EndDate.ToShortDateString());

            //Hide filterbar first.
            SetFilterControls(FilterControls.NoFilters);

            //Hide sections pane initially.
            SetTotalsVisible(false);
            SetSectionPaneVisible(false);
        }

        #endregion

        #region Public Events

        //General EventHandler Events.
        public event EventHandler OnAdd;
        public event EventHandler OnEdit;
        public event EventHandler OnDelete;
        public event EventHandler OnArchive;
        public event EventHandler OnImport;
        public event EventHandler OnExport;
        public event EventHandler OnSearch;
        public event EventHandler OnSearchTextChanged;
        public event EventHandler OnFinalize;
        public event EventHandler OnRefresh;
        public event EventHandler OnRecordDoubleClicked;
        public event EventHandler OnRestore;
        public event EventHandler OnRecordsSelectionChanged;
        public event EventHandler OnShowFinalizeChecked;

        //Section Pane EventHandler Events.
        public event EventHandler OnSectionAdd;
        public event EventHandler OnSectionDelete;
        public event EventHandler OnSectionRefresh;

        //DataGridView Events.
        public event DataGridViewCellFormattingEventHandler OnCellFormatting;

        #endregion

        #region IWindowStatusUpdater call

        //Finds all parent forms if it implements IWindowStatusUpdater, updates status label.
        void UpdateWindowStatus(string status) {
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                         wnd.UpdateWindowStatus(status);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        #endregion

        #region Private Variables

        DataTable _TotalsTable = new DataTable();
        Boolean IsLoaded = false;
        String selectedRecords = String.Empty;

        #endregion

        #region Private Functions

        //Hide search box if the searchbox content has empty string.
        void HideSearchbox() {
            //Only do this function when the SEARCH function is enabled.
            if ((VisibleToolbarButtons & PageToolbarButtons.Search) == PageToolbarButtons.Search) {
                if (txtSearch.Text.Length == 0) {
                    txtSearch.Text = "";
                    lblSearch.Visible = false;
                    txtSearch.Visible = false;
                    IsSearching = false;
                    RefreshRecords();
                    UpdateWindowStatus("Ready");
                }
            }
        }

        //Show the label and search textbox.
        void ShowSearchbox() {
            if ((VisibleToolbarButtons & PageToolbarButtons.Search) == PageToolbarButtons.Search) {
                lblSearch.Visible = true;
                txtSearch.Visible = true;
                IsSearching = true;
            }
        }

        //Remove all data from the records datagridview.
        void ClearRecordsDatagrid() {
            RecordsDataGrid.DataSource = null;
            RecordsDataGrid.Columns.Clear();
        }

        //Clear all info in the totals datagridview.
        void ClearTotalsDatagrid() {
            TotalsDataGrid.DataSource = null;
            TotalsDataGrid.Columns.Clear();
        }

        //Communicate with an IWindowStatusUpdater, and reset the record count status.
        void ResetRecordsCounterStatus() {
            //Get each parent of Form type, then check if it implements IWindowStatusUpdater interface.
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateTotalRecordCountStatus(0);
                        wnd.UpdateSelectedRecordCountStatus(0);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        //Show the total records count through the IWindowStatusUpdater interface.
        void ShowTotalRecordsCount() {
            //Recursively find the parent form that implements IWindowStatusUpdater.
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateTotalRecordCountStatus(RecordsDataGrid.Rows.Count);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        //Display the number of selected records through the IWindowStatusUpdater interface.
        void ShowSelectedRecordsCount() {
            //Recursively find the parent form that implements IWindowStatusUpdater.
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateSelectedRecordCountStatus(RecordsDataGrid.SelectedRows.Count);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        //Trigger the OnRecordDoubleClicked event.
        void ViewSelectedItem() {
            //Do this only when the view button is visible.
            if (btnView.Enabled && btnView.Visible) {
                if (OnRecordDoubleClicked != null) OnRecordDoubleClicked(this, new EventArgs());
            }
        }

        #endregion

        #region Control Properties

        private PageToolbarButtons _toolbarbuttons = PageToolbarButtons.View | PageToolbarButtons.Refresh | PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Search;
        
        [EditorBrowsable( EditorBrowsableState.Always)]
        [Browsable(true)]
        //Get or sets the visible toolbar button flags.
        public PageToolbarButtons VisibleToolbarButtons {
            get { return _toolbarbuttons; }
            set { _toolbarbuttons = value; }
        }

        private FilterControls _filtercontrols = FilterControls.NoFilters;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        //Gets or sets the visible filter control flags.
        public FilterControls VisibleFilterControls {
            get { return _filtercontrols; }
            set { _filtercontrols = value;}
        }

        private bool _searching = false;
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        //Gets or sets whether the page is on active search mode (searchbox visible).
        public bool IsSearching {
            get { return _searching; }
            set { _searching = value; }
        }

        string _FinalRecordsColumnName = "IsFinal";
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        //Determines the column name where the recored is marked "Locked"
        public string FinalRecordsColumnName {
            get { return _FinalRecordsColumnName; }
            set { _FinalRecordsColumnName = value; }
        }

        bool _AllowsArchiveOfUnlockedRecords = false;
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        //If true, user can archive "locked" records.
        public bool AllowsArchiveOfUnlockedRecords {
            get { return _AllowsArchiveOfUnlockedRecords; }
            set { _AllowsArchiveOfUnlockedRecords = value; }
        }

        bool _AllowsDeleteOfLockedRecords = false;
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        //If true, users can permanently delete "final" records.
        public bool AllowsDeleteOfLockedRecords {
            get { return _AllowsDeleteOfLockedRecords; }
            set { _AllowsDeleteOfLockedRecords = value; }
        }

        #endregion

        #region Control Visibility Flags

        //Flags to determine which toolbar buttons are visible.
        public enum PageToolbarButtons {
            Add         = 1,            Edit        = 2,
            Delete      = 4,            Archive     = 8,
            Export      = 16,           Refresh     = 32,
            MarkFinal   = 64,           Search      = 128,
            View        = 256,          Restore     = 512,
            Import      = 1024,         ToggleTotals= 2048,
        }

        //Flags to determine the visible filter controls.
        public enum FilterControls {
            NoFilters           = 1,    FinalCheckbox       = 2,
            SingleDatePeriod    = 4,    RangeDatePeriod     = 8,
            MonthDatePicker     = 16,   MonthYearDatePicker = 32
        }

        #endregion

        #region Controls visibility control functions

        //Hides all controls in the filter panel.
        void HideAllFilterControls() {
            chkShowFinal.Visible = lblDatePeriod.Visible =
            dpStart.Visible = dpEnd.Visible =
            lblTo.Visible = false;
        }

        //Hides all toolbar controls.
        void HideAllToolbarButtons() {
            btnAdd.Visible = btnEdit.Visible =
            btnDelete.Visible = btnExport.Visible =
            btnArchive.Visible = btnRefresh.Visible =
            btnMarkFinal.Visible = lblSearch.Visible = 
            btnView.Visible = txtSearch.Visible = 
            btnSearch.Visible = btnRestore.Visible = 
            btnImport.Visible = false;
        }

        //Set which filter controls will be visible via flags.
        public void SetFilterControls(FilterControls controls) {
            //Hide all controls first.
            HideAllFilterControls();

            //Perform flag checks to determine which controls are visible.
            if ((controls & FilterControls.NoFilters) == FilterControls.NoFilters) {
                FiltersPanel.Visible = false;
            } else {
                FiltersPanel.Visible = true;
                //FiltersPanel.BringToFront();
                if ((controls & FilterControls.FinalCheckbox) == FilterControls.FinalCheckbox) {
                    chkShowFinal.Visible = true;
                }
                if ((controls & FilterControls.SingleDatePeriod) == FilterControls.SingleDatePeriod) {
                    lblDatePeriod.Visible = dpStart.Visible = true;
                    dpStart.Width = 130;
                    DateFilterPanel.Width = 200;
                    btnSet.Visible = btnSet.Enabled = false;
                }
                if ((controls & FilterControls.RangeDatePeriod) == FilterControls.RangeDatePeriod) {
                    lblDatePeriod.Visible = dpStart.Visible = lblTo.Visible = dpEnd.Visible = true;
                    DateFilterPanel.Width = 345;
                    btnSet.Visible = btnSet.Enabled = true;
                }
                if ((controls & FilterControls.MonthDatePicker) == FilterControls.MonthDatePicker) {
                    lblDatePeriod.Visible = dpStart.Visible = true;
                    DateFilterPanel.Width = 200;
                    dpStart.Width = 130;
                    SetMonthPicker();
                    btnSet.Visible = btnSet.Enabled = false;
                }
                if ((controls & FilterControls.MonthYearDatePicker) == FilterControls.MonthYearDatePicker) {
                    lblDatePeriod.Visible = dpStart.Visible = true;
                    DateFilterPanel.Width = 200;
                    dpStart.Width = 130;
                    SetMonthYearPicker();
                    btnSet.Visible = btnSet.Enabled = false;
                }
            }

            //Save the settings on the property.
            VisibleFilterControls = controls;
        }

        //Set which toolbar buttons will be visible via flags.
        public void SetToolbarButtons(PageToolbarButtons buttons) {
            //Hide all toolbar buttons first.
            HideAllToolbarButtons();

            //Perform bit tests to determine which control will be visible.
            if ((buttons & PageToolbarButtons.Add) == PageToolbarButtons.Add)
                btnAdd.Visible = true;

            if ((buttons & PageToolbarButtons.Edit) == PageToolbarButtons.Edit)
                btnEdit.Visible = true;

            if ((buttons & PageToolbarButtons.Delete) == PageToolbarButtons.Delete)
                btnDelete.Visible = true;

            if ((buttons & PageToolbarButtons.Export) == PageToolbarButtons.Export)
                btnExport.Visible = true;

            if ((buttons & PageToolbarButtons.Archive) == PageToolbarButtons.Archive)
                btnArchive.Visible = true;

            if ((buttons & PageToolbarButtons.Refresh) == PageToolbarButtons.Refresh)
                btnRefresh.Visible = true;

            if ((buttons & PageToolbarButtons.MarkFinal) == PageToolbarButtons.MarkFinal)
                btnMarkFinal.Visible = true;

            if ((buttons & PageToolbarButtons.Search) == PageToolbarButtons.Search)
                btnSearch.Visible = true;

            if ((buttons & PageToolbarButtons.View) == PageToolbarButtons.View)
                btnView.Visible = true;

            if ((buttons & PageToolbarButtons.Restore) == PageToolbarButtons.Restore)
                btnRestore.Visible = true;

            if ((buttons & PageToolbarButtons.Import) == PageToolbarButtons.Import)
                btnImport.Visible = true;

            //Save settings on property.
            VisibleToolbarButtons = buttons;

            //Trigger visiblility/enabled of buttons depending on selected records.
            SetButtonToolbarVisiblityOnRecordSelect();
        }

        #endregion

        #region Toolbar Text Visibility Toggle

        //Boolean flag to remember if toolbar buttons' text are visible.
        bool ToolbarButtonTextVisible = true;

        //Set toolbar display style as Image and Text.
        void ShowToolbarButtonText() {
            btnAdd.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnEdit.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnDelete.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnArchive.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnExport.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnMarkFinal.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnRefresh.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnView.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnRestore.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnImport.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnToggleTotals.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            ToolbarButtonTextVisible = true;
        }

        //Set toolbar display style as Image only.
        void HideToolbarButtonText() {
            btnAdd.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnEdit.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnDelete.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnArchive.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnExport.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnMarkFinal.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnRefresh.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnView.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnRestore.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnImport.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnToggleTotals.DisplayStyle = ToolStripItemDisplayStyle.Image;
            ToolbarButtonTextVisible = false;
        }

        #endregion

        #region Window Resize Eventhandlers

        //The window RESIZE event handler.
        private void SystemPage_Resize(object sender, EventArgs e) {
            //Responsive design:
            //Show: toolbar button text when width is 750 or wider.
            //Hide: toolbar button text when width is less than 750.

            //This ensure more space for narrower window.
            if (Width < 750) {
                if (ToolbarButtonTextVisible)
                    HideToolbarButtonText();
            } else {
                if (ToolbarButtonTextVisible == false)
                    ShowToolbarButtonText();
            }
        }

        #endregion

        #region Public Functions

        //Sets if the view button should be visible or not.
        public void SetViewButtonVisible(bool visible) {
            btnView.Visible = visible;
        }

        //Sets if the edit button should be visible or not.
        public void SetEditButtonVisible(bool visible) {
            btnEdit.Visible = visible;
        }

        //Sets if the final button should be visible or not.
        public void SetFinalButtonVisible(bool visible) {
            btnMarkFinal.Visible = visible;
        }

        //Sets the visibility of the archive button.
        public void SetArchiveButtonVisible(bool visible) {
            btnArchive.Visible = visible;
        }

        //Sets if the delete button is visible or not.
        public void SetDeleteButtonVisible(bool visible) {
            btnDelete.Visible = visible;
        }

        //Sets if the summation button toolbar is visible or not.
        public void SetTotalsButtonVisible(bool visible) {
            btnToggleTotals.Visible = btnToggleTotals.Enabled = visible;
        }
        
        //Refreshes the section pane.
        public void RefreshSections() {
            if (OnSectionRefresh != null) OnSectionRefresh(this, new EventArgs());
        }

        //Set if section pane is visible or not.();
        public void SetSectionPaneVisible(bool visibility) {
            SplitContainerSectionView.Panel1Collapsed = !visibility;
        }

        //Sets the header text, and makes it visible.
        public void SetHeaderText(string text) {
            lblHeaderText.Text = text;
            if (HeaderPanel.Visible == false) HeaderPanel.Visible = true;
        }

        //Sets if header text is visible.
        public void SetHeaderVisible(bool visible) {
            HeaderPanel.Visible = visible;
        }

        //Setups the totals DataGridView, adds columns and rows.
        public void InitializeTotals() {
            //Clean up tables.
            if (_TotalsTable != null) _TotalsTable.Dispose();

            //Assign new data table, then add columns.
            _TotalsTable = new DataTable();
            _TotalsTable.Columns.Add("Item");
            _TotalsTable.Columns.Add("Value");
            _TotalsTable.Columns["Value"].DataType = typeof(object);

            //Set the datasource of the totals datagrid to the current data table.
            GetTotalsDataGridView().DataSource = _TotalsTable;

            //Setup the "Value" column header width to be filled.
            GetTotalsDataGridView().Columns["Item"].Width = 120;
            GetTotalsDataGridView().Columns["Value"].DefaultCellStyle.Format = "#,##0.00";
            GetTotalsDataGridView().Columns["Value"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GetTotalsDataGridView().ClearSelection();
        }

        //Make the columns of totals datagrid autofit to its contents.
        public void AutoFitTotalsDataGridItems() {
            //Make columns fit contents.
            if (TotalsDataGrid.Columns.Count > 0) {
                foreach (DataGridViewColumn col in TotalsDataGrid.Columns) {
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    int nwidt = col.Width;
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    if(col.DisplayIndex < TotalsDataGrid.ColumnCount)
                        col.Width = nwidt;
                    else
                        col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        //Adds a header row (bold) on total datagrid.
        public DataRow AddTotalInfoHeader(string heading) {
            if (_TotalsTable != null) {
                DataRow row = _TotalsTable.NewRow();
                row["Item"] = heading;
                _TotalsTable.Rows.Add(row);
                GetTotalsDataGridView().ClearSelection();

                DataGridViewRow r = GetTotalsDataGridView().Rows[GetTotalsDataGridView().Rows.Count - 1];
                if(r != null) r.DefaultCellStyle.Font = new Font(GetTotalsDataGridView().Font, FontStyle.Bold);
                return row;
            } else {
                throw new Exception("Totals should be initialized first before adding items.");
            }
        }

        //Modify the ITEM content of the specified totals item.
        public void EditTotalInfoHeader(DataRow header, string heading) {
            header["Item"] = heading;
        }

        //Adds an item to the total datagrid.
        public void AddTotalInfo(string item, object value) {
            if (_TotalsTable != null) {
                if (item != null) {
                    if (!item.Trim().Equals("")) {
                        DataRow row = _TotalsTable.NewRow();
                        row["Item"] = item;
                        row["Value"] = value;
                        _TotalsTable.Rows.Add(row);
                        GetTotalsDataGridView().ClearSelection();
                    }
                }
            } else {
                throw new Exception("Totals should be initialized first before adding items.");
            }
        }

        //Deletes all total info.
        public void ClearAllTotal() {
            if (_TotalsTable != null) {
                _TotalsTable.Rows.Clear();
            }
        }

        //Disables common editing controls at invoke (Edit and Mark Final).
        public void DisableEditingControls() {
            //Get the currently visible toolbar buttons.
            PageToolbarButtons buttons = VisibleToolbarButtons;

            //Disable EDIT command, if active.
            if ((buttons & PageToolbarButtons.Edit) == PageToolbarButtons.Edit)
                    btnEdit.Enabled = false;

            //Disable MARK FINAL command, if active.
            /*  overridden 
            if ((buttons & PageToolbarButtons.MarkFinal) == PageToolbarButtons.MarkFinal)
                    btnMarkFinal.Enabled = false;
            */
        }

        //Force hide searchbox.
        public void ForceHideSearchBox() {
            //Do only if the SEARCH functionality is enabled.
            if ((VisibleToolbarButtons & PageToolbarButtons.Search) == PageToolbarButtons.Search) {
                txtSearch.Text = "";
                lblSearch.Visible = false;
                txtSearch.Visible = false;
                IsSearching = false;
            }
        }

        //Returns the specifiefd Records DataGridView column.
        public DataGridViewColumn GetColumn(string name) {
            return RecordsDataGrid.Columns[name];
        }

        //Add an external control the the filters flow panel, for extra filter options.
        public void AddFilterControl(Control control) {
            FiltersCheckPanel.Controls.Add(control);
        }

        //Add an external toolbar button for other commands.
        public void AddCustomToolbarButton(ToolStripButton button) {
            MainToolStrip.Items.Add(button);
        }

        //Programmatically trigger the ADD button (automatic ADD record calls).
        public void InvokeAdd() {
            btnAdd.PerformClick();
        }

        //Sets the date picker to month and year view.
        public void SetMonthYearPicker() {
            dpStart.Format = DateTimePickerFormat.Custom;
            dpStart.CustomFormat = "MMMM yyyy";
            dpStart.ShowUpDown = true;
        }

        //Sets the date picker to month selection only.
        public void SetMonthPicker() {
            dpStart.Format = DateTimePickerFormat.Custom;
            dpStart.CustomFormat = "MMMM";
            dpStart.ShowUpDown = true;
        }

        //Get the month from the FROM DatePicker.
        public string GetPickerFromMonth() {
            return dpStart.Value.ToString("MM");
        }

        //Get the year from the FROM DatePicker
        public string GetPickerFromYear() {
            return dpStart.Value.ToString("yyyy");
        }

        //Get the date (MM/dd/yyyy) from the FROM DatePicker
        public string GetPickerFromDate() {
            return dpStart.Value.ToString("MM/dd/yyyy");
        }

        //Get the month from the TO DatePicker
        public string GetPickerToMonth() {
            return dpEnd.Value.ToString("MM");
        }

        //Get the year from the TO DatePicker
        public string GetPickerToYear() {
            return dpEnd.Value.ToString("yyyy");
        }

        //Get the date (MM/dd/yyyy) from the TO DatePicker
        public string GetPickerToDate() {
            return dpEnd.Value.ToString("MM/dd/yyyy");
        }

        //Retrieves all row value (string) on the specififed column.
        //Optional: Retrieve specific rows based on a condition.
        public string[] GetRecordsValueArray(string columnname, string columncondition = "", string condition = "") {
            List<string> vals = new List<string>();
            foreach (DataGridViewRow r in RecordsDataGrid.Rows) {
                string val = (r.Cells[columnname].Value.ToString());
                if (columncondition.Equals(""))
                    vals.Add(val);
                else {
                    string cond = (r.Cells[columncondition].Value.ToString());
                    if (cond.Equals(condition)) {
                        vals.Add(val);
                    }
                }
            }
            return vals.ToArray();
        }

        //Gets the total count of records in the Records DataGridView.
        public int GetRecordsCount() {
            return RecordsDataGrid.Rows.Count;
        }

        //Gets the total count of records under specififed condition.
        public int GetRecordsCount(string colname = "", string condition = "") {
            if (colname.Equals(""))
                return RecordsDataGrid.Rows.Count;
            else {
                int ctr = 0;
                try {
                    foreach (DataGridViewRow r in RecordsDataGrid.Rows) {
                        if (r.Cells[colname].Value.ToString().Equals(condition))
                            ctr++;
                    }
                } catch {
                    return 0;
                }
                return ctr;
            }
        }

        //Returns the number of selected rows that matches the specified criteria.
        public int GetRecordsSelectedRowValuesCount(string columnname, string columncondition = "", string condition = "") {
            List<string> vals = new List<string>();
            foreach (DataGridViewRow r in RecordsDataGrid.SelectedRows) {
                string val = (r.Cells[columnname].Value.ToString());
                if(columncondition.Equals(""))
                    vals.Add(val);
                else {
                    string cond = (r.Cells[columncondition].Value.ToString());
                    if(cond.Equals(condition))
                        vals.Add(val);
                }
            }
            return vals.Count();
        }

        //Returns string array of values of selected rows.
        public string[] GetRecordsSelectedRowValues(string columnname) {
            List<string> vals = new List<string>();
            foreach (DataGridViewRow r in RecordsDataGrid.SelectedRows) {
                vals.Add(r.Cells[columnname].Value.ToString());
            }
            if (vals.Count == 0) return null;
            else return vals.ToArray();
        }

        //Hide column of the specified name.
        public void HideColumn(string colname) {
            try {
                RecordsDataGrid.Columns[colname].Visible = false;
            } catch (Exception Ex) {
                System.Diagnostics.Trace.WriteLine(Ex.Message);
            }
        }

        //Specifies the string format of the selected column.
        public void SetColumnFormat(string colname, string format) {
            try {
                RecordsDataGrid.Columns[colname].DefaultCellStyle.Format = format;
            } catch (Exception Ex) {
                System.Diagnostics.Trace.WriteLine(Ex.Message);
            }
        }

        //Sets the header text of the specified column.
        public void SetColumnHeader(string colname, string header) {
            try {
                RecordsDataGrid.Columns[colname].HeaderText = header;
            } catch (Exception Ex) {
                System.Diagnostics.Trace.WriteLine(Ex.Message);
            }
        }

        //Sets the width of the specified column to fit its largest content.
        public void RecordsDGVFitColumnToContent(String colname) {
            if (RecordsDataGrid.Columns.Contains(colname)) {
                DataGridViewColumn col = RecordsDataGrid.Columns[colname];
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                int nwidt = col.Width;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = nwidt;
            }
        }

        //Refreshes the internal IDs of each row.
        public void UpdateInternalIDs() {
            //Add internal id for internal purposes (like remembering the selected items).
            try {
                using (DataTable dt = (DataTable)GetRecordsDataSource()) {
                    if (dt.Columns.Contains("_INTERNALID_")) {
                        int idctr = 0;
                        if(RecordsDataGrid.Columns["_INTERNALID_"].Visible) RecordsDataGrid.Columns["_INTERNALID_"].Visible = false;
                        foreach (DataRow r in dt.Rows) {
                            r["_INTERNALID_"] = idctr++;
                        }
                    }
                }
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        //Specifies the DataSource of the Records DataGridView.
        public void SetRecordsDataSource(DataTable table) {
            SuspendLayout();
            RecordsDataGrid.Columns.Clear();

            //Add internal id for internal purposes (like remembering the selected items).
            table.Columns.Add("_INTERNALID_");

            /*
            if (table.Columns.Contains("_INTERNALID_")) {
                int idctr = 0;
                foreach (DataRow r in table.Rows) {
                    r["_INTERNALID_"] = idctr++;
                }
            }
             */
             
            RecordsDataGrid.DataSource = table;
            UpdateInternalIDs();

            /*  Autofitting all columns slows down the program on large records.
            if (RecordsDataGrid.Columns.Count > 0) {
                foreach (DataGridViewColumn col in RecordsDataGrid.Columns) {
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    int nwidt = col.Width;
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    col.Width = nwidt;
                }
            }
            */

            //Add extension column.
            DataGridViewColumn spacer = new DataGridViewTextBoxColumn();
            spacer.HeaderText = "";
            spacer.ReadOnly = true;
            spacer.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            RecordsDataGrid.Columns.Add(spacer);


            ResumeLayout();
        }

        //Returns the data source of the records datagridview.
        public object GetRecordsDataSource() {
            return RecordsDataGrid.DataSource;
        }

        //Set the DataSource of the Totals DataGridView.
        public void SetTotalsDataSource(DataTable table) {
            //Clear then set DataSource.
            TotalsDataGrid.Columns.Clear();
            TotalsDataGrid.DataSource = table;

            //Add extension column.
            DataGridViewColumn spacer = new DataGridViewTextBoxColumn();
            spacer.ReadOnly = true;
            spacer.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            TotalsDataGrid.Columns.Add(spacer);
        }

        //Determines of the Records DataGridView will allow multiselection.
        public void SetRecordsDataGridMultiselect(bool multiselect) {
            RecordsDataGrid.MultiSelect = multiselect;
        }

        //Determines if the Totals DataGridView will be visible.
        public void SetTotalsVisible(bool visibility) {
            SplitContainerMain.Panel2Collapsed = !visibility;
        }

        //Gets wheter the totals panel is visible or not.
        public bool IsTotalsVisible() {
            return !SplitContainerMain.Panel2Collapsed;
        }

        //Returns the Totals DataGridView object.
        public DataGridView GetTotalsDataGridView() {
            return TotalsDataGrid;
        }

        //Returns the Records DataGridView object.
        public DataGridView GetRecordsDataGridView() {
            return RecordsDataGrid;
        }

        //Triggers the OnRefresh event.
        public void RefreshRecords() {
            SuspendLayout();
            if (OnRefresh != null) {
                OnRefresh(this, new EventArgs());
            }
            ShowTotalRecordsCount();
            ResumeLayout();
        }

        //Returns the text/string in the searchbox.
        public string GetSearchText() {
            return txtSearch.Text;
        }

        //Returns if the SHOW FINAL filter is checked or not.
        public bool GetShowFinal() {
            return chkShowFinal.Checked;
        }

        //Gets the starting date string.
        public string GetStartDateString() {
            return dpStart.Text;
        }

        //Gets the ending date string.
        public string GetEndDateString() {
            return dpEnd.Text;
        }

        //Gets the end DatePicker DateTime value.
        public DateTime GetEndDate() {
            return dpEnd.Value;
        }

        //Gets the start DatePicker DateTime value
        public DateTime GetStartDate() {
            return dpStart.Value;
        }

        //Sets the start date period.
        public void SetStartDate(string date) {
            dpStart.Text = date;
        }

        //Sets the end date period.
        public void SetEndDate(string date) {
            dpEnd.Text = date;
        }

        //Determines if the Records DataGridView allows editing.
        public void SetRecordsDataGridLocked(bool locked){
            RecordsDataGrid.ReadOnly = locked;
        }

        //Returns the selected rows in the Records DataGridView.
        public DataGridViewSelectedRowCollection GetRecordsSelectedRows() {
            return RecordsDataGrid.SelectedRows;
        }

        //Returns the single selected row in the Records DataGridView.
        public DataGridViewRow GetRecordsSelectedRow() {
            if (RecordsDataGrid.SelectedRows.Count > 0)
                return RecordsDataGrid.SelectedRows[0];
            else
                return null;
        }

        //Gets the values of the selected rows in the Records DataGridView.
        //Key:   Column Header
        //Value: Data
        public Dictionary<string, string> GetRecordsSelectedRowValues() {
            Dictionary<string, string> cols = new Dictionary<string,string>();
            if (RecordsDataGrid.SelectedRows.Count > 0) {
                DataGridViewRow r = RecordsDataGrid.SelectedRows[0];
                foreach (DataGridViewColumn c in RecordsDataGrid.Columns) {
                    if(r.Cells[c.HeaderText].Value != null)
                        cols.Add(c.HeaderText, r.Cells[c.HeaderText].Value.ToString());
                }
            } else
                return null;
            return cols;
        }

        //Gets the value of the selected record in the specified column.
        public string Cell(string col) {
            try {
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    if (RecordsDataGrid.SelectedRows[0].Cells[col].Value != null)
                        return RecordsDataGrid.SelectedRows[0].Cells[col].Value.ToString();
                }
            } catch (Exception ex) {
                System.Diagnostics.Trace.WriteLine("SystemPage Error: " + ex.Message);
            }
            return String.Empty;
        }

        //Gets the value of the nth selected record in the specified column.
        public string Cell(string col, int rowindex) {
            try {
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    if (RecordsDataGrid.SelectedRows[rowindex].Cells[col].Value != null)
                        return RecordsDataGrid.SelectedRows[rowindex].Cells[col].Value.ToString();
                }
            } catch (Exception ex) {
                System.Diagnostics.Trace.WriteLine("SystemPage Error: " + ex.Message);
            }
            return String.Empty;
        }

        //Gets the instance of the From/Start DatePicker.
        public DateTimePicker GetDatePicker1() {
            return dpStart;
        }

        //Set the checked status of the set final checkbox.
        public void SetShowFinalCheckboxChecked(bool ischecked){
            chkShowFinal.Checked = ischecked;
        }
        
        #endregion

        #region Summation Functions

        //Gets the count of the rows that matches the specified condition.
        public int GetRowCount(string column, object condition) {
            //Prepare variables
            int total = 0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null)
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.Rows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.Rows) {
                    if (row.Cells[column].Value.Equals(condition))
                        total++;
                }
            }

            return total;
        }

        //Gets the count of the rows that matches the specified condition.
        public int GetSelectedRowCount(string column, object condition) {
            //Prepare variables
            int total = 0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null)
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.SelectedRows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.SelectedRows) {
                    if (row.Cells[column].Value.Equals(condition))
                        total++;
                }
            }

            return total;
        }

        //Gets the total of all values on the selected column.
        public double GetColumnTotal(string column) {
            //Prepare variables
            double total = 0.00, val = 0.0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null || (col.CellType != typeof(double) && col.CellType != typeof(int) &&
                col.ValueType != typeof(double) && col.ValueType != typeof(int) ) )
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.Rows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.Rows) {
                    val = 0.0;
                    Double.TryParse(row.Cells[column].Value.ToString(), out val);
                    total += val;
                }
            }

            return total;
        }

        //Gets the total of all values on the selected column of the selected rows.
        public double GetColumnTotalSelectedRows(string column) {
            //Prepare variables
            double total = 0.00, val = 0.0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null || (col.CellType != typeof(double) && col.CellType != typeof(int) &&
                col.ValueType != typeof(double) && col.ValueType != typeof(int)))
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.SelectedRows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.SelectedRows) {
                    val = 0.0;
                    Double.TryParse(row.Cells[column].Value.ToString(), out val);
                    total += val;
                }
            }

            return total;
        }

        //Gets the total of all valus on the selected column, based on exact condition.
        public double GetColumnTotal(string column, string columncondition, object condition) {
            //Prepare variables
            double total = 0.00, val = 0.0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null || (col.CellType != typeof(double) && col.CellType != typeof(int) &&
                col.ValueType != typeof(double) && col.ValueType != typeof(int)))
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.Rows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.Rows) {
                    if (row.Cells[columncondition].Value.Equals(condition)) {
                        val = 0.0;
                        Double.TryParse(row.Cells[column].Value.ToString(), out val);
                        total += val;
                    }
                }
            }

            return total;
        }

        //Gets the total of all valus on the selected column, based on exact condition. Selected rows.
        public double GetColumnTotalSelectedRows(string column, string columncondition, object condition) {
            //Prepare variables
            double total = 0.00, val = 0.0;
            DataGridViewColumn col = RecordsDataGrid.Columns[column];

            //Perform checks if columns indicated do exist.
            if (col == null || (col.CellType != typeof(double) && col.CellType != typeof(int) &&
                col.ValueType != typeof(double) && col.ValueType != typeof(int)))
                return total;

            //Loop through all rows.
            if (RecordsDataGrid.SelectedRows.Count > 0) {
                foreach (DataGridViewRow row in RecordsDataGrid.SelectedRows) {
                    if (row.Cells[columncondition].Value.Equals(condition)) {
                        val = 0.0;
                        Double.TryParse(row.Cells[column].Value.ToString(), out val);
                        total += val;
                    }
                }
            }

            return total;
        }

        //Gets the summation of all values group by items on specified column.
        public IEnumerable<TotalsItem> GetColumnTotalGroupedBy(string column, string group_by) {
            IEnumerable<DataGridViewRow> rows = GetRecordsDataGridView().Rows.Cast<DataGridViewRow>();
            return (from c in rows group c by c.Cells[group_by].Value.ToString() into a orderby a.Key select new TotalsItem(a.Key.ToString(), a.Sum(x => Convert.ToDouble(x.Cells[column].Value))));
        }

        //Gets the summation of all values group by items on specified column.
        public IEnumerable<TotalsItem> GetColumnTotalGroupedBySelectedRows(string column, string group_by) {
            IEnumerable<DataGridViewRow> rows = GetRecordsDataGridView().SelectedRows.Cast<DataGridViewRow>();
            return (from c in rows group c by c.Cells[group_by].Value.ToString() into a orderby a.Key select new TotalsItem(a.Key.ToString(), a.Sum(x => Convert.ToDouble(x.Cells[column].Value))));
        }
        
        #endregion

        #region View, Edit, Delete, Archive, and Mark Final visibility

        //Rule: these buttons should only be visible if there is a selected record in the Records Datagrid.
        //Special: Edit and export should only be visible when there is exactly one record selected in the Records datagrid (multiselect).
        void SetButtonToolbarVisiblityOnRecordSelect() {
            //Retrieves the available/set toolbar commands.
            PageToolbarButtons buttons = VisibleToolbarButtons;

            //The number of selected rows.
            int sel = RecordsDataGrid.SelectedRows.Count;

            //Test first if the "locked" column exists.
            /*
            try {
                if (!FinalRecordsColumnName.Trim().Equals("")) {
                    string ccc = RecordsDataGrid.Columns[FinalRecordsColumnName].HeaderText;
                }
            } catch {
                FinalRecordsColumnName = "";
            }
             */

            //Check first if there is any marked "final" in the selected items.
            bool lockedFound = false;
            bool unlockedFound = false;
            bool finalcol_exists = false;
            //Ignore check if records datagrid is not multiselect.
            if (RecordsDataGrid.Columns.Contains(FinalRecordsColumnName)) {
                finalcol_exists = true;
                if (!FinalRecordsColumnName.Trim().Equals("")) {
                    foreach (DataGridViewRow drow in GetRecordsSelectedRows()) {
                        if ((bool)drow.Cells[FinalRecordsColumnName].Value == true && !lockedFound)
                            lockedFound = true;
                        if ((bool)drow.Cells[FinalRecordsColumnName].Value == false && !unlockedFound)
                            unlockedFound = true;
                        if (lockedFound && unlockedFound) break;    //stop loop if both cases already found.
                    }
                }
            }

            if ((buttons & PageToolbarButtons.Edit) == PageToolbarButtons.Edit) {
                if (sel == 1) {
                    btnEdit.Enabled = true;
                    btnEdit.Visible = true;
                }  else
                    btnEdit.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.Delete) == PageToolbarButtons.Delete) {
                if (sel > 0) {
                    if (!finalcol_exists || (!lockedFound && AllowsDeleteOfLockedRecords) || (lockedFound && AllowsDeleteOfLockedRecords) || (!lockedFound && !AllowsDeleteOfLockedRecords)) {
                        btnDelete.Enabled = true;
                    } else {
                        btnDelete.Enabled = false;
                    }
                } else {
                    btnDelete.Enabled = false;
                }
            }

            if ((buttons & PageToolbarButtons.Archive) == PageToolbarButtons.Archive) {
                if (sel > 0) {
                    if (!finalcol_exists || (!lockedFound && AllowsArchiveOfUnlockedRecords) || (lockedFound && AllowsArchiveOfUnlockedRecords) || (lockedFound && !AllowsArchiveOfUnlockedRecords)) {
                        btnArchive.Enabled = btnArchive.Visible = true;
                    } else {
                        btnArchive.Enabled = false;
                    }
                }  else
                    btnArchive.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.MarkFinal) == PageToolbarButtons.MarkFinal) {
                if (sel > 0) {
                    if (!finalcol_exists || unlockedFound) {
                        btnMarkFinal.Enabled = true;
                        btnMarkFinal.Visible = true;
                    } else {
                        btnMarkFinal.Enabled = false;
                    }
                } else
                    btnMarkFinal.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.Export) == PageToolbarButtons.Export) {
                if (sel > 0)
                    btnExport.Enabled = true;
                else 
                    btnExport.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.View) == PageToolbarButtons.View) {
                if (sel == 1)
                    btnView.Enabled = true;
                else
                    btnView.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.Restore) == PageToolbarButtons.Restore) {
                if (sel > 0)
                    btnRestore.Enabled = true;
                else
                    btnRestore.Enabled = false;
            }

            if ((buttons & PageToolbarButtons.ToggleTotals) == PageToolbarButtons.ToggleTotals) {
                btnToggleTotals.Enabled = true;
                btnToggleTotals.Visible = true;
            }
        }

        #endregion

        #region Window eventhandlers

        //Event when page changed visibility
        private void SystemPage_VisibleChanged(object sender, EventArgs e) {
            if(Visible && IsLoaded) RefreshRecords();
        }

        //Form closing window event handler.
        private void SystemPage_FormClosing(object sender, FormClosingEventArgs e) {
            ResetRecordsCounterStatus();    //Call the IWindowStatusUpdater to reset the status counter.
        }

        //Form load event handler.
        private void SystemPage_Load(object sender, EventArgs e) {
            //Set the visible toolbar buttons once page loads.
            SetButtonToolbarVisiblityOnRecordSelect();
            //Trigger refresh on page load();
            RefreshRecords();           
            RecordsDataGrid.Focus();
            IsLoaded = true;
        }

        #endregion

        #region Toolbar Eventhandlers

        //Show or hide the totals panel.
        private void btnToggleTotals_Click(object sender, EventArgs e) {
            if (SplitContainerMain.Panel2Collapsed) {
                // collapsed -> expanded
                SplitContainerMain.Panel2Collapsed = false;
                btnToggleTotals.Text = "Hide Totals";
            } else {
                // expanded -> collapsed
                SplitContainerMain.Panel2Collapsed = true;
                btnToggleTotals.Text = "Show Totals";
            }
        }

        //Trigger OnImport event when import button is clicked/pressed.
        private void btnImport_Click(object sender, EventArgs e) {
            if(btnImport.Visible && btnImport.Enabled && Visible)
                if (OnImport != null) OnImport(sender, new EventArgs());
        }

        //Triggers when focus left the search textbox.
        private void txtSearch_Leave(object sender, EventArgs e) {
            HideSearchbox();
        }

        //Event when VIEW command is clicked.
        private void btnView_Click(object sender, EventArgs e) {
            ViewSelectedItem();
        }

        //Eventhandler for the ADD command.
        private void btnAdd_Click(object sender, EventArgs e) {
            if (btnAdd.Visible && btnAdd.Enabled && Visible)
                if (OnAdd != null) OnAdd(sender, e);
        }

        //Eventhandler for the EDIT command.
        private void btnEdit_Click(object sender, EventArgs e) {
            if (btnEdit.Visible && btnEdit.Enabled && Visible)
                if (OnEdit != null) OnEdit(sender, e);
        }

        //Event handler for DELETE command.
        private void btnDelete_Click(object sender, EventArgs e) {
            if (btnDelete.Visible && btnDelete.Enabled && Visible) {
                if (OnDelete != null) OnDelete(sender, e);
            }
        }

        //Event handler for ARCHIVE command.
        private void btnArchive_Click(object sender, EventArgs e) {
            if (btnArchive.Visible && btnArchive.Enabled && Visible) {
                if (OnArchive != null) OnArchive(sender, e);
            }
        }

        //Event handler for EXPORT command.
        private void btnExport_Click(object sender, EventArgs e) {
            if (btnExport.Visible && btnExport.Enabled && Visible) {
                if (OnExport != null) OnExport(sender, e);
            }
        }

        //Event handler for REFRESH command.
        private void btnRefresh_Click(object sender, EventArgs e) {
            if (btnRefresh.Visible && btnRefresh.Enabled && Visible) {
                RefreshRecords();
            }
        }

        //Event handler for MARK FINAL command.
        private void btnMarkFinal_Click(object sender, EventArgs e) {
            if (btnMarkFinal.Visible && btnMarkFinal.Enabled && Visible) {
                if (OnFinalize != null) OnFinalize(sender, e);
            }
        }

        //Event handler for SEARCH command.
        private void btnSearch_Click(object sender, EventArgs e) {
            if (btnSearch.Visible && btnSearch.Enabled) {
                if (txtSearch.Visible) {
                    //Perform search if search box is visible.
                    if (txtSearch.Text.Length > 0) {
                        if (OnSearch != null) OnSearch(sender, e);
                        txtSearch.Focus();
                        txtSearch.SelectAll();
                        UpdateWindowStatus("Showing search results for: \"" + txtSearch.Text + "\"");
                    } else if(txtSearch.Text.Length <= 0)
                        HideSearchbox();
                } else {
                    //Make the search box visible on trigger search.
                    ShowSearchbox();
                    txtSearch.Focus();
                    txtSearch.SelectAll();
                }
            }
        }

        //Event handler for REFRESH command with search query reset.
        private void resetSearchToolStripMenuItem_Click(object sender, EventArgs e) {
            if (Visible && btnRefresh.Enabled && btnRefresh.Visible) {
                txtSearch.Text = "";
                HideSearchbox();
            }
        }

        //Eventhandler for text change of search textbox.
        private void txtSearch_TextChanged(object sender, EventArgs e) {
            if (OnSearchTextChanged != null) OnSearchTextChanged(sender, e);
            if (txtSearch.Text != "") {
                UpdateWindowStatus("Press [Enter] to search for: \"" + txtSearch.Text + "\"");
            } else {
                UpdateWindowStatus("Type to search or press [Enter] to cancel...");
            }
        }

        //Eventhandler when user enters the search textbox.
        private void txtSearch_Enter(object sender, EventArgs e) {
            if (txtSearch.Focused) {
                if (txtSearch.Text != "") {
                    UpdateWindowStatus("Press [Enter] to search for: \"" + txtSearch.Text + "\"");
                } else {
                    UpdateWindowStatus("Type to search or press [Enter] to cancel...");
                }
            }
        }

        //Eventhandler for search shortcut key.
        private void searchToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnSearch.Visible && btnSearch.Enabled && Visible) {
                ShowSearchbox();
                txtSearch.Focus();
            }
        }

        //Search textbox Keydown.
        private void txtSearch_KeyDown(object sender, KeyEventArgs e) {
            //Do:
            //Search on Enter
            //Cancel search on ESC
            //Clear all search text on CTRL + BACK
            if (e.KeyCode == Keys.Enter) {
                btnSearch_Click(btnSearch, new EventArgs());
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            if (e.KeyCode == Keys.Escape) {
                HideSearchbox();
            }
            if (e.KeyCode == Keys.Back && e.Modifiers == Keys.Control) {
                txtSearch.Text = "";
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        //Restore button event handler.
        private void btnRestore_Click(object sender, EventArgs e) {
            if (btnRestore.Visible && btnRestore.Enabled && Visible) {
                if (OnRestore != null) OnRestore(sender, e);
            }
        }

        #endregion

        #region Filters Eventhandlers

        //Refresh records based on selected date range.
        private void btnSet_Click(object sender, EventArgs e) {
            if (btnSet.Visible && btnSet.Enabled) {
                if ((VisibleFilterControls & FilterControls.RangeDatePeriod) == FilterControls.RangeDatePeriod) RefreshRecords();
            }
        }
        
        //Eventhandler when SHOW FINAL checkbox check changed.
        private void chkShowFinal_CheckedChanged(object sender, EventArgs e) {
            //if(Visible) RefreshRecords();
            if (OnShowFinalizeChecked != null) OnShowFinalizeChecked(sender, e);
        }

        //Eventhandler when the Start DatePicker changed values.
        private void dpStart_ValueChanged(object sender, EventArgs e) {
            //Do:
            //Prevent START/FROM Date to surpass END/TO Date.
            if (Visible) {
                if (dpEnd.Value < dpStart.Value) {
                    if (dpEnd.Visible) {
                        Notifier.ShowWarning("Start date cannot be later than end date.", "Invalid Period");
                        dpStart.Value = dpEnd.Value;
                        return;
                    }
                }

                if ((VisibleFilterControls & FilterControls.RangeDatePeriod) != FilterControls.RangeDatePeriod)
                    RefreshRecords();

                if (dpStart.Visible && dpEnd.Visible)
                    GlobalVariables.StartDate = DateTime.Parse(dpStart.Value.ToString());
            }
        }

        //Eventhandler when the End DatePicker changed values.
        private void dpEnd_ValueChanged(object sender, EventArgs e) {
            //Do:
            //Prevent the END/TO date be set earlier than START/FROM date.
            //Refresh records when date is updated.
            if (Visible) {
                if (dpEnd.Value < dpStart.Value) {
                    Notifier.ShowWarning("End date cannot be earlier than start date.", "Invalid Period");
                    dpEnd.Value = dpStart.Value;
                } else {
                    if ((VisibleFilterControls & FilterControls.RangeDatePeriod) != FilterControls.RangeDatePeriod)
                    RefreshRecords();
                    if (dpStart.Visible && dpEnd.Visible)
                        GlobalVariables.EndDate = DateTime.Parse(dpEnd.Text);
                }
            }
        }

        #endregion

        #region TotalsDataGrid Eventhandlers

        #endregion

        #region RecordsDataGrid Eventhandlers

        //Remembers the selected record when datagridview is sorted.
        private void RecordsDataGrid_Sorted(object sender, EventArgs e) {}

        //Refresh selections when databinding completes (i.e. sorted)
        private void RecordsDataGrid_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            if (RecordsDataGrid.Columns.Contains("_INTERNALID_")) {
                foreach (DataGridViewRow r in RecordsDataGrid.Rows) {
                    if (!selectedRecords.Trim().Equals("") && r.Cells["_INTERNALID_"].Value.ToString().Equals(selectedRecords)) {
                        r.Selected = true;
                    }
                }
            }
        }

        //Eventhandler when the Records is Formatting cells.
        private void RecordsDataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e) {
            if (OnCellFormatting != null) OnCellFormatting(sender, e);
        }

        //Eventhandler when user clicks on the Records DataGridView.
        private void RecordsDataGrid_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == System.Windows.Forms.MouseButtons.Left) {
                if (RecordsDataGrid.HitTest(e.X, e.Y) == DataGridView.HitTestInfo.Nowhere) {
                    RecordsDataGrid.ClearSelection();
                    RecordsDataGrid.CurrentCell = null;
                }
            }
        }

        //Eventhandler when selected records are changed.
        private void RecordsDataGrid_SelectionChanged(object sender, EventArgs e) {
            SetButtonToolbarVisiblityOnRecordSelect();
            ShowSelectedRecordsCount();
            if (OnRecordsSelectionChanged != null) OnRecordsSelectionChanged(sender, e);
            if (RecordsDataGrid.Columns.Contains("_INTERNALID_")) {
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    selectedRecords = RecordsDataGrid.SelectedRows[0].Cells["_INTERNALID_"].Value.ToString();
                }
            }
        }

        //Eventhandler when user double-clicks on a record.
        private void RecordsDataGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e) {
            DataGridView.HitTestInfo htt = RecordsDataGrid.HitTest(e.X, e.Y);
            if(htt != DataGridView.HitTestInfo.Nowhere && e.RowIndex >= 0) ViewSelectedItem();
        }

        //Eventhandler when user presses a key while Records DataGridView is in focus.
        private void RecordsDataGrid_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter && RecordsDataGrid.IsCurrentCellInEditMode == false) {
                ViewSelectedItem();
                e.SuppressKeyPress = true;
            }
        }

        //Eventhandler when new rows are added on the Records DataGridView.
        private void RecordsDataGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) {
            ShowTotalRecordsCount();
            
        }

        #endregion

        #region status toolstip

        //Eventhandler when user hovers on each toolstrip button.
        private void ToolStripButtons_MouseHover(object sender, EventArgs e) {
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        if (sender == btnAdd) {
                            wnd.UpdateWindowStatus("Adds a new record.");
                        } else if (sender == btnEdit) {
                            wnd.UpdateWindowStatus("Updates the currently selected record(s).");
                        } else if (sender == btnDelete) {
                            wnd.UpdateWindowStatus("Permanently removes the currently selected record(s). Proceed with caution.");
                        } else if (sender == btnArchive) {
                            wnd.UpdateWindowStatus("Archives the selected record(s) in which, if needed, can be restored in later time.");
                        } else if (sender == btnExport) {
                            wnd.UpdateWindowStatus("Exports the current data into a formatted spreadsheet document.");
                        } else if (sender == btnMarkFinal) {
                            wnd.UpdateWindowStatus("Locks the currently selected record(s) from further modifications.");
                        } else if (sender == btnRefresh) {
                            wnd.UpdateWindowStatus("Reloads data from the database.");
                        } else if (sender == btnSearch) {
                            wnd.UpdateWindowStatus("Search records based on specified criteria.");
                        }
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        //Eventhandler when user leaves mouse on each toolstip button.
        private void ToolStripButtons_MouseLeave(object sender, EventArgs e) {
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateWindowStatus("Ready");
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        #endregion

        #region Sections Pane Eventhandlers

        //Eventhandler for the section pane add button.
        private void btnSectionAdd_Click(object sender, EventArgs e) {
            if (btnSectionAdd.Visible && btnSectionAdd.Enabled) {
                if (OnSectionAdd != null) OnSectionAdd(sender, e);
            }
        }

        //Eventhandler for the sectin pane delete button.
        private void btnSectionDelete_Click(object sender, EventArgs e) {
            if (btnSectionDelete.Visible && btnSectionDelete.Enabled) {
                if (OnSectionDelete != null) OnSectionDelete(sender, e);
            }
        }

        #endregion

    }

}
