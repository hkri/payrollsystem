﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Sql;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;

namespace PayrollSystem {

    internal static class DatabaseManager {

        //External Database internal version.
        //Note: This should be use to check integrity of the database if compatible
        //      with this payroll system.
        //      Access Database Table: _DB_DATA
        //      Access Database Col:   VER
        public static string EXDB_INT_VER = "DB_003";

        static string CEDB_CONN_STRING = "";
        static string EXDB_CONN_STRING = "";

        internal static SqlCeConnection CEDB_CONN = null;
        internal static OleDbConnection EXDB_CONN = null;

        internal static OleDBDatabase MainDatabase = null;
        internal static CEDatabase LocalDatabase = null;

        internal static string EXDB_FILEPATH = "";
        internal static string EXDB_FILENAME = "";

        internal static OleDBDatabase GetMainDatabase() {
            return MainDatabase;
        }

        internal static CEDatabase GetLocalDatabase() {
            return LocalDatabase;
        }

        internal static void ShutdownExternalDatabase() {
            if (EXDB_CONN != null) {
                if (EXDB_CONN.State != ConnectionState.Closed) {
                    try {
                        EXDB_CONN.Close();
                    } catch (Exception ex) {
                        Trace.WriteLine("DatabaseManager Error: " + ex);
                    }
                }
                if (EXDB_CONN.State == ConnectionState.Closed) {
                    EXDB_CONN.Dispose();
                    EXDB_CONN = null;
                }
            }
        }

        internal static bool CheckExternalDBConnection() {
            if (EXDB_CONN == null) return false;
            else {
                if (EXDB_CONN.State == ConnectionState.Open) return true;
                else return false;
            }
        }

        internal static bool ExportDatabase(string dest) {
            bool success = false;
            string DBNAME = Properties.Settings.Default.ActiveDatabase;
            string filepath = GlobalVariables.DB_DIR + "\\" + DBNAME;
            try {
                File.Copy(filepath, dest, true);
                success = true;
            } catch (Exception ex) {
                Notifier.ShowError("The system encountered an error while exporting the database.");
                Trace.WriteLine("DatabaseManager error: " + ex.Message);
            }
            return success;
        }

        internal static bool BackupDatabase() {
            bool success = false;
            string BACKUP_PATH = Properties.Settings.Default.BackupPath;
            string DBNAME = Properties.Settings.Default.ActiveDatabase;
            if (Directory.Exists(BACKUP_PATH) == false) {
                try {
                    Directory.CreateDirectory(BACKUP_PATH);
                } catch (Exception ex) {
                    Trace.WriteLine("DatabaseManager error: " + ex.Message);
                    return false;
                }
            }
            string filepath = GlobalVariables.DB_DIR + "\\" + DBNAME;
            string dest = BACKUP_PATH + "\\" + DBNAME;
            try {
                File.Copy(filepath, dest, true);
                success = true;
            } catch (Exception ex) {
                Notifier.ShowError("The system encountered an error while making backup of the database.");
                Trace.WriteLine("DatabaseManager error: " + ex.Message);
            }
            return success;
        }

        internal static void CopyDatabaseToSystemDirectory(string filepath) {
            string filename = Path.GetFileName(filepath);
            string dest = GlobalVariables.DB_DIR + "\\" + filename;
            if (Path.GetDirectoryName(filepath).Equals(GlobalVariables.DB_DIR, StringComparison.CurrentCultureIgnoreCase))
                return;
            if (File.Exists(dest)) {
                FileInfo inf1 = new FileInfo(filepath);
                FileInfo inf2 = new FileInfo(dest);
                string date1 = inf1.LastWriteTime.ToString("MM/dd/yyyy HH:mm");
                string date2 = inf2.LastWriteTime.ToString("MM/dd/yyyy HH:mm");
                if (Notifier.ShowConfirm("Database with the same filename already exists.\n\nBefore overwriting, make sure the selected file is newer that the file to be overwritten.\n\nSource: " + filepath + "\n(" + date1 + ")\n\nDestination: " + dest + "\n(" + date2  + ")\n\nDo you wish to proceed?", "Database Exists") == System.Windows.Forms.DialogResult.No) {
                    return;
                }
            }
            try {
                Trace.WriteLine("Src: " + filepath);
                Trace.WriteLine("Dest: " + filepath);
                File.Copy(filepath, dest, true);
            } catch (Exception ex) {
                Notifier.ShowError("The system encountered an error while moving the database file to the working directory.");
                Trace.WriteLine("DatabaseManager error: " + ex.Message);
            }
        }

        internal static void BuildExternalDBConnectionString() {
            //Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\myFolder\myAccessFile.accdb;Jet OLEDB:Database Password=MyDbPassword;
            //Before building connection string ,test if database file exists.
            EXDB_FILENAME = Properties.Settings.Default.ActiveDatabase;
            EXDB_FILEPATH = GlobalVariables.DB_DIR + "\\" + EXDB_FILENAME;
            if (File.Exists(EXDB_FILEPATH)) {
                string provider = "Provider=Microsoft.ACE.OLEDB.12.0;";
                string data_source = "Data Source=" + EXDB_FILEPATH + ";";
                string password = "Jet OLEDB:Database Password=5$Z1anTruck1ng;";
                EXDB_CONN_STRING = provider + data_source + password;
            } else {
                Trace.WriteLine("DatabaseManager: " + EXDB_FILEPATH + " - Database file not found.");
                EXDB_CONN_STRING = "";
            }
        }

        internal static void BuildLocalDBConnectionString(){
            //Data Source=MyData.sdf;Persist Security Info=False;
            string data_source = "Data Source=database.sdf;";
            string sec_info = "Persist Security Info=False;";
            CEDB_CONN_STRING = data_source + sec_info;
        }

        internal static bool CreateExternalDBConnection() {
            try {
                BuildExternalDBConnectionString();
                ShutdownExternalDatabase(); //Shutdown existing database, if active.
                EXDB_CONN = new OleDbConnection(EXDB_CONN_STRING);
                EXDB_CONN.Open();
                MainDatabase = new OleDBDatabase(EXDB_CONN);
                return true;
            } catch (Exception ex) {
                EXDB_CONN = null;
                if (EXDB_FILENAME.Trim() != "")
                    Notifier.ShowError("The system failed to access the specified payroll system database:\n\n" + EXDB_FILEPATH + "\n\nIt may be because of the following reason(s):\n\n> Database not found\n> File is not accessible\n> Database is corrupted\n\nPlease try to reconnect the system to the database, or load a new database via Database menu.");
                else
                    Notifier.ShowError("Please load or create a new valid 5SZian Payroll System database to begin.");
                Trace.WriteLine("External DB Connection: " + ex.Message);
                return false;
            }
        }

        internal static bool CreateLocalDBConnection() {
            try {
                CEDB_CONN = new SqlCeConnection(CEDB_CONN_STRING);
                CEDB_CONN.Open();
                LocalDatabase = new CEDatabase(CEDB_CONN);
                return true;
            } catch (Exception ex) {
                CEDB_CONN = null;
                Notifier.ShowError("The app experienced an internal error. Please contact a system administrator.\n\nError: application internal database inaccessible.");
                Trace.WriteLine("Local DB Connection:" + ex.Message);
                return false;
            }
        }
        
    }

}
