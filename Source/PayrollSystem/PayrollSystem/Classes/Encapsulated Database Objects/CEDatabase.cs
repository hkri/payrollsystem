﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Data.SqlServerCe;

namespace PayrollSystem {

    public class CEDatabase : IDisposable {

        #region Variables

        SqlCeConnection conn = null;
        Interfaces.IDatabaseStatusUpdater updater = null;

        #endregion

        #region Constructor

        public CEDatabase(SqlCeConnection connection, Interfaces.IDatabaseStatusUpdater host = null) {
            conn = connection;
            if (host != null) {
                updater = host;
            }
        }

        #endregion

        #region Destructor

        public void Dispose() {
            if (conn != null) {
                if (conn.State != ConnectionState.Closed) conn.Close();
                conn.Dispose();
            }
        }

        #endregion

        #region Public Database Status Functions

        public bool Open() {
            try {
                if (conn.State != ConnectionState.Open) {
                    conn.Open();
                    if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(true);
                    return true;
                }
                if (conn.State == ConnectionState.Open) return true;
            } catch (Exception ex) {
                Trace.WriteLine("CEDatabaseError: " + ex.Message);
                if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(false);
            }
            return false;
        }

        public void Close() {
            try {
                if (conn.State != ConnectionState.Closed) {
                    conn.Close();
                    if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(false);
                }
            } catch (Exception ex) {
                Trace.WriteLine("CEDatabaseError: " + ex.Message);
            }
        }

        #endregion

        #region Public Querying Functions

        public DataTable ExecuteQuery(string statement, bool isStoreProc, SqlCeParameter[] parameters = null) {
            DataTable dt = new DataTable();
            if (conn != null) {
                try {
                    Open();
                    using (SqlCeCommand cmd = new SqlCeCommand(statement, conn)) {
                        if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                        else cmd.CommandType = CommandType.Text;
                        if (parameters != null) {
                            foreach (SqlCeParameter o in parameters)
                                cmd.Parameters.Add(o);
                        }
                        using (SqlCeDataAdapter adpt = new SqlCeDataAdapter(cmd)) {
                            cmd.ExecuteNonQuery();
                            adpt.Fill(dt);
                        }
                    }
                } catch (Exception ex) {
                    Trace.WriteLine("CEDatabaseError: " + ex.Message);
                }
            }
            return dt;
        }

        public bool ExecuteNonQuery(string statement, bool isStoreProc, SqlCeParameter[] parameters = null) {
            bool success = false;
            if (conn != null) {
                if (Open()) {
                    try {
                        using (SqlCeCommand cmd = new SqlCeCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            if (parameters != null) {
                                foreach (SqlCeParameter o in parameters) {
                                    cmd.Parameters.Add(o);
                                }
                            }
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("CEDatabaseError: " + ex.Message);
                    }
                }
            }
            return success;
        }

        #endregion

    }

}
