﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Data.OleDb;

namespace PayrollSystem {

    public class OleDBDatabase : IDisposable {

        #region Variables

        OleDbConnection conn = null;
        Interfaces.IDatabaseStatusUpdater updater = null;

        #endregion

        #region Constructor

        public OleDBDatabase(OleDbConnection connection, Interfaces.IDatabaseStatusUpdater host = null) {
            conn = connection;
            if (host != null) updater = host;
            Open();
        }

        #endregion

        #region Destructor

        public void Dispose() {
            if (conn != null) {
                if (conn.State != ConnectionState.Closed) conn.Close();
                conn.Dispose();
            }
        }

        #endregion

        #region Public Database Status Functions

        public bool Open() {
            try {
                if (conn.State != ConnectionState.Open) {
                    conn.Open();
                    if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(true);
                    return true;
                }
                if (conn.State == ConnectionState.Open) return true;
            } catch (Exception ex) {
                Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(false);
            }
            return false;
        }

        public void Close() {
            try {
                if (conn.State != ConnectionState.Closed) {
                    conn.Close();
                    if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(false);
                }
            } catch (Exception ex) {
                Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                if (conn.State == ConnectionState.Open) updater.UpdateDBConnectivityStatus(false);
            }
        }

        #endregion

        #region Public Querying Functions

        //write data to exsting DataTable
        public void ExecuteQuery(string statement, bool isStoreProc, DataTable dt, OleDbParameter[] parameters = null) {
            if (conn != null) {
                if (Open()) {
                    try {
                        using (OleDbCommand cmd = new OleDbCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            if (parameters != null) {
                                foreach (OleDbParameter o in parameters)
                                    cmd.Parameters.Add(o);
                            }
                            using (OleDbDataAdapter adpt = new OleDbDataAdapter(cmd)) {
                                cmd.ExecuteNonQuery();
                                adpt.Fill(dt);
                            }
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                    }
                }
            }
        }

        public DataTable ExecuteQuery(string statement, bool isStoreProc, OleDbParameter[] parameters = null) {
            DataTable dt = new DataTable();
            if (conn != null) {
                if (Open()) {
                    try {
                        using (OleDbCommand cmd = new OleDbCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            if (parameters != null) {
                                foreach (OleDbParameter o in parameters)
                                    cmd.Parameters.Add(o);
                            }
                            using (OleDbDataAdapter adpt = new OleDbDataAdapter(cmd)) {
                                cmd.ExecuteNonQuery();
                                adpt.Fill(dt);
                            }
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                    }
                }
            }
            return dt;
        }
        
        public bool ExecuteQueryFillSource(DataTable datasource, string statement, bool isStoreProc, OleDbParameter[] parameters = null) {
            if (conn != null) {
                if (Open()) {
                    try {
                        using (OleDbCommand cmd = new OleDbCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            if (parameters != null) {
                                foreach (OleDbParameter o in parameters)
                                    cmd.Parameters.Add(o);
                            }
                            using (OleDbDataAdapter adpt = new OleDbDataAdapter(cmd)) {
                                cmd.ExecuteNonQuery();
                                adpt.Fill(datasource);
                            }
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool ExecuteNonQuery(string statement, bool isStoreProc, OleDbParameter[] parameters = null) {
            bool success = false;
            if (conn != null) {
                if (Open()) {
                    try {
                        using (OleDbCommand cmd = new OleDbCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            if (parameters != null) {
                                foreach (OleDbParameter o in parameters)
                                    cmd.Parameters.Add(o);
                            }
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                    }
                }
            }
            return success;
        }
        
        public bool ExecuteNonQueryTransaction(string statement, bool isStoreProc, OleDbTransaction transaction, OleDbParameter[] parameters = null) {
            bool success = false;
            if (conn != null) {
                if (Open()) {
                    try {
                        using (OleDbCommand cmd = new OleDbCommand(statement, conn)) {
                            if (isStoreProc) cmd.CommandType = CommandType.StoredProcedure;
                            else cmd.CommandType = CommandType.Text;
                            cmd.Transaction = transaction;
                            if (parameters != null) {
                                foreach (OleDbParameter o in parameters)
                                    cmd.Parameters.Add(o);
                            }
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                    } catch (Exception ex) {
                        Trace.WriteLine("OleDBDatabase Error: " + ex.Message);
                    }
                }
            }
            return success;
        }

        #endregion

    }

}
