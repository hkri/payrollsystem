﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    public class PasswordEncryptor {

        #region variables

        //Store array
        byte[] store = new byte[16];
        byte[] shift = new byte[16];

        //Constants
        byte A = 0, B = 0, C = 0, D = 0;

        #endregion

        #region constructor

        public PasswordEncryptor() {
            Initialize();
        }

        #endregion

        #region public function

        void Initialize() {
            //initial store values.
            for (int i = 0; i < store.Length; i++) {
                store[i] = (byte)(Math.Pow(2, 10) * Math.Abs(Math.Cos(i + 1)));
            }
            //shifts
            shift[0] = 4; shift[1] = 3; shift[2] = 7; shift[3] = 2;
            shift[4] = 3; shift[5] = 1; shift[6] = 9; shift[7] = 4;
            shift[8] = 6; shift[9] = 3; shift[10] = 9; shift[11] = 1;
            shift[12] = 6; shift[13] = 3; shift[14] = 7; shift[15] = 2;
        }

        byte shiftbits(byte input, byte c) {
            return (byte)((byte)(input << c) | (byte)(input >> (8 - c)));
        }

        public string EncryptString(string input) {
            string output = "";
            A = 183;
            B = 13;
            C = 97;
            D = 134;
            byte f = 0;
            foreach (char c in input) {
                for (int i = 0; i < store.Length; i++) {
                    if (i >= 0 && i <= 3) {
                        f = (byte)((A & D) | (~C & B));
                    } else if (i >= 4 && i <= 7) {
                        f = (byte)((B & C) | (~B & A));
                    } else if (i >= 8 && i <= 11) {
                        f = (byte)((~D) | (A & C));
                    } else if (i >= 12 && i <= 15) {
                        f = (byte)(~C | (B & D));
                    }
                    byte tmp = D;
                    D = C; C = B;
                    B = (byte)(B + shiftbits((byte)(f + (byte)(c) + store[i]), shift[i]));
                    A = tmp;
                    store[i] = B;
                }
            }
            foreach (byte b in store) {
                output += b.ToString("X2");
            }
            return output.ToLower();
        }

        #endregion

    }

}
