﻿using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    public static class Extensions {

        /// <summary>
        /// Extension method for double-buffering datagrids.
        /// </summary>
        /// <param name="dgv">DataGridView to target.</param>
        /// <param name="setting">Sets the double buffering of the datagridview.</param>
        public static void DGVDoubleBuffered(this DataGridView dgv, bool setting) {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                  BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }

    }

}
