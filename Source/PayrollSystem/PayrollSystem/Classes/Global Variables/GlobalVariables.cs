﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    internal static class GlobalVariables {

        #region static variables for global use

        /* File name for the error log file */
        internal static string LOGFILE = "errors.log";

        /* Condition to silent quit confirm dialogs */
        internal static bool FORCE_SHUTDOWN = false;

        /* Light render (toolbars) */
        internal static LightToolstripRenderer renderer = new LightToolstripRenderer();

        /* Directory of system databases. */
        internal static string DB_DIR = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\5SZian Payroll System\Databases";

        /* Working start and end dates */
        internal static DateTime StartDate = DateTime.Now.AddDays(-30);
        internal static DateTime EndDate = DateTime.Now;

        internal static void UpdateWorkingDates(string dstart, string dend) {
            StartDate = DateTime.Parse(dstart);
            EndDate = DateTime.Parse(dend);
        }

        /* Remember some settings in some pages for better user experience. */
        internal static bool CREPage_SummaryHeaderExpanded = true;  //Summary header collapsed/expanded flag.
        internal static DateTime PayrollPage_Date = DateTime.Now;   //Remembers the date filter on the payroll browser page.
        internal static DateTime CRPage_Date = DateTime.Now;        //Remembers the date filter on the cash report browser page.

        #endregion

    }

}
