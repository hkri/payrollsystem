﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem {

    internal static class UserManager {

        internal static string USERNAME = "";

        internal static string USERID = "";

        internal static string ACCOUNT_NAME = "";

        internal static int ACCOUNT_TYPE = 0;

        internal static void SetActiveUser(string uid, string un, string aname, int type) {
            USERNAME = un;
            USERID = uid;
            ACCOUNT_NAME = aname;
            ACCOUNT_TYPE = type;
        }

    }

}
