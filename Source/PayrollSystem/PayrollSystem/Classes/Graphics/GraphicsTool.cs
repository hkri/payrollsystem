﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

namespace PayrollSystem {

    public class GraphicsTool {

        public static GraphicsPath CreateRoundedRect(int width, int height, int border_radius) {
            GraphicsPath gp = new GraphicsPath();
            int r = border_radius, d = r * 2;
            gp.AddLine(r, 0, width - r, 0);
            gp.AddArc(new Rectangle(width - d, 0, d, d), 270, 90);
            gp.AddLine(width, r, width, height - r);
            gp.AddArc(new Rectangle(width - d, height - d, d, d), 0, 90);
            gp.AddLine(width - r, height, r, height);
            gp.AddArc(new Rectangle(0, height - d, d, d), 90, 90);
            gp.AddLine(0, height - r, 0, r);
            gp.AddArc(new Rectangle(0, 0, d, d), 180, 90);
            gp.CloseFigure();
            return gp;
        }

    }

}
