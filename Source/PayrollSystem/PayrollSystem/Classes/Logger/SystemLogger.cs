﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
using System.Diagnostics;

namespace PayrollSystem {

    internal static class SystemLogger {

        //Creates a log to the database.
        public static void WriteLog(CEDatabase db, string user, string message) {
            SqlCeParameter[] param = new SqlCeParameter[3];
            param[0] = new SqlCeParameter("@usr", user);
            param[1] = new SqlCeParameter("@dt", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss"));
            param[1].DbType = System.Data.DbType.DateTime;
            param[2] = new SqlCeParameter("@msg", message);
            db.ExecuteNonQuery(QueryStrings.LocalDatabaseTables.SystemLog.Add_SystemLog, false, param);
        }

        //Creates a log to the global variable "LOGFILE".
        public static void WriteLog(string log) {
            try {
                using (FileStream fs = new FileStream(GlobalVariables.LOGFILE, FileMode.Append)) {
                    using (StreamWriter wt = new StreamWriter(fs)) {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("[").Append(DateTime.Now.ToString("MM/dd/yy HH:mm:ss")).Append("]: ");
                        sb.Append(log);
                        wt.WriteLine(sb.ToString());
                        wt.Flush();
                        wt.Close();
                    }
                    fs.Close();
                }
            } catch (Exception ex) {
                Debug.WriteLine(ex.Message);
                Trace.WriteLine(ex.Message);
            }
        }

    }

}
