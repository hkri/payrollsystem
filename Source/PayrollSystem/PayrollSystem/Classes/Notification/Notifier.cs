﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollSystem {

    public static class Notifier {

        #region Mesagebox

        public static DialogResult ShowConfirm(string message, string title, bool canCancel = false) {
            if(!canCancel)
                return MessageBox.Show(message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            else
                return MessageBox.Show(message, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
        }

        public static DialogResult ShowWarning(string message, string title) {
            return MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static DialogResult ShowInformation(string message, string title) {
            return MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult ShowError(string message) {
            return MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

    }

}
