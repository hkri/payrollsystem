﻿/*
 * This class contains all SQL queries required to get/set data
 * from/to the in-proccess database (SQL CE).
 */

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem.QueryStrings {

    /* SQL CE Queries for Local Store Database (database.mdf) */
    public static class LocalDatabaseTables {

        /* Users table quries */
        public static class Users {
            public static string Query_Users_Login = "SELECT ID, Username, Password, AccountName AS [Account Name], Type FROM Users WHERE Username=@un";
            public static string Query_Users = "SELECT A.ID AS [ID], Username, Password, AccountName AS [Account Name], B.TypeName AS [Type] FROM Users A INNER JOIN AccountType B ON A.Type=B.ID";
            public static string Add_Users = "INSERT INTO Users(Username, Password, AccountName, Type) VALUES(@un, @pw, @an, @typ)";
            public static string Edit_Users = "UPDATE Users SET Username = @un, Password = @pw, AccountName = @an, Type = @typ WHERE ID = @id";
            public static string Delete_Users = "DELETE FROM Users WHERE ID = @id";
        }

        /* System Log */
        public static class SystemLog {
            public static string Query_SystemLog = "SELECT ID, User, DateTime AS [Timestamp], Message FROM SystemLog";
            public static string Add_SystemLog = "INSERT INTO SystemLog([User], [DateTime], [Message]) VALUES(@usr, @dt, @msg)";
        }

        /* Database Required Objects Queries */
        public static class DatabaseRequiredObjects {
            public static string Query_DatabaseRequiredObjects = "SELECT ObjectName FROM DatabaseRequiredObjects";
        }

    }

}
