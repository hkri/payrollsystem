﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PayrollSystem {

    public static class InputValidators {

        #region public static functions

        public static double DBNullToDbl(object value) {
            if (value.Equals(DBNull.Value)) return 0.0;
            return (double)value;
        }

        public static string DBNullToStr(object value) {
            if (value.Equals(DBNull.Value)) return String.Empty;
            return value.ToString();
        }

        public static string EscapeSquareBrackets(string text) {
            return text.Replace("[", "[[]");
        }

        public static bool IsEmpty(string text) {
            return (text.Length <= 0) ? true : false;
        }

        public static bool IsEmptyTrimmed(string text) {
            return (text.Trim().Length <= 0) ? true : false;
        }

        public static bool ContainsSpaces(string text) {
            return (text.Contains(" ")) ? true : false;
        }

        public static bool IsNumericWithDecimal(string text) {
            double s = 0.0;
            return Double.TryParse(text, out s);
        }

        public static bool IsNumericIntegral(string text) {
            int s = 0;
            return Int32.TryParse(text, out s);
        }

        public static bool ContainsAlphabet(string text) {
            bool containsAlpha = false;
            char[] special_symbols = {   '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                                         '`', '~', '!', '@', '#', '$', '%', '^', 
                                         '&', '*', '(', ')', '-', '_', '=', '+', 
                                         '[', '{', ']', '}', '\\', '|', ';', ':', 
                                         '\'', '"', ',', '<', '.', '>', '/', '?' };
            foreach (char c in text.Trim()) {
                if (special_symbols.Contains(c) == false) {
                    containsAlpha = true;
                    break;
                }
            }
            return containsAlpha;
        }

        #endregion

    }

}
