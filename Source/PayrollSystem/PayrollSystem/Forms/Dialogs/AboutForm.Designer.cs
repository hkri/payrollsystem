﻿namespace PayrollSystem {
    partial class AboutForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.tmrScroller = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.ContentPanel = new PayrollSystem.BufferedPanel();
            this.lblAboutInfo = new System.Windows.Forms.Label();
            this.LogoPanel = new PayrollSystem.BufferedPanel();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrScroller
            // 
            this.tmrScroller.Enabled = true;
            this.tmrScroller.Interval = 16;
            this.tmrScroller.Tick += new System.EventHandler(this.tmrScroller_Tick);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label1.Location = new System.Drawing.Point(179, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "5SZian Trucking Services";
            // 
            // ContentPanel
            // 
            this.ContentPanel.BorderColor = System.Drawing.Color.Black;
            this.ContentPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.ContentPanel.Controls.Add(this.lblAboutInfo);
            this.ContentPanel.Location = new System.Drawing.Point(179, 78);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(284, 91);
            this.ContentPanel.TabIndex = 8;
            this.ContentPanel.Click += new System.EventHandler(this.lblAboutInfo_Click);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAboutInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.lblAboutInfo.Location = new System.Drawing.Point(5, 94);
            this.lblAboutInfo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblAboutInfo.Name = "lblAboutInfo";
            this.lblAboutInfo.Size = new System.Drawing.Size(274, 256);
            this.lblAboutInfo.TabIndex = 9;
            this.lblAboutInfo.Text = resources.GetString("lblAboutInfo.Text");
            this.lblAboutInfo.Click += new System.EventHandler(this.lblAboutInfo_Click);
            // 
            // LogoPanel
            // 
            this.LogoPanel.BackgroundImage = global::PayrollSystem.Properties.Resources.About_Logo;
            this.LogoPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LogoPanel.BorderColor = System.Drawing.Color.Black;
            this.LogoPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.LogoPanel.Location = new System.Drawing.Point(1, 52);
            this.LogoPanel.Name = "LogoPanel";
            this.LogoPanel.Size = new System.Drawing.Size(158, 106);
            this.LogoPanel.TabIndex = 0;
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(475, 210);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.LogoPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About This App";
            this.Click += new System.EventHandler(this.AboutForm_Click);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.AboutForm_PreviewKeyDown);
            this.ContentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BufferedPanel LogoPanel;
        private System.Windows.Forms.Timer tmrScroller;
        private BufferedPanel ContentPanel;
        private System.Windows.Forms.Label lblAboutInfo;
        private System.Windows.Forms.Label label1;
    }
}