﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace PayrollSystem {
    public partial class AboutForm : Form {

        #region Win32 Pinvoke

        [DllImport("user32.dll")]
        private static extern int HideCaret(IntPtr hwnd);

        #endregion

        #region constructor

        public AboutForm() {
            InitializeComponent();

            /*
            foreach (Control c in Controls) {
                c.Click += AboutForm_Click;
            }
            */

            lblAboutInfo.Text = lblAboutInfo.Text.Replace(@"{VERSION}", "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            ytop = lblAboutInfo.Top;
        }

        #endregion

        #region Window eventhandlers

        private void AboutForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) {
            if (e.KeyCode == Keys.Escape) {
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }

        private void AboutForm_Click(object sender, EventArgs e) {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        #endregion

        #region EventHandlers for controls

        //Enable or disable scroller timer.
        private void lblAboutInfo_Click(object sender, EventArgs e) {
            tmrScroller.Enabled = !tmrScroller.Enabled;
        }

        //Handler scroller timer event.
        float ytop = 0;
        private void tmrScroller_Tick(object sender, EventArgs e) {
            //lblAboutInfo.Top -= 1;
            ytop -= 0.50f;
            lblAboutInfo.Top = (int)ytop;
            if (lblAboutInfo.Bottom < 0) {
                lblAboutInfo.Top = ContentPanel.Height;
                ytop = lblAboutInfo.Top;
            }
            //HideCaret(rtbAbout.Handle);
        }

        #endregion

    }
}
