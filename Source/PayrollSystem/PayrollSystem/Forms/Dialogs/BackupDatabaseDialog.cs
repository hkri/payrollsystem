﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class BackupDatabaseDialog : FileMaintenanceDialog {

        #region Controls  
      
        private LinkLabel lnkMoreInfo;
        private Label label6;
        private BufferedPanel InfoPanel;
        private Label label1;
        private Label label2;
        private Button btnBrowse;
        internal TextBox txtBackupPath;
        private Label label4;
        private Label label5;
        private Label label3;

        #endregion

        #region Constructor

        public BackupDatabaseDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.lnkMoreInfo = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.InfoPanel = new PayrollSystem.BufferedPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtBackupPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.InfoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.lnkMoreInfo);
            this.ContentPanel.Controls.Add(this.btnBrowse);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.txtBackupPath);
            this.ContentPanel.Controls.Add(this.InfoPanel);
            this.ContentPanel.Size = new System.Drawing.Size(457, 273);
            this.ContentPanel.TabIndex = 0;
            // 
            // lnkMoreInfo
            // 
            this.lnkMoreInfo.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lnkMoreInfo.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.Location = new System.Drawing.Point(357, 14);
            this.lnkMoreInfo.Name = "lnkMoreInfo";
            this.lnkMoreInfo.Size = new System.Drawing.Size(88, 24);
            this.lnkMoreInfo.TabIndex = 2;
            this.lnkMoreInfo.TabStop = true;
            this.lnkMoreInfo.Text = "Show Info";
            this.lnkMoreInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lnkMoreInfo.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkMoreInfo_LinkClicked);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label6.Location = new System.Drawing.Point(12, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(433, 24);
            this.label6.TabIndex = 13;
            this.label6.Text = "About Database Backups";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InfoPanel
            // 
            this.InfoPanel.Controls.Add(this.label1);
            this.InfoPanel.Controls.Add(this.label2);
            this.InfoPanel.Controls.Add(this.label3);
            this.InfoPanel.Location = new System.Drawing.Point(12, 44);
            this.InfoPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(433, 121);
            this.InfoPanel.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "It is important to periodically backup your database.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(427, 45);
            this.label2.TabIndex = 2;
            this.label2.Text = "The system\'s database can also be corrupted under unfortunate circumstances such " +
    "as hardware failure, unauthorized or accidental file modification, etc.";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(427, 38);
            this.label3.TabIndex = 3;
            this.label3.Text = "Although not required, it is advised that you store backups in a different storag" +
    "e device.";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBrowse.Location = new System.Drawing.Point(350, 227);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(92, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "C&hange";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtBackupPath
            // 
            this.txtBackupPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBackupPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtBackupPath.Location = new System.Drawing.Point(15, 228);
            this.txtBackupPath.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtBackupPath.MaxLength = 32;
            this.txtBackupPath.Name = "txtBackupPath";
            this.txtBackupPath.ReadOnly = true;
            this.txtBackupPath.Size = new System.Drawing.Size(329, 21);
            this.txtBackupPath.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(12, 171);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(433, 24);
            this.label4.TabIndex = 15;
            this.label4.Text = "Backup Database";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(12, 205);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Backup Directory:";
            // 
            // BackupDatabaseDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(457, 323);
            this.Name = "BackupDatabaseDialog";
            this.Text = "Database Backup";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.BackupDatabaseDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.BackupDatabaseDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.InfoPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        #region Public Functions

        public void SetFieldValues(){
            txtBackupPath.Text = Properties.Settings.Default.BackupPath;
        }

        public void GetFieldValues(out string backup_directory){
            backup_directory = txtBackupPath.Text;
        }

        #endregion

        #region Local Functions

        void SaveBackupPath() {
            Properties.Settings.Default.BackupPath = txtBackupPath.Text;
            Properties.Settings.Default.Save();
        }

        //362
        //235
        void HideInfoPanel() {
            InfoPanel.Visible = false;
            Height = 235;
            lnkMoreInfo.Text = "Show Info";
        }

        void ShowInfoPanel() {
            InfoPanel.Visible = true;
            Height = 362;
            lnkMoreInfo.Text = "Hide Info";
        }

        #endregion

        #region Window Event handlers

        private void BackupDatabaseDialog_Load(object sender, EventArgs e) {
            SetOKButtonText("&Backup");
            HideInfoPanel();
            SetFieldValues();
        }

        #endregion

        #region Control Event handlers

        private void lnkMoreInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (InfoPanel.Visible) HideInfoPanel();
            else ShowInfoPanel();
        }

        private void btnBrowse_Click(object sender, EventArgs e) {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                txtBackupPath.Text = dlg.SelectedPath;
            }
        }

        #endregion

        #region Validation

        private void BackupDatabaseDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (Directory.Exists(txtBackupPath.Text)) {
                e.Cancel = false;
                SaveBackupPath();
            } else {
                Notifier.ShowWarning("The filepath specified for backup does not exist. Please update it.", "Invalid Backup Path");
                return;
            }
        }

        #endregion

    }

}