﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class CashReportDialog : FileMaintenanceDialog {

        #region Controls

        //Form controls.
        private Label label3;
        internal TextBox txtTitle;
        private Label label4;
        private DateTimePicker dpStart;
        private Label label2;
        private Label label5;
        private DateTimePicker dpEnd;
        private Label lblDatePeriod;
        private ToolTip ToolTipHelp;
        private System.ComponentModel.IContainer components;
        private Label label1;

        #endregion

        #region Constructor

        //Constructor
        public CashReportDialog() {
            InitializeComponent();
        }

        //Designer initialization.
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashReportDialog));
            this.label3 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dpStart = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblDatePeriod = new System.Windows.Forms.Label();
            this.ToolTipHelp = new System.Windows.Forms.ToolTip(this.components);
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lblDatePeriod);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.dpEnd);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.dpStart);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtTitle);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(333, 186);
            this.ContentPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(306, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Cash Report Info";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitle
            // 
            this.txtTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtTitle.Location = new System.Drawing.Point(91, 48);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTitle.MaxLength = 128;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(227, 21);
            this.txtTitle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Title:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(306, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "Date Period";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dpStart
            // 
            this.dpStart.CustomFormat = "MMMM dd, yyyy (ddd)";
            this.dpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpStart.Location = new System.Drawing.Point(91, 113);
            this.dpStart.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dpStart.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dpStart.Name = "dpStart";
            this.dpStart.Size = new System.Drawing.Size(227, 21);
            this.dpStart.TabIndex = 1;
            this.dpStart.ValueChanged += new System.EventHandler(this.dpStart_ValueChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Start:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 145);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "End:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dpEnd
            // 
            this.dpEnd.CustomFormat = "MMMM dd, yyyy (ddd)";
            this.dpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpEnd.Location = new System.Drawing.Point(91, 144);
            this.dpEnd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dpEnd.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dpEnd.Name = "dpEnd";
            this.dpEnd.Size = new System.Drawing.Size(227, 21);
            this.dpEnd.TabIndex = 2;
            this.dpEnd.ValueChanged += new System.EventHandler(this.dpEnd_ValueChanged);
            // 
            // lblDatePeriod
            // 
            this.lblDatePeriod.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDatePeriod.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDatePeriod.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatePeriod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblDatePeriod.Location = new System.Drawing.Point(291, 82);
            this.lblDatePeriod.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblDatePeriod.Name = "lblDatePeriod";
            this.lblDatePeriod.Size = new System.Drawing.Size(27, 18);
            this.lblDatePeriod.TabIndex = 20;
            this.lblDatePeriod.Text = "?";
            this.lblDatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipHelp.SetToolTip(this.lblDatePeriod, resources.GetString("lblDatePeriod.ToolTip"));
            this.lblDatePeriod.Visible = false;
            // 
            // ToolTipHelp
            // 
            this.ToolTipHelp.AutoPopDelay = 30000;
            this.ToolTipHelp.InitialDelay = 500;
            this.ToolTipHelp.ReshowDelay = 100;
            // 
            // CashReportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(333, 236);
            this.Name = "CashReportDialog";
            this.Text = "Cash Report";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.CashReportDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.CashReportDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables

        string[] ExistingCashReportsList;
        string OldTitle = "";

        #endregion

        #region Public Functions

        //Sets the list of existing cash reports to prevent duplications
        public void SetExistingCR(string[] cashReports) {
            ExistingCashReportsList = cashReports;
        }

        //Sets the initial field contents, then set to editing mode.
        public void SetFieldValues(string title, string dateStart, string dateEnd) {
            txtTitle.Text = title;
            dpStart.Text = dateStart;
            dpEnd.Text = dateEnd;
            OldTitle = txtTitle.Text;
            dpStart.Enabled = dpEnd.Enabled = false;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
            lblDatePeriod.Visible = true;
        }

        //Gets the input values.
        public void GetFieldValues(out string title, out string dateStart, out string dateEnd) {
            title = txtTitle.Text.Trim();
            dateStart = dpStart.Value.ToShortDateString();
            dateEnd = dpEnd.Value.ToShortDateString();
        }

        #endregion

        #region Local Functions
        #endregion

        #region Window Event handlers

        //Window load eventhandler.
        private void CashReportDialog_Load(object sender, EventArgs e) {
            //Set current date as date end, and current day -30 days as starting date.
            //Only when FileMaintenanceMode is adding.
            if (FileMaintenanceMode != FileMaintenanceDialogMode.Editing) {
                dpStart.Value = DateTime.Now.AddDays(-30);
                dpEnd.Value = DateTime.Now;
            }
        }

        #endregion

        #region Control Event handlers

        //DatePicker End event handler
        private void dpEnd_ValueChanged(object sender, EventArgs e) {
            //Prevent end date go before start date.
            if (dpStart.Value > dpEnd.Value) 
                dpEnd.Value = dpStart.Value;
            dpStart.MaxDate = dpEnd.Value;
        }

        //DatePickeer Start eventhandler.
        private void dpStart_ValueChanged(object sender, EventArgs e) {
            //Prevent start date go beyond end date.
            if (dpStart.Value > dpEnd.Value)
                dpStart.Value = dpEnd.Value;
            dpEnd.MinDate = dpStart.Value;
        }

        #endregion

        #region Validation

        //Validate inputs before responding OK Dialog Result.
        private void CashReportDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            //Basic validations
            if (InputValidators.IsEmptyTrimmed(txtTitle.Text)) {
                Notifier.ShowWarning("Please enter the cash report's title", "Invalid Input");
                txtTitle.Focus();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtTitle.Text) == false) {
                Notifier.ShowWarning("Please use alphanumeric characters for the cash report's title.", "Invalid Input");
                txtTitle.Focus();
                txtTitle.SelectAll();
                return;
            }

            //Existing CR validations.
            if (ExistingCashReportsList != null) {
                foreach (string s in ExistingCashReportsList) {
                    if (txtTitle.Text.Trim().Equals(s, StringComparison.CurrentCultureIgnoreCase)) {
                        if ((FileMaintenanceMode == FileMaintenanceDialogMode.Editing && txtTitle.Text.Trim().Equals(OldTitle)) == false) {
                            Notifier.ShowWarning("Cash report with the same title already exists. Please think of another title for this cash report.", "Invalid Input");
                            txtTitle.Focus();
                            txtTitle.SelectAll();
                            return;
                        }
                    }
                }
            }

            //Show confirmation message box to confirm the info.
            //It is easier to read info on a readable format.
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Add) {
                if (Notifier.ShowConfirm(txtTitle.Text + "\n\nPeriod Covered:\n" + dpStart.Value.ToString("MMMM d, yyyy") + " to " +
                    dpEnd.Value.ToString("MMMM d, yyyy") + ".\n\nProceed to creating this Cash Report?", "Confirm Details")
                    == System.Windows.Forms.DialogResult.Yes) {
                    e.Cancel = false;       //Submit dialog as OK.
                }
            } else {
                if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing && txtTitle.Text.Trim().Equals(OldTitle.Trim()))
                    DialogResult = System.Windows.Forms.DialogResult.Cancel;
                e.Cancel = false;
            }
        }

        #endregion

    }

}