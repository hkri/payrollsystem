﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class CashReportSectionDialog : FileMaintenanceDialog {
        private Label label1;
        internal TextBox txtTitle;
        private Label label2;

        #region Controls
        #endregion

        #region Constructor

        //Constructor
        public CashReportSectionDialog() {
            InitializeComponent();
        }

        //Designer-set initializations.
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.txtTitle);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(358, 89);
            this.ContentPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(331, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Cash Report Section Details";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitle
            // 
            this.txtTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtTitle.Location = new System.Drawing.Point(80, 48);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTitle.MaxLength = 32;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(261, 21);
            this.txtTitle.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Title:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CashReportSectionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(358, 139);
            this.Name = "CashReportSectionDialog";
            this.Text = "Cash Report Section";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.CashReportSectionDialog_OKButtonClicked);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables

        List<String> ExistingSections = new List<String>();
        String OldTitle = "";

        #endregion

        #region Public Functions

        //Sets the exisiting section titles list to prevent naming duplicates.
        public void SetExistingSections(List<String> lst) {
            ExistingSections = lst;
        }
        
        //Set vallues of the field inputs from this dialogs
        public void SetFieldValues(string title) {
            txtTitle.Text = title.Trim();
            OldTitle = txtTitle.Text;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        //Get the field inputs from this dialog.
        public void GetFieldValues(out string title) {
            title = txtTitle.Text.Trim();
        }

        #endregion

        #region Local Functions
        #endregion

        #region Window Event handlers
        #endregion

        #region Control Event handlers
        #endregion

        #region Validation

        //Validate input when OK is pressed.
        private void CashReportSectionDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            //Basic validations.
            if (InputValidators.IsEmptyTrimmed(txtTitle.Text)) {
                Notifier.ShowWarning("Please enter the title of the section.", "Invalid Input");
                txtTitle.Focus();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtTitle.Text) == false) {
                Notifier.ShowWarning("Please use alphanumeric characters.", "Invalid Input");
                txtTitle.Focus();
                txtTitle.SelectAll();
                return;
            }

            //Check title if existing.
            foreach (string s in ExistingSections) {
                if (s.Trim().Equals(txtTitle.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) {
                    if ((FileMaintenanceMode == FileMaintenanceDialogMode.Editing && OldTitle.Trim().Equals(txtTitle.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) == false) {
                        //On adding mode, and fails.
                        Notifier.ShowWarning("Section with the same title already exists.", "Invalid Input");
                        txtTitle.Focus();
                        txtTitle.SelectAll();
                        return;
                    } else {
                        //On editing mode, and title still same with old.
                        DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    }
                }
            }

            e.Cancel = false;   //Send DialogResult OK means no input errors.
        }

        #endregion

    }

}