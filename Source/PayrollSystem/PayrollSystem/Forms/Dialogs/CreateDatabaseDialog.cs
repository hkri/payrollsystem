﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class CreateDatabaseDialog : FileMaintenanceDialog {

        #region Controls        
        private Label label3;
        private Label label2;
        internal TextBox txtDefaultFilePath;
        private Button btnBrowse;
        private Label label6;
        private Label label4;
        internal TextBox txtDBName;
        private Label label5;
        private BufferedPanel InfoPanel;
        private LinkLabel lnkMoreInfo;
        private Label label1;
        #endregion

        #region Constructor

        public CreateDatabaseDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDefaultFilePath = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.InfoPanel = new PayrollSystem.BufferedPanel();
            this.lnkMoreInfo = new System.Windows.Forms.LinkLabel();
            this.ContentPanel.SuspendLayout();
            this.InfoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lnkMoreInfo);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.txtDBName);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.InfoPanel);
            this.ContentPanel.Size = new System.Drawing.Size(457, 278);
            this.ContentPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "This payroll system uses databases to store your company records.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(427, 60);
            this.label2.TabIndex = 2;
            this.label2.Text = "If your current database went corrupt, reached its maximum file size (max 2GB), o" +
    "r simply want to create a new one for your new set of records, simply enter the " +
    "filename below, then press OK to save.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(427, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "System databases are always stored and accessed in this filepath:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDefaultFilePath
            // 
            this.txtDefaultFilePath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtDefaultFilePath.Location = new System.Drawing.Point(6, 120);
            this.txtDefaultFilePath.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDefaultFilePath.MaxLength = 32;
            this.txtDefaultFilePath.Name = "txtDefaultFilePath";
            this.txtDefaultFilePath.ReadOnly = true;
            this.txtDefaultFilePath.Size = new System.Drawing.Size(332, 21);
            this.txtDefaultFilePath.TabIndex = 0;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(344, 119);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(86, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Open";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(12, 205);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(433, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "Create Database";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDBName
            // 
            this.txtDBName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDBName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtDBName.Location = new System.Drawing.Point(124, 242);
            this.txtDBName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDBName.MaxLength = 32;
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(318, 21);
            this.txtDBName.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(12, 242);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Database Name:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label6.Location = new System.Drawing.Point(12, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(433, 24);
            this.label6.TabIndex = 10;
            this.label6.Text = "Information about System Databases";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InfoPanel
            // 
            this.InfoPanel.Controls.Add(this.label1);
            this.InfoPanel.Controls.Add(this.label2);
            this.InfoPanel.Controls.Add(this.btnBrowse);
            this.InfoPanel.Controls.Add(this.txtDefaultFilePath);
            this.InfoPanel.Controls.Add(this.label3);
            this.InfoPanel.Location = new System.Drawing.Point(12, 44);
            this.InfoPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(433, 155);
            this.InfoPanel.TabIndex = 11;
            // 
            // lnkMoreInfo
            // 
            this.lnkMoreInfo.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lnkMoreInfo.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.Location = new System.Drawing.Point(357, 14);
            this.lnkMoreInfo.Name = "lnkMoreInfo";
            this.lnkMoreInfo.Size = new System.Drawing.Size(88, 24);
            this.lnkMoreInfo.TabIndex = 1;
            this.lnkMoreInfo.TabStop = true;
            this.lnkMoreInfo.Text = "Show Info";
            this.lnkMoreInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lnkMoreInfo.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkMoreInfo_LinkClicked);
            // 
            // CreateDatabaseDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(457, 328);
            this.Name = "CreateDatabaseDialog";
            this.Text = "System Database";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.CreateDatabaseDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.CreateDatabaseDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.InfoPanel.ResumeLayout(false);
            this.InfoPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Public Functions

        public void GetFieldValues(out string db_filename) {
            db_filename = GlobalVariables.DB_DIR + "/" + txtDBName.Text + ".accdb";
        }

        #endregion

        #region local variables

        string filename = "";

        #endregion

        #region Local Functions

        void HideInfoPanel() {
            InfoPanel.Visible = false;
            Height = 207;
            lnkMoreInfo.Text = "Show Info";
        }

        void ShowInfoPanel() {
            InfoPanel.Visible = true;
            Height = 367;
            lnkMoreInfo.Text = "Hide Info";
        }

        #endregion

        #region Window Event handlers

        private void CreateDatabaseDialog_Load(object sender, EventArgs e) {
            HideInfoPanel();
            if (Directory.Exists(GlobalVariables.DB_DIR) == false) {
                Directory.CreateDirectory(GlobalVariables.DB_DIR);
            }
            txtDefaultFilePath.Text = GlobalVariables.DB_DIR;
        }

        #endregion

        #region Control Event handlers

        private void btnBrowse_Click(object sender, EventArgs e) {
            Process.Start("explorer.exe", GlobalVariables.DB_DIR);
        }

        private void lnkMoreInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            //367
            //207
            if (InfoPanel.Visible) HideInfoPanel();
            else ShowInfoPanel();
        }

        #endregion

        #region Validation

        private void CreateDatabaseDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (txtDBName.Text.Length <= 0) {
                Notifier.ShowWarning("Filename cannot be empty.", "Invalid Input");
                txtDBName.Focus();
                return;
            }
            if (txtDBName.Text.IndexOfAny(Path.GetInvalidPathChars()) >= 0) {
                string invalidchars = "";
                foreach (char c in Path.GetInvalidPathChars()) invalidchars += c + "";
                Notifier.ShowWarning("Filename cannot contain any of the following characters: " + invalidchars, "Invalid Input");
                txtDBName.Focus();
                return;
            }
            filename = Path.GetFileNameWithoutExtension(txtDBName.Text);
            filename += ".accdb";
            if (File.Exists(GlobalVariables.DB_DIR + @"\" + filename)) {
                filename = "";
                Notifier.ShowWarning("Database already exists.", "Invalid Database Filename");
            } else {
                e.Cancel = false;
            }
        }

        #endregion

    }

}