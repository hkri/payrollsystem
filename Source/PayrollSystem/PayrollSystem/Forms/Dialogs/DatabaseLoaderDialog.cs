﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

namespace PayrollSystem {

    class DatabaseLoaderDialog : FileMaintenanceDialog {
        private Button btnImport;
        private ListBox lstDB;
        private Label label1;
        private Label label3;

        #region Controls
        #endregion

        #region Constructor

        public DatabaseLoaderDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstDB = new System.Windows.Forms.ListBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.btnImport);
            this.ContentPanel.Controls.Add(this.lstDB);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Size = new System.Drawing.Size(365, 337);
            this.ContentPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(341, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Available Databases";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(341, 48);
            this.label1.TabIndex = 7;
            this.label1.Text = "Select the database you wish to load from the list below. If the database you nee" +
    "d is not on the list, press Import to add it to the list.";
            // 
            // lstDB
            // 
            this.lstDB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lstDB.FormattingEnabled = true;
            this.lstDB.IntegralHeight = false;
            this.lstDB.Location = new System.Drawing.Point(12, 104);
            this.lstDB.Name = "lstDB";
            this.lstDB.Size = new System.Drawing.Size(338, 186);
            this.lstDB.TabIndex = 0;
            this.lstDB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstDB_KeyDown);
            this.lstDB.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstDB_MouseDoubleClick);
            this.lstDB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstDB_MouseDown);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(256, 298);
            this.btnImport.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(94, 26);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "Import...";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // DatabaseLoaderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(365, 387);
            this.Name = "DatabaseLoaderDialog";
            this.Text = "Load Database";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.DatabaseLoaderDialog_OKButtonClicked);
            this.Activated += new System.EventHandler(this.DatabaseLoaderDialog_Activated);
            this.Load += new System.EventHandler(this.DatabaseLoaderDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        #region Public Functions

        public string GetFilename() {
            return lstDB.SelectedItem.ToString();
        }

        #endregion

        #region Local Functions

        void LocateDatabases() {
            int selection_index = lstDB.SelectedIndex;
            lstDB.Items.Clear();
            string[] files = Directory.GetFiles(GlobalVariables.DB_DIR);
            foreach (string p in files) {
                if (Path.GetExtension(p).ToLower() == ".accdb") {
                    lstDB.Items.Add(Path.GetFileName(p));
                }
            }
            if(selection_index >= 0) lstDB.SelectedIndex = selection_index;
        }

        #endregion

        #region Window Event handlers

        private void DatabaseLoaderDialog_Load(object sender, EventArgs e) {
            Text = "Load Database";
        }

        private void DatabaseLoaderDialog_Activated(object sender, EventArgs e) {
            LocateDatabases();  //Refresh every refocus on window.
            if (lstDB.Items.Count > 0) {
                if(lstDB.SelectedIndex < 0)
                    lstDB.SelectedIndex = 0;
            }
        }

        #endregion

        #region Control Event handlers

        private void lstDB_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (lstDB.SelectedIndex >= 0)
                TriggerOKButton();
        }

        private void btnImport_Click(object sender, EventArgs e) {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Database Files (*.accdb)|*.accdb";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                DatabaseManager.CopyDatabaseToSystemDirectory(dlg.FileName);
                LocateDatabases();
            }
        }

        private void lstDB_MouseDown(object sender, MouseEventArgs e) {
            
        }

        private void lstDB_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                DatabaseLoaderDialog_OKButtonClicked(this, new DialogEventArgs());
            }
        }

        #endregion

        #region Validation

        private void DatabaseLoaderDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (lstDB.SelectedIndex > -1) {
                e.Cancel = false;
            } else {
                Notifier.ShowWarning("Please select a database from the list.", "No Item Selected");
            }
        }

        #endregion



    }

}