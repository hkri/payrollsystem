﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    partial class EmployeeDialog : FileMaintenanceDialog {

        #region Constructor

        public EmployeeDialog() {
            InitializeComponent();
        }

        #endregion

        #region Local Variables

        List<String> existing_names = new List<String>();
        List<String> existing_names_archived = new List<String>();

        string old_name = "";

        #endregion

        #region Public Functions

        public void SetExistingNames(List<String> names) {
            existing_names = names;
        }

        public void SetExistingNamesArchived(List<String> names) {
            existing_names_archived = names;
        }

        public string GetFullName() {
            string fn = txtFN.Text.Trim(), mn = txtMN.Text.Trim(), ln = txtLN.Text.Trim();
            string fullname = fn + " ";
            if (mn != "") fullname += mn + " ";
            fullname += ln;
            return fullname;
        }

        public void SetFieldValues(string FN, string MN, string LN, string hired) {
            txtFN.Text = FN.Trim();
            txtMN.Text = MN.Trim();
            txtLN.Text = LN.Trim();
            dpHired.Text = hired;
            old_name = GetFullName();
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        public void GetFieldValues(out string FN, out string MN, out string LN, out string hired) {
            FN = txtFN.Text.Trim();
            MN = txtMN.Text.Trim();
            LN = txtLN.Text.Trim();
            hired = dpHired.Value.ToShortDateString();
        }

        #endregion

        #region Local Functions
        #endregion

        #region Window Event handlers

        private void EmployeeDialog_Load(object sender, EventArgs e) {
            dpHired.MaxDate = DateTime.Now;
        }

        #endregion

        #region Control Event handlers
        #endregion

        #region Validation

        private void EmployeeDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtFN.Text)) {
                Notifier.ShowWarning("Please input first name of employee.", "Invalid Input");
                txtFN.Focus();
                return;
            } 
            if (InputValidators.IsEmptyTrimmed(txtLN.Text)) {
                Notifier.ShowWarning("Please input last name of employee.", "Invalid Input");
                txtLN.Focus();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtFN.Text) == false || InputValidators.IsNumericWithDecimal(txtFN.Text)) {
                Notifier.ShowWarning("Employee's first name doesn't seem to be a valid name.", "Invalid Input");
                txtFN.Focus();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtLN.Text) == false || InputValidators.IsNumericWithDecimal(txtLN.Text)) {
                Notifier.ShowWarning("Employee's last name doesn't seem to be a valid name.", "Invalid Input");
                txtLN.Focus();
                return;
            }
            if (dpHired.Value > DateTime.Now) {
                Notifier.ShowWarning("Are you a soothsayer? You can't possibly hire someone on a future date.", "Invalid Input");
                txtLN.Focus();
                return;
            }

            string fn = GetFullName();
            if (fn.Equals(old_name) == false) {
                foreach (string n in existing_names) {
                    if (fn.Equals(n)) {
                        Notifier.ShowWarning("Employee with the same name already exists.\n\nIf this is really a rare case of name duplicate of two different people, you can add \"_1\" at the end of his/her surname.", "Name Duplicate Detected");
                        txtLN.Text += "_1";
                        txtLN.Focus();
                        return;
                    }
                } 
                foreach (string n in existing_names_archived) {
                    if (fn.Equals(n)) {
                        Notifier.ShowWarning("Employee with the same name already exists in the Employee archives. You can restore that record instead.", "Name Duplicate Detected");
                        txtFN.Focus();
                        return;
                    }
                }
            }

            e.Cancel = false;
        }

        #endregion

    }

}