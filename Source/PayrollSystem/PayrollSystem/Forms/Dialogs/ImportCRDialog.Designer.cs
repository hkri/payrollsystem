﻿namespace PayrollSystem {
    partial class ImportCRDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportCRDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlRecords = new PayrollSystem.BufferedPanel();
            this.RecordsDataGrid = new PayrollSystem.BufferedDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabStripMain = new System.Windows.Forms.ToolStrip();
            this.tbFundTransfer = new System.Windows.Forms.ToolStripButton();
            this.tbExpenses = new System.Windows.Forms.ToolStripButton();
            this.tbInvalid = new System.Windows.Forms.ToolStripButton();
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.btnCreateCSVTemp = new System.Windows.Forms.Button();
            this.ContentPanel.SuspendLayout();
            this.pnlRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).BeginInit();
            this.TabStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.btnCreateCSVTemp);
            this.ContentPanel.Controls.Add(this.pnlRecords);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.btnSelectFile);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(754, 433);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label1.Location = new System.Drawing.Point(23, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(705, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "You can add multiple cash report entries at once by importing CSV files that cont" +
    "ains the records. CSV files (*.csv) can be made via any text editor or spreadshe" +
    "et application.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSelectFile.FlatAppearance.BorderSize = 0;
            this.btnSelectFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSelectFile.Image = global::PayrollSystem.Properties.Resources.import;
            this.btnSelectFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelectFile.Location = new System.Drawing.Point(380, 95);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(160, 28);
            this.btnSelectFile.TabIndex = 3;
            this.btnSelectFile.Text = "  Open CSV &File";
            this.btnSelectFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label2.Location = new System.Drawing.Point(23, 55);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(705, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlRecords
            // 
            this.pnlRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRecords.BorderColor = System.Drawing.Color.Black;
            this.pnlRecords.Borders = System.Windows.Forms.AnchorStyles.None;
            this.pnlRecords.Controls.Add(this.RecordsDataGrid);
            this.pnlRecords.Controls.Add(this.TabStripMain);
            this.pnlRecords.Controls.Add(this.bufferedPanel1);
            this.pnlRecords.Location = new System.Drawing.Point(0, 133);
            this.pnlRecords.Name = "pnlRecords";
            this.pnlRecords.Size = new System.Drawing.Size(754, 300);
            this.pnlRecords.TabIndex = 8;
            // 
            // RecordsDataGrid
            // 
            this.RecordsDataGrid.AllowUserToAddRows = false;
            this.RecordsDataGrid.AllowUserToDeleteRows = false;
            this.RecordsDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.RecordsDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.RecordsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.RecordsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RecordsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.RecordsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.RecordsDataGrid.ColumnHeadersHeight = 28;
            this.RecordsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.RecordsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.RecordsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RecordsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.RecordsDataGrid.Location = new System.Drawing.Point(0, 31);
            this.RecordsDataGrid.MultiSelect = false;
            this.RecordsDataGrid.Name = "RecordsDataGrid";
            this.RecordsDataGrid.ReadOnly = true;
            this.RecordsDataGrid.RowHeadersVisible = false;
            this.RecordsDataGrid.RowTemplate.Height = 28;
            this.RecordsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RecordsDataGrid.ShowCellErrors = false;
            this.RecordsDataGrid.ShowEditingIcon = false;
            this.RecordsDataGrid.Size = new System.Drawing.Size(754, 269);
            this.RecordsDataGrid.TabIndex = 9;
            this.RecordsDataGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.RecordsDataGrid_CellFormatting);
            this.RecordsDataGrid.SelectionChanged += new System.EventHandler(this.RecordsDataGrid_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TabStripMain
            // 
            this.TabStripMain.AllowItemReorder = true;
            this.TabStripMain.AutoSize = false;
            this.TabStripMain.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TabStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbFundTransfer,
            this.tbExpenses,
            this.tbInvalid});
            this.TabStripMain.Location = new System.Drawing.Point(0, 1);
            this.TabStripMain.Name = "TabStripMain";
            this.TabStripMain.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.TabStripMain.Size = new System.Drawing.Size(754, 30);
            this.TabStripMain.TabIndex = 3;
            this.TabStripMain.Text = "toolStrip1";
            // 
            // tbFundTransfer
            // 
            this.tbFundTransfer.Checked = true;
            this.tbFundTransfer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tbFundTransfer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbFundTransfer.Image = ((System.Drawing.Image)(resources.GetObject("tbFundTransfer.Image")));
            this.tbFundTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbFundTransfer.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbFundTransfer.Name = "tbFundTransfer";
            this.tbFundTransfer.Size = new System.Drawing.Size(89, 25);
            this.tbFundTransfer.Text = "Fund Transfer";
            this.tbFundTransfer.CheckedChanged += new System.EventHandler(this.tbFundTransfer_CheckedChanged);
            this.tbFundTransfer.Click += new System.EventHandler(this.tbFundTransfer_Click);
            // 
            // tbExpenses
            // 
            this.tbExpenses.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbExpenses.Image = ((System.Drawing.Image)(resources.GetObject("tbExpenses.Image")));
            this.tbExpenses.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExpenses.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbExpenses.Name = "tbExpenses";
            this.tbExpenses.Size = new System.Drawing.Size(65, 25);
            this.tbExpenses.Text = "Expenses";
            this.tbExpenses.CheckedChanged += new System.EventHandler(this.tbExpenses_CheckedChanged);
            this.tbExpenses.Click += new System.EventHandler(this.tbExpenses_Click);
            // 
            // tbInvalid
            // 
            this.tbInvalid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbInvalid.Image = ((System.Drawing.Image)(resources.GetObject("tbInvalid.Image")));
            this.tbInvalid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbInvalid.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbInvalid.Name = "tbInvalid";
            this.tbInvalid.Size = new System.Drawing.Size(50, 25);
            this.tbInvalid.Text = "Invalid";
            this.tbInvalid.ToolTipText = "Invalid (Invalid dates, non-existent plate numbers and employees)";
            this.tbInvalid.CheckedChanged += new System.EventHandler(this.tbInvalid_CheckedChanged);
            this.tbInvalid.Click += new System.EventHandler(this.tbInvalid_Click);
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 0);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(754, 1);
            this.bufferedPanel1.TabIndex = 9;
            // 
            // btnCreateCSVTemp
            // 
            this.btnCreateCSVTemp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCreateCSVTemp.FlatAppearance.BorderSize = 0;
            this.btnCreateCSVTemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnCreateCSVTemp.Image = global::PayrollSystem.Properties.Resources.add_folder;
            this.btnCreateCSVTemp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateCSVTemp.Location = new System.Drawing.Point(214, 95);
            this.btnCreateCSVTemp.Name = "btnCreateCSVTemp";
            this.btnCreateCSVTemp.Size = new System.Drawing.Size(160, 28);
            this.btnCreateCSVTemp.TabIndex = 10;
            this.btnCreateCSVTemp.Text = "  &New CSV Template";
            this.btnCreateCSVTemp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCreateCSVTemp.UseVisualStyleBackColor = true;
            this.btnCreateCSVTemp.Click += new System.EventHandler(this.btnCreateCSVTemp_Click);
            // 
            // ImportCRDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 483);
            this.MinimumSize = new System.Drawing.Size(735, 522);
            this.Name = "ImportCRDialog";
            this.Text = "Import Cash Report";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.ImportCRDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.ImportCRDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.pnlRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).EndInit();
            this.TabStripMain.ResumeLayout(false);
            this.TabStripMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Label label2;
        private BufferedPanel pnlRecords;
        private System.Windows.Forms.ToolStrip TabStripMain;
        private System.Windows.Forms.ToolStripButton tbFundTransfer;
        private System.Windows.Forms.ToolStripButton tbExpenses;
        private BufferedPanel bufferedPanel1;
        private System.Windows.Forms.Button btnCreateCSVTemp;
        protected internal BufferedDataGridView RecordsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.ToolStripButton tbInvalid;
    }
}