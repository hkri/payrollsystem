﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

using CsvHelper;

namespace PayrollSystem {

    public partial class ImportCRDialog : FileMaintenanceDialog {

        #region data

        internal DataTable _DATASRC = new DataTable();

        string[] _PlateNo = ManageVehiclesPage.QueryVehiclesPlateNoAndType().Select(x => x.Value.Trim().ToUpper()).ToArray();
        List<Employee> _employees = EmployeesPage.QueryEmployeesList();

        List<Employee> _indexedEmployees = new List<Employee>();

        public DateTime _STARTDATE;
        public DateTime _ENDDATE;

        #endregion

        #region constructor

        // Constructor
        public ImportCRDialog() {
            InitializeComponent();

            //Set custom renderers
            TabStripMain.Renderer = new LightTabStripRenderer();

            //Fill data source with necessary columns.
            _DATASRC.Columns.Add("Remarks");
            _DATASRC.Columns.Add("FT/EX");
            _DATASRC.Columns.Add("Date");
            _DATASRC.Columns.Add("Plate No");
            _DATASRC.Columns.Add("Particular");
            _DATASRC.Columns.Add("Amount");
            _DATASRC.Columns.Add("Petron Sucat Diesel");
            _DATASRC.Columns.Add("Other Diesel");
            _DATASRC.Columns.Add("Violations");
            _DATASRC.Columns.Add("Toll");
            _DATASRC.Columns.Add("Trucking Expenses");
            _DATASRC.Columns.Add("RMV");
            _DATASRC.Columns.Add("Service Fee");
            _DATASRC.Columns.Add("Others");
            _DATASRC.Columns.Add("Driver");
            _DATASRC.Columns.Add("Driver ID");
            _DATASRC.Columns.Add("Driver CA");
            _DATASRC.Columns.Add("Helper");
            _DATASRC.Columns.Add("Helper ID");
            _DATASRC.Columns.Add("Helper CA");

            //Setup data source datatype columns.
            _DATASRC.Columns["Date"].DataType = typeof(DateTime);
            _DATASRC.Columns["Amount"].DataType = typeof(double);
            _DATASRC.Columns["Petron Sucat Diesel"].DataType = typeof(double);
            _DATASRC.Columns["Other Diesel"].DataType = typeof(double);
            _DATASRC.Columns["Violations"].DataType = typeof(double);
            _DATASRC.Columns["Toll"].DataType = typeof(double);
            _DATASRC.Columns["Trucking Expenses"].DataType = typeof(double);
            _DATASRC.Columns["RMV"].DataType = typeof(double);
            _DATASRC.Columns["Service Fee"].DataType = typeof(double);
            _DATASRC.Columns["Others"].DataType = typeof(double);
            _DATASRC.Columns["Driver CA"].DataType = typeof(double);
            _DATASRC.Columns["Helper CA"].DataType = typeof(double);

            //Setup datagrid datasource
            RecordsDataGrid.Columns.Clear();
            RecordsDataGrid.DataSource = _DATASRC;
            
            //Set column properties
            RecordsDataGrid.Columns["FT/EX"].Visible = false;
            RecordsDataGrid.Columns["Amount"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Petron Sucat Diesel"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Other Diesel"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Violations"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Toll"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Trucking Expenses"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["RMV"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Service Fee"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Others"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Driver CA"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Helper CA"].DefaultCellStyle.Format = "#,##0.00";
            hideExpensesColumns();

            //Hide reference columns
            RecordsDataGrid.Columns["Driver ID"].Visible = false;
            RecordsDataGrid.Columns["Helper ID"].Visible = false;
            
        }

        #endregion

        #region CSV Functions

        // create a new csv file template for cash report.
        private bool createCRCSVTemplate(string filename) {
            try {
                using (FileStream fs = new FileStream(filename, FileMode.Create)) {
                    using (StreamWriter wtr = new StreamWriter(fs)) {
                        CsvWriter cwtr = new CsvWriter(wtr);

                        //Enumerate column headers.
                        List<String> cols = new List<String>();
                        cols.Add("FT/EX");
                        cols.Add("Date");
                        cols.Add("Plate No.");
                        cols.Add("Particular");
                        cols.Add("Amount");
                        cols.Add("");
                        cols.Add("Petron Sucat Diesel");
                        cols.Add("Other Diesel");
                        cols.Add("Violations");
                        cols.Add("Toll");
                        cols.Add("Trucking Expenses");
                        cols.Add("RMV");
                        cols.Add("Service Fee");
                        cols.Add("Others");
                        cols.Add("Driver");
                        cols.Add("Driver CA");
                        cols.Add("Helper");
                        cols.Add("Helper CA");

                        //Insert headers.
                        foreach (object dat in cols)
                            cwtr.WriteField(dat);
                        cwtr.NextRecord();

                        //Flush and close.
                        wtr.Flush();
                        wtr.Close();

                        //Notify success.
                        Notifier.ShowInformation("Cash Report CSV template successfully created.", "Create CSV Template");
                    }
                }
            } catch (Exception ex) {
                System.Diagnostics.Trace.WriteLine("Create CR CSV Template: " + ex.Message);
                Notifier.ShowError("Sorry, there seems to be a problem in creating the CSV template.\n\n" + ex.Message);
                return false;
            }
            return true;
        }

        // parse the imported CSV
        bool importCSV(string filename) {
            try {
                using (FileStream fs = new FileStream(filename, FileMode.Open)) {
                    //Read CSV per record
                    using (StreamReader rdr = new StreamReader(fs)) {
                        CsvReader crdr = new CsvReader(rdr);

                        //Counters
                        int invalid = 0, ftransfer = 0, expenses = 0;
                        //Clear datatable
                        _DATASRC.Clear();

                        //List available employees
                        /*
                        foreach (Employee emp in _employees)
                            System.Diagnostics.Trace.WriteLine(emp.FirstName + " " + emp.LastName);
                        */

                        while (crdr.Read()) {
                            //Get type of record.
                            string entry_type = crdr.GetField<String>("FT/EX").ToString().Trim().ToUpper();
                            bool isvalid = true;

                            //Remarks (for invalid)
                            string remarks = "";

                            //Get other fields (first tier validation)
                            //Get date
                            string date = crdr.GetField<String>("Date").ToString().Trim().ToUpper();
                            DateTime nDate = DateTime.Now;
                            if (DateTime.TryParse(date, out nDate) == false) {
                                isvalid = false;
                                remarks = "No date provided.";
                                //continue;
                            } else {
                                if (nDate.CompareTo(_STARTDATE) == -1 ) {
                                    isvalid = false;
                                    remarks = "Date beyond cash report period.";
                                } else if (nDate.CompareTo(_ENDDATE) == 1) {
                                    isvalid = false;
                                    remarks = "Date beyond cash report period.";
                                }
                            }
                            
                            //Get particular
                            string particular = crdr.GetField<String>("Particular").ToString().Trim().ToUpper();

                            //Get plate no
                            string plateno = crdr.GetField<String>("Plate No.").ToString().Trim().ToUpper();

                            //Filter plate numbers. If plate number is not registered on database, do not include.
                            if (plateno != "" && _PlateNo.Contains(plateno) == false) {
                                isvalid = false;
                                remarks = "Plate number does not exist in database.";
                                //continue;
                            }

                            //Get amount
                            double amount = 0.00;
                            Double.TryParse(crdr.GetField<String>("Amount").ToString().Trim(), out amount);

                            //Expenses values
                            double sucat = 0, fuel = 0, violations = 0, toll = 0, texp = 0, rmv = 0, sf = 0, others = 0, driverCA = 0, helperCA = 0;

                            //Drivers and helpers
                            string drivername = "", helpername = "";
                            string driverID = "", helperID = "";
                            bool isDriverOK = false, isHelperOK = false;

                            //Check record type (second tier validation)
                            if (entry_type.Equals("ft", StringComparison.InvariantCultureIgnoreCase)) {
                                if(isvalid) ftransfer++;
                            } else if (entry_type.Equals("ex", StringComparison.InvariantCultureIgnoreCase)) {
                                //Get breakdown values.
                                if(isvalid) expenses++;

                                //Get expenses breakdown.
                                Double.TryParse(crdr.GetField<String>("Petron Sucat Diesel").ToString().Trim(), out sucat);
                                Double.TryParse(crdr.GetField<String>("Other Diesel").ToString().Trim(), out fuel);
                                Double.TryParse(crdr.GetField<String>("Violations").ToString().Trim(), out violations);
                                Double.TryParse(crdr.GetField<String>("Toll").ToString().Trim(), out toll);
                                Double.TryParse(crdr.GetField<String>("Trucking Expenses").ToString().Trim(), out texp);
                                Double.TryParse(crdr.GetField<String>("RMV").ToString().Trim(), out rmv);
                                Double.TryParse(crdr.GetField<String>("Service Fee").ToString().Trim(), out sf);
                                Double.TryParse(crdr.GetField<String>("Others").ToString().Trim(), out others);
                                Double.TryParse(crdr.GetField<String>("Driver CA").ToString().Trim(), out driverCA);
                                Double.TryParse(crdr.GetField<String>("Helper CA").ToString().Trim(), out helperCA);

                                //Get driver name. Check if exists on database.
                                //For optimized searching, index previously searched employees.
                                //First search: indexed
                                //Second search: employees database
                                // > Index if match found
                                drivername = crdr.GetField<String>("Driver").ToString().Trim();
                                if (drivername != "") {
                                    //If driver name is not empty, there should be a driver name provided.
                                    //Search indexed.
                                    string match = findEmployeeIndexed(drivername).Trim();
                                    if (match != "") {
                                        isDriverOK = true;
                                        driverID = match;
                                    } else {
                                        match = findEmployeeAllList(drivername).Trim();
                                        if (match != "") {
                                            isDriverOK = true;
                                            driverID = match;
                                        }
                                    }
                                    if (isDriverOK == false) {
                                        //driverCA = 0;
                                        //isvalid = false;
                                        //Invalidate record.
                                        isvalid = false;
                                        remarks = "Employee not found.";
                                    }
                                } else {
                                    //No driver specififed, so no CA
                                    driverCA = 0;
                                }
                                
                                //Get helper name. Check if exists on database
                                helpername = crdr.GetField<String>("Helper").ToString().Trim();
                                if (helpername != "") {
                                    //If helper name is not empty, there should be a driver name provided.
                                    //Search indexed.
                                    string match = findEmployeeIndexed(helpername).Trim();
                                    if (match != "") {
                                        isHelperOK = true;
                                        helperID = match;
                                    } else {
                                        match = findEmployeeAllList(helpername).Trim();
                                        if (match != "") {
                                            isHelperOK = true;
                                            helperID = match;
                                        }
                                    }
                                    if (isHelperOK == false) {
                                        //driverCA = 0;
                                        //isvalid = false;
                                        //Invalidate record.
                                        isvalid = false;
                                        remarks = "Employee not found.";
                                    }
                                } else {
                                    //No driver specififed, so no CA
                                    helperCA = 0;
                                }
                                                               
                                //Get sum of all breakdown values
                                double breakdown_total = fuel + violations + toll + texp + rmv + sf + others + driverCA + helperCA;
                                if (breakdown_total != 0) {
                                    if (breakdown_total != amount) {
                                        //if amount and breakdown is not equal, breakdown should overwrite amount
                                        amount = breakdown_total;
                                    }
                                }

                            } else {
                                isvalid = false;
                                remarks = "Invalid category.";
                            }

                            if (isvalid == false) {
                                entry_type = "INV";
                                invalid++;
                            }
                            
                            //Insert record to data source
                            DataRow drow = _DATASRC.NewRow();
                            drow.SetField("Remarks", remarks);
                            drow.SetField("FT/EX", entry_type);
                            drow.SetField("Date", nDate);
                            drow.SetField("Particular", particular);
                            drow.SetField("Plate No", plateno);
                            drow.SetField("Amount", amount);

                            drow.SetField("Petron Sucat Diesel", sucat);
                            drow.SetField("Other Diesel", fuel);
                            drow.SetField("Violations", violations);
                            drow.SetField("Toll", toll);
                            drow.SetField("Trucking Expenses", texp);
                            drow.SetField("RMV", rmv);
                            drow.SetField("Service Fee", sf);
                            drow.SetField("Others", others);
                            drow.SetField("Driver CA", driverCA);
                            drow.SetField("Helper CA", helperCA);

                            drow.SetField("Driver", drivername);
                            drow.SetField("Driver ID", driverID);
                            drow.SetField("Helper", helpername);
                            drow.SetField("Helper ID", helperID);
                            _DATASRC.Rows.Add(drow);

                        }
                        //Show counters
                        tbExpenses.Text = "Expenses (" + expenses + ")";
                        tbFundTransfer.Text = "Fund Transfer (" + ftransfer + ")";
                        if (invalid > 0) {
                            tbInvalid.Text = "Invalid (" + invalid + ")";
                            tbInvalid.Visible = true;
                        } else
                            tbInvalid.Visible = false;
                        refreshRecords(); 
                        fitCellWidth();
                        if (expenses + ftransfer > 0)
                            SetOKButtonEnabled(true);
                        else
                            SetOKButtonEnabled(false);
                    }
                }
            } catch (Exception ex) {
                Notifier.ShowError("Oh no, there was an error parsing the CSV file. Please try again.\n\n" + ex.Message);
            }
            return true;
        }

        #endregion

        #region control_eventhandlers

        //When OK button is cliked.
        private void ImportCRDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            e.Cancel = false;
        }

        // create a new CSV template when link is clicked.
        private void lnkNewCRTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            using (SaveFileDialog dlg = new SaveFileDialog()) {
                dlg.Filter = "Comma Separated Values (CSV) file (*.csv)|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    createCRCSVTemplate(dlg.FileName);
                }
            }
        }

        // create a new CSV template when link is clicked.
        private void btnCreateCSVTemp_Click(object sender, EventArgs e) {
            using (SaveFileDialog dlg = new SaveFileDialog()) {
                dlg.Filter = "Comma Separated Values (CSV) file (*.csv)|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    createCRCSVTemplate(dlg.FileName);
                }
            }
        }

        // import the specified CSV.
        private void btnSelectFile_Click(object sender, EventArgs e) {
            using (OpenFileDialog dlg = new OpenFileDialog()) {
                dlg.Filter = "Comma Separated Values (CSV) file (*.csv)|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    importCSV(dlg.FileName);
                }
            }
        }

        // conditional formatting.
        private void RecordsDataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e) {
            if (tbInvalid.Checked == false) {
                string[] targets = { "Amount", "Fuel", "Violations", "Toll", "Trucking Expenses", "RMV", "Service Fee", "Others", "Driver CA", "Helper CA" };
                if (targets.Contains(RecordsDataGrid.Columns[e.ColumnIndex].Name)) {
                    double val = Convert.ToDouble(RecordsDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    if (val == 0.00 && RecordsDataGrid.Columns[e.ColumnIndex].Name != "Amount")
                        e.CellStyle.ForeColor = Color.FromArgb(200, 200, 200);
                    else if (val < 0) {
                        //ffd0c8
                        e.CellStyle.BackColor = ColorTranslator.FromHtml("#ffd0c8");
                    }
                }
            } else {
                e.CellStyle.ForeColor = Color.FromArgb(150,150,150);
            }
            /*
            if (RecordsDataGrid.Columns[e.ColumnIndex].Name == "Amount") {
                double val = Convert.ToDouble(RecordsDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
            }
            */
        }

        #endregion

        #region window_eventhandlers

        // handle window on load event.
        private void ImportCRDialog_Load(object sender, EventArgs e) {
            SetTitle("Import Cash Report CSV [" + _STARTDATE.ToShortDateString() + " - " + _ENDDATE.ToShortDateString() + "]");
            SetOKButtonText("Import");
            MaximizeBox = true;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            SetOKButtonEnabled(false);
            //fitCellWidth();
        }

        #endregion

        #region tab_eventhandlers

        // uncheck all tabs.
        void deselectAllTabs() {
            tbExpenses.Checked = false;
            tbFundTransfer.Checked = false;
            tbInvalid.Checked = false;
        }

        // Expenses tab selected.
        private void tbExpenses_Click(object sender, EventArgs e) {
            deselectAllTabs();
            tbExpenses.Checked = true;
            showExpensesColumns();
        }

        // Invalid tab selected.
        private void tbInvalid_Click(object sender, EventArgs e) {
            deselectAllTabs();
            tbInvalid.Checked = true;
            showExpensesColumns();
            ShowRemarksColumn();
        }

        // Fund transfer tab selected.
        private void tbFundTransfer_Click(object sender, EventArgs e) {
            deselectAllTabs();
            tbFundTransfer.Checked = true;
            hideExpensesColumns();
        }

        // Load fund transfer data source on check.
        private void tbFundTransfer_CheckedChanged(object sender, EventArgs e) {
            if (tbFundTransfer.Checked) {
                //LOAD FUND TRANSFER DATA SOURCE
                refreshRecords();
            }
        }

        // Load invalid records on check
        private void tbInvalid_CheckedChanged(object sender, EventArgs e) {
            if (tbInvalid.Checked) {
                refreshRecords();
            }
        }

        // Load expenses on check.
        private void tbExpenses_CheckedChanged(object sender, EventArgs e) {
            if (tbExpenses.Checked) {
                //LOAD EXPENSES TRANSFER DATA SOURCE
                refreshRecords();
            }
        }

        //Disable selections when on invalid tab
        private void RecordsDataGrid_SelectionChanged(object sender, EventArgs e) {
            if (tbInvalid.Checked)
                RecordsDataGrid.ClearSelection();
        }

        #endregion

        #region local functions

        //Search indexed employee items for match.
        string findEmployeeIndexed(string key) {
            foreach (Employee e in _indexedEmployees) {
                bool res = e.CompareEmployeeNameString(key);
                if (res) return e.EmployeeID;
            }
            return String.Empty;
        }

        //search all employee database for match.
        string findEmployeeAllList(string key) {
            foreach (Employee e in _employees) {
                bool match = e.CompareEmployeeNameString(key);
                if (match) {
                    //if found, add to indexed.
                    _indexedEmployees.Add(e);
                    return e.EmployeeID;
                }
            }
            return String.Empty;
        }

        //Fit column width based on content.
        void fitCellWidth() {
            for(int i = 0; i < RecordsDataGrid.Columns.Count; i++) {
                DataGridViewColumn col = RecordsDataGrid.Columns[i];
                int nwidth = 0;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                nwidth = col.Width;
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = nwidth;
            }
        }

        //Refresh records, hide/show records depending on selected category
        void refreshRecords() {
            if(tbExpenses.Checked)
                _DATASRC.DefaultView.RowFilter = "[FT/EX] = 'EX'";
            else if (tbFundTransfer.Checked)
                _DATASRC.DefaultView.RowFilter = "[FT/EX] = 'FT'";
            else if (tbInvalid.Checked)
                _DATASRC.DefaultView.RowFilter = "[FT/EX] = 'INV'";
            fitCellWidth();
        }

        //Show remarks column.
        void ShowRemarksColumn() {
            RecordsDataGrid.Columns["Remarks"].Visible = true;
        }

        //Hide remarks column.
        void HideRemarksColumn() {
            RecordsDataGrid.Columns["Remarks"].Visible = false;
        }

        //Show fuels, rmv, blah blah blah columns.
        void showExpensesColumns() {
            RecordsDataGrid.Columns["Petron Sucat Diesel"].Visible = true;
            RecordsDataGrid.Columns["Other Diesel"].Visible = true;
            RecordsDataGrid.Columns["Violations"].Visible = true;
            RecordsDataGrid.Columns["Toll"].Visible = true;
            RecordsDataGrid.Columns["Trucking Expenses"].Visible = true;
            RecordsDataGrid.Columns["RMV"].Visible = true;
            RecordsDataGrid.Columns["Service Fee"].Visible = true;
            RecordsDataGrid.Columns["Others"].Visible = true;
            RecordsDataGrid.Columns["Driver"].Visible = true;
            RecordsDataGrid.Columns["Driver CA"].Visible = true;
            RecordsDataGrid.Columns["Helper"].Visible = true;
            RecordsDataGrid.Columns["Helper CA"].Visible = true;
            HideRemarksColumn();
        }

        //Hide fuels, rmv, and other expenses columns
        void hideExpensesColumns() {
            RecordsDataGrid.Columns["Petron Sucat Diesel"].Visible = false;
            RecordsDataGrid.Columns["Other Diesel"].Visible = false;
            RecordsDataGrid.Columns["Violations"].Visible = false;
            RecordsDataGrid.Columns["Toll"].Visible = false;
            RecordsDataGrid.Columns["Trucking Expenses"].Visible = false;
            RecordsDataGrid.Columns["RMV"].Visible = false;
            RecordsDataGrid.Columns["Service Fee"].Visible = false;
            RecordsDataGrid.Columns["Others"].Visible = false;
            RecordsDataGrid.Columns["Driver"].Visible = false;
            RecordsDataGrid.Columns["Driver CA"].Visible = false;
            RecordsDataGrid.Columns["Helper"].Visible = false;
            RecordsDataGrid.Columns["Helper CA"].Visible = false;
            HideRemarksColumn();
        }

        #endregion

    }
}
