﻿//.NET Namespaces
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

//CsvHelper Namespace
using CsvHelper;

namespace PayrollSystem {

    class ImportTruckingLogDialog : FileMaintenanceDialog {

        #region Controls
        
        private Label label2;
        private LinkLabel lnkCSVWiki;
        private Label label4;
        private Label label3;
        private BufferedPanel MoreInfoPanel;
        private LinkLabel lnkMoreInfo;
        protected internal BufferedDataGridView RecordsDataGrid;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private Button btnSelectFile;
        private Label lblStatus;
        private Label label1;

        #endregion

        #region Constructor

        //Constructor.
        public ImportTruckingLogDialog() {
            InitializeComponent(); 

            //Collapse more info.
            Height = 420;
            lnkMoreInfo.Text = "More Info...";
        }
        
        //Form designer initialization function.
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportTruckingLogDialog));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lnkCSVWiki = new System.Windows.Forms.LinkLabel();
            this.MoreInfoPanel = new PayrollSystem.BufferedPanel();
            this.lnkMoreInfo = new System.Windows.Forms.LinkLabel();
            this.RecordsDataGrid = new PayrollSystem.BufferedDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.MoreInfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lblStatus);
            this.ContentPanel.Controls.Add(this.btnSelectFile);
            this.ContentPanel.Controls.Add(this.RecordsDataGrid);
            this.ContentPanel.Controls.Add(this.lnkMoreInfo);
            this.ContentPanel.Controls.Add(this.MoreInfoPanel);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(604, 443);
            this.ContentPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(577, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "You can add multiple trucking log records at once by importing CSV files that con" +
    "tains the records. CSV files (*.csv) can be made via any text editor or spreadsh" +
    "eet application.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(577, 42);
            this.label2.TabIndex = 2;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 43);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(577, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Date,Plate Number,DR No,Destination,Rate,Driver,Driver Rate,Helper,Helper Rate";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(0, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "For more info about creating CSV files, visit:";
            // 
            // lnkCSVWiki
            // 
            this.lnkCSVWiki.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkCSVWiki.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkCSVWiki.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkCSVWiki.Location = new System.Drawing.Point(265, 67);
            this.lnkCSVWiki.Name = "lnkCSVWiki";
            this.lnkCSVWiki.Size = new System.Drawing.Size(312, 25);
            this.lnkCSVWiki.TabIndex = 5;
            this.lnkCSVWiki.TabStop = true;
            this.lnkCSVWiki.Text = "Wikipedia - Comma Separated Values (CSV)";
            this.lnkCSVWiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkCSVWiki.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkCSVWiki.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCSVWiki_LinkClicked);
            // 
            // MoreInfoPanel
            // 
            this.MoreInfoPanel.BorderColor = System.Drawing.Color.Black;
            this.MoreInfoPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.MoreInfoPanel.Controls.Add(this.label2);
            this.MoreInfoPanel.Controls.Add(this.lnkCSVWiki);
            this.MoreInfoPanel.Controls.Add(this.label3);
            this.MoreInfoPanel.Controls.Add(this.label4);
            this.MoreInfoPanel.Location = new System.Drawing.Point(12, 53);
            this.MoreInfoPanel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MoreInfoPanel.Name = "MoreInfoPanel";
            this.MoreInfoPanel.Size = new System.Drawing.Size(577, 93);
            this.MoreInfoPanel.TabIndex = 6;
            this.MoreInfoPanel.Visible = false;
            // 
            // lnkMoreInfo
            // 
            this.lnkMoreInfo.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkMoreInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkMoreInfo.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.Location = new System.Drawing.Point(12, 157);
            this.lnkMoreInfo.Name = "lnkMoreInfo";
            this.lnkMoreInfo.Size = new System.Drawing.Size(577, 15);
            this.lnkMoreInfo.TabIndex = 1;
            this.lnkMoreInfo.TabStop = true;
            this.lnkMoreInfo.Text = "More Info...";
            this.lnkMoreInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkMoreInfo.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkMoreInfo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkMoreInfo_LinkClicked);
            // 
            // RecordsDataGrid
            // 
            this.RecordsDataGrid.AllowUserToAddRows = false;
            this.RecordsDataGrid.AllowUserToDeleteRows = false;
            this.RecordsDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.RecordsDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.RecordsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.RecordsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RecordsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.RecordsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.RecordsDataGrid.ColumnHeadersHeight = 28;
            this.RecordsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.RecordsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.RecordsDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.RecordsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.RecordsDataGrid.Location = new System.Drawing.Point(0, 213);
            this.RecordsDataGrid.MultiSelect = false;
            this.RecordsDataGrid.Name = "RecordsDataGrid";
            this.RecordsDataGrid.ReadOnly = true;
            this.RecordsDataGrid.RowHeadersVisible = false;
            this.RecordsDataGrid.RowTemplate.Height = 28;
            this.RecordsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RecordsDataGrid.ShowCellErrors = false;
            this.RecordsDataGrid.ShowEditingIcon = false;
            this.RecordsDataGrid.Size = new System.Drawing.Size(604, 230);
            this.RecordsDataGrid.TabIndex = 8;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectFile.FlatAppearance.BorderSize = 0;
            this.btnSelectFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSelectFile.Image = global::PayrollSystem.Properties.Resources.import;
            this.btnSelectFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelectFile.Location = new System.Drawing.Point(12, 179);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(165, 28);
            this.btnSelectFile.TabIndex = 0;
            this.btnSelectFile.Text = "  Import from CSV";
            this.btnSelectFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblStatus.Location = new System.Drawing.Point(224, 187);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(365, 18);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "12 Records Found: 11 Records OK, 1 Invalid";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ImportTruckingLogDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(604, 493);
            this.Name = "ImportTruckingLogDialog";
            this.Text = "Import Trucking Log CSV";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.ImportTruckingLogDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.ImportTruckingLogDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.MoreInfoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
        #region Local Variables

        //Data source for the data grid view.
        DataTable dt_records = new DataTable();

        //IEnumerables for records lookup.
        IEnumerable<DataRow> ePlateNo, eDestinations, eEmployees;

        //Record counters.
        int recordCounter = 0, recordOKCounter = 0, recordFailCounter = 0;

        //Records list.
        List<String[]> lstRecords = new List<String[]>();
        List<TruckingLogRecord> lstFinalRecords = new List<TruckingLogRecord>();

        #endregion

        #region Public Functions

        //Returns collection of valid date to be inserted to the trucking log records.
        public List<TruckingLogRecord> GetFieldValues() {
            return lstFinalRecords;
        }

        #endregion

        #region Local Functions

        //Initialize data table of records with columns.
        void InitializeRecordsDataSource() {
            if (dt_records != null) dt_records.Dispose();
            dt_records = new DataTable();

            dt_records.Columns.Add("Date", typeof(string));
            dt_records.Columns.Add("Plate No", typeof(string));
            dt_records.Columns.Add("DR No", typeof(string));
            dt_records.Columns.Add("Destination", typeof(string));
            dt_records.Columns.Add("Rate", typeof(double));
            dt_records.Columns.Add("Driver", typeof(string));
            dt_records.Columns.Add("Driver Rate", typeof(double));
            dt_records.Columns.Add("Helper", typeof(string));
            dt_records.Columns.Add("Helper Rate", typeof(double));
        }

        //Load lookup values then store to IEnumerable.
        void LoadLookup() {
            //Load employees
            String query = "SELECT EmployeeID, FirstName, MiddleName, LastName FROM Employee ";
            query += "WHERE IsArchived = false";
            eEmployees = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false).Rows.Cast<DataRow>();

            //Load plate numbers.
            query = "SELECT PlateNo, B.Type FROM Vehicles AS A INNER JOIN VehicleType AS B ON A.VehicleType = B.ID ";
            ePlateNo = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false).Rows.Cast<DataRow>();

            //Load rates.
            query = "SELECT ID, Destination FROM Rates ";
            eDestinations = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false).Rows.Cast<DataRow>();
        }
        
        //Parse CSV using CsvHelper
        void ParseCSV(string filename) {
            try {
                using (StreamReader fs = File.OpenText(filename)) {
                    //Variables for record count

                    //List of records read.
                    lstRecords.Clear();
                    using (CsvReader csv = new CsvReader(fs)) {

                        //Read records
                        csv.Configuration.HasHeaderRecord = false;
                        while (csv.Read()) {
                            //Determine if record is complete, if not move on to the next one.
                            if (csv.CurrentRecord.Count() != 9) continue;

                            //Read data.
                            string[] data = new string[9];
                            data.Initialize();

                            for (int i = 0; i < data.Length; i++)
                                csv.TryGetField<string>(i, out data[i]);

                            //Add record to list.
                            lstRecords.Add(data);

                        }
                    }

                    //Close file stream.
                    fs.Close();

                    //Verify records to insert, then display to DataGridView.
                    VerifyRecords();
                }
            } catch (Exception ex) {
                Notifier.ShowError(ex.Message);
            }

        }

        //Count status of parsed CSV.
        void SetStatus(int totalRecords, int validRecords, int invalidRecords) {
            lblStatus.Text = totalRecords + " records found: " + validRecords + " records OK, " + invalidRecords + " invalid";
        }

        //Clear status of CSV counter.
        void ClearStatus() {
            lblStatus.Text = "";
        }

        //Function to verify if records are valid to be inserted. If it is, show it in the datagridview.
        void VerifyRecords() {
            //Clear the data source.
            dt_records.Rows.Clear();

            //Reset the final records list.
            lstFinalRecords.Clear();

            //Reset counters
            recordCounter = recordOKCounter = recordFailCounter = 0;

            //Make formatted name into IEnumerable.
            IEnumerable<String> nameList = eEmployees.Select(x => (x["LastName"] + ", " + x["FirstName"]).Trim().ToLower());
            nameList = nameList.Union(eEmployees.Select(x => (x["FirstName"] + " " + x["LastName"]).Trim().ToLower()));
            nameList = nameList.Union(eEmployees.Select(x => String.Join(" ", new String[] { x["FirstName"].ToString(), x["MiddleName"].ToString(), x["LastName"].ToString()}.Where(z => !string.IsNullOrEmpty(z))).Trim().ToLower()));
            nameList = nameList.Union(eEmployees.Select(x => x["LastName"].ToString() + ", " + String.Join(" ", new String[] { x["FirstName"].ToString(), x["MiddleName"].ToString()}.Where(z => !string.IsNullOrEmpty(z))).Trim().ToLower()));

            //Final list of names on all formats (List is faster).
            List<String> nameListFinal = nameList.ToList();
            nameListFinal.Add("");

            //List of employees.
            List<DataRow> lstEmployees = eEmployees.ToList();
           
            //Loop through each records.
            foreach (string[] rec in lstRecords) {
                //Read all fields, then convert to respective data types.
                double rate = 0.0, driverRate = 0.0, helperRate = 0.0;

                string date = rec[0];
                string plateno = rec[1].Trim();
                string drno = rec[2];
                string destination = rec[3];
                string driver = rec[5].Trim();
                string driverID = "";
                string helper = rec[7].Trim();
                string helperID = "";

                bool isbackload = false;

                Double.TryParse(rec[4], out rate);
                Double.TryParse(rec[6], out driverRate);
                Double.TryParse(rec[8], out helperRate);


                //Verify each fields if it is correct.

                //Verify date.
                DateTime rDate = DateTime.Now;
                if (DateTime.TryParse(date, out rDate)) {
                    date = rDate.ToShortDateString();
                } else {
                    //Date is invalid format, skip.
                    recordFailCounter++;
                    continue;
                }

                //Verify plate number if existent in database.
                if (ePlateNo.Select(x => x["PlateNo"].ToString().Trim().ToLower()).Contains(plateno.ToLower()) == false) {
                    //Plate number doesn't exist from records. Ignore.
                    recordFailCounter++;
                    continue;
                }

                //Get vehicle type from plate number.
                string vtype = ePlateNo.Where(x => x["PlateNo"].ToString().Trim().Equals(plateno, StringComparison.CurrentCultureIgnoreCase)).Select(x => x["Type"].ToString()).First();
                
                //Parse if "(backload)" is included in destination.
                if (destination.Trim().ToLower().Contains("(backload)")) {
                    isbackload = true;
                    int n = destination.Trim().ToLower().IndexOf("(backload)");
                    destination = destination.Remove(n).Trim();
                }

                //Verify Driver
                if (driver.Trim().Equals("") == false) {
                    bool employeeMatch = false;
                    foreach (DataRow eRow in lstEmployees) {
                        //First name, Middle name
                        string[] npart = { eRow["FirstName"].ToString(), eRow["MiddleName"].ToString() };

                        // First Middle Last
                        string f_fml = string.Join(" ", npart.Where(x => !string.IsNullOrEmpty(x))) + " " + eRow["LastName"].ToString();

                        //Last, First Middle
                        string f_lfm = eRow["LastName"].ToString() + ", " + string.Join(" ", npart.Where(x => !string.IsNullOrEmpty(x)));

                        //First Last
                        string f_fl = eRow["FirstName"] + " " + eRow["LastName"];

                        //Last, First
                        string f_lf = eRow["LastName"] + ", " + eRow["FirstName"];
                        
                        //Check name if exists on all formats.
                        if (driver.Equals(f_fml, StringComparison.CurrentCultureIgnoreCase) || driver.Equals(f_lfm, StringComparison.CurrentCultureIgnoreCase) ||
                           driver.Equals(f_fl, StringComparison.CurrentCultureIgnoreCase) || driver.Equals(f_lf, StringComparison.CurrentCultureIgnoreCase)) {

                            employeeMatch = true;
                            driverID = "{" + eRow["EmployeeID"].ToString() + "}";
                            break;
                        }
                    }

                    if (employeeMatch == false) {
                        //Employee does not exist on records.
                        recordFailCounter++;
                        continue;
                    }
                }

                //Verify Helper
                if (helper.Trim().Equals("") == false) {
                    bool employeeMatch = false;
                    foreach (DataRow eRow in lstEmployees) {
                        //First name, Middle name
                        string[] npart = { eRow["FirstName"].ToString(), eRow["MiddleName"].ToString() };

                        // First Middle Last
                        string f_fml = string.Join(" ", npart.Where(x => !string.IsNullOrEmpty(x))) + " " + eRow["LastName"].ToString();

                        //Last, First Middle
                        string f_lfm = eRow["LastName"].ToString() + ", " + string.Join(" ", npart.Where(x => !string.IsNullOrEmpty(x)));

                        //First Last
                        string f_fl = eRow["FirstName"] + " " + eRow["LastName"];

                        //Last, First
                        string f_lf = eRow["LastName"] + ", " + eRow["FirstName"];

                        //Check name if exists on all formats.
                        if (helper.Equals(f_fml, StringComparison.CurrentCultureIgnoreCase) || helper.Equals(f_lfm, StringComparison.CurrentCultureIgnoreCase) ||
                           helper.Equals(f_fl, StringComparison.CurrentCultureIgnoreCase) || helper.Equals(f_lf, StringComparison.CurrentCultureIgnoreCase)) {
                            employeeMatch = true;
                            helperID = "{" + eRow["EmployeeID"].ToString() + "}";
                            break;
                        }
                    }

                    if (employeeMatch == false) {
                        //Employee does not exist on records.
                        recordFailCounter++;
                        continue;
                    }
                }
                
                /*
                //Verify Driver.
                if (nameListFinal.Contains(driver.Trim().ToLower()) == false) {
                    //Employee does not exist from records.
                    recordFailCounter++;
                    continue;
                }

                //Verify Helper.
                if (nameListFinal.Contains(helper.Trim().ToLower()) == false) {
                    //Employee does not exist from records.
                    recordFailCounter++;
                    continue;
                }
                */

                //Add to final list collection.
                TruckingLogRecord fRec = new TruckingLogRecord(date, plateno, drno, destination, rate, driverID, driverRate, helperID, helperRate, isbackload);
                lstFinalRecords.Add(fRec);
                
                //Add "(Backload)" to destinations that are backloads.
                if (isbackload)
                    destination += " (Backload)";

                //Insert data as new row to the datasource of the datagridview.
                DataRow row = dt_records.NewRow();
                row[0] = date;
                row[1] = plateno;
                row[2] = drno;
                row[3] = destination;
                row[4] = rate;
                row[5] = driver;
                row[6] = driverRate;
                row[7] = helper;
                row[8] = helperRate;
                dt_records.Rows.Add(row);
                recordOKCounter++;
            }

            //Set record counter
            recordCounter = lstRecords.Count;

            //Show count status
            SetStatus(recordCounter, recordOKCounter, recordFailCounter);
        }

        #endregion

        #region Window Event handlers

        //Handle window load event.
        private void ImportTruckingLogDialog_Load(object sender, EventArgs e) {
            //Set title
            Text = "Import CSV";

            //Initialie DataTable and set it as DataSource of DataGridViewer.
            InitializeRecordsDataSource();
            RecordsDataGrid.Columns.Clear();
            RecordsDataGrid.DataSource = dt_records;

            //Set format of column.
            RecordsDataGrid.Columns["Rate"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Driver Rate"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Helper Rate"].DefaultCellStyle.Format = "#,##0.00";

            //Double buffer DataGridView
            Extensions.DGVDoubleBuffered(RecordsDataGrid, true);

            //Clear status label
            ClearStatus();
            
            //Load lookup values.
            LoadLookup();
        }

        #endregion

        #region Control Event handlers

        //Open file.
        private void btnSelectFile_Click(object sender, EventArgs e) {
            using (OpenFileDialog dlg = new OpenFileDialog()) {
                dlg.Filter = "Comma Separated Values - CSV (*.csv)|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    ParseCSV(dlg.FileName);
                }
            }
        }

        //Event handler when the CSV Wikipedia link was clicked.
        private void lnkCSVWiki_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start("https://en.wikipedia.org/wiki/Comma-separated_values");
        }

        //Show/hide info about CSVs and whatsoever.
        private void lnkMoreInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            //420, 532
            MoreInfoPanel.Visible = !MoreInfoPanel.Visible;
            if (MoreInfoPanel.Visible) {
                Height = 522;
                lnkMoreInfo.Text = "Less Info...";
            } else {
                Height = 420;
                lnkMoreInfo.Text = "More Info...";
            }
        }

        #endregion
                
        #region Validation

        //Handle OK Button click.
        private void ImportTruckingLogDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (lstFinalRecords.Count <= 0) {
                Notifier.ShowWarning("There are no trucking logs to import.", "Import Records");
                return;
            }

            if (Notifier.ShowConfirm("You are about to import " + lstFinalRecords.Count + " record(s). Do you want to continue?", "Confirm Import") == System.Windows.Forms.DialogResult.No)
                return;

            e.Cancel = false;
        }

        #endregion

    }

}