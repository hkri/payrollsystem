﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class IncomeExpenseDialog : FileMaintenanceDialog {

        #region Controls

        private DateTimePicker dpDate;
        private Label label2;
        internal TextBox txtParticular;
        private Label label1;
        private Label label4;
        private CheckBox chkBreakdown;
        internal TextBox txtAmnt;
        private Label label6;
        private Label label5;
        private Label lblFuel;
        internal TextBox txtFuel;
        private Label label8;
        private BufferedPanel AmountDetailsPanel;
        private Label lblTExpenses;
        internal TextBox txtTExpenses;
        private Label lblToll;
        internal TextBox txtToll;
        private Label lblViolatons;
        internal TextBox txtViolations;
        private Label lblOthers;
        internal TextBox txtOthers;
        private Label lblSF;
        internal TextBox txtSF;
        private Label lblRMV;
        internal TextBox txtRMV;
        private Label label15;
        private Label lblHelperCA;
        internal TextBox txtHelperCA;
        private Label lblHelper;
        private Label lblDriverCA;
        internal TextBox txtDriverCA;
        private Label lblDriver;
        private BufferedPanel bufferedPanel1;
        private BufferedPanel bufferedPanel2;
        internal ComboBox cboHelper;
        internal ComboBox cboDriver;
        internal ComboBox cboPlateNo;
        private Label lblBreakdown;
        private Label label7;
        internal TextBox txtPetronSucatDiesel;
        private ToolTip toolTip1;
        private IContainer components;
        private Label label3;

        #endregion

        #region Constructor

        //Constructor
        public IncomeExpenseDialog() {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        //VS Designer initializations.
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.txtParticular = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dpDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAmnt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkBreakdown = new System.Windows.Forms.CheckBox();
            this.txtFuel = new System.Windows.Forms.TextBox();
            this.lblFuel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AmountDetailsPanel = new PayrollSystem.BufferedPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPetronSucatDiesel = new System.Windows.Forms.TextBox();
            this.cboHelper = new System.Windows.Forms.ComboBox();
            this.cboDriver = new System.Windows.Forms.ComboBox();
            this.bufferedPanel2 = new PayrollSystem.BufferedPanel();
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.lblHelperCA = new System.Windows.Forms.Label();
            this.txtHelperCA = new System.Windows.Forms.TextBox();
            this.lblHelper = new System.Windows.Forms.Label();
            this.lblDriverCA = new System.Windows.Forms.Label();
            this.txtDriverCA = new System.Windows.Forms.TextBox();
            this.lblDriver = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblOthers = new System.Windows.Forms.Label();
            this.txtOthers = new System.Windows.Forms.TextBox();
            this.lblSF = new System.Windows.Forms.Label();
            this.txtSF = new System.Windows.Forms.TextBox();
            this.lblRMV = new System.Windows.Forms.Label();
            this.txtRMV = new System.Windows.Forms.TextBox();
            this.lblTExpenses = new System.Windows.Forms.Label();
            this.txtTExpenses = new System.Windows.Forms.TextBox();
            this.lblToll = new System.Windows.Forms.Label();
            this.txtToll = new System.Windows.Forms.TextBox();
            this.lblViolatons = new System.Windows.Forms.Label();
            this.txtViolations = new System.Windows.Forms.TextBox();
            this.cboPlateNo = new System.Windows.Forms.ComboBox();
            this.lblBreakdown = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ContentPanel.SuspendLayout();
            this.AmountDetailsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lblBreakdown);
            this.ContentPanel.Controls.Add(this.cboPlateNo);
            this.ContentPanel.Controls.Add(this.chkBreakdown);
            this.ContentPanel.Controls.Add(this.txtAmnt);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.dpDate);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.txtParticular);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.AmountDetailsPanel);
            this.ContentPanel.Size = new System.Drawing.Size(642, 461);
            this.ContentPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(276, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Basic Info";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtParticular
            // 
            this.txtParticular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtParticular.Location = new System.Drawing.Point(114, 48);
            this.txtParticular.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtParticular.MaxLength = 128;
            this.txtParticular.Name = "txtParticular";
            this.txtParticular.Size = new System.Drawing.Size(174, 21);
            this.txtParticular.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Particular:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dpDate
            // 
            this.dpDate.CustomFormat = "MMMM dd, yyyy";
            this.dpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpDate.Location = new System.Drawing.Point(114, 76);
            this.dpDate.Name = "dpDate";
            this.dpDate.Size = new System.Drawing.Size(174, 21);
            this.dpDate.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.label4.Location = new System.Drawing.Point(12, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "Plate No:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label5.Location = new System.Drawing.Point(12, 136);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(276, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "Amount";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAmnt
            // 
            this.txtAmnt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtAmnt.Location = new System.Drawing.Point(114, 170);
            this.txtAmnt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtAmnt.MaxLength = 32;
            this.txtAmnt.Name = "txtAmnt";
            this.txtAmnt.Size = new System.Drawing.Size(174, 21);
            this.txtAmnt.TabIndex = 3;
            this.txtAmnt.Text = "0.00";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.label6.Location = new System.Drawing.Point(12, 170);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Amount:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkBreakdown
            // 
            this.chkBreakdown.AutoSize = true;
            this.chkBreakdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.chkBreakdown.Location = new System.Drawing.Point(114, 201);
            this.chkBreakdown.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkBreakdown.Name = "chkBreakdown";
            this.chkBreakdown.Size = new System.Drawing.Size(177, 17);
            this.chkBreakdown.TabIndex = 4;
            this.chkBreakdown.Text = "Compute from Breakdown";
            this.toolTip1.SetToolTip(this.chkBreakdown, "Note: If this checkbox is ticked, amount will be computed based\r\non the amounts y" +
        "ou\'ve entered on each item under the \"Amount\r\nBreakdown\" and \"Cash Advances\" sec" +
        "tions.");
            this.chkBreakdown.UseVisualStyleBackColor = true;
            this.chkBreakdown.CheckedChanged += new System.EventHandler(this.chkBreakdown_CheckedChanged);
            // 
            // txtFuel
            // 
            this.txtFuel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtFuel.Location = new System.Drawing.Point(148, 79);
            this.txtFuel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtFuel.MaxLength = 32;
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(174, 21);
            this.txtFuel.TabIndex = 1;
            this.txtFuel.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtFuel.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblFuel
            // 
            this.lblFuel.Location = new System.Drawing.Point(24, 79);
            this.lblFuel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblFuel.Name = "lblFuel";
            this.lblFuel.Size = new System.Drawing.Size(118, 18);
            this.lblFuel.TabIndex = 18;
            this.lblFuel.Text = "Other Diesel:";
            this.lblFuel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label8.Location = new System.Drawing.Point(24, 14);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5);
            this.label8.Size = new System.Drawing.Size(298, 24);
            this.label8.TabIndex = 19;
            this.label8.Text = "Amount Breakdown";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AmountDetailsPanel
            // 
            this.AmountDetailsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.AmountDetailsPanel.BorderColor = System.Drawing.Color.Black;
            this.AmountDetailsPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.AmountDetailsPanel.Controls.Add(this.label7);
            this.AmountDetailsPanel.Controls.Add(this.txtPetronSucatDiesel);
            this.AmountDetailsPanel.Controls.Add(this.cboHelper);
            this.AmountDetailsPanel.Controls.Add(this.cboDriver);
            this.AmountDetailsPanel.Controls.Add(this.bufferedPanel2);
            this.AmountDetailsPanel.Controls.Add(this.bufferedPanel1);
            this.AmountDetailsPanel.Controls.Add(this.lblHelperCA);
            this.AmountDetailsPanel.Controls.Add(this.txtHelperCA);
            this.AmountDetailsPanel.Controls.Add(this.lblHelper);
            this.AmountDetailsPanel.Controls.Add(this.lblDriverCA);
            this.AmountDetailsPanel.Controls.Add(this.txtDriverCA);
            this.AmountDetailsPanel.Controls.Add(this.lblDriver);
            this.AmountDetailsPanel.Controls.Add(this.label15);
            this.AmountDetailsPanel.Controls.Add(this.lblOthers);
            this.AmountDetailsPanel.Controls.Add(this.txtOthers);
            this.AmountDetailsPanel.Controls.Add(this.lblSF);
            this.AmountDetailsPanel.Controls.Add(this.txtSF);
            this.AmountDetailsPanel.Controls.Add(this.lblRMV);
            this.AmountDetailsPanel.Controls.Add(this.txtRMV);
            this.AmountDetailsPanel.Controls.Add(this.lblTExpenses);
            this.AmountDetailsPanel.Controls.Add(this.txtTExpenses);
            this.AmountDetailsPanel.Controls.Add(this.lblToll);
            this.AmountDetailsPanel.Controls.Add(this.txtToll);
            this.AmountDetailsPanel.Controls.Add(this.lblViolatons);
            this.AmountDetailsPanel.Controls.Add(this.txtViolations);
            this.AmountDetailsPanel.Controls.Add(this.label8);
            this.AmountDetailsPanel.Controls.Add(this.lblFuel);
            this.AmountDetailsPanel.Controls.Add(this.txtFuel);
            this.AmountDetailsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.AmountDetailsPanel.Location = new System.Drawing.Point(305, 0);
            this.AmountDetailsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.AmountDetailsPanel.Name = "AmountDetailsPanel";
            this.AmountDetailsPanel.Size = new System.Drawing.Size(337, 461);
            this.AmountDetailsPanel.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 18);
            this.label7.TabIndex = 44;
            this.label7.Text = "Petron Sucat Diesel:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPetronSucatDiesel
            // 
            this.txtPetronSucatDiesel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtPetronSucatDiesel.Location = new System.Drawing.Point(148, 48);
            this.txtPetronSucatDiesel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPetronSucatDiesel.MaxLength = 32;
            this.txtPetronSucatDiesel.Name = "txtPetronSucatDiesel";
            this.txtPetronSucatDiesel.Size = new System.Drawing.Size(174, 21);
            this.txtPetronSucatDiesel.TabIndex = 0;
            // 
            // cboHelper
            // 
            this.cboHelper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHelper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboHelper.FormattingEnabled = true;
            this.cboHelper.Location = new System.Drawing.Point(148, 392);
            this.cboHelper.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboHelper.Name = "cboHelper";
            this.cboHelper.Size = new System.Drawing.Size(174, 21);
            this.cboHelper.TabIndex = 10;
            this.cboHelper.Leave += new System.EventHandler(this.EmployeePicker_Leave);
            // 
            // cboDriver
            // 
            this.cboDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDriver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboDriver.FormattingEnabled = true;
            this.cboDriver.Location = new System.Drawing.Point(148, 330);
            this.cboDriver.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboDriver.Name = "cboDriver";
            this.cboDriver.Size = new System.Drawing.Size(174, 21);
            this.cboDriver.TabIndex = 8;
            this.cboDriver.Leave += new System.EventHandler(this.EmployeePicker_Leave);
            // 
            // bufferedPanel2
            // 
            this.bufferedPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(212)))), ((int)(((byte)(215)))));
            this.bufferedPanel2.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel2.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bufferedPanel2.Location = new System.Drawing.Point(1, 0);
            this.bufferedPanel2.Name = "bufferedPanel2";
            this.bufferedPanel2.Size = new System.Drawing.Size(1, 461);
            this.bufferedPanel2.TabIndex = 42;
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackColor = System.Drawing.Color.White;
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 0);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(1, 461);
            this.bufferedPanel1.TabIndex = 41;
            // 
            // lblHelperCA
            // 
            this.lblHelperCA.Location = new System.Drawing.Point(24, 423);
            this.lblHelperCA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblHelperCA.Name = "lblHelperCA";
            this.lblHelperCA.Size = new System.Drawing.Size(118, 18);
            this.lblHelperCA.TabIndex = 40;
            this.lblHelperCA.Text = "Helper CA:";
            this.lblHelperCA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtHelperCA
            // 
            this.txtHelperCA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtHelperCA.Location = new System.Drawing.Point(148, 423);
            this.txtHelperCA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtHelperCA.MaxLength = 32;
            this.txtHelperCA.Name = "txtHelperCA";
            this.txtHelperCA.Size = new System.Drawing.Size(174, 21);
            this.txtHelperCA.TabIndex = 11;
            this.txtHelperCA.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtHelperCA.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblHelper
            // 
            this.lblHelper.Location = new System.Drawing.Point(24, 392);
            this.lblHelper.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblHelper.Name = "lblHelper";
            this.lblHelper.Size = new System.Drawing.Size(118, 18);
            this.lblHelper.TabIndex = 38;
            this.lblHelper.Text = "Helper:";
            this.lblHelper.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDriverCA
            // 
            this.lblDriverCA.Location = new System.Drawing.Point(24, 361);
            this.lblDriverCA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblDriverCA.Name = "lblDriverCA";
            this.lblDriverCA.Size = new System.Drawing.Size(118, 18);
            this.lblDriverCA.TabIndex = 36;
            this.lblDriverCA.Text = "Driver CA:";
            this.lblDriverCA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDriverCA
            // 
            this.txtDriverCA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtDriverCA.Location = new System.Drawing.Point(148, 361);
            this.txtDriverCA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDriverCA.MaxLength = 32;
            this.txtDriverCA.Name = "txtDriverCA";
            this.txtDriverCA.Size = new System.Drawing.Size(174, 21);
            this.txtDriverCA.TabIndex = 9;
            this.txtDriverCA.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtDriverCA.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblDriver
            // 
            this.lblDriver.Location = new System.Drawing.Point(24, 330);
            this.lblDriver.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblDriver.Name = "lblDriver";
            this.lblDriver.Size = new System.Drawing.Size(118, 18);
            this.lblDriver.TabIndex = 34;
            this.lblDriver.Text = "Driver:";
            this.lblDriver.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label15.Location = new System.Drawing.Point(24, 296);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(5);
            this.label15.Size = new System.Drawing.Size(298, 24);
            this.label15.TabIndex = 32;
            this.label15.Text = "Cash Advances";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOthers
            // 
            this.lblOthers.Location = new System.Drawing.Point(24, 265);
            this.lblOthers.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Size = new System.Drawing.Size(118, 18);
            this.lblOthers.TabIndex = 31;
            this.lblOthers.Text = "Others:";
            this.lblOthers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOthers
            // 
            this.txtOthers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtOthers.Location = new System.Drawing.Point(148, 265);
            this.txtOthers.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtOthers.MaxLength = 32;
            this.txtOthers.Name = "txtOthers";
            this.txtOthers.Size = new System.Drawing.Size(174, 21);
            this.txtOthers.TabIndex = 7;
            this.txtOthers.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtOthers.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblSF
            // 
            this.lblSF.Location = new System.Drawing.Point(24, 234);
            this.lblSF.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblSF.Name = "lblSF";
            this.lblSF.Size = new System.Drawing.Size(118, 18);
            this.lblSF.TabIndex = 29;
            this.lblSF.Text = "Service Fee:";
            this.lblSF.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSF
            // 
            this.txtSF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtSF.Location = new System.Drawing.Point(148, 234);
            this.txtSF.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtSF.MaxLength = 32;
            this.txtSF.Name = "txtSF";
            this.txtSF.Size = new System.Drawing.Size(174, 21);
            this.txtSF.TabIndex = 6;
            this.txtSF.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtSF.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblRMV
            // 
            this.lblRMV.Location = new System.Drawing.Point(24, 203);
            this.lblRMV.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblRMV.Name = "lblRMV";
            this.lblRMV.Size = new System.Drawing.Size(118, 18);
            this.lblRMV.TabIndex = 27;
            this.lblRMV.Text = "RM Vehicle:";
            this.lblRMV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRMV
            // 
            this.txtRMV.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtRMV.Location = new System.Drawing.Point(148, 203);
            this.txtRMV.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtRMV.MaxLength = 32;
            this.txtRMV.Name = "txtRMV";
            this.txtRMV.Size = new System.Drawing.Size(174, 21);
            this.txtRMV.TabIndex = 5;
            this.txtRMV.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtRMV.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblTExpenses
            // 
            this.lblTExpenses.Location = new System.Drawing.Point(24, 172);
            this.lblTExpenses.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblTExpenses.Name = "lblTExpenses";
            this.lblTExpenses.Size = new System.Drawing.Size(118, 18);
            this.lblTExpenses.TabIndex = 25;
            this.lblTExpenses.Text = "Trucking Expenses:";
            this.lblTExpenses.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTExpenses
            // 
            this.txtTExpenses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtTExpenses.Location = new System.Drawing.Point(148, 172);
            this.txtTExpenses.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTExpenses.MaxLength = 32;
            this.txtTExpenses.Name = "txtTExpenses";
            this.txtTExpenses.Size = new System.Drawing.Size(174, 21);
            this.txtTExpenses.TabIndex = 4;
            this.txtTExpenses.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtTExpenses.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblToll
            // 
            this.lblToll.Location = new System.Drawing.Point(24, 141);
            this.lblToll.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblToll.Name = "lblToll";
            this.lblToll.Size = new System.Drawing.Size(118, 18);
            this.lblToll.TabIndex = 23;
            this.lblToll.Text = "Toll:";
            this.lblToll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtToll
            // 
            this.txtToll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtToll.Location = new System.Drawing.Point(148, 141);
            this.txtToll.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtToll.MaxLength = 32;
            this.txtToll.Name = "txtToll";
            this.txtToll.Size = new System.Drawing.Size(174, 21);
            this.txtToll.TabIndex = 3;
            this.txtToll.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtToll.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // lblViolatons
            // 
            this.lblViolatons.Location = new System.Drawing.Point(24, 110);
            this.lblViolatons.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblViolatons.Name = "lblViolatons";
            this.lblViolatons.Size = new System.Drawing.Size(118, 18);
            this.lblViolatons.TabIndex = 21;
            this.lblViolatons.Text = "Violations:";
            this.lblViolatons.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtViolations
            // 
            this.txtViolations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtViolations.Location = new System.Drawing.Point(148, 110);
            this.txtViolations.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtViolations.MaxLength = 32;
            this.txtViolations.Name = "txtViolations";
            this.txtViolations.Size = new System.Drawing.Size(174, 21);
            this.txtViolations.TabIndex = 2;
            this.txtViolations.TextChanged += new System.EventHandler(this.BreakdownField_TextChanged);
            this.txtViolations.Leave += new System.EventHandler(this.BreakdownField_Leave);
            // 
            // cboPlateNo
            // 
            this.cboPlateNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlateNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboPlateNo.FormattingEnabled = true;
            this.cboPlateNo.Location = new System.Drawing.Point(114, 105);
            this.cboPlateNo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboPlateNo.Name = "cboPlateNo";
            this.cboPlateNo.Size = new System.Drawing.Size(174, 21);
            this.cboPlateNo.TabIndex = 2;
            // 
            // lblBreakdown
            // 
            this.lblBreakdown.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblBreakdown.Location = new System.Drawing.Point(114, 222);
            this.lblBreakdown.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblBreakdown.Name = "lblBreakdown";
            this.lblBreakdown.Size = new System.Drawing.Size(174, 98);
            this.lblBreakdown.TabIndex = 45;
            this.lblBreakdown.Text = "Note: If this checkbox is ticked, amount will be computed based on the amounts yo" +
    "u\'ve entered on each item under the \"Amount Breakdown\" and \"Cash Advances\" secti" +
    "ons.";
            this.lblBreakdown.Visible = false;
            // 
            // IncomeExpenseDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(642, 511);
            this.Name = "IncomeExpenseDialog";
            this.Text = "Income";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.IncomeExpenseDialog_OKButtonClicked);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.AmountDetailsPanel.ResumeLayout(false);
            this.AmountDetailsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables



        #endregion

        #region Properties

        int _ParticularType = 0;    //Default is Income.
        //The type of particular to enter
        //0 = Income
        //1 = Expense
        public int ParticularType {
            get { return _ParticularType; }
            set { _ParticularType = value; }
        }


        #endregion

        #region Public Functions

        //Sets the driver and helpers list to current employees.
        public void SetEmployees(List<ComboboxDataItem> employees) {
            BindingList<ComboboxDataItem> lstDriver = new BindingList<ComboboxDataItem>(employees);
            lstDriver.Insert(0, new ComboboxDataItem("", ""));
            cboDriver.DataSource = lstDriver;
            cboDriver.DisplayMember = "Value";
            cboDriver.ValueMember = "ID";

            BindingList<ComboboxDataItem> lstHelper = new BindingList<ComboboxDataItem>(employees);
            cboHelper.DataSource = lstHelper;
            cboHelper.DisplayMember = "Value";
            cboHelper.ValueMember = "ID";
        }

        //Link plate numbers records.
        public void SetPlateNo(ComboboxDataItem[] platenos) {
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>(platenos.ToList());
            lst.Insert(0, new ComboboxDataItem("", ""));
           
            cboPlateNo.DataSource = lst;
            cboPlateNo.DisplayMember = "Value";
            cboPlateNo.ValueMember = "ID";
        }

        //Set mode 
        public void SetupInputs(int mode) {
            //Modes:
            //0 = Income.
            //1 = Expenses.
            ParticularType = mode;  //Save on property.

            //Resize windows and hide controls.
            if (mode == 0) {
                //322, 294
                Size = new Size(322, 294);
                CenterToScreen();
                chkBreakdown.Visible = false;
                lblBreakdown.Visible = false;
                AmountDetailsPanel.Visible = false;
                Text = "Fund Transfer";
            } else if (mode == 1) {
                //All fields enabled: 653, 518
                //Details disabled: 322, 413
                chkBreakdown.Visible = true;
                //lblBreakdown.Visible = true;
                AmountDetailsPanel.Visible = false;
                Size = new Size(322, 320);
                CenterToScreen();
                Text = "Expense";
            }
        }

        //Sets the limit of the date picker.
        public void SetDateLimits(string startdate, string enddate) {
            dpDate.MinDate = DateTime.Parse(startdate);
            dpDate.MaxDate = DateTime.Parse(enddate);
            dpDate.Value = dpDate.MinDate; //Start date on min date.
        }

        //Sets the date as the last date used on adding items.
        public void SetLastDate(string lastdate) {
            dpDate.Value = DateTime.Parse(lastdate);
        }

        //Retrives the date on the picker.
        public string GetDate() {
            return dpDate.Value.ToShortDateString();
        }

        //Set the field values (Income only);
        public void SetFieldValuesIncome(string particular, string date, string plateno, string amt) {
            txtParticular.Text = particular;
            dpDate.Text = date;
            cboPlateNo.Text = plateno;
            txtAmnt.Text = amt;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        //Get field values (Income only)
        public void GetFieldValuesIncome(out string particular, out string date, out string plateno, out double amount) {
            particular = txtParticular.Text;
            date = dpDate.Value.ToShortDateString();
            plateno = cboPlateNo.Text.ToString().Trim();
            amount = Convert.ToDouble(txtAmnt.Text);
        }

        //Set the field values (Income only);
        public void SetFieldValuesExpense(string particular, string date, string plateno, double amount,
                                          double fuel, double violations, double toll, double truckingexpenses,
                                          double rmv, double servicefee, double others, string driver, double driverca,
                                          string helper, double helperca, double sucatDiesel) {
            txtParticular.Text = particular;
            dpDate.Text = date;
            cboPlateNo.Text = plateno;
            txtAmnt.Text = Convert.ToDouble(amount).ToString("0.00");

            if (fuel + violations + toll + truckingexpenses + rmv + servicefee + others + driverca + helperca == amount) {
                txtFuel.Text = Convert.ToDouble(fuel).ToString("0.00");
                txtViolations.Text = Convert.ToDouble(violations).ToString("0.00");
                txtToll.Text = Convert.ToDouble(toll).ToString("0.00");
                txtTExpenses.Text = Convert.ToDouble(truckingexpenses).ToString("0.00");
                txtRMV.Text = Convert.ToDouble(rmv).ToString("0.00");
                txtSF.Text = Convert.ToDouble(servicefee).ToString("0.00");
                txtOthers.Text = Convert.ToDouble(others).ToString("0.00");

                cboDriver.SelectedValue =  driver ;
                txtDriverCA.Text = Convert.ToDouble(driverca).ToString("0.00");
                cboHelper.SelectedValue =  helper ;
                txtHelperCA.Text = Convert.ToDouble(helperca).ToString("0.00");
                txtPetronSucatDiesel.Text = Convert.ToDouble(sucatDiesel).ToString("0.00");
                chkBreakdown.Checked = true;
            } else {
                chkBreakdown.Checked = false;
            }

            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        //Get field values (Income only)
        public void GetFieldValuesExpense(out string particular, out string date, out string plateno, out double amount,
                                          out double fuel, out double violations, out double toll, out double truckingexpenses,
                                          out double rmv, out double servicefee, out double others, out string driver, out double driverca,
                                          out string helper, out double helperca, out double sucatDiesel) {
            particular = txtParticular.Text;
            date = dpDate.Value.ToShortDateString();
            plateno = cboPlateNo.Text.ToString().Trim();
            amount = Convert.ToDouble(txtAmnt.Text);
            if (chkBreakdown.Checked) {
                fuel = (txtFuel.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtFuel.Text);
                violations = (txtViolations.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtViolations.Text);
                toll = (txtToll.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtToll.Text);
                truckingexpenses = (txtTExpenses.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtTExpenses.Text);
                rmv = (txtRMV.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtRMV.Text);
                servicefee = (txtSF.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtSF.Text);
                others = (txtOthers.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtOthers.Text);

                driver = (cboDriver.SelectedValue != null) ? cboDriver.SelectedValue.ToString() : "";
                driverca = (txtDriverCA.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtDriverCA.Text);
                helper = (cboHelper.SelectedValue != null) ? cboHelper.SelectedValue.ToString() : "";
                helperca = (txtHelperCA.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtHelperCA.Text);
                sucatDiesel = (txtPetronSucatDiesel.Text.Trim().Equals("")) ? 0.00 : Convert.ToDouble(txtPetronSucatDiesel.Text);
            } else {
                sucatDiesel = fuel = violations = toll = truckingexpenses = rmv = servicefee = others = driverca = helperca = 0.0;
                driver = helper = String.Empty;
            }
        }

        #endregion

        #region Local Functions

        //Shows or hides the breakdown input fields (Expense mode only)
        void SetBreakdownFieldsVisible(bool visible) {
            SuspendLayout();
            if (ParticularType == 1) {
                if (visible) {
                    txtAmnt.ReadOnly = true;
                    AmountDetailsPanel.Visible = true;
                    //658, 550
                    Size = new Size(658, 550);
                    ComputeAmount();
                    //reposition window to center.
                    Rectangle screendim = Screen.GetWorkingArea(this);
                    int cX = (screendim.Width - Width) / 2;
                    int cY = (screendim.Height - Height) / 2;
                    Left = screendim.X + cX;
                    Top = screendim.Y + cY;
                } else {
                    txtAmnt.Text = "0.00";
                    txtAmnt.ReadOnly = false;
                    AmountDetailsPanel.Visible = false;
                    Size = new Size(322, 320);
                }
            }
            ResumeLayout();
        }

        //Computes the total amount based on breakdown.
        //Works on expense expanded mode only.
        void ComputeAmount() {
            if (ParticularType == 1 && chkBreakdown.Checked) {
                TextBox[] breakdownfields = { txtFuel, txtViolations, txtToll, txtTExpenses,
                                              txtRMV, txtSF, txtOthers, txtDriverCA, txtHelperCA
                                            };

                double totalAmt = 0.00;

                foreach (TextBox tb in breakdownfields) {
                    if (InputValidators.IsEmptyTrimmed(tb.Text) == false) {
                        if (InputValidators.IsNumericWithDecimal(tb.Text)) {
                            totalAmt += Convert.ToDouble(tb.Text);
                        }
                    }
                }

                txtAmnt.Text = totalAmt.ToString("0.00");
            }
        }
        
        #endregion

        #region Window Event handlers
        #endregion

        #region Control Event handlers

        //Handle event when "Compute from Breakdown" check changed.
        private void chkBreakdown_CheckedChanged(object sender, EventArgs e) {
            SetBreakdownFieldsVisible(chkBreakdown.Checked);
            if (chkBreakdown.Checked) txtPetronSucatDiesel.Focus();
            else txtAmnt.Focus();
        }

        //Validate fields (color respective labels) when each
        //field loses the focus.
        Color errorColor = Color.FromArgb(220, 10, 10);
        Color normalColor = Color.FromArgb(47, 47, 47);
        private void BreakdownField_Leave(object sender, EventArgs e) {
            TextBox tb = (TextBox)sender;    //Cast to textbox the sender.

            //Perform input validation.
            bool ok = true;
            if (InputValidators.IsEmptyTrimmed(tb.Text) == false) {
                if (InputValidators.IsNumericWithDecimal(tb.Text) == false)
                    ok = false;
            }

            //Color corresponding label.
            TextBox txtb = txtFuel;
            if (tb == txtb) {
                Label lbl = lblFuel;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtViolations;
            if (tb == txtb) {
                Label lbl = lblViolatons;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtToll;
            if (tb == txtb) {
                Label lbl = lblToll;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtTExpenses;
            if (tb == txtb) {
                Label lbl = lblTExpenses;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtRMV;
            if (tb == txtb) {
                Label lbl = lblRMV;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtSF;
            if (tb == txtb) {
                Label lbl = lblSF;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtOthers;
            if (tb == txtb) {
                Label lbl = lblOthers;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtViolations;
            if (tb == txtb) {
                Label lbl = lblViolatons;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
            }

            txtb = txtDriverCA;
            if (tb == txtb) {
                Label lbl = lblDriverCA;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
                if (InputValidators.IsEmptyTrimmed(tb.Text) == false && cboDriver.Text.Trim().Equals(""))
                    lblDriver.ForeColor = errorColor;
                else
                    lblDriver.ForeColor = normalColor;
            }

            txtb = txtHelperCA;
            if (tb == txtb) {
                Label lbl = lblHelperCA;
                if (ok) lbl.ForeColor = normalColor;
                else lbl.ForeColor = errorColor;
                if (InputValidators.IsEmptyTrimmed(tb.Text) == false && cboHelper.Text.Trim().Equals(""))
                    lblHelper.ForeColor = errorColor;
                else
                    lblHelper.ForeColor = normalColor;
            }

        }

        //Show validation hints when user leaves the driver and helper combobox.
        private void EmployeePicker_Leave(object sender, EventArgs e) {
            ComboBox cb = (ComboBox)sender;
            if (cb == cboDriver) {
                if (InputValidators.IsEmptyTrimmed(txtDriverCA.Text) == false) {
                    if (cb.Text.Trim() == "") lblDriver.ForeColor = errorColor;
                    else lblDriver.ForeColor = normalColor;
                }
            }

            if (cb == cboHelper) {
                if (InputValidators.IsEmptyTrimmed(txtHelperCA.Text) == false) {
                    if (cb.Text.Trim() == "") lblHelper.ForeColor = errorColor;
                    else lblHelper.ForeColor = normalColor;
                }
            }
        }

        //Compute total amount everytime text change on each breakdown field.
        private void BreakdownField_TextChanged(object sender, EventArgs e) {
            ComputeAmount();
        }

        #endregion

        #region Validation

        //Validate once OK button is clicked.
        private void IncomeExpenseDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            //Validate only the important fields (bold)
            //Basic validations.
            if (InputValidators.IsEmptyTrimmed(txtParticular.Text)) {
                Notifier.ShowWarning("Please enter the particular's name/description.", "Invalid Input");
                txtParticular.Focus();
                return;
            }
            if (InputValidators.IsEmptyTrimmed(txtAmnt.Text)) {
                Notifier.ShowWarning("Please enter the amount.", "Invalid Input");
                txtAmnt.Focus();
                return;
            } 
            if (InputValidators.ContainsAlphabet(txtParticular.Text) == false) {
                Notifier.ShowWarning("Please use alphanumeric characters for the particular's description.", "Invalid Input");
                txtParticular.Focus();
                txtParticular.SelectAll();
                return;
            }
            if (InputValidators.IsNumericWithDecimal(txtAmnt.Text) == false) {
                Notifier.ShowWarning("The amount you've entered contains invalid characters.", "Invalid Input");
                txtAmnt.Focus();
                txtAmnt.SelectAll();
                return;
            } 

            //Validate breakdown values.
            //On Expense mode only.
            if (ParticularType == 1 && chkBreakdown.Checked) {

                TextBox[] breakdownfields = { txtFuel, txtViolations, txtToll, txtTExpenses,
                                              txtRMV, txtSF, txtOthers, txtDriverCA, txtHelperCA
                                            };

                foreach (TextBox tb in breakdownfields) {
                    if (InputValidators.IsEmptyTrimmed(tb.Text) == false) {
                        if (InputValidators.IsNumericWithDecimal(tb.Text) == false) {
                            Notifier.ShowWarning("The amount you've entered contains invalid characters.", "Invalid Input");
                            tb.Focus();
                            tb.SelectAll();
                            return;
                        }
                    }
                }

                if ((InputValidators.IsEmptyTrimmed(txtDriverCA.Text) == false) && InputValidators.IsNumericWithDecimal(txtDriverCA.Text)) {
                    if (cboDriver.Text.Trim().Equals("")) {
                        Double dCA = Convert.ToDouble(txtDriverCA.Text);
                        if (dCA != 0) {
                            Notifier.ShowWarning("You've entered a value on the driver's CA. Please identify the driver", "Invalid Input");
                            cboDriver.Focus();
                            lblDriver.ForeColor = errorColor;
                            return;
                        } else
                            lblDriver.ForeColor = normalColor;
                    } else
                        lblDriver.ForeColor = normalColor;
                }

                if ((InputValidators.IsEmptyTrimmed(txtHelperCA.Text) == false) && InputValidators.IsNumericWithDecimal(txtHelperCA.Text)) {
                    if (cboHelper.Text.Trim().Equals("")) {
                        Double hCA = Convert.ToDouble(txtHelperCA.Text);
                        if (hCA != 0) {
                            Notifier.ShowWarning("You've entered a value on the helper's CA. Please identify the helper", "Invalid Input");
                            cboHelper.Focus();
                            lblHelper.ForeColor = errorColor;
                            return;
                        } else
                            lblHelper.ForeColor = normalColor;
                    } else
                        lblHelper.ForeColor = normalColor;
                }

                if (InputValidators.IsEmptyTrimmed(cboDriver.Text) == false && InputValidators.IsEmptyTrimmed(cboHelper.Text) == false) {
                    if(cboDriver.Text.Trim().Equals(cboHelper.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)){
                        Notifier.ShowWarning("Driver and helper cannot be the same employee.", "Invalid Input");
                        cboDriver.Focus();
                        return;
                    }
                }

                if (InputValidators.IsEmptyTrimmed(cboDriver.Text) == false || InputValidators.IsEmptyTrimmed(cboHelper.Text) == false) {
                    if (cboPlateNo.Text.Trim().Equals("")) {
                        Notifier.ShowWarning("You must enter the plate number associated with the driver/helper.", "Invalid Input");
                        cboPlateNo.Focus();
                        return;
                    }
                }

            }

            e.Cancel = false;
        }

        #endregion

    }

}