﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class OldPasswordDialog : FileMaintenanceDialog {

        #region controls

        private Label label1;
        internal TextBox txtOldPW;
        private Label label5;
        private Label label3;

        #endregion

        #region Constructor

        public OldPasswordDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOldPW = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.txtOldPW);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Size = new System.Drawing.Size(389, 140);
            this.ContentPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(365, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Old Password";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(365, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "To change the password of the current user account, please enter the account\'s ol" +
    "d password.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOldPW
            // 
            this.txtOldPW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtOldPW.Location = new System.Drawing.Point(122, 94);
            this.txtOldPW.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtOldPW.MaxLength = 32;
            this.txtOldPW.Name = "txtOldPW";
            this.txtOldPW.Size = new System.Drawing.Size(255, 21);
            this.txtOldPW.TabIndex = 0;
            this.txtOldPW.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 94);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Old Password:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OldPasswordDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(389, 190);
            this.Name = "OldPasswordDialog";
            this.Text = "Enter Old Password";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.OldPasswordDialog_OKButtonClicked);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Public Variables

        internal string OldPassword = "";

        #endregion

        #region Control Eventhandlers

        private void OldPasswordDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            PasswordEncryptor encrypt = new PasswordEncryptor();
            String eOldPassword = txtOldPW.Text;
            if (OldPassword == eOldPassword) {
                e.Cancel = false;
            } else {
                MessageBox.Show("The old password that you've entered is incorrect.", "Change Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtOldPW.Focus();
                txtOldPW.SelectAll();
            }
        }

        #endregion


    }

}