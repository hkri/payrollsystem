﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class PayrollDialog : FileMaintenanceDialog {

        #region Controls

        private Label label4;
        private DateTimePicker dpEnd;
        private Label label3;
        private DateTimePicker dpStart;
        internal TextBox txtTitle;
        private Label label5;
        private Label label1;
        private Label label2;

        #endregion

        #region Constructor

        //Constructor and initializations.
        public PayrollDialog() {
            //Setup form
            InitializeComponent();  
        }

        //Form designer initializations.
        private void InitializeComponent() {
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dpStart = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dpEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.dpEnd);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.dpStart);
            this.ContentPanel.Controls.Add(this.txtTitle);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Size = new System.Drawing.Size(301, 178);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtTitle
            // 
            this.txtTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtTitle.Location = new System.Drawing.Point(91, 48);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTitle.MaxLength = 128;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(195, 21);
            this.txtTitle.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Title:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dpStart
            // 
            this.dpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpStart.Location = new System.Drawing.Point(91, 111);
            this.dpStart.Name = "dpStart";
            this.dpStart.Size = new System.Drawing.Size(195, 21);
            this.dpStart.TabIndex = 1;
            this.dpStart.ValueChanged += new System.EventHandler(this.dpStart_ValueChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date Start:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 139);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Date End:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dpEnd
            // 
            this.dpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpEnd.Location = new System.Drawing.Point(91, 138);
            this.dpEnd.Name = "dpEnd";
            this.dpEnd.Size = new System.Drawing.Size(195, 21);
            this.dpEnd.TabIndex = 2;
            this.dpEnd.ValueChanged += new System.EventHandler(this.dpEnd_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(274, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label5.Location = new System.Drawing.Point(12, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(274, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "Payroll Period";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PayrollDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(301, 228);
            this.Name = "PayrollDialog";
            this.Text = "Payroll";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.PayrollDialog_OKButtonClicked);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables

        List<String> existing_titles = new List<String>();
        String old_title = "";

        #endregion

        #region Public Functions

        //Set the starting date, based on current date selection.
        public void SetStartingDate(string date) {
            dpStart.Value = DateTime.Parse(date);
            dpEnd.Value = dpStart.Value.AddDays(30);
        }

        //Sets a list of existing title, to avoid repeating old titles
        public void SetExistingtitles(List<String> list) {
            existing_titles = list;
        }

        //Sets the initial values of the field, and set dialog mode to editing.
        public void SetFieldValues(string title, string startdate, string enddate) {
            txtTitle.Text = title.Trim();
            dpStart.Text = startdate;
            dpEnd.Text = enddate;
            old_title = txtTitle.Text;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        //Returns all input to the calling method.
        public void GetFieldValues(out string title, out string startdate, out string enddate) {
            title = txtTitle.Text.Trim();
            startdate = dpStart.Value.ToShortDateString();
            enddate = dpEnd.Value.ToShortDateString();
        }

        #endregion

        #region Local Functions
        #endregion

        #region Window Event handlers
        #endregion

        #region Control Event handlers

        //Set the date end min date when start date is updated.
        private void dpStart_ValueChanged(object sender, EventArgs e) {
            dpEnd.MinDate = dpStart.Value;
        }

        //set the date start max date when date end is updated.
        private void dpEnd_ValueChanged(object sender, EventArgs e) {
            dpStart.MaxDate = dpEnd.Value;
        }

        #endregion

        #region Validation

        //Validate inputs.
        private void PayrollDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtTitle.Text)) {
                Notifier.ShowWarning("Please give a title to this payroll.", "Invalid Input");
                txtTitle.Focus();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtTitle.Text) == false) {
                Notifier.ShowWarning("The title doesn't seem to be in a valid format. Please use alphanumeric characters.", "Invalid Input");
                txtTitle.Focus();
                txtTitle.SelectAll();
                return;
            }

            foreach (string ext in existing_titles) {
                if(ext.Equals(txtTitle.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)){
                    if ((FileMaintenanceMode == FileMaintenanceDialogMode.Editing && old_title.Trim().Equals(txtTitle.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) == false) {
                        Notifier.ShowWarning("Payroll with the same title already exists.", "Invalid Input");
                        txtTitle.Focus();
                        txtTitle.SelectAll();
                        return;
                    }
                }
            }

            e.Cancel = false;
        }

        #endregion

    }

}