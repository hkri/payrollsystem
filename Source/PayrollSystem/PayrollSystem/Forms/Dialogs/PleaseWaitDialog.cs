﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class PleaseWaitDialog : Form {

        #region Controls

        private Label label6;

        #endregion

        #region Constructor

        public PleaseWaitDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(17, 17);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(357, 33);
            this.label6.TabIndex = 10;
            this.label6.Text = "The operation might take a while. Please wait.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PleaseWaitDialog
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(387, 67);
            this.ControlBox = false;
            this.Controls.Add(this.label6);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PleaseWaitDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exporting";
            this.Load += new System.EventHandler(this.PleaseWaitDialog_Load);
            this.ResumeLayout(false);

        }
        
        #endregion

        #region eventhandlers

        public void ShowPleaseWait(Form owner) {
            Show(owner);
            Refresh();
        }

        private void PleaseWaitDialog_Load(object sender, EventArgs e) {
            Refresh();
        }

        #endregion

    }

}
