﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class RateDialog : FileMaintenanceDialog {

        #region Controls

        internal TextBox txtRate;
        private Label label5;
        internal TextBox txtDestination;
        private Label label2;
        private Label label3;
        private Label label4;
        private ToolTip TooltipMain;
        private System.ComponentModel.IContainer components;
        private Label label7;
        private ErrorProvider ErrorProviderMain;
        private Label label1;

        #endregion

        #region Constructor

        public RateDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RateDialog));
            this.txtRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TooltipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ErrorProviderMain = new System.Windows.Forms.ErrorProvider(this.components);
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderMain)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.label7);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtRate);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.txtDestination);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(346, 151);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtRate
            // 
            this.txtRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.ErrorProviderMain.SetIconAlignment(this.txtRate, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.ErrorProviderMain.SetIconPadding(this.txtRate, 5);
            this.txtRate.Location = new System.Drawing.Point(103, 113);
            this.txtRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtRate.MaxLength = 12;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(228, 21);
            this.txtRate.TabIndex = 1;
            this.txtRate.TextChanged += new System.EventHandler(this.txtRate_TextChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 113);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Rate:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDestination
            // 
            this.txtDestination.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.ErrorProviderMain.SetIconAlignment(this.txtDestination, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.ErrorProviderMain.SetIconPadding(this.txtDestination, 5);
            this.txtDestination.Location = new System.Drawing.Point(103, 48);
            this.txtDestination.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDestination.MaxLength = 64;
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.Size = new System.Drawing.Size(228, 21);
            this.txtDestination.TabIndex = 0;
            this.txtDestination.TextChanged += new System.EventHandler(this.txtDestination_TextChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Destination:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(319, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "Store/Destination";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(319, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "Transport Rate";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label7.Location = new System.Drawing.Point(295, 17);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 18);
            this.label7.TabIndex = 19;
            this.label7.Text = "?";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TooltipMain.SetToolTip(this.label7, "The name of the store or destination. You cannot save rates of\r\nthe same destinat" +
        "ion under a common rate group.");
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label4.Location = new System.Drawing.Point(295, 82);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 18);
            this.label4.TabIndex = 20;
            this.label4.Text = "?";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TooltipMain.SetToolTip(this.label4, "The transport rate for the specified destination. Values cannot\r\nbe negative, and" +
        " must be numeric values only.");
            // 
            // ErrorProviderMain
            // 
            this.ErrorProviderMain.ContainerControl = this;
            this.ErrorProviderMain.Icon = ((System.Drawing.Icon)(resources.GetObject("ErrorProviderMain.Icon")));
            // 
            // RateDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(346, 201);
            this.Name = "RateDialog";
            this.Text = "Rate";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.RateDialog_OKButtonClicked);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderMain)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        #region Local Variables

        List<String> Existing = new List<string>();
        string olddest = "";

        #endregion

        #region Public Functions

        public void SetExistingDestinations(string[] list) {
            Existing.Clear();
            foreach (string e in list) {
                Existing.Add(e);
            }
        }

        public void SetFieldValues(string destination, string rate) {
            txtDestination.Text = destination;
            txtRate.Text = rate;
            olddest = txtDestination.Text;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        public void GetFieldValues(out string destination, out string rate) {
            destination = txtDestination.Text;
            rate = txtRate.Text;
        }

        #endregion

        #region Local Functions
        #endregion

        #region Window Event handlers
        #endregion

        #region Control Event handlers

        private void txtDestination_TextChanged(object sender, EventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtDestination.Text)) {
                ErrorProviderMain.SetError(txtDestination, "Destination cannot be empty.");
                return;
            }
            ErrorProviderMain.SetError(txtDestination, "");
        }

        private void txtRate_TextChanged(object sender, EventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtRate.Text)) {
                ErrorProviderMain.SetError(txtRate, "Rate cannot be empty.");
                return;
            } 
            if (InputValidators.IsNumericWithDecimal(txtRate.Text) == false) {
                ErrorProviderMain.SetError(txtRate, "Rate contains invalid characters.");
                return;
            }
            ErrorProviderMain.SetError(txtRate, "");
        }

        #endregion

        #region Validation

        private void RateDialog_OKButtonClicked(object sender, DialogEventArgs e) {

            //Basic input validations.
            if (InputValidators.IsEmptyTrimmed(txtDestination.Text)) {
                Notifier.ShowWarning("Destination field cannot be empty.", "Invalid Input");
                txtDestination.Focus();
                return;
            }
            if (InputValidators.IsEmptyTrimmed(txtRate.Text)) {
                Notifier.ShowWarning("The rate field cannot be empty.", "Invalid Input");
                txtRate.Focus();
                return;
            }
            if (InputValidators.IsNumericWithDecimal(txtRate.Text) == false) {
                Notifier.ShowWarning("The rate contains invalid character(s).", "Invalid Input");
                txtRate.Focus();
                txtRate.SelectAll();
                return;
            }
            if (InputValidators.ContainsAlphabet(txtDestination.Text) == false) {
                Notifier.ShowWarning("Destination must contain characters from the alphabet.", "Invalid Input");
                txtDestination.Focus();
                return;
            }

            //Check if destination already exists.
            foreach (string x in Existing) {
                if (txtDestination.Text.Trim().Equals(x.Trim(), StringComparison.CurrentCultureIgnoreCase)) {
                    if (!(txtDestination.Text.Trim().Equals(olddest.Trim(), StringComparison.CurrentCultureIgnoreCase) && FileMaintenanceMode == FileMaintenanceDialogMode.Editing)) {
                        Notifier.ShowWarning("The destination you've entered already has a rate for this vehicle type. Edit that destination if you want to update the rates instead.", "Invalid Input");
                        txtDestination.Focus();
                        txtDestination.SelectAll();
                        return;
                    }
                }
            }

            e.Cancel = false;   //Return OK to dialog.
        }

        #endregion

    }

}