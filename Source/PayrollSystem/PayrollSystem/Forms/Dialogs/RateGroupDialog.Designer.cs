﻿namespace PayrollSystem {
    partial class RateGroupDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RateGroupDialog));
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.txtDriverRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHelperRate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ErrorProviderMain = new System.Windows.Forms.ErrorProvider(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TooltipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderMain)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label8);
            this.ContentPanel.Controls.Add(this.label7);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.txtHelperRate);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtDriverRate);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.cboType);
            this.ContentPanel.Controls.Add(this.txtTitle);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(346, 216);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtTitle
            // 
            this.txtTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtTitle.Location = new System.Drawing.Point(114, 48);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTitle.MaxLength = 32;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(217, 21);
            this.txtTitle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Group Title:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TooltipMain.SetToolTip(this.label1, "The name of the rate group.");
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 79);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vehicle Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TooltipMain.SetToolTip(this.label6, "The vehicle type where this group belongs to.");
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(114, 79);
            this.cboType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(217, 21);
            this.cboType.TabIndex = 1;
            // 
            // txtDriverRate
            // 
            this.txtDriverRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.ErrorProviderMain.SetIconAlignment(this.txtDriverRate, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.ErrorProviderMain.SetIconPadding(this.txtDriverRate, 3);
            this.txtDriverRate.Location = new System.Drawing.Point(114, 144);
            this.txtDriverRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDriverRate.MaxLength = 12;
            this.txtDriverRate.Name = "txtDriverRate";
            this.txtDriverRate.Size = new System.Drawing.Size(217, 21);
            this.txtDriverRate.TabIndex = 2;
            this.txtDriverRate.TextChanged += new System.EventHandler(this.txtDriverRate_TextChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 144);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "Driver:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TooltipMain.SetToolTip(this.label2, "The driver rate. Cannot be negative.");
            // 
            // txtHelperRate
            // 
            this.txtHelperRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.ErrorProviderMain.SetIconAlignment(this.txtHelperRate, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.ErrorProviderMain.SetIconPadding(this.txtHelperRate, 3);
            this.txtHelperRate.Location = new System.Drawing.Point(114, 175);
            this.txtHelperRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtHelperRate.MaxLength = 12;
            this.txtHelperRate.Name = "txtHelperRate";
            this.txtHelperRate.Size = new System.Drawing.Size(217, 21);
            this.txtHelperRate.TabIndex = 3;
            this.txtHelperRate.TextChanged += new System.EventHandler(this.txtHelperRate_TextChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 175);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Helper";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TooltipMain.SetToolTip(this.label3, "Helper rate. Cannot be negative or contain non-numeric characters.");
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(12, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(319, 24);
            this.label4.TabIndex = 16;
            this.label4.Text = "Group Info";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label5.Location = new System.Drawing.Point(12, 110);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(319, 24);
            this.label5.TabIndex = 17;
            this.label5.Text = "Employee Rates";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ErrorProviderMain
            // 
            this.ErrorProviderMain.ContainerControl = this;
            this.ErrorProviderMain.Icon = ((System.Drawing.Icon)(resources.GetObject("ErrorProviderMain.Icon")));
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label7.Location = new System.Drawing.Point(304, 17);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "?";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TooltipMain.SetToolTip(this.label7, "General information about the rate group.");
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label8.Location = new System.Drawing.Point(304, 113);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "?";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TooltipMain.SetToolTip(this.label8, "Specify the driver and helper rates for this rate group. Values cannot be negativ" +
        "e.");
            // 
            // RateGroupDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 266);
            this.Name = "RateGroupDialog";
            this.Text = "Rate Group";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.RateGroupDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.RateGroupDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ComboBox cboType;
        internal System.Windows.Forms.TextBox txtHelperRate;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtDriverRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider ErrorProviderMain;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolTip TooltipMain;
        private System.Windows.Forms.Label label7;
    }
}