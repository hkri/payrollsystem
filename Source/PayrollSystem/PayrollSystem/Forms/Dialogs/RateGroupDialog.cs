﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    public partial class RateGroupDialog : FileMaintenanceDialog {

        #region Local Variables

        string old_title = "";

        #endregion

        #region Constructor

        public RateGroupDialog() {
            InitializeComponent();
        }

        #endregion

        #region Validation

        private void RateGroupDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            //No-brainer validations
            if (InputValidators.IsEmptyTrimmed(txtTitle.Text)) {
                Notifier.ShowWarning("Please enter a title/name for this rate group.", "Invalid Input");
                txtTitle.Focus();
                return;
            }
            if (cboType.SelectedIndex < 0) {
                Notifier.ShowWarning("Please select the vehicle type to be targeted by this rate group.", "Invalid Input");
                cboType.Focus();
                return;
            }
            if (InputValidators.IsNumericWithDecimal(txtDriverRate.Text) == false) {
                Notifier.ShowWarning("Please enter a valid currency in driver rate.", "Invalid Input");
                txtDriverRate.Focus();
                txtDriverRate.SelectAll();
                return;
            }
            if (InputValidators.IsNumericWithDecimal(txtHelperRate.Text) == false) {
                Notifier.ShowWarning("Pleae enter a valid currency in helper rate.", "Invalid Input");
                txtHelperRate.Focus();
                txtHelperRate.SelectAll();
                return;
            }

            //Try if title already exists for the current vehicle type.
            bool check_if_exists = true;
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing && old_title == txtTitle.Text.Trim()) {
                check_if_exists = false;
            }
            if (check_if_exists) {
                DataView ds = RateGroupPage.QueryRateGroups("{" + cboType.SelectedValue.ToString() + "}").AsDataView();
                bool duplicate = false;
                foreach (DataRowView drow in ds) {
                    string existing_group = drow["Title"].ToString().Trim();
                    if (txtTitle.Text.Trim().Equals(existing_group, StringComparison.CurrentCultureIgnoreCase)) {
                        duplicate = true;
                        break;
                    }
                }

                if (duplicate) {
                    Notifier.ShowWarning("A rate group with the same title/name already exists. Please think of another group title.", "Invalid Input");
                    txtTitle.Focus();
                    return;
                }
            }

            //Check if rates are negative.
            double drate = 0.0, hrate = 0.0;
            Double.TryParse(txtDriverRate.Text, out drate);
            Double.TryParse(txtHelperRate.Text, out hrate);
            if (drate < 0) {
                Notifier.ShowWarning("Driver rates should not be negative.", "Invalid Input");
                txtDriverRate.Focus();
                txtDriverRate.SelectAll();
                return;
            }
            if (hrate < 0) {
                Notifier.ShowWarning("Helper rates should not be negative.", "Invalid Input");
                txtHelperRate.Focus();
                txtHelperRate.SelectAll();
                return;
            }


            e.Cancel = false;
        }

        #endregion

        #region Public Functions

        public void GetFieldValues(out string group_name, out string vehicle_type, out string driver_rate, out string helper_rate) {
            group_name = txtTitle.Text.Trim();
            vehicle_type = cboType.SelectedValue.ToString();
            driver_rate = txtDriverRate.Text;
            helper_rate = txtHelperRate.Text;
        }

        public void SetFieldValues(string group_name, string vehicle_type, string driver_rate, string helper_rate) {
            txtTitle.Text = group_name;
            cboType.Text = vehicle_type;
            txtDriverRate.Text = driver_rate;
            txtHelperRate.Text = helper_rate;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
            old_title = txtTitle.Text;
        }

        public void SetVehicleTypeList(ComboboxDataItem[] items) {
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>();
            foreach (ComboboxDataItem i in items) {
                lst.Add(i);
            }
            cboType.DisplayMember = "Value";
            cboType.ValueMember = "ID";
            cboType.DataSource = lst;
        }

        #endregion

        #region Control Eventhandlers

        private void txtDriverRate_TextChanged(object sender, EventArgs e) {
            if (!InputValidators.IsNumericWithDecimal(txtDriverRate.Text)) {
                ErrorProviderMain.SetError(txtDriverRate, "Driver rate is not a valid value.");
            } else {
                ErrorProviderMain.SetError(txtDriverRate, "");
            }
        }

        private void txtHelperRate_TextChanged(object sender, EventArgs e) {
            if (!InputValidators.IsNumericWithDecimal(txtHelperRate.Text)) {
                ErrorProviderMain.SetError(txtHelperRate, "Helper rate is not a valid value.");
            } else {
                ErrorProviderMain.SetError(txtHelperRate, "");
            }
        }

        #endregion

        #region Window Eventhandlers

        private void RateGroupDialog_Load(object sender, EventArgs e) {

        }

        #endregion


    }

}
