﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class StartingBalanceDialog : FileMaintenanceDialog {

        #region Controls
        
        //Form designer-generated controls.
        private Label label3;
        private Label label2;
        private Label label1;
        internal TextBox txtCR;
        private ComboBox cboSections;
        private LinkLabel lnkChangeCR;
        private Label lblStartingBalance;
        private Label label5;
        private Label label4;
        private LinkLabel lnkRemoveStartingBalance;
        private BufferedPanel bufferedPanel1;

        #endregion

        #region Constructor

        //Do initializations here.
        public StartingBalanceDialog() {
            InitializeComponent();
        }

        //Form Designer initializations.
        private void InitializeComponent() {
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCR = new System.Windows.Forms.TextBox();
            this.lnkChangeCR = new System.Windows.Forms.LinkLabel();
            this.cboSections = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStartingBalance = new System.Windows.Forms.Label();
            this.lnkRemoveStartingBalance = new System.Windows.Forms.LinkLabel();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lnkRemoveStartingBalance);
            this.ContentPanel.Controls.Add(this.lblStartingBalance);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.cboSections);
            this.ContentPanel.Controls.Add(this.lnkChangeCR);
            this.ContentPanel.Controls.Add(this.txtCR);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.bufferedPanel1);
            this.ContentPanel.Size = new System.Drawing.Size(404, 248);
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackgroundImage = global::PayrollSystem.Properties.Resources.StartingBalanceBanner;
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 0);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(404, 50);
            this.bufferedPanel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 58);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(377, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cash Report Info";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 92);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Cash Report:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Section:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCR
            // 
            this.txtCR.BackColor = System.Drawing.Color.White;
            this.txtCR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtCR.Location = new System.Drawing.Point(118, 92);
            this.txtCR.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtCR.MaxLength = 32;
            this.txtCR.Name = "txtCR";
            this.txtCR.ReadOnly = true;
            this.txtCR.Size = new System.Drawing.Size(214, 21);
            this.txtCR.TabIndex = 8;
            // 
            // lnkChangeCR
            // 
            this.lnkChangeCR.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangeCR.AutoSize = true;
            this.lnkChangeCR.BackColor = System.Drawing.Color.Transparent;
            this.lnkChangeCR.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangeCR.Location = new System.Drawing.Point(338, 95);
            this.lnkChangeCR.Name = "lnkChangeCR";
            this.lnkChangeCR.Size = new System.Drawing.Size(51, 13);
            this.lnkChangeCR.TabIndex = 10;
            this.lnkChangeCR.TabStop = true;
            this.lnkChangeCR.Text = "Change";
            this.lnkChangeCR.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangeCR.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkChangeCR_LinkClicked);
            // 
            // cboSections
            // 
            this.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSections.FormattingEnabled = true;
            this.cboSections.Location = new System.Drawing.Point(118, 121);
            this.cboSections.Name = "cboSections";
            this.cboSections.Size = new System.Drawing.Size(214, 21);
            this.cboSections.TabIndex = 11;
            this.cboSections.SelectedIndexChanged += new System.EventHandler(this.cboSections_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(12, 150);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(377, 24);
            this.label4.TabIndex = 12;
            this.label4.Text = "Balance";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 184);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Your starting balance will be:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStartingBalance
            // 
            this.lblStartingBalance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartingBalance.Location = new System.Drawing.Point(217, 184);
            this.lblStartingBalance.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblStartingBalance.Name = "lblStartingBalance";
            this.lblStartingBalance.Size = new System.Drawing.Size(172, 18);
            this.lblStartingBalance.TabIndex = 14;
            this.lblStartingBalance.Text = "Php 0.00";
            this.lblStartingBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lnkRemoveStartingBalance
            // 
            this.lnkRemoveStartingBalance.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkRemoveStartingBalance.AutoSize = true;
            this.lnkRemoveStartingBalance.BackColor = System.Drawing.Color.Transparent;
            this.lnkRemoveStartingBalance.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkRemoveStartingBalance.Location = new System.Drawing.Point(237, 217);
            this.lnkRemoveStartingBalance.Name = "lnkRemoveStartingBalance";
            this.lnkRemoveStartingBalance.Size = new System.Drawing.Size(152, 13);
            this.lnkRemoveStartingBalance.TabIndex = 15;
            this.lnkRemoveStartingBalance.TabStop = true;
            this.lnkRemoveStartingBalance.Text = "Remove Starting Balance";
            this.lnkRemoveStartingBalance.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkRemoveStartingBalance.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkRemoveStartingBalance_LinkClicked);
            // 
            // StartingBalanceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(404, 298);
            this.Name = "StartingBalanceDialog";
            this.Text = "Manage Starting Balance";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.StartingBalanceDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.StartingBalanceDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables

        string CashReportID = "", CashReportTitle = "";
        string SectionID = "", SectionTitle = "";
        double StartingBalance = 0.0;

        DataTable secs = null;
        Boolean RemoveStartingBalance = false;

        #endregion

        #region Public Functions

        //Returns the SectionID of the selected section to base Starting Balance.
        public string GetSectionIDStartingBalance(){
            return "{" + SectionID + "}";
        }

        //Sets the SectionID of the base Starting Balance
        public void SetSectionIDStartingBalance(string SID) {
            //Get the title of the section, title of cash report, and ending balance of the specified section.
            String query = "SELECT A.Title, A.EndingBalance, A.CRID, B.Title AS [CR Title] FROM CashReportSections AS A LEFT JOIN CashReport AS B ON A.CRID = B.CRID WHERE SECID = @sid";
            
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@sid", "{" + SID + "}");
            DataTable res = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            if (res.Rows.Count > 0) {
                //Load the CashReport Parent first.
                CashReportID = res.Rows[0]["CRID"].ToString();
                CashReportTitle = res.Rows[0]["CR Title"].ToString();
                txtCR.Text = CashReportTitle;

                //Set the cash report loaded
                SetCashReport(CashReportID);

                //Set the section title.
                SectionTitle = res.Rows[0]["Title"].ToString();
                cboSections.Text = SectionTitle;
            }
        }

        #endregion

        #region Local Functions

        //Setup the cash report, then load all related sections.
        public void SetCashReport(string ID) {
            //Set variables.
            CashReportID = "{" + ID + "}";

            //Retrieve title of selected cash report.
            string query = "SELECT Title FROM CashReport WHERE CRID = @crid";

            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@crid", CashReportID);

            DataTable da = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            
            //Get only the first result.
            CashReportTitle = da.Rows[0]["Title"].ToString();
            txtCR.Text = CashReportTitle;

            //Retrieve all sections under the selected cash report.
            query = "SELECT SECID, Title, EndingBalance FROM CashReportSections WHERE CRID = @crid";

            OleDbParameter[] q = new OleDbParameter[1];
            q[0] = new OleDbParameter("@crid", CashReportID);
            if (secs != null) secs.Dispose();
            secs = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, q);
            InitializeSectionList();    //Initialize the section combobox items.
            RemoveStartingBalance = false;  //If this is modified, it means user will set starting balance.
        }

        //Sets up the section combobox items.
        void InitializeSectionList() {
            //cboSections.Items.Clear();
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>();
            if (secs.Rows.Count == 0) {
                Notifier.ShowInformation("No sections found under the selected cash report.", "No Sections");
            }
            foreach (DataRow r in secs.Rows) {
                lst.Add(new ComboboxDataItem(r["SECID"].ToString(), r["Title"].ToString()));
            }
            cboSections.DataSource = lst;
            cboSections.DisplayMember = "Value";
            cboSections.ValueMember = "ID";
        }

        #endregion

        #region Window Event handlers

        //handle the Window on load event.
        private void StartingBalanceDialog_Load(object sender, EventArgs e) {
            Text = "Manage Starting Balance";
        }

        #endregion

        #region Control Event handlers

        //handle the link "Change" cash report click event.
        private void lnkChangeCR_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            CashReportPickerDialog dlg = new CashReportPickerDialog();
            
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                Dictionary<String, String> sel = dlg.GetSelectedCashReports();
                foreach (string key in sel.Keys) {
                    SetCashReport(key);
                }
            }
        }

        //Handle event when the selected section changes.
        private void cboSections_SelectedIndexChanged(object sender, EventArgs e) {
            if (cboSections.SelectedIndex > -1 && secs != null && cboSections.Items.Count  > 0) {
                ComboboxDataItem itm = (ComboboxDataItem)cboSections.SelectedItem;
                IEnumerable<String> k = secs.Rows.Cast<DataRow>().Where(x => x["SECID"].ToString().Equals(itm.ID)).Select(x => Convert.ToDouble(x["EndingBalance"]).ToString("#,##0.00"));
                if (k.Count() > 0) {
                    lblStartingBalance.Text = "Php " + k.First();
                    SectionID = itm.ID;
                    StartingBalance = Convert.ToDouble(k.First());
                } else
                    lblStartingBalance.Text = "Php 0.00";
                //lblStartingBalance.Text = "Php " + secs.Rows.Cast<DataRow>().Where(x => x["SECID"].Equals("{" + cboSections.SelectedValue + "}")).Select(x => Convert.ToDouble(x["EndingBalance"]).ToString("#,##0.00")).First();
            }
        }

        //Removes the starting balance link.
        private void lnkRemoveStartingBalance_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            RemoveStartingBalance = true;
            CashReportID = "";
            SectionID = "";
            cboSections.DataSource = null;
            txtCR.Clear();
            StartingBalance = 0.0;
            lblStartingBalance.Text = "Php 0.00";
        }

        #endregion

        #region Validation

        //Give Dialog OK when button OK is closed, validate first.
        private void StartingBalanceDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (RemoveStartingBalance == false) {
                if (txtCR.Text.Trim().Equals("")) {
                    Notifier.ShowWarning("There are no cash report selected.", "Invalid Input");
                    return;
                }
                if (cboSections.Items.Count <= 0) {
                    Notifier.ShowWarning("No sections selected.", "Invalid Input");
                }
            }
            e.Cancel = false;
        }

        #endregion

    }

}