﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class StatusReportDialog : FileMaintenanceDialog {

        #region Controls

        internal TextBox txtStatus;
        private Label label6;
        internal ComboBox cboVehicle;
        private Label label2;
        private DateTimePicker dpDate;
        private Label label1;

        #endregion

        #region Constructor

        public StatusReportDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboVehicle = new System.Windows.Forms.ComboBox();
            this.dpDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.dpDate);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.cboVehicle);
            this.ContentPanel.Controls.Add(this.txtStatus);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(348, 115);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtStatus
            // 
            this.txtStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtStatus.Location = new System.Drawing.Point(100, 76);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtStatus.MaxLength = 32;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(233, 21);
            this.txtStatus.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vehicle:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboVehicle
            // 
            this.cboVehicle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVehicle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboVehicle.FormattingEnabled = true;
            this.cboVehicle.Location = new System.Drawing.Point(100, 14);
            this.cboVehicle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboVehicle.Name = "cboVehicle";
            this.cboVehicle.Size = new System.Drawing.Size(233, 21);
            this.cboVehicle.TabIndex = 0;
            // 
            // dpDate
            // 
            this.dpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpDate.Location = new System.Drawing.Point(100, 45);
            this.dpDate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dpDate.Name = "dpDate";
            this.dpDate.Size = new System.Drawing.Size(233, 21);
            this.dpDate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StatusReportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(348, 165);
            this.Name = "StatusReportDialog";
            this.Text = "Status Report";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.StatusReportDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.StatusReportDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        #region Local Variables

        #endregion

        #region Public Functions

        public void SetVehiclesList() {
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>();
            ComboboxDataItem[] items = ManageVehiclesPage.QueryVehicles();
            foreach (ComboboxDataItem itm in items)
                lst.Add(itm);
            cboVehicle.DataSource = lst;
            cboVehicle.DisplayMember = "Value";
            cboVehicle.ValueMember = "ID";
            if (cboVehicle.Items.Count > 0) cboVehicle.SelectedIndex = 0;
        }

        public void GetFieldValues(out string vehicle_pn, out string status, out string date) {
            vehicle_pn = cboVehicle.Text;
            status = txtStatus.Text;
            date = dpDate.Value.ToShortDateString();
        }

        #endregion

        #region Local Functions

        #endregion

        #region Window Event handlers

        private void StatusReportDialog_Load(object sender, EventArgs e) {
            SetOKButtonText("Update Status");
            SetVehiclesList();
            dpDate.Value = DateTime.Now;
        }

        #endregion

        #region Control Event handlers

        #endregion

        #region Validation

        private void StatusReportDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (txtStatus.Text.Trim() == "") {
                Notifier.ShowWarning("Status cannot be empty. Please add contents to the status report.", "Invalid Input");
                txtStatus.Focus();
                return;
            }
            if (cboVehicle.Text == "") {
                Notifier.ShowWarning("Please select a vehicle to be associated to this status report.", "Invalid Input");
                cboVehicle.Focus();
                return;
            }
            e.Cancel = false;
        }

        #endregion

    }

}