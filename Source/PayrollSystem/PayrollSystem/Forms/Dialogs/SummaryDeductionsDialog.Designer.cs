﻿namespace PayrollSystem {
    partial class SummaryDeductionsDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SummaryDeductionsDialog));
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOfficeCA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUniform = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSSS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPH = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPI = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ContentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.txtPI);
            this.ContentPanel.Controls.Add(this.label7);
            this.ContentPanel.Controls.Add(this.txtPH);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.txtSSS);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtUniform);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.txtOfficeCA);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Size = new System.Drawing.Size(264, 248);
            this.ContentPanel.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label6.Location = new System.Drawing.Point(12, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(237, 24);
            this.label6.TabIndex = 11;
            this.label6.Text = "Company Deductions";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label1.Location = new System.Drawing.Point(12, 111);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(237, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Contributions";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOfficeCA
            // 
            this.txtOfficeCA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.errorProvider1.SetIconAlignment(this.txtOfficeCA, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.errorProvider1.SetIconPadding(this.txtOfficeCA, 5);
            this.txtOfficeCA.Location = new System.Drawing.Point(124, 48);
            this.txtOfficeCA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtOfficeCA.MaxLength = 32;
            this.txtOfficeCA.Name = "txtOfficeCA";
            this.txtOfficeCA.Size = new System.Drawing.Size(125, 21);
            this.txtOfficeCA.TabIndex = 0;
            this.txtOfficeCA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOfficeCA.TextChanged += new System.EventHandler(this.Validate_Inputs);
            this.txtOfficeCA.Enter += new System.EventHandler(this.Uncommatize_Inputs);
            this.txtOfficeCA.Leave += new System.EventHandler(this.Commatize_Inputs);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(15, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "Office CA:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUniform
            // 
            this.txtUniform.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.errorProvider1.SetIconAlignment(this.txtUniform, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.errorProvider1.SetIconPadding(this.txtUniform, 5);
            this.txtUniform.Location = new System.Drawing.Point(124, 79);
            this.txtUniform.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtUniform.MaxLength = 32;
            this.txtUniform.Name = "txtUniform";
            this.txtUniform.Size = new System.Drawing.Size(125, 21);
            this.txtUniform.TabIndex = 1;
            this.txtUniform.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUniform.TextChanged += new System.EventHandler(this.Validate_Inputs);
            this.txtUniform.Enter += new System.EventHandler(this.Uncommatize_Inputs);
            this.txtUniform.Leave += new System.EventHandler(this.Commatize_Inputs);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "Uniform:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSSS
            // 
            this.txtSSS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.errorProvider1.SetIconAlignment(this.txtSSS, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.errorProvider1.SetIconPadding(this.txtSSS, 5);
            this.txtSSS.Location = new System.Drawing.Point(124, 145);
            this.txtSSS.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtSSS.MaxLength = 32;
            this.txtSSS.Name = "txtSSS";
            this.txtSSS.Size = new System.Drawing.Size(125, 21);
            this.txtSSS.TabIndex = 2;
            this.txtSSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSSS.TextChanged += new System.EventHandler(this.Validate_Inputs);
            this.txtSSS.Enter += new System.EventHandler(this.Uncommatize_Inputs);
            this.txtSSS.Leave += new System.EventHandler(this.Commatize_Inputs);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 145);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 18);
            this.label3.TabIndex = 18;
            this.label3.Text = "SSS:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPH
            // 
            this.txtPH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.errorProvider1.SetIconAlignment(this.txtPH, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.errorProvider1.SetIconPadding(this.txtPH, 5);
            this.txtPH.Location = new System.Drawing.Point(124, 176);
            this.txtPH.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPH.MaxLength = 32;
            this.txtPH.Name = "txtPH";
            this.txtPH.Size = new System.Drawing.Size(125, 21);
            this.txtPH.TabIndex = 3;
            this.txtPH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPH.TextChanged += new System.EventHandler(this.Validate_Inputs);
            this.txtPH.Enter += new System.EventHandler(this.Uncommatize_Inputs);
            this.txtPH.Leave += new System.EventHandler(this.Commatize_Inputs);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(15, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 18);
            this.label4.TabIndex = 20;
            this.label4.Text = "PhilHealth:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPI
            // 
            this.txtPI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.errorProvider1.SetIconAlignment(this.txtPI, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.errorProvider1.SetIconPadding(this.txtPI, 5);
            this.txtPI.Location = new System.Drawing.Point(124, 207);
            this.txtPI.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPI.MaxLength = 32;
            this.txtPI.Name = "txtPI";
            this.txtPI.Size = new System.Drawing.Size(125, 21);
            this.txtPI.TabIndex = 4;
            this.txtPI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPI.TextChanged += new System.EventHandler(this.Validate_Inputs);
            this.txtPI.Enter += new System.EventHandler(this.Uncommatize_Inputs);
            this.txtPI.Leave += new System.EventHandler(this.Commatize_Inputs);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(15, 207);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 18);
            this.label7.TabIndex = 22;
            this.label7.Text = "Pag-IBIG:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // SummaryDeductionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 298);
            this.Name = "SummaryDeductionsDialog";
            this.Text = "Other Deductions";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.SummaryDeductionsDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.SummaryDeductionsDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox txtOfficeCA;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtPI;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtPH;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtSSS;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtUniform;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}