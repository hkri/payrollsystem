﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PayrollSystem {
    public partial class SummaryDeductionsDialog : FileMaintenanceDialog {

        #region Local variables

        #endregion

        #region Constructor

        public SummaryDeductionsDialog() {
            InitializeComponent();
        }

        #endregion

        #region Window Eventhandlers

        private void SummaryDeductionsDialog_Load(object sender, EventArgs e) {
            Text = "Update Deductions";
            txtOfficeCA.Focus();
            txtOfficeCA.DeselectAll();
        }

        #endregion

        #region Control Eventhandlers

        private void SummaryDeductionsDialog_OKButtonClicked(object sender, DialogEventArgs e) {
             //Validate all values in the input fields if valid numeric numbers.
            if (!InputValidators.IsNumericWithDecimal(txtOfficeCA.Text)) {
                Notifier.ShowWarning("Please enter only a numeric value in the 'Office CA' field.", "Invalid Input");
                txtOfficeCA.Focus();
                return;
            }
            if (!InputValidators.IsNumericWithDecimal(txtUniform.Text)) {
                Notifier.ShowWarning("Please enter only a numeric value in the 'Uniform' field.", "Invalid Input");
                txtUniform.Focus();
                return;
            }
            if (!InputValidators.IsNumericWithDecimal(txtSSS.Text)) {
                Notifier.ShowWarning("Please enter only a numeric value in the 'SSS' field.", "Invalid Input");
                txtSSS.Focus();
                return;
            }
            if (!InputValidators.IsNumericWithDecimal(txtPH.Text)) {
                Notifier.ShowWarning("Please enter only a numeric value in the 'PhilHealth' field.", "Invalid Input");
                txtPH.Focus();
                return;
            }
            if (!InputValidators.IsNumericWithDecimal(txtPI.Text)) {
                Notifier.ShowWarning("Please enter only a numeric value in the 'Pag-IBIG' field.", "Invalid Input");
                txtPI.Focus();
                return;
            }
            e.Cancel = false;
        }

        //Add commas on textbox leave.
        private void Commatize_Inputs(object sender, EventArgs e) {
            TextBox tb = (TextBox)sender;   //Get the calling textbox.
            //Initial validations.
            Double val = 0.00;
            if (!Double.TryParse(tb.Text, out val)) {
                errorProvider1.SetError(tb, "Invalid value.");
            } else {
                tb.Text = val.ToString("#,##0.00");
            }
            tb.SelectAll();
        }

        //Remove commas on textbox enter.
        private void Uncommatize_Inputs(object sender, EventArgs e) {
            TextBox tb = (TextBox)sender;   //Get the calling textbox.
            //Initial validations.
            Double val = 0.00;
            if (!Double.TryParse(tb.Text, out val)) {
                errorProvider1.SetError(tb, "Invalid value.");
            } else {
                tb.Text = val.ToString("0.00");
            }
        }

        private void Validate_Inputs(object sender, EventArgs e) {
            TextBox tb = (TextBox)sender;   //Get the calling textbox.

            //Initial validations.
            Double val = 0.00;
            if (!Double.TryParse(tb.Text, out val)) {
                errorProvider1.SetError(tb, "Invalid value.");
            } else {
                errorProvider1.SetError(tb, "");
            }
        }

        #endregion


    }
}
