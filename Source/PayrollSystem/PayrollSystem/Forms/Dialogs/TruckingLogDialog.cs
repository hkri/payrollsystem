﻿using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class TruckingLogDialog : FileMaintenanceDialog {

        #region Controls

        private Label label3;
        internal TextBox txtVType;
        private DateTimePicker dpDate;
        private Label label2;
        internal ComboBox cboPlateNo;
        private Label label4;
        private Label label5;
        internal ComboBox cboDestination;
        private Label label6;
        private Label label7;
        internal TextBox txtDRNo;
        private Label label12;
        internal TextBox txtHelperRate;
        internal ComboBox cboHelper;
        private Label label13;
        private Label label10;
        internal TextBox txtDriverRate;
        internal ComboBox cboDriver;
        private Label label11;
        private Label label9;
        private Label label8;
        internal TextBox txtRate;
        private Label label16;
        private Label label15;
        private Label label14;
        private ToolTip MainToolTip;
        private System.ComponentModel.IContainer components;
        private CheckBox chkBackload;
        private Button btn50;
        private Button btn25;
        private Button btnReset;
        private Button btn75;
        private Label lblErrorMessage;
        private Label label1;

        #endregion

        #region Constructor

        public TruckingLogDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TruckingLogDialog));
            this.label3 = new System.Windows.Forms.Label();
            this.txtVType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dpDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cboPlateNo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDestination = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDRNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDriverRate = new System.Windows.Forms.TextBox();
            this.cboDriver = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHelperRate = new System.Windows.Forms.TextBox();
            this.cboHelper = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.MainToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.chkBackload = new System.Windows.Forms.CheckBox();
            this.btn25 = new System.Windows.Forms.Button();
            this.btn50 = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btn75 = new System.Windows.Forms.Button();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lblErrorMessage);
            this.ContentPanel.Controls.Add(this.btn75);
            this.ContentPanel.Controls.Add(this.btnReset);
            this.ContentPanel.Controls.Add(this.btn50);
            this.ContentPanel.Controls.Add(this.btn25);
            this.ContentPanel.Controls.Add(this.chkBackload);
            this.ContentPanel.Controls.Add(this.label16);
            this.ContentPanel.Controls.Add(this.label15);
            this.ContentPanel.Controls.Add(this.label14);
            this.ContentPanel.Controls.Add(this.label12);
            this.ContentPanel.Controls.Add(this.txtHelperRate);
            this.ContentPanel.Controls.Add(this.cboHelper);
            this.ContentPanel.Controls.Add(this.label13);
            this.ContentPanel.Controls.Add(this.label10);
            this.ContentPanel.Controls.Add(this.txtDriverRate);
            this.ContentPanel.Controls.Add(this.cboDriver);
            this.ContentPanel.Controls.Add(this.label11);
            this.ContentPanel.Controls.Add(this.label9);
            this.ContentPanel.Controls.Add(this.label8);
            this.ContentPanel.Controls.Add(this.txtRate);
            this.ContentPanel.Controls.Add(this.label7);
            this.ContentPanel.Controls.Add(this.txtDRNo);
            this.ContentPanel.Controls.Add(this.cboDestination);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.cboPlateNo);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.dpDate);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtVType);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(668, 303);
            this.ContentPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(306, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Trucking Information";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtVType
            // 
            this.txtVType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtVType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtVType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtVType.Location = new System.Drawing.Point(108, 143);
            this.txtVType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtVType.MaxLength = 64;
            this.txtVType.Name = "txtVType";
            this.txtVType.ReadOnly = true;
            this.txtVType.Size = new System.Drawing.Size(210, 14);
            this.txtVType.TabIndex = 2;
            this.txtVType.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(12, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dpDate
            // 
            this.dpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpDate.CustomFormat = "MMMM dd, yyyy";
            this.dpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpDate.Location = new System.Drawing.Point(108, 80);
            this.dpDate.Name = "dpDate";
            this.dpDate.Size = new System.Drawing.Size(210, 21);
            this.dpDate.TabIndex = 0;
            this.dpDate.Value = new System.DateTime(2016, 7, 3, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(12, 109);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Plate No.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboPlateNo
            // 
            this.cboPlateNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboPlateNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlateNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboPlateNo.FormattingEnabled = true;
            this.cboPlateNo.Location = new System.Drawing.Point(108, 109);
            this.cboPlateNo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboPlateNo.Name = "cboPlateNo";
            this.cboPlateNo.Size = new System.Drawing.Size(210, 21);
            this.cboPlateNo.TabIndex = 1;
            this.cboPlateNo.SelectedIndexChanged += new System.EventHandler(this.cboPlateNo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(12, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Vehicle Type:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label5.Location = new System.Drawing.Point(344, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(306, 24);
            this.label5.TabIndex = 12;
            this.label5.Text = "Destination";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboDestination
            // 
            this.cboDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDestination.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboDestination.FormattingEnabled = true;
            this.cboDestination.Location = new System.Drawing.Point(440, 82);
            this.cboDestination.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboDestination.Name = "cboDestination";
            this.cboDestination.Size = new System.Drawing.Size(210, 21);
            this.cboDestination.TabIndex = 4;
            this.cboDestination.SelectedIndexChanged += new System.EventHandler(this.cboDestination_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Location = new System.Drawing.Point(344, 82);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 18);
            this.label6.TabIndex = 13;
            this.label6.Text = "Destination:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDRNo
            // 
            this.txtDRNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDRNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtDRNo.Location = new System.Drawing.Point(108, 167);
            this.txtDRNo.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDRNo.MaxLength = 32;
            this.txtDRNo.Name = "txtDRNo";
            this.txtDRNo.Size = new System.Drawing.Size(210, 21);
            this.txtDRNo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.Location = new System.Drawing.Point(12, 167);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 18);
            this.label7.TabIndex = 16;
            this.label7.Text = "DR No.:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.Location = new System.Drawing.Point(344, 140);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 18);
            this.label8.TabIndex = 18;
            this.label8.Text = "Rate:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRate
            // 
            this.txtRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtRate.Location = new System.Drawing.Point(440, 140);
            this.txtRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtRate.MaxLength = 32;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(210, 21);
            this.txtRate.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label9.Location = new System.Drawing.Point(12, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(638, 24);
            this.label9.TabIndex = 19;
            this.label9.Text = "Employee Rates";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.Location = new System.Drawing.Point(12, 263);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 18);
            this.label10.TabIndex = 23;
            this.label10.Text = "Driver Rate:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDriverRate
            // 
            this.txtDriverRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDriverRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtDriverRate.Location = new System.Drawing.Point(108, 263);
            this.txtDriverRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDriverRate.MaxLength = 32;
            this.txtDriverRate.Name = "txtDriverRate";
            this.txtDriverRate.Size = new System.Drawing.Size(210, 21);
            this.txtDriverRate.TabIndex = 12;
            // 
            // cboDriver
            // 
            this.cboDriver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDriver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboDriver.FormattingEnabled = true;
            this.cboDriver.Location = new System.Drawing.Point(108, 232);
            this.cboDriver.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboDriver.Name = "cboDriver";
            this.cboDriver.Size = new System.Drawing.Size(210, 21);
            this.cboDriver.TabIndex = 11;
            this.cboDriver.SelectedIndexChanged += new System.EventHandler(this.cboDriver_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.Location = new System.Drawing.Point(12, 232);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "Driver:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.Location = new System.Drawing.Point(344, 263);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 18);
            this.label12.TabIndex = 27;
            this.label12.Text = "Helper Rate:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtHelperRate
            // 
            this.txtHelperRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtHelperRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtHelperRate.Location = new System.Drawing.Point(440, 263);
            this.txtHelperRate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtHelperRate.MaxLength = 32;
            this.txtHelperRate.Name = "txtHelperRate";
            this.txtHelperRate.Size = new System.Drawing.Size(210, 21);
            this.txtHelperRate.TabIndex = 14;
            // 
            // cboHelper
            // 
            this.cboHelper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboHelper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHelper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboHelper.FormattingEnabled = true;
            this.cboHelper.Location = new System.Drawing.Point(440, 232);
            this.cboHelper.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboHelper.Name = "cboHelper";
            this.cboHelper.Size = new System.Drawing.Size(210, 21);
            this.cboHelper.TabIndex = 13;
            this.cboHelper.SelectedIndexChanged += new System.EventHandler(this.cboHelper_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.Location = new System.Drawing.Point(344, 232);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 18);
            this.label13.TabIndex = 24;
            this.label13.Text = "Helper:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label14.Location = new System.Drawing.Point(291, 51);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 28;
            this.label14.Text = "?";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MainToolTip.SetToolTip(this.label14, resources.GetString("label14.ToolTip"));
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label15.Location = new System.Drawing.Point(623, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 18);
            this.label15.TabIndex = 29;
            this.label15.Text = "?";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MainToolTip.SetToolTip(this.label15, resources.GetString("label15.ToolTip"));
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.label16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label16.Location = new System.Drawing.Point(623, 201);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 18);
            this.label16.TabIndex = 30;
            this.label16.Text = "?";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MainToolTip.SetToolTip(this.label16, resources.GetString("label16.ToolTip"));
            // 
            // MainToolTip
            // 
            this.MainToolTip.AutoPopDelay = 30000;
            this.MainToolTip.InitialDelay = 500;
            this.MainToolTip.ReshowDelay = 100;
            // 
            // chkBackload
            // 
            this.chkBackload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkBackload.AutoSize = true;
            this.chkBackload.Location = new System.Drawing.Point(440, 113);
            this.chkBackload.Name = "chkBackload";
            this.chkBackload.Size = new System.Drawing.Size(78, 17);
            this.chkBackload.TabIndex = 5;
            this.chkBackload.Text = "Backload";
            this.chkBackload.UseVisualStyleBackColor = true;
            this.chkBackload.CheckedChanged += new System.EventHandler(this.chkBackload_CheckedChanged);
            // 
            // btn25
            // 
            this.btn25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn25.Location = new System.Drawing.Point(440, 167);
            this.btn25.Name = "btn25";
            this.btn25.Size = new System.Drawing.Size(45, 23);
            this.btn25.TabIndex = 7;
            this.btn25.Text = "25%";
            this.btn25.UseVisualStyleBackColor = true;
            this.btn25.Visible = false;
            this.btn25.Click += new System.EventHandler(this.btn25_Click);
            // 
            // btn50
            // 
            this.btn50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn50.Location = new System.Drawing.Point(492, 167);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(45, 23);
            this.btn50.TabIndex = 8;
            this.btn50.Text = "50%";
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Visible = false;
            this.btn50.Click += new System.EventHandler(this.btn50_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReset.Location = new System.Drawing.Point(595, 167);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(55, 23);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btn75
            // 
            this.btn75.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn75.Location = new System.Drawing.Point(543, 167);
            this.btn75.Name = "btn75";
            this.btn75.Size = new System.Drawing.Size(45, 23);
            this.btn75.TabIndex = 9;
            this.btn75.Text = "75%";
            this.btn75.UseVisualStyleBackColor = true;
            this.btn75.Visible = false;
            this.btn75.Click += new System.EventHandler(this.btn75_Click);
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.lblErrorMessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMessage.ForeColor = System.Drawing.Color.White;
            this.lblErrorMessage.Location = new System.Drawing.Point(12, 14);
            this.lblErrorMessage.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Padding = new System.Windows.Forms.Padding(5);
            this.lblErrorMessage.Size = new System.Drawing.Size(638, 24);
            this.lblErrorMessage.TabIndex = 36;
            this.lblErrorMessage.Text = "Error: No rates found for the selected vehicle\'s type.";
            this.lblErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TruckingLogDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(668, 353);
            this.Name = "TruckingLogDialog";
            this.Text = "Trucking Log";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.TruckingLogDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.TruckingLogDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local Variables

        bool finalize = false;

        BindingList<ComboboxDataItem> vehicles = new BindingList<ComboboxDataItem>();
        BindingList<ComboboxRateItem> rates = new BindingList<ComboboxRateItem>();

        BindingList<ComboboxDataItem> drivers = new BindingList<ComboboxDataItem>();
        BindingList<ComboboxDataItem> helpers = new BindingList<ComboboxDataItem>();
        List<ComboboxDataItem> allemployees = new List<ComboboxDataItem>();

        List<ComboboxDataItem> vtypes = new List<ComboboxDataItem>();

        #endregion

        #region Public Data functions

        public void SetFieldValues(string date, string plateno, string drno, string destination, string rate, string driver, string driver_rate, string helper, string helper_rate, string backload) {
            dpDate.Text = date;
            cboPlateNo.Text = plateno;
            txtDRNo.Text = drno;
            cboDestination.Text = destination;
            txtRate.Text = rate;
            cboDriver.Text = driver;
            txtDriverRate.Text = driver_rate;
            cboHelper.Text = helper;
            txtHelperRate.Text = helper_rate;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
            chkBackload.Checked = Convert.ToBoolean(backload);
        }

        public void GetFieldValues(out string date, out string plateno, out string drno, out string destination, out string rate, out string driver, out string driver_rate, out string helper, out string helper_rate, out bool backload) {
            date = dpDate.Value.ToShortDateString();
            plateno = cboPlateNo.Text;
            drno = txtDRNo.Text.Trim();
            destination = cboDestination.Text;
            rate = Convert.ToDouble(txtRate.Text).ToString("0.00");
            driver = "{" + cboDriver.SelectedValue.ToString() + "}";
            driver_rate = (txtDriverRate.Text.Trim() != "") ? Convert.ToDouble(txtDriverRate.Text).ToString("0.00") : "0.00";
            helper = "{" + cboHelper.SelectedValue.ToString() + "}";
            helper_rate = (txtHelperRate.Text.Trim() != "") ? Convert.ToDouble(txtHelperRate.Text).ToString("0.00") : "0.00";
            backload = chkBackload.Checked;
        }

        #endregion

        #region Public Functions

        /* Determine if should be finalized */
        public bool IsFinalizing() {
            return finalize;
        }

        /* Dates */ 
        public void SetDatePickerMax(string date) {
            dpDate.MaxDate = DateTime.Parse(date);
        }

        public void SetDatePickerMin(string date) {
            dpDate.MinDate = DateTime.Parse(date);
        }

        public void SetDatePickerDate(string date) {
            dpDate.Text = date;
        }

        /* Plate number */
        public void SetupPlateNo(ComboboxDataItem[] platenos) {
            vehicles.Clear();
            cboPlateNo.Items.Clear();
            foreach (ComboboxDataItem row in platenos) {
                vehicles.Add(row);
            }
            cboPlateNo.DataSource = vehicles;
            cboPlateNo.DisplayMember = "Value";
            cboPlateNo.ValueMember = "ID";
            cboPlateNo.SelectedIndex = -1;
        }

        /* Vehicle type */
        public void SetupVehicleTypes(ComboboxDataItem[] types) {
            vtypes.Clear();
            vtypes = types.ToList();
        }

        /* Employees */
        public void SetupEmployeesSelection(List<ComboboxDataItem> employees) {
            allemployees = employees;
            allemployees.Insert(0, new ComboboxDataItem("", ""));
        }
        
        #endregion

        #region Local Functions

        void SetEmployees() {
            drivers = new BindingList<ComboboxDataItem>(allemployees);
            helpers = new BindingList<ComboboxDataItem>(allemployees);
            cboDriver.DataSource = drivers;
            cboDriver.DisplayMember = "Value";
            cboDriver.ValueMember = "ID";
            cboHelper.DataSource = helpers;
            cboHelper.DisplayMember = "Value";
            cboHelper.ValueMember = "ID";
        }

        void ClearEmployees() {
            cboDriver.DataSource = null;
            cboHelper.DataSource = null;
        }

        void SetNoticeLabel(string message) {
            //392
            lblErrorMessage.Text = message;
            lblErrorMessage.BackColor = ColorTranslator.FromHtml("#4389c5");
            lblErrorMessage.Visible = true;
            Height = 392;
        }

        void SetErrorLabel(string message) {
            //392
            lblErrorMessage.Text = message;
            lblErrorMessage.BackColor = Color.FromArgb(211, 71, 94);
            lblErrorMessage.Visible = true;
            Height = 392;
        }

        void HideMessageLabel() {
            //358
            lblErrorMessage.Text = "";
            lblErrorMessage.Visible = false;
            Height = 358;
        }

        void SelectVehicleType(string type_id) {
            foreach (ComboboxDataItem itm in vtypes) {
                if (type_id == itm.ID) {
                    txtVType.Text = itm.Value;
                    GetRates("{" + itm.ID + "}");
                    break;
                }
            }
        }

        /* Rates */
        void GetRates(string typeID) {
            rates = new BindingList<ComboboxRateItem>(RatesPage.QueryRates(typeID));
            if (rates.Count > 0) {
                updating_destinations = true;
                cboDestination.DataSource = rates;
                cboDestination.DisplayMember = "Destination";
                cboDestination.ValueMember = "ID";
                cboDestination.SelectedIndex = -1;
                SetNoticeLabel("Please choose the destination.");
                updating_destinations = false;
            } else {
                cboDestination.DataSource = null;
                cboDestination.Items.Clear();
                SetErrorLabel("Error: No rates found for the selected vehicle's type.");
            }
        }

        string GetSelectedRate(double discount = 0.0) {
            ComboboxRateItem itm = (ComboboxRateItem)cboDestination.SelectedItem;
            if (itm != null) {
                double rate = itm.Rate - (itm.Rate * discount);
                return rate.ToString("0.00");
            }
            return "";
        }

        string GetSelectedDriverRate(double discount = 0.0) {
            ComboboxRateItem itm = (ComboboxRateItem)cboDestination.SelectedItem;
            if (itm != null) {
                double rate = itm.DriverRate - (itm.DriverRate * discount);
                return rate.ToString("0.00");
            }
            return "";
        }

        string GetSelectedHelperRate(double discount = 0.0) {
            ComboboxRateItem itm = (ComboboxRateItem)cboDestination.SelectedItem;
            if (itm != null) {
                double rate = itm.HelperRate - (itm.HelperRate * discount);
                return rate.ToString("0.00");
            }
            return "";
        }

        #endregion

        #region Window Event handlers

        private void TruckingLogDialog_Load(object sender, EventArgs e) {
            //dpDate.MaxDate = DateTime.Now;
            SetNoticeLabel("Please set the date of the trucking service, then the plate no. of the vehicle involved.");
            if(FileMaintenanceMode == FileMaintenanceDialogMode.Editing)
                HideMessageLabel();
        }

        #endregion

        #region Control Event handlers

        private void cboDriver_SelectedIndexChanged(object sender, EventArgs e) {
            if (cboDriver.Text.Trim() == "") txtDriverRate.Text = "";
            else txtDriverRate.Text = GetSelectedDriverRate();
        }

        private void cboHelper_SelectedIndexChanged(object sender, EventArgs e) {
            if (cboHelper.Text.Trim() == "") txtHelperRate.Text = "";
            else txtHelperRate.Text = GetSelectedHelperRate();
        }

        private void btn50_Click(object sender, EventArgs e) {
            txtRate.Text = GetSelectedRate(.50);
            if (cboDriver.Text.Trim() != "") {
                txtDriverRate.Text = GetSelectedDriverRate(.50);
            } else {
                txtDriverRate.Text = "";
            }
            if (cboHelper.Text.Trim() != "") {
                txtHelperRate.Text = GetSelectedHelperRate(.50);
            } else {
                txtHelperRate.Text = "";
            }
        }

        private void btn75_Click(object sender, EventArgs e) {
            txtRate.Text = GetSelectedRate(.25);
            if (cboDriver.Text.Trim() != "") {
                txtDriverRate.Text = GetSelectedDriverRate(.25);
            } else {
                txtDriverRate.Text = "";
            }
            if (cboHelper.Text.Trim() != "") {
                txtHelperRate.Text = GetSelectedHelperRate(.25);
            } else {
                txtHelperRate.Text = "";
            }
        }

        private void btnReset_Click(object sender, EventArgs e) {
            txtRate.Text = GetSelectedRate(0.0);
            if (cboDriver.Text.Trim() != "") {
                txtDriverRate.Text = GetSelectedDriverRate();
            } else {
                txtDriverRate.Text = "";
            }
            if (cboHelper.Text.Trim() != "") {
                txtHelperRate.Text = GetSelectedHelperRate();
            } else {
                txtHelperRate.Text = "";
            }
        }


        private void btn25_Click(object sender, EventArgs e) {
            txtRate.Text = GetSelectedRate(.75);
            if (cboDriver.Text.Trim() != "") {
                txtDriverRate.Text = GetSelectedDriverRate(.75);
            } else {
                txtDriverRate.Text = "";
            }
            if (cboHelper.Text.Trim() != "") {
                txtHelperRate.Text = GetSelectedHelperRate(.75);
            } else {
                txtHelperRate.Text = "";
            }
        }

        bool updating_destinations = false;
        private void cboDestination_SelectedIndexChanged(object sender, EventArgs e) {
            if (cboDestination.Items.Count > 0) {
                if (cboDestination.SelectedIndex > -1) {
                    if (updating_destinations == false) HideMessageLabel();
                    txtRate.Text = GetSelectedRate();
                    SetEmployees();
                } else {
                    txtRate.Text = "";
                    ClearEmployees();
                }
            }
        }

        private void cboPlateNo_SelectedIndexChanged(object sender, EventArgs e) {
            if(cboPlateNo.SelectedValue != null)
                SelectVehicleType(cboPlateNo.SelectedValue.ToString());
        }

        private void chkBackload_CheckedChanged(object sender, EventArgs e) {
            if (chkBackload.Checked) {
                btn25.Visible = btn50.Visible = btn75.Visible = true;
            } else {
                btnReset.PerformClick();
                btn25.Visible = btn50.Visible = btn75.Visible = false;
            }
        }

        #endregion

        #region Validation

        private void TruckingLogDialog_OKButtonClicked(object sender, DialogEventArgs e) {

            bool completed = true;
            List<String> incomplete_fields = new List<String>();

            if (InputValidators.IsEmptyTrimmed(cboPlateNo.Text)) {
                Notifier.ShowWarning("Please select a vehicle plate number.", "Invalid Input");
                cboPlateNo.Focus();
                return;
            }

            if (InputValidators.IsEmptyTrimmed(txtDRNo.Text)) {
                completed = false;
                incomplete_fields.Add("DR No.");
            }

            if (InputValidators.IsEmptyTrimmed(cboDestination.Text)) {
                Notifier.ShowWarning("Please indicate destination of this transport log.", "Invalid Input");
                cboDestination.Focus();
                return;
            }

            if (InputValidators.IsEmptyTrimmed(txtRate.Text)) {
                Notifier.ShowWarning("Please specify the trucking rate for this destination.", "Invalid Input");
                txtRate.Focus();
                return;
            }

            if (InputValidators.IsNumericWithDecimal(txtRate.Text) == false) {
                Notifier.ShowWarning("The trucking rate you've entered contains invalid characters. Please enter only numeric values.", "Invalid Input");
                txtRate.Focus();
                return;
            }

            if (Convert.ToDouble(txtRate.Text) < 0.0) {
                Notifier.ShowWarning("Trucking rate cannot be negative.", "Invalid Input");
                txtRate.Focus();
                return;
            }

            if (InputValidators.IsEmptyTrimmed(cboDriver.Text)) {
                txtDriverRate.Text = "";
                completed = false;
                incomplete_fields.Add("Driver and Driver Rate");
            } else {
                if (InputValidators.IsEmptyTrimmed(txtDriverRate.Text)) {
                    Notifier.ShowWarning("Please enter driver rate.", "Invalid Input");
                    txtDriverRate.Focus();
                    return;
                }
                if (InputValidators.IsNumericWithDecimal(txtDriverRate.Text) == false) {
                    Notifier.ShowWarning("Driver rate seems to contain invalid characters. Please enter numeric values only.", "Invalid Input");
                    txtDriverRate.Focus();
                    return;
                }
                if (Convert.ToDouble(txtDriverRate.Text) < 0.0) {
                    Notifier.ShowWarning("Driver rate cannot be negative.", "Invalid Input");
                    txtRate.Focus();
                    return;
                }
            }

            if (InputValidators.IsEmptyTrimmed(cboHelper.Text)) {
                txtHelperRate.Text = "";
                completed = false;
                incomplete_fields.Add("Helper and helper Rate");
            } else {
                if (InputValidators.IsEmptyTrimmed(txtHelperRate.Text)) {
                    Notifier.ShowWarning("Please enter helper rate.", "Invalid Input");
                    txtHelperRate.Focus();
                    return;
                }
                if (InputValidators.IsNumericWithDecimal(txtHelperRate.Text) == false) {
                    Notifier.ShowWarning("Helper rate seems to contain invalid characters. Please enter numeric values only.", "Invalid Input");
                    txtHelperRate.Focus();
                    return;
                } 
                if (Convert.ToDouble(txtHelperRate.Text) < 0.0) {
                    Notifier.ShowWarning("Helper rate cannot be negative.", "Invalid Input");
                    txtRate.Focus();
                    return;
                }
            }

            if (cboDriver.Text.Trim() != "" && cboHelper.Text.Trim() != "") {
                if (cboDriver.Text.Trim().Equals(cboHelper.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) {
                    Notifier.ShowWarning("An employee cannot be a driver and a helper at the same time.", "Invalid Input");
                    cboDriver.Focus();
                    return;
                }
            }

            if (completed == false) {
                string list_of_incomplete = "";
                foreach (string s in incomplete_fields) {
                    list_of_incomplete += "\u2022 " + s + "\n";
                }
                if (Notifier.ShowConfirm("It seems that you haven't completed this trucking log entry. Before you can finalize this record, all fields must be completed.\n\nIncomplete fields:\n\n" + list_of_incomplete + "\nContinue saving this record and complete it later?", "Incomplete Record") == System.Windows.Forms.DialogResult.No) {
                    return;
                }
            } else {
                DialogResult msg = MessageBox.Show("The entry you're about to save is complete. You have the option to finalize this now to prevent further modifications.\n\nFinalize upon saving?", "Save Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (msg == System.Windows.Forms.DialogResult.Cancel) {
                    return;
                } else if (msg == System.Windows.Forms.DialogResult.Yes) {
                    finalize = true;
                }
            }
            e.Cancel = false;
        }

        #endregion

    }

}