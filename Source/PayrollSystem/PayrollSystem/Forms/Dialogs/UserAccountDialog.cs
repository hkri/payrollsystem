﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class UserAccountDialog : FileMaintenanceDialog {

        #region Controls

        internal TextBox txtUN;
        private Label label2;
        internal TextBox txtPW;
        private Label label3;
        private Label label4;
        internal ComboBox cboType;
        internal TextBox txtAN;
        private Label label5;
        private Label label6;
        internal TextBox txtCPW;
        private Label label7;
        private LinkLabel lnkChangePassword;
        private Label label1;

        #endregion

        #region Constructor

        public UserAccountDialog() {
            InitializeComponent();
            SetupUserTypes();
        }

        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPW = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCPW = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lnkChangePassword = new System.Windows.Forms.LinkLabel();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lnkChangePassword);
            this.ContentPanel.Controls.Add(this.txtCPW);
            this.ContentPanel.Controls.Add(this.label7);
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.cboType);
            this.ContentPanel.Controls.Add(this.txtAN);
            this.ContentPanel.Controls.Add(this.label5);
            this.ContentPanel.Controls.Add(this.label4);
            this.ContentPanel.Controls.Add(this.label3);
            this.ContentPanel.Controls.Add(this.txtPW);
            this.ContentPanel.Controls.Add(this.label2);
            this.ContentPanel.Controls.Add(this.txtUN);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(377, 239);
            this.ContentPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUN
            // 
            this.txtUN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtUN.Location = new System.Drawing.Point(139, 43);
            this.txtUN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtUN.MaxLength = 32;
            this.txtUN.Name = "txtUN";
            this.txtUN.Size = new System.Drawing.Size(226, 21);
            this.txtUN.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 170);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPW
            // 
            this.txtPW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtPW.Location = new System.Drawing.Point(139, 170);
            this.txtPW.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPW.MaxLength = 32;
            this.txtPW.Name = "txtPW";
            this.txtPW.Size = new System.Drawing.Size(226, 21);
            this.txtPW.TabIndex = 4;
            this.txtPW.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(353, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Basic Info";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(353, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Password";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAN
            // 
            this.txtAN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtAN.Location = new System.Drawing.Point(139, 74);
            this.txtAN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtAN.MaxLength = 32;
            this.txtAN.Name = "txtAN";
            this.txtAN.Size = new System.Drawing.Size(226, 21);
            this.txtAN.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Account Name:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(139, 105);
            this.cboType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(226, 21);
            this.cboType.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "User Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCPW
            // 
            this.txtCPW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtCPW.Location = new System.Drawing.Point(139, 201);
            this.txtCPW.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtCPW.MaxLength = 32;
            this.txtCPW.Name = "txtCPW";
            this.txtCPW.Size = new System.Drawing.Size(226, 21);
            this.txtCPW.TabIndex = 5;
            this.txtCPW.UseSystemPasswordChar = true;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 201);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 18);
            this.label7.TabIndex = 10;
            this.label7.Text = "Confirm Password:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lnkChangePassword
            // 
            this.lnkChangePassword.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangePassword.BackColor = System.Drawing.Color.Gainsboro;
            this.lnkChangePassword.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangePassword.Location = new System.Drawing.Point(293, 136);
            this.lnkChangePassword.Name = "lnkChangePassword";
            this.lnkChangePassword.Size = new System.Drawing.Size(72, 24);
            this.lnkChangePassword.TabIndex = 3;
            this.lnkChangePassword.TabStop = true;
            this.lnkChangePassword.Text = "Change";
            this.lnkChangePassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lnkChangePassword.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkChangePassword.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkChangePassword_LinkClicked);
            // 
            // UserAccountDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(377, 289);
            this.Name = "UserAccountDialog";
            this.Text = "User Account";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.UserAccountDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.UserAccountDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Local functions

        void EnableChangePasswordLink() {
            lnkChangePassword.Visible = true;
        }

        void DisableChangePasswordLink() {
            lnkChangePassword.Visible = false;
        }

        void DisablePasswordFields() {
            txtPW.Enabled = txtCPW.Enabled = false;
            txtPW.Text = txtCPW.Text = "password";
        }

        void EnablePasswordFields() {
            txtPW.Enabled = txtCPW.Enabled = true;
            txtPW.Text = txtCPW.Text = "";
        }

        void SetupUserTypes() {
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>();
            lst.Add(new ComboboxDataItem("0", "Administrator"));
            lst.Add(new ComboboxDataItem("1", "Accountant"));
            cboType.ValueMember = "ID";
            cboType.DisplayMember = "Value";
            cboType.DataSource = lst;
            if (cboType.Text.Trim() == "") cboType.SelectedIndex = 0;
        }

        #endregion

        #region window eventhandlers

        private void UserAccountDialog_Load(object sender, EventArgs e) {
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                EnableChangePasswordLink();
                DisablePasswordFields();
            } else if(FileMaintenanceMode == FileMaintenanceDialogMode.Add){
                DisableChangePasswordLink();
            }
        }

        #endregion

        #region local variables

        string oldPassword = "";
        string oldUN = "";
        int admins = 0;
        string[] existing_un = null;

        #endregion

        #region public functions

        public void SetOldUN(string un) {
            oldUN = un;
        }

        public void SetExistingUN(string[] un) {
            existing_un = un;
        }

        public void SetAdminUserCount(int count) {
            admins = count;
        }

        public void SetFieldValues(string username, string account_name, string utype, string oldpw) {
            txtUN.Text = username;
            txtAN.Text = account_name;
            oldPassword = oldpw;
            cboType.Text = utype;
            oldUN = username;
        }

        public void GetFieldValues(out string username, out string account_name, out string account_type, out string password) {
            username = txtUN.Text;
            account_name = txtAN.Text;
            account_type = cboType.SelectedValue.ToString();
            password = (txtPW.Enabled == false && FileMaintenanceMode == FileMaintenanceDialogMode.Editing) ? oldPassword : txtPW.Text;
        }

        #endregion

        #region validation

        private void UserAccountDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtUN.Text)) {
                Notifier.ShowWarning("Username cannot be empty.", "Invalid Input");
                txtUN.Focus();
                return;
            } 
            if (InputValidators.IsEmptyTrimmed(txtAN.Text)) {
                Notifier.ShowWarning("Account name cannot be empty.", "Invalid Input");
                txtAN.Focus();
                return;
            }
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                if (admins <= 1 && cboType.Text != "Administrator") {
                    Notifier.ShowWarning("There should be at least one (1) system administrator.", "Invalid Input");
                    cboType.Focus();
                    cboType.Text = "Administrator";
                    return;
                }
            }
            if (InputValidators.IsNumericWithDecimal(txtUN.Text)) {
                Notifier.ShowWarning("Username should be a combination of alphanumeric (A-Z, 0-9) characters.", "Invalid Input");
                txtUN.Focus();
                txtUN.SelectAll();
                return;
            } 
            if (InputValidators.IsNumericWithDecimal(txtAN.Text)) {
                Notifier.ShowWarning("Username should be a combination of alphanumeric (A-Z, 0-9) characters.", "Invalid Input");
                txtAN.Focus();
                txtAN.SelectAll();
                return;
            }
            if (InputValidators.ContainsSpaces(txtUN.Text)) {
                Notifier.ShowWarning("Usernames should not contain spaces.", "Invalid Input");
                txtUN.Focus();
                return;
            }
            if (txtPW.Enabled && txtCPW.Enabled) {
                if (txtPW.Text.Length == 0) {
                    Notifier.ShowWarning("You must enter a password to secure your account.", "Invalid Input");
                    txtPW.Focus();
                    return;
                }
                if (txtPW.Text != txtCPW.Text) {
                    Notifier.ShowWarning("Passwords do not match. Please try again.", "Invalid Input");
                    txtCPW.Focus();
                    txtCPW.SelectAll();
                    return;
                }
            }
            if (existing_un != null) {
                foreach (string s in existing_un) {
                    if (s.Equals(txtUN.Text)) {
                        if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                            //check if old un
                            if (s != oldUN) {
                                Notifier.ShowWarning("Username already exists. Please enter a different one.", "Invalid Input");
                                txtUN.Focus();
                                txtUN.SelectAll();
                                return;
                            }
                        } else {
                            Notifier.ShowWarning("Username already exists. Please enter a different one.", "Invalid Input");
                            txtUN.Focus();
                            txtUN.SelectAll();
                            return;
                        }
                    }
                }
            }
            e.Cancel = false;
        }

        #endregion

        #region control eventhandlers

        private void lnkChangePassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            OldPasswordDialog dlg = new OldPasswordDialog();
            dlg.OldPassword = oldPassword;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //allow user to enter new password here.
                EnablePasswordFields();
                DisableChangePasswordLink();
            }
        }

        #endregion

    }

}
