﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace PayrollSystem {

    class VehicleDialog : FileMaintenanceDialog {
        internal TextBox txtPN;
        private Label label6;
        internal ComboBox cboType;
        private Label label1;

        #region Controls
        #endregion

        #region Constructor

        public VehicleDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.txtPN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.label6);
            this.ContentPanel.Controls.Add(this.cboType);
            this.ContentPanel.Controls.Add(this.txtPN);
            this.ContentPanel.Controls.Add(this.label1);
            this.ContentPanel.Size = new System.Drawing.Size(270, 113);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtPN
            // 
            this.txtPN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtPN.Location = new System.Drawing.Point(91, 31);
            this.txtPN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPN.MaxLength = 16;
            this.txtPN.Name = "txtPN";
            this.txtPN.Size = new System.Drawing.Size(172, 21);
            this.txtPN.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Plate No:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 62);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(91, 62);
            this.cboType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(172, 21);
            this.cboType.TabIndex = 1;
            // 
            // VehicleDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(270, 163);
            this.Name = "VehicleDialog";
            this.Text = "Vehicle";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.VehicleDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.VehicleDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion


        #region Local Variables

        string[] existing_pn = null;
        string oldpn = "";

        #endregion

        #region Public Functions

        public void SetVehicleTypes(ComboboxDataItem[] items) {
            BindingList<ComboboxDataItem> lst = new BindingList<ComboboxDataItem>();
            foreach (ComboboxDataItem i in items) lst.Add(i);
            cboType.DataSource = lst;
            cboType.DisplayMember = "Value";
            cboType.ValueMember = "ID";
            if (cboType.Items.Count > 0) {
                cboType.SelectedIndex = 0;
            }
        }

        public void SetExistingPlateNumbers(string[] pns) {
            existing_pn = pns;
        }

        public void SetFieldValues(string plateno, string type) {
            txtPN.Text = plateno;
            oldpn = plateno;
            cboType.Text = type;
        }

        public void GetFieldValues(out string plateno, out string typeid) {
            plateno = txtPN.Text;
            typeid = cboType.SelectedValue.ToString();
        }

        #endregion

        #region Local Functions

        #endregion

        #region Window Event handlers

        private void VehicleDialog_Load(object sender, EventArgs e) {

        }

        #endregion

        #region Control Event handlers
        #endregion

        #region Validation

        private void VehicleDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (txtPN.Text.Trim() == "") {
                Notifier.ShowWarning("Plate number field cannot be empty.", "Invalid Input");
                return;
            }
            if (cboType.Text.Trim() == "") {
                Notifier.ShowWarning("Please add vehicle types first before adding vehicles.", "Invalid Input");
                return;
            }
            if (existing_pn != null) {
                foreach (string p in existing_pn) {
                    if (txtPN.Text.Trim().Equals(p.Trim(), StringComparison.CurrentCultureIgnoreCase)) {
                        if (p != oldpn) {
                            Notifier.ShowWarning("Vehicle with the same plate number already exists.", "Invalid Input");
                            return;
                        }
                    }
                }
            }
            e.Cancel = false;
        }

        #endregion

    }

}