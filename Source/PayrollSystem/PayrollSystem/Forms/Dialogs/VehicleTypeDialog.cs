﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollSystem {

    class VehicleTypeDialog : FileMaintenanceDialog {

        #region Control Declarations

        internal TextBox txtType; 
        private Label lbl1;

        #endregion

        #region Constructor

        public VehicleTypeDialog() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.txtType = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.ContentPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.lbl1);
            this.ContentPanel.Controls.Add(this.txtType);
            this.ContentPanel.Size = new System.Drawing.Size(407, 108);
            this.ContentPanel.TabIndex = 0;
            // 
            // txtType
            // 
            this.txtType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtType.Location = new System.Drawing.Point(12, 58);
            this.txtType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtType.MaxLength = 64;
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(383, 21);
            this.txtType.TabIndex = 0;
            // 
            // lbl1
            // 
            this.lbl1.Location = new System.Drawing.Point(12, 14);
            this.lbl1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(383, 34);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Please enter the vehicle type description in the field below.";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VehicleTypeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(407, 158);
            this.Name = "VehicleTypeDialog";
            this.Text = "Vehicle Type";
            this.OKButtonClicked += new PayrollSystem.Eventhandlers.FileMaintenanceDialogEventHandler(this.VehicleTypeDialog_OKButtonClicked);
            this.Load += new System.EventHandler(this.VehicleTypeDialog_Load);
            this.ContentPanel.ResumeLayout(false);
            this.ContentPanel.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        #region Local Variables

        string[] existing_types = null;
        string old_vehicletype = "";

        #endregion

        #region public functions

        public void SetExistingTypes(string[] types) {
            existing_types = types;
        }

        public void GetFieldValues(out string vehicletype) {
            vehicletype = txtType.Text;
        }

        public void SetFieldValues(string vehicletype) {
            txtType.Text = vehicletype;
            old_vehicletype = vehicletype;
            FileMaintenanceMode = FileMaintenanceDialogMode.Editing;
        }

        #endregion

        #region validations

        private void VehicleTypeDialog_OKButtonClicked(object sender, DialogEventArgs e) {
            if (InputValidators.IsEmptyTrimmed(txtType.Text)) {
                Notifier.ShowWarning("Vehicle type description cannot be empty.", "Invalid Input");
                txtType.Focus();
                return;
            } 
            if (InputValidators.ContainsAlphabet(txtType.Text) == false) {
                Notifier.ShowWarning("Vehicle type description is not in a valid name format. Please use alphabetic characters.", "Invalid Input");
                txtType.Focus();
                return;
            }
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                if (old_vehicletype == txtType.Text) {
                    DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    return;
                }
            }
            if (existing_types != null) {
                foreach (string s in existing_types) {
                    if (txtType.Text.Trim().Equals(s.Trim(), StringComparison.CurrentCultureIgnoreCase)) {
                        if (txtType.Text != old_vehicletype) {
                            Notifier.ShowWarning("Type with the same name already exists.", "Invalid Input");
                            return;
                        }
                    }
                }
            }
            e.Cancel = false;
        }

        #endregion

        #region Window eventhandlers

        private void VehicleTypeDialog_Load(object sender, EventArgs e) {
            if (FileMaintenanceMode == FileMaintenanceDialogMode.Editing) {
                old_vehicletype = txtType.Text;
            }
        }

        #endregion

        #region private functions

        #endregion

    }

}
