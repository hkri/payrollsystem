﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace PayrollSystem {
    public partial class LoginWindow : Form {

        #region Constructor

        public LoginWindow() {
            InitializeComponent();
        }

        #endregion

        #region form eventhandlers

        private void LoginWindow_Load(object sender, EventArgs e) {

        }

        #endregion

        #region validation functions

        bool ValidateLoginEntry() {
            LoginErrorProvider.Clear();
            if (txtUsername.Text == "") {
                LoginErrorProvider.SetError(lblUsername, "Please enter your username.");
                txtUsername.Focus();
                return false;
            }
            if (txtPassword.Text == "") {
                LoginErrorProvider.SetError(lblPassword, "Please enter your password.");
                txtPassword.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region Validate Login

        bool Login() {
            bool success = false;
            CEDatabase db = DatabaseManager.GetLocalDatabase();
            SqlCeParameter[] prms = new SqlCeParameter[1];
            prms[0] = new SqlCeParameter("@un", txtUsername.Text);
            using (DataTable res = db.ExecuteQuery(QueryStrings.LocalDatabaseTables.Users.Query_Users_Login, false, prms)) {
                if (res.Rows.Count > 0) {
                    string un = res.Rows[0]["Username"].ToString();
                    string pw = res.Rows[0]["Password"].ToString();
                    string uid = res.Rows[0]["ID"].ToString();
                    string an = res.Rows[0]["Account Name"].ToString();
                    int type = Convert.ToInt32(res.Rows[0]["Type"].ToString());
                    if (txtUsername.Text.Equals(un) && txtPassword.Text.Equals(pw)) {
                        success = true;
                        UserManager.SetActiveUser(uid, un, an, type);
                        SystemLogger.WriteLog(db, un + " (" + an + ")", "Successful login.");
                    }
                }
            }
            if (success == false)
                SystemLogger.WriteLog(db, "LOGIN MODULE", "Unsuccessful login attempt.");
            return success;
        }

        #endregion

        #region button eventhandlers

        private void btnLogin_Click(object sender, EventArgs e) {
            if (ValidateLoginEntry()) {
                if (Login()) DialogResult = System.Windows.Forms.DialogResult.OK;
                else {
                    Notifier.ShowError("Incorrect username and/or password. Please try again.");
                    txtPassword.Focus();
                    txtPassword.SelectAll();
                }
            }
        }

        #endregion

    }
}
