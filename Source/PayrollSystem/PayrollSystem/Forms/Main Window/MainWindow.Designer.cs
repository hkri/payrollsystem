﻿namespace PayrollSystem {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.XMainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.payrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.managePayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.truckingLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehiclesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.manageVehiclesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ratesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageRatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollToolStripMenuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.cashReportToolStripMenuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.statusReportToolStripMenuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesToolStripMenuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.truckingLogToolStripMenuItemArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reconnectDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.createNewDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageDatabasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.exportDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.viewErrorLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuViewRNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutPayrollSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ActiveUserMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ActiveUserTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageUserAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeSomeLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundLoadingDatagridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatusSelectedCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatusTotalCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.DatabaseStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.DatabaseStatusDBName = new System.Windows.Forms.ToolStripStatusLabel();
            this.PagePanel = new PayrollSystem.BufferedPanel();
            this.XMainMenuStrip.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // XMainMenuStrip
            // 
            this.XMainMenuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.XMainMenuStrip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XMainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.payrollToolStripMenuItem,
            this.recordsToolStripMenuItem,
            this.vehiclesToolStripMenuItem,
            this.ratesToolStripMenuItem,
            this.archivesToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.ActiveUserMenuItem,
            this.testToolStripMenuItem});
            this.XMainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.XMainMenuStrip.Name = "XMainMenuStrip";
            this.XMainMenuStrip.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.XMainMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.XMainMenuStrip.Size = new System.Drawing.Size(840, 24);
            this.XMainMenuStrip.TabIndex = 0;
            // 
            // payrollToolStripMenuItem
            // 
            this.payrollToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managePayrollToolStripMenuItem});
            this.payrollToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.payrollToolStripMenuItem.Name = "payrollToolStripMenuItem";
            this.payrollToolStripMenuItem.Size = new System.Drawing.Size(51, 24);
            this.payrollToolStripMenuItem.Text = "&Payroll";
            // 
            // managePayrollToolStripMenuItem
            // 
            this.managePayrollToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.managePayrollToolStripMenuItem.Name = "managePayrollToolStripMenuItem";
            this.managePayrollToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.managePayrollToolStripMenuItem.Text = "&Manage Payroll";
            this.managePayrollToolStripMenuItem.Click += new System.EventHandler(this.managePayrollToolStripMenuItem_Click);
            // 
            // recordsToolStripMenuItem
            // 
            this.recordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashReportToolStripMenuItem,
            this.toolStripMenuItem2,
            this.truckingLogToolStripMenuItem,
            this.employeesToolStripMenuItem});
            this.recordsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.recordsToolStripMenuItem.Name = "recordsToolStripMenuItem";
            this.recordsToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.recordsToolStripMenuItem.Text = "&Records";
            // 
            // cashReportToolStripMenuItem
            // 
            this.cashReportToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cashReportToolStripMenuItem.Name = "cashReportToolStripMenuItem";
            this.cashReportToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.cashReportToolStripMenuItem.Text = "&Cash Report";
            this.cashReportToolStripMenuItem.Click += new System.EventHandler(this.cashReportToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(131, 6);
            // 
            // truckingLogToolStripMenuItem
            // 
            this.truckingLogToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.truckingLogToolStripMenuItem.Name = "truckingLogToolStripMenuItem";
            this.truckingLogToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.truckingLogToolStripMenuItem.Text = "&Trucking Log";
            this.truckingLogToolStripMenuItem.Click += new System.EventHandler(this.truckingLogToolStripMenuItem_Click);
            // 
            // employeesToolStripMenuItem
            // 
            this.employeesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.employeesToolStripMenuItem.Name = "employeesToolStripMenuItem";
            this.employeesToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.employeesToolStripMenuItem.Text = "&Employees";
            this.employeesToolStripMenuItem.Click += new System.EventHandler(this.employeesToolStripMenuItem_Click);
            // 
            // vehiclesToolStripMenuItem
            // 
            this.vehiclesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusReportToolStripMenuItem,
            this.toolStripMenuItem1,
            this.manageVehiclesToolStripMenuItem,
            this.vehicleTypesToolStripMenuItem});
            this.vehiclesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.vehiclesToolStripMenuItem.Name = "vehiclesToolStripMenuItem";
            this.vehiclesToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.vehiclesToolStripMenuItem.Text = "&Vehicles";
            // 
            // statusReportToolStripMenuItem
            // 
            this.statusReportToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.statusReportToolStripMenuItem.Name = "statusReportToolStripMenuItem";
            this.statusReportToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.statusReportToolStripMenuItem.Text = "&Status Report";
            this.statusReportToolStripMenuItem.Click += new System.EventHandler(this.statusReportToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(150, 6);
            // 
            // manageVehiclesToolStripMenuItem
            // 
            this.manageVehiclesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.manageVehiclesToolStripMenuItem.Name = "manageVehiclesToolStripMenuItem";
            this.manageVehiclesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.manageVehiclesToolStripMenuItem.Text = "Manage &Vehicles";
            this.manageVehiclesToolStripMenuItem.Click += new System.EventHandler(this.manageVehiclesToolStripMenuItem_Click);
            // 
            // vehicleTypesToolStripMenuItem
            // 
            this.vehicleTypesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.vehicleTypesToolStripMenuItem.Name = "vehicleTypesToolStripMenuItem";
            this.vehicleTypesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.vehicleTypesToolStripMenuItem.Text = "Vehicle &Types";
            this.vehicleTypesToolStripMenuItem.Click += new System.EventHandler(this.vehicleTypesToolStripMenuItem_Click);
            // 
            // ratesToolStripMenuItem
            // 
            this.ratesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageRatesToolStripMenuItem,
            this.rateGroupsToolStripMenuItem});
            this.ratesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ratesToolStripMenuItem.Name = "ratesToolStripMenuItem";
            this.ratesToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.ratesToolStripMenuItem.Text = "Ra&tes";
            // 
            // manageRatesToolStripMenuItem
            // 
            this.manageRatesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.manageRatesToolStripMenuItem.Name = "manageRatesToolStripMenuItem";
            this.manageRatesToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.manageRatesToolStripMenuItem.Text = "Manage &Rates";
            this.manageRatesToolStripMenuItem.Click += new System.EventHandler(this.manageRatesToolStripMenuItem_Click);
            // 
            // rateGroupsToolStripMenuItem
            // 
            this.rateGroupsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.rateGroupsToolStripMenuItem.Name = "rateGroupsToolStripMenuItem";
            this.rateGroupsToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.rateGroupsToolStripMenuItem.Text = "Rate &Groups";
            this.rateGroupsToolStripMenuItem.Click += new System.EventHandler(this.rateGroupsToolStripMenuItem_Click);
            // 
            // archivesToolStripMenuItem
            // 
            this.archivesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.payrollToolStripMenuItemArchive,
            this.cashReportToolStripMenuItemArchive,
            this.statusReportToolStripMenuItemArchive,
            this.employeesToolStripMenuItemArchive,
            this.truckingLogToolStripMenuItemArchive});
            this.archivesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.archivesToolStripMenuItem.Name = "archivesToolStripMenuItem";
            this.archivesToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.archivesToolStripMenuItem.Text = "&Archives";
            // 
            // payrollToolStripMenuItemArchive
            // 
            this.payrollToolStripMenuItemArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.payrollToolStripMenuItemArchive.Name = "payrollToolStripMenuItemArchive";
            this.payrollToolStripMenuItemArchive.Size = new System.Drawing.Size(141, 22);
            this.payrollToolStripMenuItemArchive.Text = "&Payroll";
            this.payrollToolStripMenuItemArchive.Click += new System.EventHandler(this.payrollToolStripMenuItemArchive_Click);
            // 
            // cashReportToolStripMenuItemArchive
            // 
            this.cashReportToolStripMenuItemArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cashReportToolStripMenuItemArchive.Name = "cashReportToolStripMenuItemArchive";
            this.cashReportToolStripMenuItemArchive.Size = new System.Drawing.Size(141, 22);
            this.cashReportToolStripMenuItemArchive.Text = "&Cash Report";
            this.cashReportToolStripMenuItemArchive.Click += new System.EventHandler(this.cashReportToolStripMenuItemArchive_Click);
            // 
            // statusReportToolStripMenuItemArchive
            // 
            this.statusReportToolStripMenuItemArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.statusReportToolStripMenuItemArchive.Name = "statusReportToolStripMenuItemArchive";
            this.statusReportToolStripMenuItemArchive.Size = new System.Drawing.Size(141, 22);
            this.statusReportToolStripMenuItemArchive.Text = "&Status Report";
            this.statusReportToolStripMenuItemArchive.Click += new System.EventHandler(this.statusReportToolStripMenuItemArchive_Click);
            // 
            // employeesToolStripMenuItemArchive
            // 
            this.employeesToolStripMenuItemArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.employeesToolStripMenuItemArchive.Name = "employeesToolStripMenuItemArchive";
            this.employeesToolStripMenuItemArchive.Size = new System.Drawing.Size(141, 22);
            this.employeesToolStripMenuItemArchive.Text = "&Employees";
            this.employeesToolStripMenuItemArchive.Click += new System.EventHandler(this.employeesToolStripMenuItemArchive_Click);
            // 
            // truckingLogToolStripMenuItemArchive
            // 
            this.truckingLogToolStripMenuItemArchive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.truckingLogToolStripMenuItemArchive.Name = "truckingLogToolStripMenuItemArchive";
            this.truckingLogToolStripMenuItemArchive.Size = new System.Drawing.Size(141, 22);
            this.truckingLogToolStripMenuItemArchive.Text = "&Trucking Log";
            this.truckingLogToolStripMenuItemArchive.Click += new System.EventHandler(this.truckingLogToolStripMenuItemArchive_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDatabaseToolStripMenuItem,
            this.reconnectDatabaseToolStripMenuItem,
            this.toolStripMenuItem3,
            this.createNewDatabaseToolStripMenuItem,
            this.backupDatabaseToolStripMenuItem,
            this.manageDatabasesToolStripMenuItem,
            this.toolStripMenuItem4,
            this.exportDatabaseToolStripMenuItem});
            this.databaseToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.databaseToolStripMenuItem.Text = "&Database";
            // 
            // loadDatabaseToolStripMenuItem
            // 
            this.loadDatabaseToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.loadDatabaseToolStripMenuItem.Name = "loadDatabaseToolStripMenuItem";
            this.loadDatabaseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadDatabaseToolStripMenuItem.Text = "&Load Database...";
            this.loadDatabaseToolStripMenuItem.Click += new System.EventHandler(this.importDatabaseToolStripMenuItem_Click);
            // 
            // reconnectDatabaseToolStripMenuItem
            // 
            this.reconnectDatabaseToolStripMenuItem.Enabled = false;
            this.reconnectDatabaseToolStripMenuItem.Name = "reconnectDatabaseToolStripMenuItem";
            this.reconnectDatabaseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.reconnectDatabaseToolStripMenuItem.Text = "Reconnect Database";
            this.reconnectDatabaseToolStripMenuItem.Click += new System.EventHandler(this.reconnectDatabaseToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(189, 6);
            // 
            // createNewDatabaseToolStripMenuItem
            // 
            this.createNewDatabaseToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.createNewDatabaseToolStripMenuItem.Name = "createNewDatabaseToolStripMenuItem";
            this.createNewDatabaseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.createNewDatabaseToolStripMenuItem.Text = "Create &New Database...";
            this.createNewDatabaseToolStripMenuItem.Click += new System.EventHandler(this.createNewDatabaseToolStripMenuItem_Click);
            // 
            // backupDatabaseToolStripMenuItem
            // 
            this.backupDatabaseToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.backupDatabaseToolStripMenuItem.Name = "backupDatabaseToolStripMenuItem";
            this.backupDatabaseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.backupDatabaseToolStripMenuItem.Text = "&Backup Database...";
            this.backupDatabaseToolStripMenuItem.Click += new System.EventHandler(this.backupDatabaseToolStripMenuItem_Click);
            // 
            // manageDatabasesToolStripMenuItem
            // 
            this.manageDatabasesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.manageDatabasesToolStripMenuItem.Name = "manageDatabasesToolStripMenuItem";
            this.manageDatabasesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.manageDatabasesToolStripMenuItem.Text = "&Manage Databases...";
            this.manageDatabasesToolStripMenuItem.Click += new System.EventHandler(this.manageDatabasesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(189, 6);
            // 
            // exportDatabaseToolStripMenuItem
            // 
            this.exportDatabaseToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.exportDatabaseToolStripMenuItem.Name = "exportDatabaseToolStripMenuItem";
            this.exportDatabaseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.exportDatabaseToolStripMenuItem.Text = "E&xport Database...";
            this.exportDatabaseToolStripMenuItem.Click += new System.EventHandler(this.exportDatabaseToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fullScreenToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // fullScreenToolStripMenuItem
            // 
            this.fullScreenToolStripMenuItem.Name = "fullScreenToolStripMenuItem";
            this.fullScreenToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.fullScreenToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.fullScreenToolStripMenuItem.Text = "Fullscreen";
            this.fullScreenToolStripMenuItem.Click += new System.EventHandler(this.fullScreenToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userManualToolStripMenuItem,
            this.toolStripMenuItem5,
            this.viewErrorLogsToolStripMenuItem,
            this.toolStripMenuItem7,
            this.mnuViewRNotes,
            this.aboutPayrollSystemToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 24);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // userManualToolStripMenuItem
            // 
            this.userManualToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.userManualToolStripMenuItem.Name = "userManualToolStripMenuItem";
            this.userManualToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.userManualToolStripMenuItem.Text = "User &Manual";
            this.userManualToolStripMenuItem.Visible = false;
            this.userManualToolStripMenuItem.Click += new System.EventHandler(this.userManualToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(185, 6);
            this.toolStripMenuItem5.Visible = false;
            // 
            // viewErrorLogsToolStripMenuItem
            // 
            this.viewErrorLogsToolStripMenuItem.Name = "viewErrorLogsToolStripMenuItem";
            this.viewErrorLogsToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.viewErrorLogsToolStripMenuItem.Text = "System Logs";
            this.viewErrorLogsToolStripMenuItem.Click += new System.EventHandler(this.viewErrorLogsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(185, 6);
            // 
            // mnuViewRNotes
            // 
            this.mnuViewRNotes.Name = "mnuViewRNotes";
            this.mnuViewRNotes.Size = new System.Drawing.Size(188, 22);
            this.mnuViewRNotes.Text = "Whats New?";
            this.mnuViewRNotes.Click += new System.EventHandler(this.mnuViewRNotes_Click);
            // 
            // aboutPayrollSystemToolStripMenuItem
            // 
            this.aboutPayrollSystemToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.aboutPayrollSystemToolStripMenuItem.Image = global::PayrollSystem.Properties.Resources.information_frame;
            this.aboutPayrollSystemToolStripMenuItem.Name = "aboutPayrollSystemToolStripMenuItem";
            this.aboutPayrollSystemToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.aboutPayrollSystemToolStripMenuItem.Text = "&About Payroll System...";
            this.aboutPayrollSystemToolStripMenuItem.Click += new System.EventHandler(this.aboutPayrollSystemToolStripMenuItem_Click);
            // 
            // ActiveUserMenuItem
            // 
            this.ActiveUserMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ActiveUserMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ActiveUserTypeMenuItem,
            this.manageUserAccountsToolStripMenuItem,
            this.toolStripMenuItem6,
            this.logoutToolStripMenuItem,
            this.exitSystemToolStripMenuItem,
            this.writeSomeLogToolStripMenuItem});
            this.ActiveUserMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ActiveUserMenuItem.Name = "ActiveUserMenuItem";
            this.ActiveUserMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ActiveUserMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.ActiveUserMenuItem.Size = new System.Drawing.Size(74, 24);
            this.ActiveUserMenuItem.Text = "Active User";
            // 
            // ActiveUserTypeMenuItem
            // 
            this.ActiveUserTypeMenuItem.Enabled = false;
            this.ActiveUserTypeMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActiveUserTypeMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.ActiveUserTypeMenuItem.Name = "ActiveUserTypeMenuItem";
            this.ActiveUserTypeMenuItem.Size = new System.Drawing.Size(184, 22);
            this.ActiveUserTypeMenuItem.Text = "Administrator";
            // 
            // manageUserAccountsToolStripMenuItem
            // 
            this.manageUserAccountsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.manageUserAccountsToolStripMenuItem.Name = "manageUserAccountsToolStripMenuItem";
            this.manageUserAccountsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.manageUserAccountsToolStripMenuItem.Text = "Manage &User Accounts";
            this.manageUserAccountsToolStripMenuItem.Click += new System.EventHandler(this.manageUserAccountsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(181, 6);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.logoutToolStripMenuItem.Text = "&Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitSystemToolStripMenuItem
            // 
            this.exitSystemToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.exitSystemToolStripMenuItem.Name = "exitSystemToolStripMenuItem";
            this.exitSystemToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exitSystemToolStripMenuItem.Text = "E&xit System";
            this.exitSystemToolStripMenuItem.Click += new System.EventHandler(this.exitSystemToolStripMenuItem_Click);
            // 
            // writeSomeLogToolStripMenuItem
            // 
            this.writeSomeLogToolStripMenuItem.Name = "writeSomeLogToolStripMenuItem";
            this.writeSomeLogToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.writeSomeLogToolStripMenuItem.Text = "Write some log";
            this.writeSomeLogToolStripMenuItem.Visible = false;
            this.writeSomeLogToolStripMenuItem.Click += new System.EventHandler(this.writeSomeLogToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backgroundLoadingDatagridToolStripMenuItem});
            this.testToolStripMenuItem.Enabled = false;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(40, 24);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Visible = false;
            // 
            // backgroundLoadingDatagridToolStripMenuItem
            // 
            this.backgroundLoadingDatagridToolStripMenuItem.Name = "backgroundLoadingDatagridToolStripMenuItem";
            this.backgroundLoadingDatagridToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.backgroundLoadingDatagridToolStripMenuItem.Text = "Background Loading Datagrid";
            this.backgroundLoadingDatagridToolStripMenuItem.Click += new System.EventHandler(this.backgroundLoadingDatagridToolStripMenuItem_Click);
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblStatusSelectedCount,
            this.lblStatusTotalCount,
            this.DatabaseStatus,
            this.DatabaseStatusDBName});
            this.MainStatusStrip.Location = new System.Drawing.Point(0, 460);
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.Size = new System.Drawing.Size(840, 25);
            this.MainStatusStrip.SizingGrip = false;
            this.MainStatusStrip.TabIndex = 1;
            this.MainStatusStrip.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoToolTip = true;
            this.lblStatus.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.lblStatus.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.lblStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Padding = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblStatus.Size = new System.Drawing.Size(505, 20);
            this.lblStatus.Spring = true;
            this.lblStatus.Text = "Ready";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStatusSelectedCount
            // 
            this.lblStatusSelectedCount.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusSelectedCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblStatusSelectedCount.Name = "lblStatusSelectedCount";
            this.lblStatusSelectedCount.Size = new System.Drawing.Size(89, 20);
            this.lblStatusSelectedCount.Text = "Selected 20 of";
            // 
            // lblStatusTotalCount
            // 
            this.lblStatusTotalCount.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusTotalCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblStatusTotalCount.Name = "lblStatusTotalCount";
            this.lblStatusTotalCount.Size = new System.Drawing.Size(68, 20);
            this.lblStatusTotalCount.Text = "50 records";
            // 
            // DatabaseStatus
            // 
            this.DatabaseStatus.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.DatabaseStatus.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.DatabaseStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatabaseStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.DatabaseStatus.Image = global::PayrollSystem.Properties.Resources.database_error;
            this.DatabaseStatus.Name = "DatabaseStatus";
            this.DatabaseStatus.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.DatabaseStatus.Size = new System.Drawing.Size(69, 20);
            this.DatabaseStatus.Text = "Failed";
            this.DatabaseStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.DatabaseStatus.ToolTipText = "Database is running smoothly.";
            // 
            // DatabaseStatusDBName
            // 
            this.DatabaseStatusDBName.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.DatabaseStatusDBName.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.DatabaseStatusDBName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatabaseStatusDBName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.DatabaseStatusDBName.Name = "DatabaseStatusDBName";
            this.DatabaseStatusDBName.Size = new System.Drawing.Size(94, 20);
            this.DatabaseStatusDBName.Text = "(No Database)";
            // 
            // PagePanel
            // 
            this.PagePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.PagePanel.BackgroundImage = global::PayrollSystem.Properties.Resources.Application_Icon_Form_BG;
            this.PagePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PagePanel.BorderColor = System.Drawing.Color.Black;
            this.PagePanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.PagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PagePanel.Location = new System.Drawing.Point(0, 24);
            this.PagePanel.Name = "PagePanel";
            this.PagePanel.Size = new System.Drawing.Size(840, 436);
            this.PagePanel.TabIndex = 2;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(840, 485);
            this.Controls.Add(this.PagePanel);
            this.Controls.Add(this.MainStatusStrip);
            this.Controls.Add(this.XMainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(450, 250);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "5SZian Payroll System v.2.00";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.XMainMenuStrip.ResumeLayout(false);
            this.XMainMenuStrip.PerformLayout();
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip XMainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem payrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehiclesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ratesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ActiveUserMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managePayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem truckingLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem manageVehiclesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehicleTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageRatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageDatabasesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem createNewDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem loadDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payrollToolStripMenuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem truckingLogToolStripMenuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem statusReportToolStripMenuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem cashReportToolStripMenuItemArchive;
        private System.Windows.Forms.ToolStripMenuItem userManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem aboutPayrollSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageUserAccountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitSystemToolStripMenuItem;
        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private BufferedPanel PagePanel;
        private System.Windows.Forms.ToolStripMenuItem ActiveUserTypeMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusSelectedCount;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusTotalCount;
        private System.Windows.Forms.ToolStripStatusLabel DatabaseStatus;
        private System.Windows.Forms.ToolStripStatusLabel DatabaseStatusDBName;
        private System.Windows.Forms.ToolStripMenuItem reconnectDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuViewRNotes;
        private System.Windows.Forms.ToolStripMenuItem writeSomeLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewErrorLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundLoadingDatagridToolStripMenuItem;
    }
}

