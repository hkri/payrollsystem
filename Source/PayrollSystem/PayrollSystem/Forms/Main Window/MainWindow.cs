﻿/*
 * Main window form codes.
 * Container for all pages.
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using PayrollSystem.QueryStrings;

namespace PayrollSystem {

    public partial class MainWindow : Form, Interfaces.IWindowStatusUpdater, Interfaces.IDatabaseStatusUpdater {

        #region Local Variables

        Hashtable helpTexts = new Hashtable();  //Collection of help text (Menu strip items)

        //Collection of pages
        Dictionary<ToolStripMenuItem, SystemPage> pageCollection = new Dictionary<ToolStripMenuItem, SystemPage>();

        //Default window title
        string WindowTitle = "5SZian Payroll System v." + Application.ProductVersion;

        //Cache database status image.
        Bitmap img_db_error = Properties.Resources.database_error;
        Bitmap img_db_ok = Properties.Resources.database_ok;

        //Current page
        Form curr_page = null;

        //Window pre-fullscreen settings
        FormWindowState pf_state = FormWindowState.Normal;

        #endregion

        #region Constructor

        public MainWindow() {
            InitializeComponent();
            InitializeControlHelpTexts();

            //Initialize toolstrip renderers
            XMainMenuStrip.Renderer = MainStatusStrip.Renderer = new LightToolstripRenderer();
        
        }

        #endregion

        #region Window location checker

        /* This prevents window to be offscreen because of unupdated window location in settings file. */
        void CheckWindowOffscreen(Form window) {
            Screen[] screens = Screen.AllScreens;
            bool visible = false;
            foreach (Screen s in screens) {
                Rectangle wnd = new Rectangle(window.Left, window.Top, window.Width, window.Height);
                Rectangle screen_bounds = s.Bounds;
                if (wnd.IntersectsWith(screen_bounds)) {
                    visible = true;
                    break;
                }
            }
            if (visible == false) {
                Rectangle rt = Screen.PrimaryScreen.WorkingArea;
                window.Location = new Point(rt.Left, rt.Top);
                window.Size = new Size(800, 450);
            }
        }
        
        #endregion

        #region Add help text to controls

        void InitializeControlHelpTexts() {
            SetHelpText(managePayrollToolStripMenuItem, "View or create new payrolls.");
            SetHelpText(cashReportToolStripMenuItem, "View or create new cash reports.");
            SetHelpText(truckingLogToolStripMenuItem, "Manage trucking logs.");
            SetHelpText(employeesToolStripMenuItem, "Manage employee records.");
            SetHelpText(statusReportToolStripMenuItem, "Manage status reports of vehicles.");
            SetHelpText(manageVehiclesToolStripMenuItem, "View, udpate, or add company vehicle records.");
            SetHelpText(vehicleTypesToolStripMenuItem, "Manage what vehicle types are available in the system.");
            SetHelpText(manageRatesToolStripMenuItem, "Save or update rates for each destination/store.");
            SetHelpText(rateGroupsToolStripMenuItem, "Set rate groups to determine driver and helper rates.");
            SetHelpText(manageDatabasesToolStripMenuItem, "Manage company databases used by this payroll system.");
            SetHelpText(backupDatabaseToolStripMenuItem, "Create backup of current database.");
            SetHelpText(createNewDatabaseToolStripMenuItem, "Make a new system database.");
            SetHelpText(loadDatabaseToolStripMenuItem, "Import an existing database and set it as the active one.");
            SetHelpText(exportDatabaseToolStripMenuItem, "Export the current database to a different location.");
            SetHelpText(payrollToolStripMenuItemArchive, "View or restore archived payroll records.");
            SetHelpText(truckingLogToolStripMenuItemArchive, "View or restore archived trucking logs.");
            SetHelpText(employeesToolStripMenuItemArchive, "View or restore archived employee records.");
            SetHelpText(statusReportToolStripMenuItemArchive, "View or restore archived status reports.");
            SetHelpText(cashReportToolStripMenuItemArchive, "View or restore archived cash reports.");
            SetHelpText(userManualToolStripMenuItem, "View the system's user manual for help and other references.");
            SetHelpText(aboutPayrollSystemToolStripMenuItem, "Information about this system.");
            SetHelpText(manageUserAccountsToolStripMenuItem, "Create, edit, or delete system user accounts.");
            SetHelpText(logoutToolStripMenuItem, "Logout from the system and show the login window.");
            SetHelpText(exitSystemToolStripMenuItem, "Quit the payroll system.");
            SetHelpText(ActiveUserMenuItem, "The currently signed in user. Click to manage accounts or to logout.");
        }

        #endregion

        #region Menu help text hashtable

        void SetHelpText(ToolStripMenuItem target, String helpText) {
            helpTexts.Add(target, helpText);
            AddHelpTextEventHandler(target);
        }

        #endregion

        #region Add help text functions

        void AddHelpTextEventHandler(ToolStripMenuItem ctrl) {

            //Control Enter
            ctrl.MouseEnter -= Control_HelpTextEnter;
            ctrl.MouseEnter += Control_HelpTextEnter;

            //Control Leave.
            ctrl.MouseLeave -= Control_HelpTextLeave;
            ctrl.MouseLeave += Control_HelpTextLeave;
        }

        void Control_HelpTextEnter(object sender, EventArgs e) {
            UpdateStatus((string)helpTexts[(ToolStripMenuItem)sender]);
        }

        void Control_HelpTextLeave(object sender, EventArgs e) {
            ResetStatus();
        }

        #endregion

        #region Interface implementation

        public void UpdateWindowStatus(string status) {
            UpdateStatus(status);
        }

        public void ResetWindowStatus() {
            ResetStatus();
        }

        public void UpdateSelectedRecordCountStatus(int selectedRecordCount) {
            UpdateSelectedRecordCount(selectedRecordCount);
        }

        public void UpdateTotalRecordCountStatus(int totalRecordCount) {
            UpdateTotalRecordCount(totalRecordCount);
        }

        public void UpdateDBConnectivityStatus(bool isConnected) {
            if (isConnected) {
                DatabaseStatus.Image = img_db_ok;
                DatabaseStatus.Text = "Connected";
                reconnectDatabaseToolStripMenuItem.Enabled = false;
                ShowMenuItems();
            } else {
                DatabaseStatus.Image = img_db_error;
                DatabaseStatus.Text = "Disconnected";
                reconnectDatabaseToolStripMenuItem.Enabled = true;
                ClearPageViewer();
                HideMenuItemsNoDatabase();
            }
        }

        public void UpdateActiveDatabaseName(string db_name) {
            DatabaseStatusDBName.Text = System.IO.Path.GetFileName(db_name);
        }

        #endregion

        #region Status Update Functions

        public void ResetStatus() {
            lblStatus.Text = "Ready";
        }

        public void UpdateStatus(string status) {
            lblStatus.Text = status;
        }

        public void UpdateSelectedRecordCount(int selectedRecordCount = 0) {
            if (selectedRecordCount > 0)
                lblStatusSelectedCount.Text = "Selected " + selectedRecordCount + " of";
            else
                lblStatusSelectedCount.Text = "";
        }

        public void UpdateTotalRecordCount(int totalRecordCount = 0) {
            if (totalRecordCount > 0) {
                string word = (totalRecordCount == 1) ? " record" : " records";
                lblStatusTotalCount.Text = totalRecordCount + word;
            } else
                lblStatusTotalCount.Text = "";
        }

        #endregion

        #region Settings

        void SaveMainWindowSettings() {
            if (WindowState != FormWindowState.Maximized && WindowState != FormWindowState.Minimized) {
                Properties.Settings.Default.WindowSize = Size;
                Properties.Settings.Default.WindowLocation = Location;
            }
            if(WindowState != FormWindowState.Minimized) Properties.Settings.Default.WindowState = WindowState;
            Properties.Settings.Default.Save();
        }

        void LoadMainWindowSettings() {
            Location = Properties.Settings.Default.WindowLocation;
            WindowState = Properties.Settings.Default.WindowState;
            Size = Properties.Settings.Default.WindowSize;
        }

        #endregion

        #region PageViewer functions

        public void LoadPage(SystemPage pg) {
            if (curr_page != null) {
                if (curr_page.GetType() == pg.GetType()) {
                    return;
                }
            }
            ClearPageViewer(true);
            pg.TopLevel = false;
            pg.Dock = DockStyle.Fill;
            PagePanel.Controls.Add(pg);
            Text = pg.Text + " - " + WindowTitle;
            curr_page = pg;
            pg.Width = PagePanel.Width;
            pg.Height = PagePanel.Height;
            pg.Show();
            PagePanel.Refresh();
        }

        public void LoadPageNext(Form pg) {
            try {
                if (curr_page != null)
                    if (curr_page.GetType() == pg.GetType()) return;

                pg.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                pg.TopLevel = false;
                pg.ShowInTaskbar = false;
                pg.ShowIcon = false;
                pg.Dock = DockStyle.Fill;

                //ClearPageViewer(true);
                PagePanel.Controls.Clear();
                PagePanel.Controls.Add(pg);
                pg.Width = PagePanel.Width;
                pg.Height = PagePanel.Height;
                Text = pg.Text + " - " + WindowTitle;
                curr_page = pg;
                UpdateSelectedRecordCount();
                UpdateTotalRecordCount();
                pg.Show();
            } catch (Exception ex) {
                Trace.WriteLine("LoadPage Error: " + ex.Message);
            }
        }

        public void LoadPage(Form pg) {
            try {
                if (curr_page != null) {
                    if (curr_page.GetType() == pg.GetType()) {
                        return;
                    }
                }
                pg.TopLevel = false;
                pg.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                pg.ShowInTaskbar = false;
                pg.ShowIcon = false;
                pg.Dock = DockStyle.Fill;
                ClearPageViewer(true);
                PagePanel.Controls.Add(pg);
                pg.Width = PagePanel.Width;
                pg.Height = PagePanel.Height;
                pg.Show();
                Text = pg.Text + " - " + WindowTitle;
                curr_page = pg;
            } catch (Exception ex) {
                Trace.WriteLine("LoadPage Error: " + ex.Message);
            }
        }

        void ClearPageViewer(bool loading_page = false) {
            //Remove each page.
            foreach(Control c in PagePanel.Controls){
                if (c is Form) {
                    Form f = (Form)c;
                    f.Close();
                }
            }
            //Reset record counter status.
            if (curr_page != null && curr_page.IsDisposed == false)
                curr_page.Dispose();
            curr_page = null;
            UpdateSelectedRecordCount();
            UpdateTotalRecordCount();
            if(loading_page == false)Text = WindowTitle;
        }

        #endregion

        #region Active User Setup Functions

        void GetActiveUser() {
            ActiveUserMenuItem.Text = UserManager.ACCOUNT_NAME + " [ &- ]";
            ActiveUserTypeMenuItem.Text = (UserManager.ACCOUNT_TYPE == 0) ? "Administrator" : "Accountant";
        }
        
        #endregion

        #region Local Functions

        void HideMenuItemsNoDatabase() {
            payrollToolStripMenuItem.Visible = recordsToolStripMenuItem.Visible =
            vehiclesToolStripMenuItem.Visible = ratesToolStripMenuItem.Visible =
            archivesToolStripMenuItem.Visible = false;

            exportDatabaseToolStripMenuItem.Enabled = backupDatabaseToolStripMenuItem.Enabled = false;
        }

        void ShowMenuItems() {
            payrollToolStripMenuItem.Visible = recordsToolStripMenuItem.Visible =
            vehiclesToolStripMenuItem.Visible = ratesToolStripMenuItem.Visible =
            archivesToolStripMenuItem.Visible = true;

            exportDatabaseToolStripMenuItem.Enabled = backupDatabaseToolStripMenuItem.Enabled = true;
        }

        void DisconnectDatabase() {
            DatabaseManager.ShutdownExternalDatabase();
            UpdateDBConnectivityStatus(DatabaseManager.CheckExternalDBConnection());
            ClearPageViewer();
        }

        void ConnectDatabase() {
            //Connect to the external database.
            bool connected = DatabaseManager.CreateExternalDBConnection();     //Try to establish connection with external database.
            UpdateDBConnectivityStatus(connected);
            UpdateActiveDatabaseName(DatabaseManager.EXDB_FILENAME);

            //Once connected to database, check if the external database's internal version matches
            //what this system requires.
            try {
                //Access the _DB_DATA table.
                string cmd = "SELECT VER FROM _DB_DATA WHERE VER='" + DatabaseManager.EXDB_INT_VER + "'";
                DataTable _ver = DatabaseManager.GetMainDatabase().ExecuteQuery(cmd, false);
                if (_ver.Rows.Count <= 0) {
                    Notifier.ShowError("Oops! The loaded database seems to be incompatible with this version of 5SZian Payroll System.\n\nThe app will now disconnect from this database. Please contact the system administrator or developer for support.");
                    DisconnectDatabase();
                }
            } catch {
                Trace.WriteLine("ConnectDatabase: _DB_DATA not found or accessible.");
            }
            
            //Hide operation commands when no database loaded.
            if (connected == false) {
                HideMenuItemsNoDatabase();
            }

        }

        void LoadDatabase(string filepath) {
            if (File.Exists(filepath)) {

                if (DatabaseManager.CheckExternalDBConnection()) {
                    //Disconnect currently connected.
                    if (Notifier.ShowConfirm("There is another database currently loaded in the payroll system. Continue loading?", "Load Database") == System.Windows.Forms.DialogResult.No) {
                        return;
                    } else {
                        DatabaseManager.ShutdownExternalDatabase();
                    }
                }

                string filename = Path.GetFileName(filepath);

                Properties.Settings.Default.ActiveDatabase = filename;
                Properties.Settings.Default.Save();
                bool connected = DatabaseManager.CreateExternalDBConnection();
                UpdateDBConnectivityStatus(connected);
                UpdateActiveDatabaseName(DatabaseManager.EXDB_FILENAME);
                if (connected) {

                } else {
                    Notifier.ShowError("There was a problem connecting with the specified database. Please contact a system administrator.");
                }

            }
        }

        #endregion

        #region Window Eventhandlers

        //Hides menu bar and toolstrip when hits certain bound of width and height
        private void MainWindow_Resize(object sender, EventArgs e) {
            //450, 250
            if (Width <= 460 || Height <= 260) {
                if (XMainMenuStrip.Visible && MainStatusStrip.Visible)
                    XMainMenuStrip.Visible = MainStatusStrip.Visible = false;
            } else {
                if (XMainMenuStrip.Visible == false && MainStatusStrip.Visible == false)
                    XMainMenuStrip.Visible = MainStatusStrip.Visible = true;
            }
        }

        private void MainWindow_Load(object sender, EventArgs e) {
            LoadMainWindowSettings();   //Load window settings (size and location)
            GetActiveUser();            //Loads active user info from global module.
            ClearPageViewer();          //Clears page viewer of any page/controls.
            CheckWindowOffscreen(this); //Moves window back to a screen when out of bounds.
            ConnectDatabase();          //Connects system to active database.
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e) {
            if (GlobalVariables.FORCE_SHUTDOWN == false) {
                if (MessageBox.Show("Are you sure you want to quit the payroll system?", "Confirm Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) {
                    e.Cancel = true;
                } else {
                    SaveMainWindowSettings();   //Save window settings (size and location)
                }
            }
        }

        #endregion

        #region Payroll Menu Item Event Handlers

        private void managePayrollToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new PayrollPage());
        }

        #endregion

        #region Records Menu Item Event Handlers

        private void cashReportToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new CashReportPage());
        }

        private void truckingLogToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new TruckingLogPage());
        }

        private void employeesToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new EmployeesPage());
        }

        #endregion

        #region Vehicles Menu Item Event Handlers

        private void statusReportToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new StatusReportPage());
        }

        private void manageVehiclesToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new ManageVehiclesPage());
        }

        private void vehicleTypesToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new VehicleTypesPage());
        }

        #endregion

        #region Rates Menu Item Event Handlers

        private void manageRatesToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new RatesPage());
        }

        private void rateGroupsToolStripMenuItem_Click(object sender, EventArgs e) {
            LoadPage(new RateGroupPage());
        }

        #endregion

        #region Database Menu Item Event Handlers

        private void manageDatabasesToolStripMenuItem_Click(object sender, EventArgs e) {
            Process.Start("explorer.exe", GlobalVariables.DB_DIR);
        }

        private void createNewDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            CreateDatabaseDialog dlg = new CreateDatabaseDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //Retrieve filename from dialog.
                string fn;
                dlg.GetFieldValues(out fn);

                //Start filestream and copy
                try {
                    File.WriteAllBytes(fn, Properties.Resources.database);

                    Notifier.ShowInformation("Database successfully created.", "New Database");

                    if (Notifier.ShowConfirm("Connect system to the newly created database?", "Connect Database") == System.Windows.Forms.DialogResult.Yes) {
                        ClearPageViewer();
                        Properties.Settings.Default.ActiveDatabase = Path.GetFileName(fn);
                        Properties.Settings.Default.Save();
                        ConnectDatabase();
                    } else {
                        Process.Start("explorer.exe", Path.GetDirectoryName(fn));
                    }
                } catch (Exception ex) {
                    Trace.WriteLine(ex.Message);
                }

            }
        }

        private void backupDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            if (Notifier.ShowConfirm("This action will disconnect the system from the current database and will reconnect once operations are done. Are you sure you want to continue?", "Closing Database...") == System.Windows.Forms.DialogResult.Yes) {
                ClearPageViewer();
                DisconnectDatabase();
                BackupDatabaseDialog dlg = new BackupDatabaseDialog();
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    //Disconnect from database, close all open pages, then proceed to backup.
                    if (DatabaseManager.BackupDatabase()) {
                        Notifier.ShowInformation("Database backup operation was successful.", "Database Backup");
                    }
                }
                ConnectDatabase();
            }
        }

        private void importDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            DatabaseLoaderDialog dlg = new DatabaseLoaderDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                ClearPageViewer();
                Properties.Settings.Default.ActiveDatabase = dlg.GetFilename();
                Properties.Settings.Default.Save();
                ConnectDatabase();
            }
        }

        private void exportDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            if (Notifier.ShowConfirm("This action will disconnect the system from the current database and will reconnect once operations are done. Are you sure you want to continue?", "Closing Database...") == System.Windows.Forms.DialogResult.Yes) {
                ClearPageViewer();
                DisconnectDatabase();
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "Database File (*.accdb) |*.accdb";
                dlg.Title = "Export Database";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    //Disconnect from database, close all open pages, then proceed to backup.
                    if (DatabaseManager.ExportDatabase(dlg.FileName)) {
                        Notifier.ShowInformation("Database was successfully exported to:\n\n" + dlg.FileName, "Database Export");
                    }
                }
                ConnectDatabase();
            }
        }

        private void reconnectDatabaseToolStripMenuItem_Click(object sender, EventArgs e) {
            ConnectDatabase();
        }

        #endregion

        #region Archives Menu Item Event Handlers

        private void payrollToolStripMenuItemArchive_Click(object sender, EventArgs e) {
            LoadPage(new ArchivePayrollPage());
        }

        private void truckingLogToolStripMenuItemArchive_Click(object sender, EventArgs e) {
            LoadPage(new ArchiveTruckingLogPage());
        }

        private void employeesToolStripMenuItemArchive_Click(object sender, EventArgs e) {
            LoadPage(new ArchiveEmployeesPage());
        }

        private void statusReportToolStripMenuItemArchive_Click(object sender, EventArgs e) {
            LoadPage(new ArchiveStatusReportPage());
        }

        private void cashReportToolStripMenuItemArchive_Click(object sender, EventArgs e) {
            LoadPage(new ArchivedCashReportPage());
        }

        #endregion

        #region Help Menu Item Event Handlers

        private void userManualToolStripMenuItem_Click(object sender, EventArgs e) {

        }

        private void aboutPayrollSystemToolStripMenuItem_Click(object sender, EventArgs e) {
            AboutForm dlg = new AboutForm();
            dlg.ShowDialog();
        }

        private void viewErrorLogsToolStripMenuItem_Click(object sender, EventArgs e) {
            if (File.Exists("errors.log")) {
                Process proc = new Process();
                Process.Start("notepad.exe", "errors.log");
            } else {
                Notifier.ShowInformation("No errors were found so far. Move along.", "No errors found");
            }
        }

        #endregion
        
        #region Accounts Menu Eventhandlers

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Are you sure you want to log out from the system?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes) {
                LoginWindow login = new LoginWindow();
                ClearPageViewer();
                DisconnectDatabase();
                Hide();
                if (login.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    Show();
                    Focus();
                    ConnectDatabase();
                    GetActiveUser();
                } else {
                    GlobalVariables.FORCE_SHUTDOWN = true;
                    Application.Exit();
                }
            }
        }

        private void manageUserAccountsToolStripMenuItem_Click(object sender, EventArgs e) {
            UserAccountsPage users = new UserAccountsPage();
            LoadPage(users);
        }

        private void exitSystemToolStripMenuItem_Click(object sender, EventArgs e) {
            Close();
        }

        #endregion

        #region Window Menu Eventhandlers
        
        //For testing purposes: write log file.
        private void writeSomeLogToolStripMenuItem_Click(object sender, EventArgs e) {
            SystemLogger.WriteLog("Hello world.");
        }

        //Opens the "Release Notes.txt" file.
        private void mnuViewRNotes_Click(object sender, EventArgs e) {
            if (File.Exists("Release Notes.txt")) {
                Process.Start("notepad.exe", "Release Notes.txt");
            } else
                Notifier.ShowWarning("Release notes not available.", "View Release Notes");
        }

        /* Toggles fullscreen mode of the window */
        private void fullScreenToolStripMenuItem_Click(object sender, EventArgs e) {
            if (FormBorderStyle == System.Windows.Forms.FormBorderStyle.Sizable) {
                //Windowed -> Fullscreen
                SuspendLayout();
                pf_state = WindowState;
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                WindowState = FormWindowState.Normal;
                WindowState = FormWindowState.Maximized;
                ResumeLayout();
                fullScreenToolStripMenuItem.Checked = true;
            } else if (FormBorderStyle == System.Windows.Forms.FormBorderStyle.None) {
                //Fullscreen -> Windowed
                SuspendLayout();
                WindowState = pf_state;
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                ResumeLayout();
                fullScreenToolStripMenuItem.Checked = false;
            }
        }
        #endregion

        #region Test Functions (Development purposes)

        //Loads a form that test background-loading database operations.
        private void backgroundLoadingDatagridToolStripMenuItem_Click(object sender, EventArgs e) {
            bool running = false;
            foreach (Form frm in Application.OpenForms) {
                if (frm is BackgroundLoadingDataSample) {
                    running = true;
                    frm.Focus();
                }
            }
            if (!running) {
                BackgroundLoadingDataSample wnd = new BackgroundLoadingDataSample();
                wnd.Show();
            }
        }

        #endregion

    }

}
