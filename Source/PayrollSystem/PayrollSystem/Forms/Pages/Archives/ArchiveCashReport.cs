﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ArchivedCashReportPage : SystemPage {

        #region Constructor

        //Constructor, set toolbar buttons.
        public ArchivedCashReportPage() {
            InitializeComponent();
            SetToolbarButtons(PageToolbarButtons.Refresh | PageToolbarButtons.Delete | PageToolbarButtons.Restore | PageToolbarButtons.Search);
            SetRecordsDataGridMultiselect(false);
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ArchivedCashReportPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "ArchivedCashReportPage";
            this.Text = "Cash Report Archive";
            this.OnDelete += new System.EventHandler(this.ArchivedCashReportPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ArchivedCashReportPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ArchivedCashReportPage_OnRefresh);
            this.OnRestore += new System.EventHandler(this.ArchivedCashReportPage_OnRestore);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Local Variables

        //Images for locked records.
        DataTable dt;
        Image lockicon = Properties.Resources._lock;

        #endregion

        #region CRUD

        //Handle delete event.
        private void ArchivedCashReportPage_OnDelete(object sender, EventArgs e) {

            //Check first if cash report is being used on a payroll. It it is, prevent from permanenet delete.
            String qry = "SELECT ID FROM PayrollCashReportItem AS A INNER JOIN Payroll AS B ON A.PAYROLLID = B.PID ";
            qry += "WHERE A.CRITEMID = @id";

            //SQL Parameter.
            OleDbParameter[] prm = new OleDbParameter[1];
            prm[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

            DataTable exst = DatabaseManager.GetMainDatabase().ExecuteQuery(qry, false, prm);

            if (exst.Rows.Count > 0) {
                Notifier.ShowWarning("Cash report cannot be permanently deleted because it is being used by a payroll record.", "Cash Report Referenced");
                return;
            }

            if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected cash report?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                //SQL Command
                String query = "DELETE FROM CashReport ";
                query += "WHERE CRID = @id";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[1];
                p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                //Execute query.
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);

                //Notify user.
                if (ok) {
                    Notifier.ShowInformation("Permanently deleted cash report.", "Deleted");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to permanently delete cash report.");
            }
        }

        //Eventhandler for search event.
        private void ArchivedCashReportPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        //Restore selected cash report.
        private void ArchivedCashReportPage_OnRestore(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to restore the selected cash report?", "Restore Cash Report") == System.Windows.Forms.DialogResult.Yes) {
                    String query = "UPDATE CashReport SET IsArchived = false ";
                    query += "WHERE CRID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Successfully restored cash report.", "Restore Cash Report");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to restore cash report due to an error.");
                }
            }
        }

        //Retrieve cash report records.
        private void ArchivedCashReportPage_OnRefresh(object sender, EventArgs e) {
            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@year", GetStartDate().Year);
            p[0].DbType = DbType.Int16;
            p[1] = new OleDbParameter("@search", GetSearchText());
            p[2] = new OleDbParameter("@isarchived", "y");

            //Get data from database.
            if (dt != null) dt.Dispose();
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReport", true, p);

            //Insert image representations of locked/final records.
            DataColumn icol = dt.Columns.Add("IMG", typeof(Image));
            icol.SetOrdinal(0);
            foreach (DataRow r in dt.Rows) {
                if ((bool)r["IsFinal"]) r[0] = lockicon;
            }

            //Set DataSource
            SetRecordsDataSource(dt);

            //Customize the lock image column.
            DataGridViewColumn finalcol = GetColumn("IMG");
            finalcol.Width = 30;
            finalcol.Resizable = DataGridViewTriState.False;
            finalcol.DefaultCellStyle.NullValue = null;
            finalcol.ToolTipText = "Finalized";

            //Remove the column header text of the lock indicator.
            SetColumnHeader("IMG", "");

            //Hide unnecessary columns.
            HideColumn("ID");
            HideColumn("IsFinal");
            HideColumn("IsArchived");

            //Sort by start date.
            GetRecordsDataGridView().Sort(GetRecordsDataGridView().Columns["Date Start"], System.ComponentModel.ListSortDirection.Descending);

            RecordsDGVFitColumnToContent("Title");
            RecordsDGVFitColumnToContent("Author");
        }

        #endregion

    }

}
