﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ArchiveEmployeesPage : SystemPage {

        #region Constructor

        public ArchiveEmployeesPage() {
            InitializeComponent();
            SetToolbarButtons(PageToolbarButtons.Refresh | PageToolbarButtons.Delete | PageToolbarButtons.Restore | PageToolbarButtons.Search);
            SetRecordsDataGridMultiselect(false);
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ArchiveEmployeesPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "ArchiveEmployeesPage";
            this.Text = "Employees Archive";
            this.OnDelete += new System.EventHandler(this.ArchiveEmployeesPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ArchiveEmployeesPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ArchiveEmployeesPage_OnRefresh);
            this.OnRestore += new System.EventHandler(this.ArchiveEmployeesPage_OnRestore);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region CRUD

        private void ArchiveEmployeesPage_OnRestore(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to restore the selected employee record?", "Restore Record") == System.Windows.Forms.DialogResult.Yes) {
                    String query = "UPDATE Employee SET IsArchived = false ";
                    query += "WHERE EmployeeID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell("EmployeeID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Employee record was successfully restored.", "Restore Record");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Restore operation failed due to an error.");
                }
            }
        }

        private void ArchiveEmployeesPage_OnRefresh(object sender, EventArgs e) {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@isarchived", "y");

            DataTable dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            SetRecordsDataSource(dt);
            HideColumn("EmployeeID");
            HideColumn("First Name");
            HideColumn("Middle Name");
            HideColumn("Last Name");
            HideColumn("FullName2");
            SetColumnHeader("FullName1", "Employee");
            SetColumnHeader("FullName2", "Employee");

            RecordsDGVFitColumnToContent("FullName1");
            RecordsDGVFitColumnToContent("FullName2");
        }

        private void ArchiveEmployeesPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void ArchiveEmployeesPage_OnDelete(object sender, EventArgs e) {

            
            //Prevent delete if used on cash report.
            String qry = "SELECT ITEMID FROM CashReportItems AS A INNER JOIN Employee AS B ON A.Driver = B.EmployeeID OR A.Helper = B.EmployeeID ";
            qry += "WHERE B.EmployeeID = @id";

            //SQL Parameter.
            OleDbParameter[] prm = new OleDbParameter[1];
            prm[0] = new OleDbParameter("@id", "{" + Cell("EmployeeID") + "}");

            DataTable exst = DatabaseManager.GetMainDatabase().ExecuteQuery(qry, false, prm);

            if (exst.Rows.Count > 0) {
                Notifier.ShowWarning("Employee cannot be permanently deleted because it is being used in a cash report.", "Employee Referenced");
                return;
            }

            //Prevent delete if used in trucking log
            qry = "SELECT ID FROM TransportLog AS A INNER JOIN Employee AS B ON A.Driver = B.EmployeeID OR A.Helper = B.EmployeeID ";
            qry += "WHERE B.EmployeeID = @id";

            //SQL Parameter.
            OleDbParameter[] prm1 = new OleDbParameter[1];
            prm1[0] = new OleDbParameter("@id", "{" + Cell("EmployeeID") + "}");

            exst = DatabaseManager.GetMainDatabase().ExecuteQuery(qry, false, prm1);

            if (exst.Rows.Count > 0) {
                Notifier.ShowWarning("Employee cannot be permanently deleted because it is being used in a trucking log.", "Employee Referenced");
                return;
            }
            
            if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected employee record?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                //SQL Command
                String query = "DELETE FROM Employee ";
                query += "WHERE EmployeeID = @id";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[1];
                p[0] = new OleDbParameter("@id", "{" + Cell("EmployeeID") + "}");

                //Execute query.
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);

                //Notify user.
                if (ok) {
                    Notifier.ShowInformation("Permanently deleted employee record.", "Deleted");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to permanently delete employee record.");
            }

        }

        #endregion

    }

}
