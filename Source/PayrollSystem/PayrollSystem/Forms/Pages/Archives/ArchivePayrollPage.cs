﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ArchivePayrollPage : SystemPage {

        #region Constructor

        public ArchivePayrollPage() {
            InitializeComponent();
            SetToolbarButtons(PageToolbarButtons.Refresh | PageToolbarButtons.Delete | PageToolbarButtons.Restore | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.MonthYearDatePicker);
            SetRecordsDataGridMultiselect(false);
            SetStartDate(DateTime.Now.ToShortDateString());
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ArchivePayrollPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.FinalRecordsColumnName = "IsFinal";
            this.Name = "ArchivePayrollPage";
            this.Text = "Payroll Archive";
            this.OnDelete += new System.EventHandler(this.ArchivePayrollPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ArchivePayrollPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ArchivePayrollPage_OnRefresh);
            this.OnRestore += new System.EventHandler(this.ArchivePayrollPage_OnRestore);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Variables

        // Images for lock state.
        Image lockicon = Properties.Resources._lock;
        
        // Images for computed.
        Image star_shaded = Properties.Resources.star_shaded;
        Image star_unshaded = Properties.Resources.star_unshaded;

        #endregion

        #region CRUD
        
        private void ArchivePayrollPage_OnDelete(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected payroll record?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    //SQL Command
                    String query = "DELETE FROM Payroll ";
                    query += "WHERE PID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

                    //Execute query.
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);

                    //Notify user.
                    if (ok) {
                        Notifier.ShowInformation("Permanently deleted payroll record.", "Deleted");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to permanently delete payroll.");
                }
            }
        }
        
        private void ArchivePayrollPage_OnRestore(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to restore the selected payroll record?", "Restore Record") == System.Windows.Forms.DialogResult.Yes) {
                    String query = "UPDATE Payroll SET IsArchived = false ";
                    query += "WHERE PID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Payroll record was successfully restored.", "Restore Record");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to restore payroll due to an error.");
                }
            }
        }

        private void ArchivePayrollPage_OnRefresh(object sender, EventArgs e) {
            //Query all Cash Reports

            String query = "SELECT PID, Title, DateStart, DateEnd, Author, IsArchived, IsFinal, IsComputed FROM Payroll ";
            query += "WHERE IsArchived = true AND (MONTH(DateStart) = @startmonth AND YEAR(DateStart) = @startyear) AND (Title LIKE '%' & @search & '%') ";
            query += "ORDER BY DateStart, Title";

            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@startmonth", GetStartDate().Month);
            p[1] = new OleDbParameter("@startyear", GetStartDate().Year);
            p[2] = new OleDbParameter("@search", GetSearchText());

            DataTable dt_payrolls = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

            //Move ifFinal at beginning of columns.
            dt_payrolls.Columns["IsFinal"].SetOrdinal(0);
            dt_payrolls.Columns["IsComputed"].SetOrdinal(1);

            //Insert image representations of locked/final records.
            DataColumn icol = dt_payrolls.Columns.Add("IMG", typeof(Image));
            DataColumn icompute = dt_payrolls.Columns.Add("IMGCOMPUTED", typeof(Image));
            icompute.SetOrdinal(0);
            icol.SetOrdinal(0);
            foreach (DataRow r in dt_payrolls.Rows) {
                if ((bool)r["IsFinal"]) r[0] = lockicon;
            }
            foreach (DataRow r in dt_payrolls.Rows) {
                if ((bool)r["IsComputed"]) r[1] = star_shaded;
                else r[1] = star_unshaded;
            }

            //Set datasource of datagrSet
            SetRecordsDataSource(dt_payrolls);

            //Hide unnecessary columns.
            HideColumn("PID");
            HideColumn("IsArchived");
            HideColumn("IsFinal");
            HideColumn("IsComputed");

            //Set column user-friendly title.
            SetColumnHeader("DateStart", "Start Date");
            SetColumnHeader("DateEnd", "End Date");

            //Customize the lock image column.
            DataGridViewColumn finalcol = GetColumn("IMG");
            finalcol.Width = 30;
            finalcol.Resizable = DataGridViewTriState.False;
            finalcol.DefaultCellStyle.NullValue = null;
            finalcol.ToolTipText = "Finalized";

            //Customize the computed image column.
            DataGridViewColumn finalcol2 = GetColumn("IMGCOMPUTED");
            finalcol2.Width = 30;
            finalcol2.Resizable = DataGridViewTriState.False;
            finalcol2.DefaultCellStyle.NullValue = null;
            finalcol2.ToolTipText = "Computed";

            //Remove the column header text of the lock indicator.
            SetColumnHeader("IMG", "");
            SetColumnHeader("IMGCOMPUTED", "");

            RecordsDGVFitColumnToContent("Title");
            RecordsDGVFitColumnToContent("Author");

        }

        private void ArchivePayrollPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        #endregion

    }

}
