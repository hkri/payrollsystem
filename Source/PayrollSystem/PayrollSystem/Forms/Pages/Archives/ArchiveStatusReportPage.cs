﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ArchiveStatusReportPage : SystemPage {

        #region Constructor

        public ArchiveStatusReportPage() {
            InitializeComponent(); 
            SetToolbarButtons(PageToolbarButtons.Refresh | PageToolbarButtons.Delete | PageToolbarButtons.Restore | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.MonthYearDatePicker);
            SetRecordsDataGridMultiselect(false);
            SetStartDate(DateTime.Now.ToShortDateString());
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ArchiveStatusReportPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "ArchiveStatusReportPage";
            this.Text = "Status Report Archive";
            this.OnDelete += new System.EventHandler(this.ArchiveStatusReportPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ArchiveStatusReportPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ArchiveStatusReportPage_OnRefresh);
            this.OnRestore += new System.EventHandler(this.ArchiveStatusReportPage_OnRestore);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region CRUD

        //Handle event delete record.
        private void ArchiveStatusReportPage_OnDelete(object sender, EventArgs e) {
            if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected status report?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                //SQL Command
                String query = "DELETE FROM StatusReport ";
                query += "WHERE ID = @id";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[1];
                p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                //Execute query.
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);

                //Notify user.
                if (ok) {
                    Notifier.ShowInformation("Permanently deleted status report.", "Deleted");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to permanently delete status report.");
            }
        }

        //On search event.
        private void ArchiveStatusReportPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        //Handle On Refresh click.
        private void ArchiveStatusReportPage_OnRefresh(object sender, EventArgs e) {
            OleDbParameter[] p = new OleDbParameter[4];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@month", GetPickerFromMonth());
            p[1].DbType = DbType.Int32;
            p[2] = new OleDbParameter("@year", GetPickerFromYear());
            p[2].DbType = DbType.Int32;
            p[3] = new OleDbParameter("@isarchived", "y");

            DataTable dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_StatusReport", true, p);
            SetRecordsDataSource(dt);
            HideColumn("ID");
            HideColumn("IsArchived");

            RecordsDGVFitColumnToContent("Status");
            RecordsDGVFitColumnToContent("Author");
        }

        //Handle Restore action.
        private void ArchiveStatusReportPage_OnRestore(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to restore the selected status report?", "Restore Status Report") == System.Windows.Forms.DialogResult.Yes) {
                    String query = "UPDATE StatusReport SET IsArchived = false ";
                    query += "WHERE ID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Successfully restored status report.", "Restore Status Report");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to restore status report due to an error.");
                }
            }
        }

        #endregion

    }

}
