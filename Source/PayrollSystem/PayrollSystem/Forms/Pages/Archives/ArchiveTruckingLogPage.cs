﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ArchiveTruckingLogPage : SystemPage {

        #region Constructor

        public ArchiveTruckingLogPage() {
            InitializeComponent();
            SetToolbarButtons(PageToolbarButtons.Refresh | PageToolbarButtons.Delete | PageToolbarButtons.Restore | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.RangeDatePeriod);
            SetRecordsDataGridMultiselect(true);
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ArchiveTruckingLogPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "ArchiveTruckingLogPage";
            this.Text = "Trucking Log Archive";
            this.OnDelete += new System.EventHandler(this.ArchiveTruckingLogPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ArchiveTruckingLogPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ArchiveTruckingLogPage_OnRefresh);
            this.OnRestore += new System.EventHandler(this.ArchiveTruckingLogPage_OnRestore);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Local Variables

        //Image for locked records.
        Image lockicon = Properties.Resources._lock;

        #endregion

        #region CRUD

        private void ArchiveTruckingLogPage_OnDelete(object sender, EventArgs e) {

            //Check first if trucking log is being used on a payroll. It it is, prevent from permanenet delete.
            String qry = "SELECT ID FROM PayrollTransportLog AS A INNER JOIN Payroll AS B ON A.PAYROLLID = B.PID ";
            qry += "WHERE A.TransactionID = @id";

            //SQL Parameter.
            OleDbParameter[] prm = new OleDbParameter[1];
            prm[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

            DataTable exst = DatabaseManager.GetMainDatabase().ExecuteQuery(qry, false, prm);

            if (exst.Rows.Count > 0) {
                Notifier.ShowWarning("Trucking log cannot be permanently deleted because it is being used by a payroll record.", "Trucking Log Referenced");
                return;
            }


            if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected trucking log?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                bool ok = true;
                foreach (DataGridViewRow sRow in GetRecordsSelectedRows()) {
                    //SQL Command
                    String query = "DELETE FROM TransportLog ";
                    query += "WHERE ID = @id";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + sRow.Cells["ID"].Value.ToString() + "}");

                    //Execute query.
                    ok &= DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                }
                //Notify user.
                if (ok) {
                    Notifier.ShowInformation("Permanently deleted trucking log.", "Deleted");
                    RefreshRecords();
                } else
                    Notifier.ShowError("A problem has occurred when deleting specified trucking logs.");
            }
        }

        private void ArchiveTruckingLogPage_OnRestore(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to restore the selected trucking log?", "Restore Trucking Log") == System.Windows.Forms.DialogResult.Yes) {
                    String query = "UPDATE TransportLog SET IsArchived = false ";
                    query += "WHERE ID = ";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[GetRecordsSelectedRows().Count];

                    //If there are more than 1 selected, append to query.
                    for (int i = 0; i < GetRecordsSelectedRows().Count; i++) {
                        string sid = "@id" + i;
                        query += sid;
                        p[i] = new OleDbParameter(sid, "{" + GetRecordsSelectedRows()[i].Cells["ID"].Value.ToString() + "}");
                        if (i < GetRecordsSelectedRows().Count - 1) query += " OR ID = ";
                    }

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Trucking log was successfully restored.", "Restore Trucking Log");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to restore record due to an error.");
                }
            }
        }

        private void ArchiveTruckingLogPage_OnRefresh(object sender, EventArgs e) {
            SuspendLayout();

            OleDbParameter[] p = new OleDbParameter[4];
            p[0] = new OleDbParameter("@datestart", GetStartDateString());
            p[1] = new OleDbParameter("@dateend", GetEndDateString());
            p[2] = new OleDbParameter("@search", InputValidators.EscapeSquareBrackets(GetSearchText()));
            p[3] = new OleDbParameter("@isarchived", "y");

            DataTable dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_TransportLog", true, p);

            //Move IsFinal at the beginning of the table.
            dt.Columns["IsFinal"].SetOrdinal(0);

            //insert an image column to the datatable.
            DataColumn icol = dt.Columns.Add("IMG", typeof(Image));
            icol.SetOrdinal(0);
            foreach (DataRow r in dt.Rows) {
                if ((bool)r["IsFinal"]) r[0] = lockicon;
            }

            SetRecordsDataSource(dt);
            HideColumn("IsArchived");
            HideColumn("IsFinal");
            HideColumn("ID");
            HideColumn("IsBackLoad");
            HideColumn("Store");

            DataGridViewColumn finalcol = GetColumn("IMG");
            finalcol.Width = 30;
            finalcol.Resizable = DataGridViewTriState.False;
            finalcol.DefaultCellStyle.NullValue = null;
            finalcol.ToolTipText = "Finalized";
            SetColumnHeader("IMG", "");

            //Set column formatting.
            SetColumnFormat("Rate", "#,##0.00");
            SetColumnFormat("Driver Rate", "#,##0.00");
            SetColumnFormat("Helper Rate", "#,##0.00");

            //Order fields.
            GetRecordsDataGridView().Sort(GetRecordsDataGridView().Columns["Date"], System.ComponentModel.ListSortDirection.Descending);

            //Make each columns in the totals datagrid fit contents.
            AutoFitTotalsDataGridItems();

            RecordsDGVFitColumnToContent("Destination");
            RecordsDGVFitColumnToContent("Driver");
            RecordsDGVFitColumnToContent("Helper");

            ResumeLayout();
        }

        private void ArchiveTruckingLogPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        #endregion
        
    }

}
