﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using Microsoft.Reporting.WinForms;
using Microsoft.ReportingServices;

using ClosedXML;
using ClosedXML.Excel;

namespace PayrollSystem {

    class PayrollEditorPage : Form {

        #region Controls

        private ToolStripButton tbCashReports;
        private ToolStripButton tbTruckingLogs;
        private ToolStripButton tbPayroll;
        private ToolStripButton tbSummary;
        public ToolStrip MainToolStrip;
        private ToolStripButton btnAdd;
        private ToolStripButton btnDelete;
        private ToolStripButton toolStripButton1;
        private ToolStripButton btnSearch;
        protected internal ToolStripTextBox txtSearch;
        private ToolStripLabel lblSearch;
        private BufferedPanel PayrollInfoPanel;
        private BufferedPanel PayrollInfoPanelBorder;
        private ToolStripButton btnBack;
        private Label lblPayrollTitle;
        private Label lblDatePeriod;
        protected internal DataGridView RecordsDataGrid;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private ToolStripButton btnRefresh;
        private BufferedPanel FiltersPanel;
        private BufferedPanel bufferedPanel3;
        private Label label6;
        internal ComboBox cboType;
        private BufferedPanel WarningPanel;
        private Label label2;
        private Label label1;
        private ToolStripButton btnViewCR;
        private ToolTip TooltipStatus;
        private System.ComponentModel.IContainer components;
        private ToolStripButton btnRecompute;
        private ToolStripButton btnCreatePayslip;
        private ToolStripButton btnUpdateUniform;
        private MenuStrip ShortcutStrip;
        private ToolStripMenuItem shortcutsToolStripMenuItem;
        private ToolStripMenuItem includeToolStripMenuItem;
        private ToolStripMenuItem excludeeToolStripMenuItem;
        private ToolStripMenuItem recomputeToolStripMenuItem;
        private ToolStripMenuItem refreshToolStripMenuItem;
        private ToolStripMenuItem viewCashReportToolStripMenuItem;
        private ToolStripMenuItem editUniformToolStripMenuItem;
        private ToolStripMenuItem generatePayslipToolStripMenuItem;
        private ToolStrip TabStripMain;

        #endregion

        #region Constructor

        //Constructor.
        public PayrollEditorPage() {
            //Initialize controls
            InitializeComponent();

            //Set custom renderers
            TabStripMain.Renderer = new LightTabStripRenderer();
            MainToolStrip.Renderer = GlobalVariables.renderer;

            //Enable double-buffering on records datagridview to avoid flicker.
            Extensions.DGVDoubleBuffered(RecordsDataGrid, true);
        }

        //Designer-time initializations
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollEditorPage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TabStripMain = new System.Windows.Forms.ToolStrip();
            this.tbCashReports = new System.Windows.Forms.ToolStripButton();
            this.tbTruckingLogs = new System.Windows.Forms.ToolStripButton();
            this.tbPayroll = new System.Windows.Forms.ToolStripButton();
            this.tbSummary = new System.Windows.Forms.ToolStripButton();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnBack = new System.Windows.Forms.ToolStripButton();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btnSearch = new System.Windows.Forms.ToolStripButton();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.lblSearch = new System.Windows.Forms.ToolStripLabel();
            this.btnRecompute = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.btnViewCR = new System.Windows.Forms.ToolStripButton();
            this.btnUpdateUniform = new System.Windows.Forms.ToolStripButton();
            this.btnCreatePayslip = new System.Windows.Forms.ToolStripButton();
            this.RecordsDataGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TooltipStatus = new System.Windows.Forms.ToolTip(this.components);
            this.ShortcutStrip = new System.Windows.Forms.MenuStrip();
            this.shortcutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.includeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excludeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recomputeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCashReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editUniformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generatePayslipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WarningPanel = new PayrollSystem.BufferedPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FiltersPanel = new PayrollSystem.BufferedPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.bufferedPanel3 = new PayrollSystem.BufferedPanel();
            this.PayrollInfoPanel = new PayrollSystem.BufferedPanel();
            this.lblPayrollTitle = new System.Windows.Forms.Label();
            this.lblDatePeriod = new System.Windows.Forms.Label();
            this.PayrollInfoPanelBorder = new PayrollSystem.BufferedPanel();
            this.TabStripMain.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).BeginInit();
            this.ShortcutStrip.SuspendLayout();
            this.WarningPanel.SuspendLayout();
            this.FiltersPanel.SuspendLayout();
            this.PayrollInfoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabStripMain
            // 
            this.TabStripMain.AllowItemReorder = true;
            this.TabStripMain.AutoSize = false;
            this.TabStripMain.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TabStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbCashReports,
            this.tbTruckingLogs,
            this.tbPayroll,
            this.tbSummary});
            this.TabStripMain.Location = new System.Drawing.Point(0, 67);
            this.TabStripMain.Name = "TabStripMain";
            this.TabStripMain.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.TabStripMain.Size = new System.Drawing.Size(917, 30);
            this.TabStripMain.TabIndex = 2;
            this.TabStripMain.Text = "toolStrip1";
            // 
            // tbCashReports
            // 
            this.tbCashReports.Checked = true;
            this.tbCashReports.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tbCashReports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbCashReports.Image = ((System.Drawing.Image)(resources.GetObject("tbCashReports.Image")));
            this.tbCashReports.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbCashReports.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbCashReports.Name = "tbCashReports";
            this.tbCashReports.Size = new System.Drawing.Size(88, 25);
            this.tbCashReports.Text = "Cash Reports";
            this.tbCashReports.Click += new System.EventHandler(this.TabButtons_Click);
            // 
            // tbTruckingLogs
            // 
            this.tbTruckingLogs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbTruckingLogs.Image = ((System.Drawing.Image)(resources.GetObject("tbTruckingLogs.Image")));
            this.tbTruckingLogs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbTruckingLogs.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbTruckingLogs.Name = "tbTruckingLogs";
            this.tbTruckingLogs.Size = new System.Drawing.Size(89, 25);
            this.tbTruckingLogs.Text = "Trucking Logs";
            this.tbTruckingLogs.Click += new System.EventHandler(this.TabButtons_Click);
            // 
            // tbPayroll
            // 
            this.tbPayroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbPayroll.Image = ((System.Drawing.Image)(resources.GetObject("tbPayroll.Image")));
            this.tbPayroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbPayroll.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbPayroll.Name = "tbPayroll";
            this.tbPayroll.Size = new System.Drawing.Size(50, 25);
            this.tbPayroll.Text = "Payroll";
            this.tbPayroll.Click += new System.EventHandler(this.TabButtons_Click);
            // 
            // tbSummary
            // 
            this.tbSummary.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbSummary.Image = ((System.Drawing.Image)(resources.GetObject("tbSummary.Image")));
            this.tbSummary.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbSummary.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tbSummary.Name = "tbSummary";
            this.tbSummary.Size = new System.Drawing.Size(67, 25);
            this.tbSummary.Text = "Summary";
            this.tbSummary.Click += new System.EventHandler(this.TabButtons_Click);
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBack,
            this.btnAdd,
            this.btnDelete,
            this.toolStripButton1,
            this.btnSearch,
            this.txtSearch,
            this.lblSearch,
            this.btnRecompute,
            this.btnRefresh,
            this.btnViewCR,
            this.btnUpdateUniform,
            this.btnCreatePayslip});
            this.MainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Padding = new System.Windows.Forms.Padding(5, 2, 1, 2);
            this.MainToolStrip.Size = new System.Drawing.Size(917, 37);
            this.MainToolStrip.TabIndex = 3;
            this.MainToolStrip.Text = "MainToolStrip";
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = global::PayrollSystem.Properties.Resources.back;
            this.btnBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBack.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(55, 30);
            this.btnBack.Text = "Back";
            this.btnBack.ToolTipText = "Return to Payrolls";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.AutoToolTip = false;
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnAdd.Image = global::PayrollSystem.Properties.Resources.add02;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(62, 30);
            this.btnAdd.Text = "Include";
            this.btnAdd.ToolTipText = "Include Cash Reports (Ctrl + I)";
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.AutoToolTip = false;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnDelete.Image = global::PayrollSystem.Properties.Resources.delete02;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(64, 30);
            this.btnDelete.Text = "Exclude";
            this.btnDelete.ToolTipText = "Exclude Cash Reports (Ctrl + E)";
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripButton1.Enabled = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(1, 30);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // btnSearch
            // 
            this.btnSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSearch.AutoSize = false;
            this.btnSearch.AutoToolTip = false;
            this.btnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSearch.Image = global::PayrollSystem.Properties.Resources.search;
            this.btnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(23, 28);
            this.btnSearch.Text = "Search";
            this.btnSearch.ToolTipText = "Search (Ctrl + F)";
            this.btnSearch.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.txtSearch.AutoSize = false;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtSearch.Margin = new System.Windows.Forms.Padding(0);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.txtSearch.Padding = new System.Windows.Forms.Padding(2);
            this.txtSearch.Size = new System.Drawing.Size(0, 21);
            this.txtSearch.Visible = false;
            // 
            // lblSearch
            // 
            this.lblSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblSearch.AutoSize = false;
            this.lblSearch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.lblSearch.Size = new System.Drawing.Size(52, 24);
            this.lblSearch.Text = "Search:";
            this.lblSearch.ToolTipText = "Search (Ctrl + F)";
            this.lblSearch.Visible = false;
            // 
            // btnRecompute
            // 
            this.btnRecompute.AutoToolTip = false;
            this.btnRecompute.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecompute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnRecompute.Image = global::PayrollSystem.Properties.Resources.star_shaded;
            this.btnRecompute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRecompute.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnRecompute.Name = "btnRecompute";
            this.btnRecompute.Size = new System.Drawing.Size(81, 30);
            this.btnRecompute.Text = "Recompute";
            this.btnRecompute.ToolTipText = "Recompute (Shift + F5)";
            this.btnRecompute.Visible = false;
            this.btnRecompute.Click += new System.EventHandler(this.btnRecompute_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AutoToolTip = false;
            this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnRefresh.Image = global::PayrollSystem.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 30);
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.ToolTipText = "Refresh (F5)";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnViewCR
            // 
            this.btnViewCR.AutoToolTip = false;
            this.btnViewCR.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewCR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnViewCR.Image = global::PayrollSystem.Properties.Resources.view;
            this.btnViewCR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnViewCR.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnViewCR.Name = "btnViewCR";
            this.btnViewCR.Size = new System.Drawing.Size(112, 30);
            this.btnViewCR.Text = "View Cash Report";
            this.btnViewCR.ToolTipText = "View Cash Report";
            this.btnViewCR.Visible = false;
            this.btnViewCR.Click += new System.EventHandler(this.btnViewCR_Click);
            // 
            // btnUpdateUniform
            // 
            this.btnUpdateUniform.AutoToolTip = false;
            this.btnUpdateUniform.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateUniform.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnUpdateUniform.Image = global::PayrollSystem.Properties.Resources.edit;
            this.btnUpdateUniform.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpdateUniform.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnUpdateUniform.Name = "btnUpdateUniform";
            this.btnUpdateUniform.Size = new System.Drawing.Size(80, 30);
            this.btnUpdateUniform.Text = "Deductions";
            this.btnUpdateUniform.ToolTipText = "Update other deductions (F2)";
            this.btnUpdateUniform.Visible = false;
            this.btnUpdateUniform.Click += new System.EventHandler(this.btnUpdateUniform_Click);
            // 
            // btnCreatePayslip
            // 
            this.btnCreatePayslip.AutoToolTip = false;
            this.btnCreatePayslip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreatePayslip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnCreatePayslip.Image = global::PayrollSystem.Properties.Resources.dollar_icon;
            this.btnCreatePayslip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCreatePayslip.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnCreatePayslip.Name = "btnCreatePayslip";
            this.btnCreatePayslip.Size = new System.Drawing.Size(96, 30);
            this.btnCreatePayslip.Text = "Create Payslip";
            this.btnCreatePayslip.ToolTipText = "Create Payslip (Ctrl + P)";
            this.btnCreatePayslip.Visible = false;
            this.btnCreatePayslip.Click += new System.EventHandler(this.btnCreatePayslip_Click);
            // 
            // RecordsDataGrid
            // 
            this.RecordsDataGrid.AllowUserToAddRows = false;
            this.RecordsDataGrid.AllowUserToDeleteRows = false;
            this.RecordsDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.RecordsDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.RecordsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.RecordsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RecordsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.RecordsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.RecordsDataGrid.ColumnHeadersHeight = 28;
            this.RecordsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.RecordsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RecordsDataGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.RecordsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RecordsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.RecordsDataGrid.Location = new System.Drawing.Point(0, 127);
            this.RecordsDataGrid.MultiSelect = false;
            this.RecordsDataGrid.Name = "RecordsDataGrid";
            this.RecordsDataGrid.ReadOnly = true;
            this.RecordsDataGrid.RowHeadersVisible = false;
            this.RecordsDataGrid.RowTemplate.Height = 28;
            this.RecordsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RecordsDataGrid.ShowCellErrors = false;
            this.RecordsDataGrid.ShowEditingIcon = false;
            this.RecordsDataGrid.Size = new System.Drawing.Size(917, 323);
            this.RecordsDataGrid.TabIndex = 15;
            this.RecordsDataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.RecordsDataGrid_CellEndEdit);
            this.RecordsDataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.RecordsDataGrid_DataError);
            this.RecordsDataGrid.SelectionChanged += new System.EventHandler(this.RecordsDataGrid_SelectionChanged);
            this.RecordsDataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RecordsDataGrid_KeyDown);
            this.RecordsDataGrid.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RecordsDataGrid_MouseDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 139;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 139;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 139;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ShortcutStrip
            // 
            this.ShortcutStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shortcutsToolStripMenuItem});
            this.ShortcutStrip.Location = new System.Drawing.Point(0, 0);
            this.ShortcutStrip.Name = "ShortcutStrip";
            this.ShortcutStrip.Size = new System.Drawing.Size(917, 24);
            this.ShortcutStrip.TabIndex = 18;
            this.ShortcutStrip.Text = "menuStrip1";
            this.ShortcutStrip.Visible = false;
            // 
            // shortcutsToolStripMenuItem
            // 
            this.shortcutsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.includeToolStripMenuItem,
            this.excludeeToolStripMenuItem,
            this.recomputeToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.viewCashReportToolStripMenuItem,
            this.editUniformToolStripMenuItem,
            this.generatePayslipToolStripMenuItem});
            this.shortcutsToolStripMenuItem.Name = "shortcutsToolStripMenuItem";
            this.shortcutsToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.shortcutsToolStripMenuItem.Text = "Shortcuts";
            // 
            // includeToolStripMenuItem
            // 
            this.includeToolStripMenuItem.Name = "includeToolStripMenuItem";
            this.includeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.includeToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.includeToolStripMenuItem.Text = "Include";
            this.includeToolStripMenuItem.Click += new System.EventHandler(this.includeToolStripMenuItem_Click);
            // 
            // excludeeToolStripMenuItem
            // 
            this.excludeeToolStripMenuItem.Name = "excludeeToolStripMenuItem";
            this.excludeeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.excludeeToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.excludeeToolStripMenuItem.Text = "Exclude";
            this.excludeeToolStripMenuItem.Click += new System.EventHandler(this.excludeeToolStripMenuItem_Click);
            // 
            // recomputeToolStripMenuItem
            // 
            this.recomputeToolStripMenuItem.Name = "recomputeToolStripMenuItem";
            this.recomputeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
            this.recomputeToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.recomputeToolStripMenuItem.Text = "Recompute";
            this.recomputeToolStripMenuItem.Click += new System.EventHandler(this.recomputeToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // viewCashReportToolStripMenuItem
            // 
            this.viewCashReportToolStripMenuItem.Name = "viewCashReportToolStripMenuItem";
            this.viewCashReportToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.viewCashReportToolStripMenuItem.Text = "View Cash Report";
            this.viewCashReportToolStripMenuItem.Click += new System.EventHandler(this.viewCashReportToolStripMenuItem_Click);
            // 
            // editUniformToolStripMenuItem
            // 
            this.editUniformToolStripMenuItem.Name = "editUniformToolStripMenuItem";
            this.editUniformToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.editUniformToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.editUniformToolStripMenuItem.Text = "Edit Other Deductions";
            this.editUniformToolStripMenuItem.Click += new System.EventHandler(this.editUniformToolStripMenuItem_Click);
            // 
            // generatePayslipToolStripMenuItem
            // 
            this.generatePayslipToolStripMenuItem.Name = "generatePayslipToolStripMenuItem";
            this.generatePayslipToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.generatePayslipToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.generatePayslipToolStripMenuItem.Text = "Generate Payslip";
            this.generatePayslipToolStripMenuItem.Click += new System.EventHandler(this.generatePayslipToolStripMenuItem_Click);
            // 
            // WarningPanel
            // 
            this.WarningPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WarningPanel.BackColor = System.Drawing.Color.White;
            this.WarningPanel.BorderColor = System.Drawing.Color.Black;
            this.WarningPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.WarningPanel.Controls.Add(this.label2);
            this.WarningPanel.Controls.Add(this.label1);
            this.WarningPanel.Location = new System.Drawing.Point(229, 243);
            this.WarningPanel.Name = "WarningPanel";
            this.WarningPanel.Size = new System.Drawing.Size(459, 83);
            this.WarningPanel.TabIndex = 17;
            this.WarningPanel.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(453, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Include Trucking Log and Cash Reports First";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.label1.Location = new System.Drawing.Point(3, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(453, 49);
            this.label1.TabIndex = 12;
            this.label1.Text = "You need to include trucking log records first before you can compute for the pay" +
    "roll.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FiltersPanel
            // 
            this.FiltersPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.FiltersPanel.BorderColor = System.Drawing.Color.Black;
            this.FiltersPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.FiltersPanel.Controls.Add(this.label6);
            this.FiltersPanel.Controls.Add(this.cboType);
            this.FiltersPanel.Controls.Add(this.bufferedPanel3);
            this.FiltersPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.FiltersPanel.Location = new System.Drawing.Point(0, 97);
            this.FiltersPanel.Name = "FiltersPanel";
            this.FiltersPanel.Size = new System.Drawing.Size(917, 30);
            this.FiltersPanel.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(643, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Employee Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboType
            // 
            this.cboType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "All",
            "Driver",
            "Helper"});
            this.cboType.Location = new System.Drawing.Point(770, 5);
            this.cboType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(135, 21);
            this.cboType.TabIndex = 10;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // bufferedPanel3
            // 
            this.bufferedPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.bufferedPanel3.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel3.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel3.Location = new System.Drawing.Point(0, 29);
            this.bufferedPanel3.Name = "bufferedPanel3";
            this.bufferedPanel3.Size = new System.Drawing.Size(917, 1);
            this.bufferedPanel3.TabIndex = 0;
            // 
            // PayrollInfoPanel
            // 
            this.PayrollInfoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.PayrollInfoPanel.BorderColor = System.Drawing.Color.Black;
            this.PayrollInfoPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.PayrollInfoPanel.Controls.Add(this.lblPayrollTitle);
            this.PayrollInfoPanel.Controls.Add(this.lblDatePeriod);
            this.PayrollInfoPanel.Controls.Add(this.PayrollInfoPanelBorder);
            this.PayrollInfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PayrollInfoPanel.Location = new System.Drawing.Point(0, 37);
            this.PayrollInfoPanel.Name = "PayrollInfoPanel";
            this.PayrollInfoPanel.Size = new System.Drawing.Size(917, 30);
            this.PayrollInfoPanel.TabIndex = 4;
            // 
            // lblPayrollTitle
            // 
            this.lblPayrollTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPayrollTitle.AutoEllipsis = true;
            this.lblPayrollTitle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPayrollTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblPayrollTitle.Location = new System.Drawing.Point(7, 6);
            this.lblPayrollTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblPayrollTitle.Name = "lblPayrollTitle";
            this.lblPayrollTitle.Size = new System.Drawing.Size(609, 18);
            this.lblPayrollTitle.TabIndex = 10;
            this.lblPayrollTitle.Text = "Cash Report Title Goes Here";
            this.lblPayrollTitle.MouseLeave += new System.EventHandler(this.lblPayrollTitle_MouseLeave);
            this.lblPayrollTitle.MouseHover += new System.EventHandler(this.lblPayrollTitle_MouseHover);
            // 
            // lblDatePeriod
            // 
            this.lblDatePeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDatePeriod.AutoEllipsis = true;
            this.lblDatePeriod.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatePeriod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.lblDatePeriod.Location = new System.Drawing.Point(698, 8);
            this.lblDatePeriod.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.lblDatePeriod.Name = "lblDatePeriod";
            this.lblDatePeriod.Size = new System.Drawing.Size(207, 15);
            this.lblDatePeriod.TabIndex = 12;
            this.lblDatePeriod.Text = "Jan 1, 2016 - Dec 31, 3000";
            this.lblDatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PayrollInfoPanelBorder
            // 
            this.PayrollInfoPanelBorder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.PayrollInfoPanelBorder.BorderColor = System.Drawing.Color.Black;
            this.PayrollInfoPanelBorder.Borders = System.Windows.Forms.AnchorStyles.None;
            this.PayrollInfoPanelBorder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PayrollInfoPanelBorder.Location = new System.Drawing.Point(0, 29);
            this.PayrollInfoPanelBorder.Name = "PayrollInfoPanelBorder";
            this.PayrollInfoPanelBorder.Size = new System.Drawing.Size(917, 1);
            this.PayrollInfoPanelBorder.TabIndex = 0;
            // 
            // PayrollEditorPage
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.ClientSize = new System.Drawing.Size(917, 450);
            this.Controls.Add(this.WarningPanel);
            this.Controls.Add(this.RecordsDataGrid);
            this.Controls.Add(this.FiltersPanel);
            this.Controls.Add(this.TabStripMain);
            this.Controls.Add(this.PayrollInfoPanel);
            this.Controls.Add(this.MainToolStrip);
            this.Controls.Add(this.ShortcutStrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.ShortcutStrip;
            this.Name = "PayrollEditorPage";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Payroll Editor";
            this.Load += new System.EventHandler(this.PayrollEditorPage_Load);
            this.Resize += new System.EventHandler(this.PayrollEditorPage_Resize);
            this.TabStripMain.ResumeLayout(false);
            this.TabStripMain.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsDataGrid)).EndInit();
            this.ShortcutStrip.ResumeLayout(false);
            this.ShortcutStrip.PerformLayout();
            this.WarningPanel.ResumeLayout(false);
            this.FiltersPanel.ResumeLayout(false);
            this.PayrollInfoPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region IWindowStatusUpdater call

        //Sets the status of the window.
        void ShowStatus(string status) {
            //Recursively find the parent form that implements IWindowStatusUpdater.
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateWindowStatus(status);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        //Show the total records count through the IWindowStatusUpdater interface.
        void ShowTotalRecordsCount() {
            //Recursively find the parent form that implements IWindowStatusUpdater.
            if (ParentForm != null) {
                ContainerControl p = (ContainerControl)ParentForm;
                while (p != null) {
                    if (p is Interfaces.IWindowStatusUpdater) {
                        Interfaces.IWindowStatusUpdater wnd = (Interfaces.IWindowStatusUpdater)p;
                        wnd.UpdateTotalRecordCountStatus(RecordsDataGrid.Rows.Count);
                        break;
                    }
                    p = (ContainerControl)ParentForm.ParentForm;
                }
            }
        }

        #endregion

        #region Local Variables

        //Cache Back page.
        SystemPage PreviousPage = null;
        int PreviousSelectedRow = -1;

        //Working Payroll Info.
        string PayrollID = "", PayrollTitle = "";
        string PayrollStartDate = "", PayrollEndDate = "";
        bool PayrollFinal = false;

        #endregion

        #region Cached Payroll Data

        //Included Cash Reports and Trucking Logs
        DataTable IncludeCashReport, IncludeTruckingLog;

        //Payroll computation.
        DataTable PayrollTable, SummaryTable;

        //Employees table.
        DataTable EmployeesTable;

        //IEnumerables
        IEnumerable<PayrollGroupingItem> DriverCAs, HelperCAs;
        IEnumerable<PayrollItem> DriversPList, HelpersPList, AllPList;
        IEnumerable<PayrollSummaryItem> SummaryAll;

        /* Compute State
         * Purpose:
         * If Payroll is in editing mode, compute state shall determine if system will write data on database
         * or retrieve data from the database.
         * IsComputed will turn to true once user navigates to "Payroll" or "Summary" tabs. Once this turns true,
         * system will write the computed data onto the payroll database.
         * If user tries to include another cash reports or trucking logs after computation, this will become false
         * then erase all payroll and summary records under the active payroll.
        */
        bool _isComputed = false;
        bool IsComputed {
            get {
                return _isComputed;
            } set {
                _isComputed = value;
                NotifyComputeState();
            }
        }

        #endregion

        #region Public Functions

        //Set page for back navigation.
        public void SetPreviousPage(SystemPage prev, int previndex) {
            PreviousPage = prev;
            PreviousSelectedRow = previndex;
        }

        //Set up the working payroll details.
        public void SetPayrollInfo(string PID, string Title, string StartDate, string EndDate, bool IsFinal){
            //Save on variable.
            PayrollID = PID;
            PayrollTitle = Title;
            PayrollStartDate = StartDate;
            PayrollEndDate = EndDate;
            PayrollFinal = IsFinal;

            //Display labels title and payroll period.
            lblPayrollTitle.Text = PayrollTitle;
            lblDatePeriod.Text = PayrollStartDate + " - " + PayrollEndDate;
            if (PayrollFinal) {
                lblPayrollTitle.Text += " [Locked]";
            }
        }

        #endregion

        #region Local Functions

        //Convert a database data into string. Handles DBNull
        string ToStr(object rowdata) {
            try {
                if (rowdata is DBNull || rowdata == null) return string.Empty;
                return rowdata.ToString();
            } catch {
                return "";
            }
        }

        //Call to refresh all records in the datagrid.
        void RefreshRecords() {
            //Prevent unwanted layout changes while updating stuff here.
            SuspendLayout();

            //Set default properties of controls.
            if (PayrollFinal == false) {    //Disable editing controls if payroll is final.
                btnAdd.Visible = true;
                btnDelete.Visible = true;
            }
            FiltersPanel.Visible = false;
            WarningPanel.Visible = false;
            btnViewCR.Visible = false;
            RecordsDataGrid.ReadOnly = true;
            btnRecompute.Visible = false;
            btnCreatePayslip.Visible = false;
            btnUpdateUniform.Visible = false;

            //Refresh data depending on selected tab.
            if (tbCashReports.Checked) {
                RefreshPayrollCashReports();    //Retrieve list of payroll Cash reports.
                btnViewCR.Visible = true;
            } else if (tbTruckingLogs.Checked) {
                RefreshPayrollTruckingLog();    //Retrieve all trucking logs included for this cash report.
            } else if (tbPayroll.Checked) {
                FiltersPanel.Visible = true;
                if (cboType.SelectedIndex < 0) cboType.SelectedIndex = 0;
                btnAdd.Visible = false;
                btnDelete.Visible = false;
                ComputePayroll();               //Compute the payroll.
            } else if (tbSummary.Checked) {
                btnAdd.Visible = false;
                btnDelete.Visible = false;
                btnCreatePayslip.Visible = true;
                ComputePayroll();               //Compute the payroll.
            }

            ShowTotalRecordsCount();    //Show total records
            ResumeLayout();             //Resume layouting logic.
        }

        //Resizes all columns to fit content, and add a filler column at the end.
        void AutosizeColumns() {
            if (RecordsDataGrid.Columns.Count > 0) {
                foreach (DataGridViewColumn col in RecordsDataGrid.Columns) {
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    int nwidt = col.Width;
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    col.Width = nwidt;
                }
            }

            
            //Add extension column.
            DataGridViewColumn spacer = new DataGridViewTextBoxColumn();
            spacer.HeaderText = "";
            spacer.ReadOnly = true;
            spacer.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            RecordsDataGrid.Columns.Add(spacer);
             
        }

        //Hide the specified column.
        void HideCol(string colname) {
            RecordsDataGrid.Columns[colname].Visible = false;
        }

        //Gets the column value of the selected row.
        object Cell(string colname) {
            if (RecordsDataGrid.SelectedRows.Count > 0) {   //Make sure there are selected records.
                return RecordsDataGrid.SelectedRows[0].Cells[colname].Value;
            }
            return null;
        }

        //Renames the header text of the specified column.
        void SetColHeader(string colname, string headertext) {
            DataGridViewColumn col = RecordsDataGrid.Columns[colname];
            if (col != null) {
                col.HeaderText = headertext;
            }
        }

        #endregion

        #region Payslip Functions

        //Function to create payslip (in Excel format).
        void CreatePayslip(string filename, string payslips_dir) {
            //Show please wait while processing.
            PleaseWaitDialog dlg = new PleaseWaitDialog();
            dlg.ShowPleaseWait(ParentForm);
            
            /////////////// QUERY PAYSLIP INFO AND CONSOLIDATE ALL ////////////////////////////
            String query = "SELECT A.Employee, (B.LastName + ', ' + B.FirstName + IIF(ISNULL(B.MiddleName), '', ' ' + B.MiddleName)) AS [EmployeeName], SUM(A.Rate) AS [Rate], SUM(A.Tax) AS [Tax], SUM(A.CF) AS [CF], SUM(A.SA) AS [SA], SUM(A.TotalDeductions) AS [TotalDeductions], SUM(A.GrossPay) AS [GrossPay], C.CA, C.Uniform, C.SSS, C.PhilHealth, C.PagIBIG FROM (PayrollEntry AS A INNER JOIN Employee AS B ON A.Employee = B.EmployeeID) LEFT JOIN PayrollSummary AS C ON A.Employee = C.Employee ";
                   query += "WHERE A.PID = @id AND C.PID = @id ";
                   query += "GROUP BY A.Employee, (B.LastName + ', ' + B.FirstName + IIF(ISNULL(B.MiddleName), '', ' ' + B.MiddleName)), C.CA, C.Uniform, C.SSS, C.PhilHealth, C.PagIBIG ";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + PayrollID + "}");

            //Execute SQL Query. (Query consolidated amounts);
            IEnumerable<DataRow> paylist = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p).Rows.Cast<DataRow>();

            ///////////////// QUERY TRUCKING LOGS INVOLVED ////////////////////////////////
            QueryPayrollTruckingLog();  //Requery payroll included in this payroll.
            IEnumerable<DataRow> truckinglogs = IncludeTruckingLog.Rows.Cast<DataRow>();

            ///////////////// QUERY CASH REPORTS INVOLVED ////////////////////////////////
            IEnumerable<DataRow> CRItemsList = QueryCashReportItems();  //Query the Cash Report Items.

            //Start writing payslip spreadsheet using ClosedXML with custom functions of ClosedXMLEditor
            using (ClosedXMLEditor xml = new ClosedXMLEditor()) {
                //Create worksheet.
                xml.SetWorksheet("Payslip").PageSetup.SetShowGridlines(false).PagesWide = 1;
                xml.ActiveWorksheet.ShowGridLines = false;

                //Resize columns.
                xml.ActiveWorksheet.Columns("A,D,K").Width = 1.0;
                xml.ActiveWorksheet.Columns("B").Width = 11.0;
                xml.ActiveWorksheet.Columns("C,G").Width = 20.0;
                xml.ActiveWorksheet.Columns("E,F,H,J").Width = 10.0;
                xml.ActiveWorksheet.Columns("I").Width = 12.0;
                          
                //Set default style.
                xml.ActiveWorksheet.Style.Font.SetFontName("Arial").Font.SetFontSize(8).NumberFormat.SetFormat("#,##0.00;(#,##0.00);-;@");

                //Insert the payroll title at the first row for reference purposes.
                List<Object> caption = new List<Object>();
                caption.Add("Payslip: " + PayrollTitle);
                xml.ActiveWorksheet.Cell("A1").InsertData(caption);
                xml.ActiveWorksheet.Range("A1:J1").Merge().Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(12).Font.SetBold();
                xml.SetRowsHeight("1", 40);


                //DataTable of trucking logs (used for reports)
                DataTable dt_trucklogs = new DataTable();
                dt_trucklogs.Columns.AddRange(new DataColumn[] {new DataColumn("Date"),
                                                        new DataColumn("PlateNo"),
                                                        new DataColumn("Destination"),
                                                        new DataColumn("DRNo"),
                                                        new DataColumn("Rate")});

                //DataTable of cash advance (used for reports)
                DataTable dt_calogs = new DataTable();
                dt_calogs.Columns.AddRange(new DataColumn[] {new DataColumn("CADate"),
                                                        new DataColumn("CAPlateNo"),
                                                        new DataColumn("CAParticular"),
                                                        new DataColumn("CAPosition"),
                                                        new DataColumn("CAAmount")});

                //Microsoft local report.
                Microsoft.Reporting.WinForms.LocalReport rpt = new LocalReport();

                using (Stream strm = Assembly.GetExecutingAssembly().GetManifestResourceStream("PayrollSystem.Reports.PayslipReport.rdlc")) {
                    rpt.LoadReportDefinition(strm); //Load teh report definition from embedded resource.
                }

                //Set data source for the trucking logs.
                rpt.DataSources.Add(new ReportDataSource("TruckingLogs", dt_trucklogs));
                rpt.DataSources.Add(new ReportDataSource("CALogs", dt_calogs));

                    //Iterate all records.
                    //TODO: One payslip (bordered) per employee.
                    int startRow = 3;
                    foreach (DataRow row in paylist) {
                        //Columns: 10 (A - J)
                        //Rows:    13 (min)
                    
                        //Format:
                        //(Column 1)
                        //1:  5SZian Trucking Services       (Merged: B:D) >> Bold
                        //2:  Payslip Period: Date - Date    (Merged: B:D)
                        //3:  Employee name                  (Merged: B:C)
                        //4:  <br>
                        //5:  Rate:             (B)    <rate>              (C)
                        //6:    TAX (2%)        (B)    <tax>               (C)
                        //7:    CF (5%)         (B)    <cf>                (C)
                        //8:  CA                (B)    <sa>                (C)   
                        //9:  Uniform           (B)    <uniform>           (C)   
                        //10: Office CA         (B)    <ca>                (C)
                        //11: Total Deductions  (B)    <total deductions   (C)   >> Bold, Bordered, bottom thin
                        //12: Gross Pay         (B)    <gross pay>         (C)   >> Bold
                        //13: Net Pay           (B)    <net pay>           (C)   >> Bold, Bordered thick (B:C)
                        //14: <br>
                    
                        //(Column 2)
                        //1:  Mga Biyahe (Trucking Log) <header>     (E:J)
                        //2:  Date (E)   Plate No. (F)   Destination (G)    Position (H)   Rate (I)  <space> (J)
                        //n:  <all trucking log entries.

                        //Get related values.
                        string employeeID = row["Employee"].ToString();
                        string employee = row["EmployeeName"].ToString();
                        double rate = Convert.ToDouble(row["Rate"]);
                        double tax = Convert.ToDouble(row["Tax"]);
                        double cf = Convert.ToDouble(row["CF"]);
                        double sa = Convert.ToDouble(row["SA"]);

                        //Update Feb 2017: Contributions (new update possible of nulls).
                        double sss = 0.00, ph = 0.00, pi = 0.00;
                        Double.TryParse(row["SSS"].ToString(), out sss);
                        Double.TryParse(row["PhilHealth"].ToString(), out ph);
                        Double.TryParse(row["PagIBIG"].ToString(), out pi);
                    
                        double grossPay = Convert.ToDouble(row["GrossPay"]);
                        double uniform = 0.00;
                        Double.TryParse(row["Uniform"].ToString(), out uniform);
                        double ca = Convert.ToDouble(row["CA"]);
                        double totalDeductions = Convert.ToDouble(row["TotalDeductions"]) + (ca + uniform + sss + ph + pi);
                        double netPay = 0.00;
                        string dateRange = DateTime.Parse(PayrollStartDate).ToString("MMM dd") + " - " + DateTime.Parse(PayrollEndDate).ToString("MMM dd, yyyy");

                        //Try parse uniform
                        netPay = grossPay - (ca + uniform + sss + ph + pi); //3.5.17 SSS, PagIbig, PhilHealth
                                        
                        //Enumerate DriverCA
                        IEnumerable<PayrollCAItem> DriverCAs = CRItemsList.Where(x => x["Driver"].ToString().Equals(employeeID, StringComparison.InvariantCultureIgnoreCase)).Select(x => new PayrollCAItem(x["ItemDate"].ToString(), x["PlateNo"].ToString(), "Driver", x["Driver"].ToString(), "XXX", Convert.ToDouble(x["DriverCA"]), x["Particular"].ToString()));
                        IEnumerable<PayrollCAItem> HelperCAs = CRItemsList.Where(x => x["Helper"].ToString().Equals(employeeID, StringComparison.InvariantCultureIgnoreCase)).Select(x => new PayrollCAItem(x["ItemDate"].ToString(), x["PlateNo"].ToString(), "Helper", x["Helper"].ToString(), "XXX", Convert.ToDouble(x["HelperCA"]), x["Particular"].ToString()));
                        IEnumerable<PayrollCAItem> AllCAs = DriverCAs.Union(HelperCAs);

                        /*
                    
                        //IEnumerable select required CAs.
                        Trace.WriteLine("EmployeeID: " + employeeID);
                        Trace.WriteLine("CA (As Driver):");
                        foreach (PayrollCAItem itm in DriverCAs)
                            Trace.WriteLine(itm.CA.ToString());
                    
                        Trace.WriteLine("CA (As Helper):");
                        foreach (PayrollCAItem itm in HelperCAs)
                            Trace.WriteLine(itm.CA.ToString());
                    

                        foreach (PayrollCAItem itm in AllCAs) {
                            Trace.WriteLine(itm.Date + " | " + itm.PlateNo + " | " + itm.Position + " | " + itm.CA);
                        }
                    
                        Trace.WriteLine("-----------------------------------");
                        */

                        //////////// <<<<<<<<<<<<<<<<<PAYSLIP BREAKDOWN>>>>>>>>>>>>>>>>>>>> ///////////////
                    
                        //Clear data tables (for reports).
                        dt_trucklogs.Clear();
                        dt_calogs.Clear();

                        //Insert payslip info.
                        List<Object[]> info_rows = new List<Object[]>();
                        info_rows.Add(new Object[] { "5SZian Trucking Services" });
                        info_rows.Add(new Object[] { "PAYSLIP PERIOD: " + dateRange});
                        info_rows.Add(new Object[] { "Employee:", employee });
                        info_rows.Add(new Object[] { });
                        info_rows.Add(new Object[] { "GROSS PAY", rate });
                        info_rows.Add(new Object[] { "   TAX (2%)", tax });
                        info_rows.Add(new Object[] { "   CF (5%)", cf });
                        info_rows.Add(new Object[] { "CA", sa });
                        info_rows.Add(new Object[] { "Uniform", uniform });
                        info_rows.Add(new Object[] { "Office CA", ca });
                        info_rows.Add(new Object[] { "SSS", sss });
                        info_rows.Add(new Object[] { "PhilHealth", ph });
                        info_rows.Add(new Object[] { "Pag-Ibig", pi });
                        info_rows.Add(new Object[] { "Deductions", totalDeductions });
                        //info_rows.Add(new Object[] { "NET PAY", grossPay });
                        info_rows.Add(new Object[] { "NET PAY", netPay });
                        xml.ActiveWorksheet.Cell(startRow, 2).InsertData(info_rows).Style = xml.ActiveWorksheet.Style;

                        //////////// <<<<<<<<<<<<<<<<<TRUCKING LOG BREAKDOWN>>>>>>>>>>>>>>>>>>>> ///////////////

                        //Insert trucking log headers and column.
                        info_rows.Clear();
                        info_rows.Add(new Object[] { "Mga Biyahe" });
                        info_rows.Add(new Object[] { "DATE", "PLATE NO.", "DESTINATION", "DR NO", "RATE" });
                        xml.ActiveWorksheet.Cell(startRow, 5).InsertData(info_rows).Style = xml.ActiveWorksheet.Style;

                        //Trace output. (Uncomment for debugging.)
                        /* Trace.WriteLine(employee + " : " + rate + " : " + " : " + tax + " : " + cf + " : " + sa + " : " + totalDeductions + " : " + grossPay + " : " + ca );
                        Trace.WriteLine("Gross: " + netPay);
                        Trace.WriteLine("[" + employee + "]"); */

                        //Loop for each trucking logs related to this employee.
                        //Required for payslip trucking log breakdown list.

                        info_rows.Clear();  //Clear the list to store trucking log entries.

                        //Create IEnumerable of trucking logs.
                        IEnumerable<DataRow> emplog = truckinglogs.Where(x => (x["DriverID"].ToString().Equals(employeeID)) || (x["HelperID"].ToString().Equals(employeeID)));

                        //Sort trucking logs by date.
                        emplog = emplog.OrderBy(x => x["PlateNo"].ToString()).ThenBy(x => DateTime.Parse(x["TransportDate"].ToString())).ThenBy(x => x["Destination"].ToString());

                        double total_tRate = 0.0;
                        foreach (DataRow drow in emplog) {

                            //Trucking log values.
                            string date = DateTime.Parse(drow["TransportDate"].ToString()).ToString("MMM dd, yyyy");
                            string plateno = drow["PlateNo"].ToString();
                            string destination = drow["Destination"].ToString();
                            string position = "";
                            double tRate = 0.0;                                          //rate for employee's trucking service.
                            //double log_sa = Convert.ToDouble(drow["DriverRate"]);        //SA/CA rate of emplyee for that byahe.
                                                
                            if (drow["DriverID"].ToString().Equals(employeeID)) {           //Employee is Driver, get Driver Rate.
                                tRate = Convert.ToDouble(drow["DriverRate"]);
                                //position = "Driver";
                            } else if (drow["HelperID"].ToString().Equals(employeeID)) {    //Employee is Helper, get Helper Rate.
                                tRate = Convert.ToDouble(drow["HelperRate"]);
                                //position = "Helper";
                            }
                        
                            //Replace position with DRNO
                            position = drow["DRNO"].ToString();

                            total_tRate += tRate;   //total for the trucking Rate.

                            //Insert trucking log records.
                            info_rows.Add(new Object[] {date, plateno, destination, position, tRate });

                            //Insert trucking logs (To DataTable)
                            DataRow arow = dt_trucklogs.NewRow();
                            arow["Date"] = DateTime.Parse(date).ToShortDateString();
                            arow["PlateNo"] = plateno;
                            arow["Destination"] = destination;
                            arow["DRNo"] = position;
                            arow["Rate"] = tRate.ToString("#,##0.00");
                            dt_trucklogs.Rows.Add(arow);

                            //Trace. (Uncomment for debugging)
                            /* Trace.WriteLine(date + " : " + plateno + " : " + destination + " : " + position + " : " + tRate); */
                        }

                        //Insert trucking log total.
                        info_rows.Add(new Object[] { "TOTAL", "", "", "", total_tRate });
                        info_rows.Add(new Object[] { });
                    
                        //Get trucking records rows.
                        int logRowCount = info_rows.Count;

                        //COMMIT TRUCKING LOGS
                        xml.ActiveWorksheet.Cell(startRow + 2, 5).InsertData(info_rows).Style = xml.ActiveWorksheet.Style;

                        //////////// <<<<<<<<<<<<<<<<<CA BREAKDOWN>>>>>>>>>>>>>>>>>>>> ///////////////
                        //Insert CA breakdown headers and column.
                        info_rows.Clear();
                        info_rows.Add(new Object[] { "Cash Advance (CA)" });
                        info_rows.Add(new Object[] { "DATE", "PLATE NO.", "PARTICULAR", "POSITION", "AMOUNT" });
                        xml.ActiveWorksheet.Cell(startRow + logRowCount + 2, 5).InsertData(info_rows).Style = xml.ActiveWorksheet.Style;

                        //UPDATE 10.22.16: ADDED CA BREAKDOWN BELOW TRUCKING LOGS
                        info_rows.Clear();  //Clear the list to store CA entries.
                        double total_CA = 0.00;
                        foreach (PayrollCAItem ca_itm in AllCAs) {
                            //Insert CA (Excel)
                            info_rows.Add(new Object[] { ca_itm.Date, ca_itm.PlateNo, ca_itm.Particular ,ca_itm.Position, ca_itm.CA});

                            //Insert CA logs (To DataTable)
                            DataRow arow = dt_calogs.NewRow();
                            arow["CADate"] = DateTime.Parse(ca_itm.Date).ToShortDateString();
                            arow["CAPlateNo"] = ca_itm.PlateNo;
                            arow["CAParticular"] = ca_itm.Particular;
                            arow["CAPosition"] = ca_itm.Position;
                            arow["CAAmount"] = ca_itm.CA.ToString("#,##0.00");
                            dt_calogs.Rows.Add(arow);

                            total_CA += ca_itm.CA;
                        }

                        //Insert CA total
                        info_rows.Add(new Object[] { "TOTAL", "", "", total_CA });

                        //COMMIT CAs
                        xml.ActiveWorksheet.Cell(startRow + 2 + logRowCount + 2, 5).InsertData(info_rows).Style = xml.ActiveWorksheet.Style;

                        //Get CA breakdown records rows.
                        int caRowCount = AllCAs.Count();

                        //Update start row to move on to next payslip.
                        //if (logRowCount - 11(the min row)) <= 0) 
                        //add no extra rows
                        //else add extra rows logRowCount - 11
                        int breakdown_len = logRowCount + caRowCount;
                        int extraRows = (breakdown_len - 7 <= 0) ? 0 : breakdown_len - 7;
                        int baseHeight = 15;

                        //Add borders to payslip
                        xml.ActiveWorksheet.Range(startRow, 1, startRow + baseHeight + extraRows, 11).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                        xml.ActiveWorksheet.Range(startRow, 4, startRow + baseHeight + extraRows, 4).Style.Border.SetRightBorder(XLBorderStyleValues.Thin);

                        ///////////////Start formating text and all.///////////////////
                        //Company header format.
                        xml.ActiveWorksheet.Range(startRow, 2, startRow, 3).Merge().Style.Font.SetBold();

                        //Payslip period format.
                        xml.ActiveWorksheet.Range(startRow + 1, 2, startRow + 1, 3).Merge();

                        //Trucking log header format.
                        xml.ActiveWorksheet.Range(startRow, 5, startRow, 11).Merge().Style.Font.SetBold();

                        //Cash Advance header format.
                        xml.ActiveWorksheet.Range(startRow + logRowCount + 2, 5, startRow + logRowCount + 2, 11).Merge().Style.Font.SetBold();

                        //Trucking log column format
                        xml.ActiveWorksheet.Range(startRow + 1, 5, startRow + 1, 11).Style.Font.SetBold().Border.SetTopBorder(XLBorderStyleValues.Thin).Border.SetBottomBorder(XLBorderStyleValues.Thin);

                        //CA column format
                        xml.ActiveWorksheet.Range(startRow + 1 + logRowCount + 2, 5, startRow + 1 + logRowCount + 2, 11).Style.Font.SetBold().Border.SetTopBorder(XLBorderStyleValues.Thin).Border.SetBottomBorder(XLBorderStyleValues.Thin);

                        //Bold employee title:
                        xml.ActiveWorksheet.Cell(startRow + 2, 2).Style.Font.SetBold();

                        //Bold Rate values.
                        xml.ActiveWorksheet.Range(startRow + 4, 2, startRow + 4, 3).Style.Font.SetBold();

                        //Bold Deduction values
                        xml.ActiveWorksheet.Range(startRow + 13, 2, startRow + 13, 3).Style.Font.SetBold();

                        //Bold Gross Pay values
                        //xml.ActiveWorksheet.Range(startRow + 9, 2, startRow + 9, 3).Style.Font.SetBold();

                        //Bold Net pay with thick borders
                        xml.ActiveWorksheet.Range(startRow + 14, 2, startRow + 14, 3).Style.Font.SetBold().Border.SetOutsideBorder(XLBorderStyleValues.Thick);

                        //Bold trucking log totals
                        xml.ActiveWorksheet.Range(startRow + logRowCount, 5, startRow + logRowCount, 11).Style.Font.SetBold().Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                        //Bold CA logs totals
                        xml.ActiveWorksheet.Range(startRow + breakdown_len + 4, 5, startRow + breakdown_len + 4, 11).Style.Font.SetBold().Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                        //Right align rate
                        xml.ActiveWorksheet.Cell(startRow + 1, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        //Right align CA amount.
                        xml.ActiveWorksheet.Cell(startRow + logRowCount + 3, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        ///////////////////////////////////////////////////////////////
                    
                        //Move 13 rows down (minimum row count) + 1 for extra space + extra rows (if payslip extended to many trucking logs)
                        startRow += 17 + extraRows; 

                        //Update: 3.5.17
                        //Add generation of individual payslips for printing.
                        //Embedded resource: PayrollSystem.Reports.PayslipReport.rdlc
                        //using (Microsoft.Reporting.WinForms.LocalReport rpt = new LocalReport()) {

                            //strm.Position = 0;
                            //rpt.LoadReportDefinition(strm); //Load teh report definition from embedded resource.

                            string reportFilename = payslips_dir + "\\" + employee + ".pdf";

                            //Pass the required parameters (payslip info)
                            rpt.SetParameters(new ReportParameter("EmployeeName", employee));
                            rpt.SetParameters(new ReportParameter("PayPeriod", dateRange));
                            rpt.SetParameters(new ReportParameter("GrossIncome", rate.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("Tax", tax.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("CF", cf.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("CA", sa.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("Uniform", uniform.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("OfficeCA", ca.ToString("#,##0.00")));

                            rpt.SetParameters(new ReportParameter("SSS", sss.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("PhilHealth", ph.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("PagIbig", pi.ToString("#,##0.00")));

                            rpt.SetParameters(new ReportParameter("TotalDeductions", totalDeductions.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("NetPay", netPay.ToString("#,##0.00")));

                            //Totals
                            rpt.SetParameters(new ReportParameter("TruckingTotal", total_tRate.ToString("#,##0.00")));
                            rpt.SetParameters(new ReportParameter("CATotal", total_CA.ToString("#,##0.00")));
                   
                            //Generate the report bytes for writing to file.
                            byte[] b = rpt.Render("PDF");
                            //rpt.DataSources.Clear();

                            try {
                                using (FileStream fs = File.Create(reportFilename, 1024 * 64, FileOptions.WriteThrough)) {
                                    //write the rendered bytes into buffer.
                                    fs.Write(b, 0, b.Length);
                                    fs.Flush();
                                    fs.Close();
                                }
                            } catch (Exception ex) {
                                Notifier.ShowError("The report \"" + reportFilename + "\" cannot be created. The file may already exists or the file is locked.");
                                SystemLogger.WriteLog(ex.Message);
                            }
                        //}
                         
                    }

                    //strm.Close();   //closes the report stream.

                    // clean reports data tables and report object.
                    rpt.Dispose();
                    rpt.ReleaseSandboxAppDomain();

                    dt_calogs.Dispose();
                    dt_trucklogs.Dispose();

                    //autofit employee name column.
                    xml.ActiveWorksheet.Column("C").AdjustToContents();

                //Save File
                if (xml.Save(filename)) {
                    dlg.Close();
                    Notifier.ShowInformation("Successfully saved payslip as Excel spreadsheet file.", "Create Payslip");
                    dlg.Dispose();
                } else {
                    dlg.Close();
                    Notifier.ShowError("Failed to create payslip.\n\nThe filename you're trying to use may be inaccessible or file is open in another application.");
                }
            }
        }

        #endregion

        #region Compute Status Functions

        //Function to give user hints about the compute state of the system.
        void NotifyComputeState() {
            if (IsComputed == false) {
                PayrollInfoPanel.BackColor = ColorTranslator.FromHtml("#ffe26f");
                PayrollInfoPanelBorder.BackColor = ColorTranslator.FromHtml("#d1b95b");
                TooltipStatus.SetToolTip(lblPayrollTitle, "Payroll is not yet computed. Navigate to 'Payroll' or 'Summary' to compute it.");
            } else {
                PayrollInfoPanel.BackColor = ColorTranslator.FromHtml("#a0dec2");
                PayrollInfoPanelBorder.BackColor = ColorTranslator.FromHtml("#83b69f");
                TooltipStatus.SetToolTip(lblPayrollTitle, "Payroll has been computed.");
            }
        }

        //Sets the IsComputed state of the payroll in the database.
        void SetComputeState(bool value) {
            //SQL Parameter.
            String query = "UPDATE Payroll SET IsComputed = @val WHERE PID = @id";
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@val", value);
            p[1] = new OleDbParameter("@id", "{" + PayrollID + "}");

            bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
            if(ok == false)
                System.Diagnostics.Trace.WriteLine("Error updating payroll IsComputed flag.");
        }

        //Gets the IsComputed State of the Payroll from the DB.
        bool GetComputeState() {
            //SQL Parameter.
            String query = "SELECT IsComputed FROM Payroll WHERE PID = @id";
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + PayrollID + "}");

            bool res = false;
            using (DataTable dd = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p)) {
                if (dd.Rows.Count > 0) {
                    object c = dd.Rows[0]["IsComputed"];
                    res = (c != null) ? Convert.ToBoolean(c) : false;
                }
            }
            return res;
        }

        #endregion

        #region Tab Control Functions

        //Tab buttons eventhandler.
        private void TabButtons_Click(object sender, EventArgs e) {
            if (((ToolStripButton)sender).Checked == false) {
                foreach (ToolStripButton b in TabStripMain.Items) {
                    b.Checked = false;
                }
                ((ToolStripButton)sender).Checked = true;
                RefreshRecords();
            }
        }

        #endregion

        #region Cash Report and Employees CRUD

        //Cash Cash Report reocrds for IEnumerable LEFT JOIN
        IEnumerable<DataRow> QueryCashReportItems() {
            //Query string
            String query = "SELECT A.ItemDate, A.PlateNo, A.Driver, A.DriverCA, A.Helper, A.HelperCA, A.Particular ";
            query += "FROM (CashReportItems AS A INNER JOIN CashReportSections AS B ON A.SECID = B.SECID) ";
            query += "INNER JOIN PayrollCashReportItem AS C ON B.CRID = C.CRITEMID ";
            query += "WHERE C.PAYROLLID = @pid";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");

            //Execute SQL
            DataTable res = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            return res.Rows.Cast<DataRow>();
        }

        //Cache employee records for IEnumerable LEFT JOIN
        void QueryEmployees() {
            String query = "SELECT EmployeeID, FirstName, LastName FROM Employee";
            if (EmployeesTable != null) EmployeesTable.Dispose();
            EmployeesTable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false);
        }

        #endregion

        #region Payroll Computation and CRUD

        //Computes the new NetPay, then updates the record on the database.
        void UpdateSummaryNetPay(int currentRow) {
            if (tbSummary.Checked && RecordsDataGrid.Rows.Count > 0) {
                //Recompute net pay.
                //Net Pay = Gross Pay - CA
                double gross = Convert.ToDouble(RecordsDataGrid["GrossPay", currentRow].Value);
                double sa = Convert.ToDouble(RecordsDataGrid["CA", currentRow].Value);
                //gross -= sa;

                double newCA = 0.00;
                if (Double.TryParse(RecordsDataGrid["Office CA", currentRow].Value.ToString(), out newCA) == false) {
                    RecordsDataGrid["Office CA", currentRow].Value = 0.00;
                }
                //double net = gross - newCA;
                //RecordsDataGrid["NetPay", currentRow].Value = net;

                double newUnif = 0.00;
                if (Double.TryParse(RecordsDataGrid["Uniform", currentRow].Value.ToString(), out newUnif) == false) {
                    RecordsDataGrid["Uniform", currentRow].Value = 0.00;
                }

                //Compute benefits
                double sss = 0.00, pagibig = 0.00, philhealth = 0.00;
                if (!Double.TryParse(RecordsDataGrid["SSS", currentRow].Value.ToString(), out sss)) {
                    RecordsDataGrid["SSS", currentRow].Value = 0.00;
                }
                if (!Double.TryParse(RecordsDataGrid["PhilHealth", currentRow].Value.ToString(), out philhealth)) {
                    RecordsDataGrid["PhilHealth", currentRow].Value = 0.00;
                }
                if (!Double.TryParse(RecordsDataGrid["PagIBIG", currentRow].Value.ToString(), out pagibig)) {
                    RecordsDataGrid["PagIBIG", currentRow].Value = 0.00;
                }

                //Compute net pay
                double net = gross - (newCA + newUnif + sss + pagibig + philhealth);
                RecordsDataGrid["NetPay", currentRow].Value = net;

                //Update values on database.
                String query = "UPDATE PayrollSummary SET ";
                query += "CA = @ca, NetPay = @net, SA = @sa, Uniform = @uni, SSS = @sss, PhilHealth = @ph, PagIBIG = @pi ";
                query += "WHERE ID = @id";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[8];
                p[0] = new OleDbParameter("@ca", newCA);
                p[1] = new OleDbParameter("@net", net);
                //10.18.16 Added SA and Uniform
                p[2] = new OleDbParameter("@sa", sa);
                p[3] = new OleDbParameter("@uni", newUnif);
                //2.27.16 Added SSS, PhilHealth, Pag-IBIG
                p[4] = new OleDbParameter("@ss", sss);
                p[5] = new OleDbParameter("@ph", philhealth);
                p[6] = new OleDbParameter("@pi", pagibig);

                p[7] = new OleDbParameter("@id", "{" + RecordsDataGrid["ID", currentRow].Value.ToString() + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok == false)
                    System.Diagnostics.Trace.WriteLine("Failed to update summary CA.");
            }
        }

        //Clears payroll entries and summaries of current Payroll.
        void ClearRecords() {    

            //Clear Payroll Entry.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
            bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("DELETE FROM PayrollEntry WHERE PID = @pid", false, p);

            //Clear Payroll summary.
            p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
            bool oknaok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("DELETE FROM PayrollSummary WHERE PID = @pid", false, p);
            if (ok == false)
                System.Diagnostics.Trace.WriteLine("Failed to clear payroll entries.");
            if (oknaok == false)
                System.Diagnostics.Trace.WriteLine("Failed to clear payroll summary entries.");
        }

        //Create a new PayrollTable (For Payroll DataGridView DataSource)
        void InitializePayrollTable() {

            /*  Columns involved
             *  *ID                   - GUID
             *  *PID                  - Payroll ID
             *  EmployeeID           - ID of Employee
             *  Employee             - Name of Employee (For viewing purposes only)
             *  Position             - position of Employee
             *  PlateNo              - Yeah
             *  Rate                 - Based from employee's Helper + Driver rates
             *  2% Tax               - 2% of Rate
             *  5% CF                - 5% of Rate
             *  SA                   - Get from CA from Cash Reports
             *  Total Deductions     - Sum of all deductions
             *  Gross Pay            - Rate - Total Deductions.
             *  
             *  Note: items with asterisks (*) are needed only when inserting record to database
            */

            if (PayrollTable != null) PayrollTable.Dispose();
            PayrollTable = new DataTable();

            //Insert the specified columns to the payroll table.
            PayrollTable.Columns.Add(new DataColumn("EmployeeID", typeof(String)));
            PayrollTable.Columns.Add(new DataColumn("Employee", typeof(String)));
            PayrollTable.Columns.Add(new DataColumn("Position", typeof(String)));
            PayrollTable.Columns.Add(new DataColumn("Plate Number", typeof(String)));
            PayrollTable.Columns.Add(new DataColumn("Rate", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("2% Tax", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("5% CF", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("SA", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("Total Deductions", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("Gross Pay", typeof(Double)));
            PayrollTable.Columns.Add(new DataColumn("DRNO", typeof(String)));
        }

        //Loads the summary info into the datagrid viewer.
        void DisplaySummary() {
            //Retrieve payroll records from database.
            String query = "SELECT A.ID, A.PID, (B.LastName + ', ' + B.FirstName) AS [Employee], A.GrossPay, A.SA AS [CA], A.Uniform, A.CA AS [Office CA], A.SSS, A.PhilHealth, A.PagIBIG , A.NetPay FROM PayrollSummary AS A INNER JOIN Employee AS B ON A.Employee = B.EmployeeID WHERE A.PID = @id ORDER BY (B.LastName + ', ' + B.FirstName)";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + PayrollID + "}");

            if (SummaryTable != null) SummaryTable.Dispose();
            SummaryTable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

            //Set DataGridView DataSource
            RecordsDataGrid.Columns.Clear();
            RecordsDataGrid.DataSource = SummaryTable;
            if (PayrollFinal == false) {
                RecordsDataGrid.ReadOnly = false;
                btnUpdateUniform.Visible = true;     //Show toolbar edit CA button.
            }

            //Make all columns read-only except for CA field.
            foreach (DataGridViewColumn col in RecordsDataGrid.Columns) {
                if (!col.Name.Equals("PhilHealth", StringComparison.CurrentCultureIgnoreCase) && 
                    !col.Name.Equals("SSS", StringComparison.CurrentCultureIgnoreCase) && 
                    col.Name.Equals("Office CA", StringComparison.CurrentCultureIgnoreCase) == false && 
                    col.Name.Equals("Uniform", StringComparison.CurrentCultureIgnoreCase) == false &&
                    !col.Name.Equals("PagIBIG", StringComparison.CurrentCultureIgnoreCase))
                    col.ReadOnly = true;
            }

            //Hide unnecessarycolumns.
            RecordsDataGrid.Columns["ID"].Visible = false;
            RecordsDataGrid.Columns["PID"].Visible = false;

            //Rename some columns.
            RecordsDataGrid.Columns["GrossPay"].HeaderText = "Gross Pay";
            RecordsDataGrid.Columns["NetPay"].HeaderText = "Net Pay";
            RecordsDataGrid.Columns["PagIBIG"].HeaderText = "Pag-IBIG";

            //Format value columsn.
            RecordsDataGrid.Columns["SSS"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["PhilHealth"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["PagIBIG"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["GrossPay"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["CA"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Uniform"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["Office CA"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["NetPay"].DefaultCellStyle.Format = "#,##0.00";

            ShowTotalRecordsCount();    //Show total records
            AutosizeColumns();          //Fit columns to contents.

            //Set compute state to green.
            if (IsComputed == false) {
                IsComputed = true;              //Set computed property to true.
                SetComputeState(IsComputed);    //Write compute state to database.
            }
        }

        //Display payroll on the datagridview.
        void DisplayPayroll() {
            if (IsComputed) {
                //Retrieve payroll records from database.
                String query = "SELECT A.ID, A.PID, (B.LastName + ', ' + B.FirstName) AS [Employee], A.[Position], A.PlateNo, A.Rate, A.Tax, A.CF, A.SA AS [CA], A.TotalDeductions, A.GrossPay FROM PayrollEntry AS A INNER JOIN Employee AS B ON A.Employee = B.EmployeeID WHERE A.PID = @id AND A.[Position] LIKE '%' & @pos & '%' ";

                //Get employee pos
                String pos = "";
                if (cboType.SelectedIndex == 1) pos = "Driver";
                else if (cboType.SelectedIndex == 2) pos = "Helper";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[2];
                p[0] = new OleDbParameter("@id", "{" + PayrollID + "}");
                p[1] = new OleDbParameter("@pos", pos);

                if (PayrollTable != null) PayrollTable.Dispose();
                PayrollTable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

                using (DataView dv = PayrollTable.AsDataView()) {
                    dv.Sort = "PlateNo, Employee";
                    PayrollTable = dv.ToTable();
                }

                //Set DataGridView DataSource
                RecordsDataGrid.Columns.Clear();
                RecordsDataGrid.DataSource = PayrollTable;

                //Hide unnecessarycolumns.
                RecordsDataGrid.Columns["ID"].Visible = false;
                RecordsDataGrid.Columns["PID"].Visible = false;

                //Rename some headers
                RecordsDataGrid.Columns["Tax"].HeaderText = "2% Tax";
                RecordsDataGrid.Columns["CF"].HeaderText = "5% CF";
                RecordsDataGrid.Columns["TotalDeductions"].HeaderText = "Total Deductions";
                RecordsDataGrid.Columns["GrossPay"].HeaderText = "Gross Pay";

                //Format value columsn.
                RecordsDataGrid.Columns["Rate"].DefaultCellStyle.Format = "#,##0.00";
                RecordsDataGrid.Columns["Tax"].DefaultCellStyle.Format = "#,##0.00";
                RecordsDataGrid.Columns["CF"].DefaultCellStyle.Format = "#,##0.00";
                RecordsDataGrid.Columns["CA"].DefaultCellStyle.Format = "#,##0.00";
                RecordsDataGrid.Columns["TotalDeductions"].DefaultCellStyle.Format = "#,##0.00";
                RecordsDataGrid.Columns["GrossPay"].DefaultCellStyle.Format = "#,##0.00";

                //Sort by Plate No, Name.
                //RecordsDataGrid.Sort(RecordsDataGrid.Columns["PlateNo"], System.ComponentModel.ListSortDirection.Ascending);

                ShowTotalRecordsCount();    //Show total records
                AutosizeColumns();          //Fit columns to contents.

                //Set compute state to green.
                if (IsComputed == false) {
                    IsComputed = true;              //Set computed property to true.
                    SetComputeState(IsComputed);    //Write compute state to database.
                }
            }
        }
        
        //Insert computed values into database.
        void InsertIntoPayroll() {
            ClearRecords(); //Clear all entries related to the current payroll.

            IEnumerable<PayrollItem> activeGroup;
            if (cboType.SelectedIndex == 0) {   //Display All
                activeGroup = AllPList;
            } else if (cboType.SelectedIndex == 1) {    //Drivers only
                activeGroup = DriversPList;
            } else if (cboType.SelectedIndex == 2) {    //Helpers only
                activeGroup = HelpersPList;
            } else
                activeGroup = null;

            //Enumerate all items and create PayrollItem object.
            if (activeGroup == null) return;

            //PAYROLL ENTRIES INSERT
            //Iterate through all items.
            foreach (PayrollItem itm in activeGroup) {
                //INSERT QUERY
                String query = "INSERT INTO PayrollEntry([PID], [Employee], [Position], [PlateNo], [Rate], [Tax], [CF], [SA], [TotalDeductions], [GrossPay]) ";
                query += "VALUES(@pid, @employee, @position, @plateno, @rate, @tax, @cf, @sa, @deductions, @gross) ";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[10];
                p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
                p[1] = new OleDbParameter("@employee", "{" + itm.EmployeeID + "}");
                p[2] = new OleDbParameter("@position", itm.Position);
                p[3] = new OleDbParameter("@plateno", itm.PlateNo);
                p[4] = new OleDbParameter("@rate", itm.Rate);
                p[5] = new OleDbParameter("@tax", itm.Tax);
                p[6] = new OleDbParameter("@cf", itm.CF);
                p[7] = new OleDbParameter("@sa", itm.SA);
                p[8] = new OleDbParameter("@deductions", itm.TotalDeductions);
                p[9] = new OleDbParameter("@gross", itm.GrossPay);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok == false)
                    System.Diagnostics.Trace.WriteLine("Failed to insert payroll record.");
            }

            //SUMMARY INSERT  
            foreach (PayrollSummaryItem itm in SummaryAll) {
                //INSERT QUERY
                String query = "INSERT INTO PayrollSummary([PID],[Employee],[GrossPay],[CA],[NetPay], [SA], [Uniform]) ";
                query += "VALUES(@pid, @employee, @gross, @ca, @net, @sa, @uni) ";

                //SQL Parameter.
                OleDbParameter[] p = new OleDbParameter[7];
                p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
                p[1] = new OleDbParameter("@employee", "{" + itm.EmployeeID + "}");
                p[2] = new OleDbParameter("@gross", itm.GrossPay);
                p[3] = new OleDbParameter("@ca", itm.CA);
                p[4] = new OleDbParameter("@net", itm.NetPay);
                p[5] = new OleDbParameter("@net", itm.SA);
                p[6] = new OleDbParameter("@net", itm.Uniform);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok == false)
                    System.Diagnostics.Trace.WriteLine("Failed to insert payroll summary.");
            }

        }

        //Compute payroll for ALL employee types and cache it (faster loading).
        void PComputeAll(IEnumerable<DataRow> eCR, IEnumerable<DataRow> eTL) {
            //Compute payroll for all types of employees.
            //Helper and Driver rates for the same employee are combined.

            /* 
             *   ID, PAYROLLID, TransactionID, TransportDate, PlateNo, 
             *   Destination, B.Rate, B.DRNO, B.Driver AS [DriverID], 
             *   (C.LastName + ', ' + C.FirstName) AS [Driver], B.DriverRate, B.Helper AS [HelperID] , 
             *   (D.LastName + ', ' + D.FirstName) AS [Helper], B.HelperRate, B.IsBackLoad

             */
            /*  Columns involved
              *  *ID                   - GUID
              *  *PID                  - Payroll ID
              *  EmployeeID           - ID of Employee
              *  Employee             - Name of Employee (For viewing purposes only)
              *  Position             - position of Employee
              *  PlateNo              - Yeah
              *  Rate                 - Based from employee's Helper + Driver rates
              *  2% Tax               - 2% of Rate
              *  5% CF                - 5% of Rate
              *  SA                   - Get from CA from Cash Reports
              *  Total Deductions     - Sum of all deductions
              *  Gross Pay            - Rate - Total Deductions.
              *  
              *  Note: items with asterisks (*) are needed only when inserting record to database
             */

            //Compute first trucking log (Note: SA will be computed on Cash Report part)

            //Employees Name IEnumerable
            IEnumerable<DataRow> EmployeesList = EmployeesTable.Rows.Cast<DataRow>();   //Cast employees list to IEnumerable for LINQ

            //Temporary List IEnumerables
            IEnumerable<PayrollGroupingItem> AllList;
            IEnumerable<PayrollGroupingItem> DriversList, HelpersList;

            //SUPER DUPER HIGHSCHOOL LEVEL LINQ ON DriversList
            //A.DriverID, (B.LastName + ', ' + B.FirstName) AS DriverName, A.PlateNo, SUM(A.DriverRate)
            //FROM DriversList AS A LEFT JOIN EmployeesList AS B ON A.DriverID = B.EmployeeID
            //GROUP BY PlateNo, EmployeeID
            //ORDER BY PlateNo, DriverName
            DriversList = eTL.Select(x => new PayrollGroupingItem(x["PlateNo"].ToString(), x["DriverID"].ToString(), "Driver", Convert.ToDouble(x["DriverRate"].ToString())));
            DriversList = DriversList.GroupBy(x => new { x.PlateNo, x.EmployeeID }).Select(x => new PayrollGroupingItem(x.Key.PlateNo, x.Key.EmployeeID, "Driver", x.Sum(y => y.Rate)));
            DriversList = DriversList.GroupJoin(EmployeesList, x => x.EmployeeID, x => x["EmployeeID"].ToString(), (x, y) => new PayrollGroupingItem(x.PlateNo, x.EmployeeID, y.Select(z => z["LastName"].ToString()).FirstOrDefault() + ", " + y.Select(z => z["FirstName"].ToString()).FirstOrDefault(), "Driver", x.Rate));
            //DriversList = DriversList.OrderBy(x => new { x.PlateNo, x.EmployeeName });

            //SUPER DUPER HIGHSCHOOL LEVEL LINQ ON HelpersList
            //A.HelperID, (B.LastName + ', ' + B.FirstName) AS HelperName, A.PlateNo, SUM(A.HelperRate)
            //FROM HelpersList AS A LEFT JOIN EmployeesList AS B ON A.HelperID = B.EmployeeID
            //GROUP BY PlateNo, EmployeeID
            //ORDER BY PlateNo, HelperName
            HelpersList = eTL.Select(x => new PayrollGroupingItem(x["PlateNo"].ToString(), x["HelperID"].ToString(), "Helper", Convert.ToDouble(x["HelperRate"].ToString())));
            HelpersList = HelpersList.GroupBy(x => new { x.PlateNo, x.EmployeeID }).Select(x => new PayrollGroupingItem(x.Key.PlateNo, x.Key.EmployeeID, "Helper", x.Sum(y => y.Rate)));
            HelpersList = HelpersList.GroupJoin(EmployeesList, x => x.EmployeeID, x => x["EmployeeID"].ToString(), (x, y) => new PayrollGroupingItem(x.PlateNo, x.EmployeeID, y.Select(z => z["LastName"].ToString()).FirstOrDefault() + ", " + y.Select(z => z["FirstName"].ToString()).FirstOrDefault(), "Helper", x.Rate));
            //HelpersList = HelpersList.OrderBy(x => new { x.PlateNo, x.EmployeeName });

            //Compute next the Employees' CAs on Included Cash Reports.
            IEnumerable<DataRow> CRItemsList = QueryCashReportItems();  //Query the Cash Report Items.

            //Driver CAs IEnumerable.
            DriverCAs = CRItemsList.Select(x => new PayrollGroupingItem(x["PlateNo"].ToString(), x["Driver"].ToString(), "Driver", Convert.ToDouble(x["DriverCA"].ToString())));
            DriverCAs = DriverCAs.GroupBy(x => new { x.PlateNo, x.EmployeeID }).Select(x => new PayrollGroupingItem(x.Key.PlateNo, x.Key.EmployeeID, "Driver", x.Sum(y => y.Rate)));
            DriverCAs = DriverCAs.GroupJoin(EmployeesList, x => x.EmployeeID, x => x["EmployeeID"].ToString(), (x, y) => new PayrollGroupingItem(x.PlateNo, x.EmployeeID, y.Select(z => z["LastName"].ToString()).FirstOrDefault() + ", " + y.Select(z => z["FirstName"].ToString()).FirstOrDefault(), "Driver", x.Rate));

            //LEFT JOIN (Include CAs on Existing entries)
            //DRIVERS LIST + DRIVER CAs PAYROLL ITEM IENUMERABLE
            DriversPList = DriversList.GroupJoin(DriverCAs, x => new { x.EmployeeID, x.PlateNo }, x => new { x.EmployeeID, x.PlateNo }, (x, y) => new PayrollItem(x.EmployeeID, x.EmployeeName, x.Position, x.PlateNo, x.Rate, y.Select(z => z.Rate).FirstOrDefault()));

            //RIGHT JOIN (Include CAs of non-existing entries (negative payroll)
            //'(DRIVERS LIST + DRIVER CAs PAYROLL ITEM IENUMERABLE)
            DriversPList = DriversPList.Union(DriverCAs.Except(DriversList, new PayrollItemComparator()).Select(x => new PayrollItem(x.EmployeeID, x.EmployeeName, x.Position, x.PlateNo, 0, x.Rate))).Where(x => x.Employee.Trim().Length > 1);

            //Helper CAs IEnumerable.
            HelperCAs = CRItemsList.Select(x => new PayrollGroupingItem(x["PlateNo"].ToString(), x["Helper"].ToString(), "Helper", Convert.ToDouble(x["HelperCA"].ToString())));
            HelperCAs = HelperCAs.GroupBy(x => new { x.PlateNo, x.EmployeeID }).Select(x => new PayrollGroupingItem(x.Key.PlateNo, x.Key.EmployeeID, "Helper", x.Sum(y => y.Rate)));
            HelperCAs = HelperCAs.GroupJoin(EmployeesList, x => x.EmployeeID, x => x["EmployeeID"].ToString(), (x, y) => new PayrollGroupingItem(x.PlateNo, x.EmployeeID, y.Select(z => z["LastName"].ToString()).FirstOrDefault() + ", " + y.Select(z => z["FirstName"].ToString()).FirstOrDefault(), "Helper", x.Rate));

            //LEFT JOIN (Include CAs on Existing entries)
            //HELPERS LIST + HELPER CAs PAYROLL ITEM IENUMERABLE
            HelpersPList = HelpersList.GroupJoin(HelperCAs, x => new { x.EmployeeID, x.PlateNo }, x => new { x.EmployeeID, x.PlateNo }, (x, y) => new PayrollItem(x.EmployeeID, x.EmployeeName, x.Position, x.PlateNo, x.Rate, y.Select(z => z.Rate).FirstOrDefault()));

            //RIGHT JOIN (Include CAs of non-existing entries (negative payroll)
            //'(HELPERS LIST + HELPER CAs PAYROLL ITEM IENUMERABLE)
            HelpersPList = HelpersPList.Union(HelperCAs.Except(HelpersList, new PayrollItemComparator()).Select(x => new PayrollItem(x.EmployeeID, x.EmployeeName, x.Position, x.PlateNo, 0, x.Rate))).Where(x => x.Employee.Trim().Length > 1);

            //UNION: IEnumerable combined list.
            AllList = DriversList.Union(HelpersList);
            AllPList = DriversPList.Union(HelpersPList);

            //SUMMARY: CONSOLIDATE ALL INTO SUMMARY.
            SummaryAll = AllPList.GroupBy(x => x.EmployeeID).Select(x => new PayrollSummaryItem(x.Key, x.FirstOrDefault().Employee, x.Sum(y => y.GrossPay), 0.00, x.Sum(y => y.SA), 0.00));

            /*
            foreach (PayrollGroupingItem i in DriverCAs) {
                System.Diagnostics.Trace.WriteLine(i.EmployeeName + " : " + i.PlateNo + " : " + i.Rate);
            }
            System.Diagnostics.Trace.WriteLine("-----");
            foreach (PayrollGroupingItem i in HelperCAs) {
                System.Diagnostics.Trace.WriteLine(i.EmployeeName + i.PlateNo + " : " + i.Rate);
            }
            System.Diagnostics.Trace.WriteLine("-------- [Summary] ---------");
            foreach (PayrollSummaryItem i in SummaryAll) {
                System.Diagnostics.Trace.WriteLine(i.EmployeeName + " : " +i.EmployeeID + " : " + i.GrossPay);
            }
            */

            //Set computed flag.
            IsComputed = true;
            SetComputeState(IsComputed);
        }

        //Function to compute payroll.
        void ComputePayroll() {
            //If payroll is not yet computed, compute it, then store to database.
            if (IsComputed == false) {
                //Query first the trucking logs and cash reports included.
                QueryPayrollCashReports();
                QueryPayrollTruckingLog();
                QueryEmployees();

                //Check if there are included records in the trucking log and cash report. If there aren't any
                //show warning, then return.
                if (IncludeTruckingLog.Rows.Count <= 0 && IncludeCashReport.Rows.Count <= 0) {
                    WarningPanel.BringToFront();
                    WarningPanel.Visible = true;
                    RecordsDataGrid.DataSource = null;
                    RecordsDataGrid.Columns.Clear();
                    return;
                }

                //Transform rows from DataTable to IEnumerable so we can perform LINQ
                IEnumerable<DataRow> eCR = IncludeCashReport.Rows.Cast<DataRow>();
                IEnumerable<DataRow> eTL = IncludeTruckingLog.Rows.Cast<DataRow>();

                PComputeAll(eCR, eTL);  //Compute Cash Report and Trucking Logs and perform lazy querying.
                InsertIntoPayroll();    //Insert computed values into database.
            }

            //Show recompute button.
            if (PayrollFinal == false) {
                btnRecompute.Visible = true;
            }

            if (tbPayroll.Checked)
                DisplayPayroll();       //Load payroll entries from database into datagridview.
            else if (tbSummary.Checked)
                DisplaySummary();       //Load payroll summary entries from database into datagridview.
        }
        
        #endregion

        #region Payroll Cash Report CRUD

        //Queries the cash reports related to this payroll.
        void QueryPayrollCashReports() {
            //Retrieve records from the database.
            String query = "SELECT A.ID, A.CRITEMID, B.Title, B.DateStart, B.DateEnd FROM PayrollCashReportItem AS A ";
            query += "INNER JOIN CashReport AS B ON A.CRITEMID = B.CRID ";
            query += "WHERE PAYROLLID = @pid";

            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");

            if (IncludeCashReport != null) IncludeCashReport.Dispose();
            IncludeCashReport = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
        }

        //If the "Cash Reports" tab is active, retrieve data.
        void RefreshPayrollCashReports() { 
            //Get all cash reports associated to this payroll.
            QueryPayrollCashReports();

            //Set DataSource of the datagridview.
            RecordsDataGrid.Columns.Clear();
            RecordsDataGrid.DataSource = IncludeCashReport;

            //Hide some column.
            HideCol("ID");
            HideCol("CRITEMID");

            AutosizeColumns();          //Fit columns to contents.
        }

        //Import cash reports to be included in the payroll.
        void ImportCashReport() {
            CashReportPickerDialog dlg = new CashReportPickerDialog();
            dlg.Multiselect = true;
            dlg.SetPeriodRange(PayrollStartDate, PayrollEndDate);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //Retrieve selected cash reports, then store in dictionary.
                Dictionary<String, String> crs = dlg.GetSelectedCashReports();

                //Insert query
                String query = "INSERT INTO PayrollCashReportItem(PAYROLLID, CRITEMID) ";
                query += "VALUES(@pid, @crid)";

                //Cache the selected cash reports.
                IEnumerable<String> existing = RecordsDataGrid.Rows.Cast<DataGridViewRow>().Select(x => x.Cells["CRITEMID"].Value.ToString().Trim());

                //Now, for each cash report selected, include it to this payroll.
                bool success = true;
                foreach (string k in crs.Keys) {
                    if (existing.Contains(k.Trim()) == false) {

                        //Prepare OleDb Parameter
                        OleDbParameter[] p = new OleDbParameter[2];
                        p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
                        p[1] = new OleDbParameter("@crid", "{" + k + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        success &= ok;
                    }
                }

                //Show notification when ok.
                if (success) {
                    Notifier.ShowInformation("Selected cash reports has been included in this payroll.", "Include Cash Reports");
                    RefreshRecords();
                    IsComputed = false;             //Flag for recomputation.
                    SetComputeState(IsComputed);    //Save compute state on database.
                } else
                    Notifier.ShowError("An error has occurred while including the selected cash reports.");
            }
        }

        //Exclude cash report from the payroll.
        void RemoveCashReport() {
            if (tbCashReports.Checked) {    //Make sure this only runs when Cash Report tab is active.
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    if (Notifier.ShowConfirm("Are you sure you want to exclude the selected cash report from this payroll?", "Exclude Cash Report") == System.Windows.Forms.DialogResult.Yes) {
                        //Command query.
                        String query = "DELETE FROM PayrollCashReportItem ";
                        query += "WHERE ID = @id";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", Cell("ID"));

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        if (ok) {
                            Notifier.ShowInformation("Successfully removed selected cash report.", "Exclude Cash Report");
                            RefreshRecords();
                            IsComputed = false;             //Flag for recomputation.
                            SetComputeState(IsComputed);    //Save compute state on database.
                        } else
                            Notifier.ShowError("Failed to exclude selected cash report.");
                    }
                }
            }
        }

        #endregion

        #region Payroll Trucking Log CRUD

        //Query the trucking logs for this payroll.
        void QueryPayrollTruckingLog() {
            //Retrieve records from the database.
            String query = "SELECT A.ID, A.PAYROLLID, A.TransactionID, B.TransportDate, B.PlateNo, (B.Destination + IIF(B.IsBackLoad = True, ' (Backload)', '')) AS [Destination], B.Rate, B.DRNO, B.Driver AS [DriverID], (C.LastName + ', ' + C.FirstName) AS [Driver], B.DriverRate, B.Helper AS [HelperID] , (D.LastName + ', ' + D.FirstName) AS [Helper], B.HelperRate, B.IsBackLoad FROM ((PayrollTransportLog AS A ";
            query += "INNER JOIN TransportLog AS B ON A.TransactionID = B.ID) ";
            query += "LEFT JOIN Employee AS C ON B.Driver = C.EmployeeID) ";
            query += "LEFT JOIN Employee AS D ON B.Helper = D.EmployeeID ";
            query += "WHERE PAYROLLID = @pid ";
            query += "ORDER BY B.PlateNo, B.TransportDate, B.Destination";

            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");

            if (IncludeTruckingLog != null) IncludeTruckingLog.Dispose();
            IncludeTruckingLog = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
        }

        //If "Trucking Log" tab is active, retrieve data.
        void RefreshPayrollTruckingLog() {
            //Query trucking logs for this payroll.
            QueryPayrollTruckingLog();

            //Set DataSource of the datagridview.
            RecordsDataGrid.Columns.Clear();
            RecordsDataGrid.DataSource = IncludeTruckingLog;

            //Hide some column.
            HideCol("ID");
            HideCol("PAYROLLID");
            HideCol("TransactionID");
            HideCol("IsBackload");
            HideCol("HelperID");
            HideCol("DriverID");

            //Rename some column header.
            SetColHeader("TransportDate", "Date");
            SetColHeader("PlateNo", "Plate Number");
            SetColHeader("DRNO", "DR No.");
            SetColHeader("DriverRate", "Driver Rate");
            SetColHeader("HelperRate", "Helper Rate");

            //Format some column.
            RecordsDataGrid.Columns["DriverRate"].DefaultCellStyle.Format = "#,##0.00";
            RecordsDataGrid.Columns["HelperRate"].DefaultCellStyle.Format = "#,##0.00";

            AutosizeColumns();          //Fit columns to contents.
        }

        //If "Trucking Log" tab, import trucking log dialog.
        void ImportTruckingLog() {
            if (tbTruckingLogs.Checked) {
                TruckingLogPickerDialog dlg = new TruckingLogPickerDialog();
                dlg.SetPeriodRange(PayrollStartDate, PayrollEndDate);
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    //Store selected trucking logs on a dictionary.
                    Dictionary<String, String> res = dlg.GetSelectedTruckingLogs();

                    //SQL Query insert
                    String query = "INSERT INTO PayrollTransportLog(PAYROLLID, TransactionID) ";
                    query += "VALUES(@pid, @tid)";

                    //Cache the already included trucking logs.
                    IEnumerable<String> existing = RecordsDataGrid.Rows.Cast<DataGridViewRow>().Select(x => x.Cells["TransactionID"].Value.ToString().Trim());

                    bool success = true;
                    foreach (string k in res.Keys) {
                        if (existing.Contains(k.Trim()) == false) {
                            //Add only the record if IT IS NOT YET INCLUDED.

                            //SQL Parameter.
                            OleDbParameter[] p = new OleDbParameter[2];
                            p[0] = new OleDbParameter("@pid", "{" + PayrollID + "}");
                            p[1] = new OleDbParameter("@tid", "{" + k.Trim() + "}");
                            bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                            success &= ok;
                        }
                    }

                    if (success) {
                        Notifier.ShowInformation("Trucking logs are now included in this payroll.", "Include Trucking Logs");
                        RefreshRecords();
                        IsComputed = false;             //Flag for recomputation.
                        SetComputeState(IsComputed);    //Save compute state on database.
                    } else
                        Notifier.ShowError("Failed to include the selected trucking logs. Please try again.");
                }
            }
        }

        //Remove the selected trucking log from the payroll.
        void ExcludeTruckingLog() {
            if (tbTruckingLogs.Checked) {    //Make sure this only runs when Cash Report tab is active.
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    if (Notifier.ShowConfirm("Are you sure you want to exclude the selected trucking log from this payroll?", "Remove Trucking Log") == System.Windows.Forms.DialogResult.Yes) {
                        //Command query.
                        String query = "DELETE FROM PayrollTransportLog ";
                        query += "WHERE ID = @id";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", Cell("ID"));

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        if (ok) {
                            Notifier.ShowInformation("Trucking log successfully removed from the payroll.", "Remove Trucking Log");
                            RefreshRecords();
                            IsComputed = false;             //Flag for recomputation.
                            SetComputeState(IsComputed);    //Save compute state on database.
                        } else
                            Notifier.ShowError("Failed to exclude selected trucking log.");
                    }
                }
            }
        }

        #endregion

        #region Window Event handlers

        //Handle datagrid selection event.
        private void RecordsDataGrid_SelectionChanged(object sender, EventArgs e) {
            //if summary is active, automatically select "CA" cell on selected row.
            if (tbSummary.Checked) {
                if (RecordsDataGrid.Rows.Count > 0 && RecordsDataGrid.SelectedRows.Count > 0) {
                    //RecordsDataGrid.CurrentCell = RecordsDataGrid.SelectedRows[0].Cells["Office CA"];
                }
            }
        }

        //Handle form load event.
        private void PayrollEditorPage_Load(object sender, EventArgs e) {
            RefreshRecords();               //Refresh when page loads.
            ShowTotalRecordsCount();        //Show total records
            IsComputed = GetComputeState(); //Gets the computed state from the database.
            SetComputeState(IsComputed);    //Save compute state on database.
        }

        //Compact toolbar when width < 500
        private void PayrollEditorPage_Resize(object sender, EventArgs e) {
            if (Width < 500) {
                foreach (ToolStripItem b in MainToolStrip.Items)
                    if(b is ToolStripButton) b.DisplayStyle = ToolStripItemDisplayStyle.Image;
            } else if (Width >= 500) {
                foreach (ToolStripItem b in MainToolStrip.Items)
                    if (b is ToolStripButton) b.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            }
        }

        #endregion

        #region Control Event handlers

        //Command to create payslip.
        private void btnCreatePayslip_Click(object sender, EventArgs e) {
            //Only execute if btnCreatePayslip is enabled and visible (safety to prevent accelerator key to trigger event on PerformClick() if command is unavailable).
            if (btnCreatePayslip.Enabled && btnCreatePayslip.Visible) {
                if (IsComputed) {   //Continue only if payroll was computed earlier.
                    using (SaveFileDialog dlg = new SaveFileDialog()) {
                        dlg.Filter = "Excel Spreadsheet (*.xlsx)|*.xlsx";
                        dlg.FileName = PayrollTitle + " - Payslip.xlsx";
                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                            //Create a directory for individual payslips.
                            string payslip_dir = Path.GetDirectoryName(dlg.FileName);
                            payslip_dir += "/" + Path.GetFileNameWithoutExtension(dlg.FileName) + " Payslips";
                            
                            try {
                                if (!Directory.Exists(payslip_dir)) Directory.CreateDirectory(payslip_dir);
                                CreatePayslip(dlg.FileName, payslip_dir);    //Create the Excel payslip spreadsheet.
                            } catch (Exception ex) {
                                SystemLogger.WriteLog(ex.Message);
                                Notifier.ShowError("It looks like you aren't allowed to save in the selected folder or the path is no longer accessible. Please select a valid location to save the payslips.");
                            }
                        }
                    }
                } else
                    Notifier.ShowWarning("Payroll must be computed first before you can generate payslips.", "Create Payslip");
            }
        }

        //Triggers force recompute in case of previous errors.
        private void btnRecompute_Click(object sender, EventArgs e) {
            if (tbPayroll.Checked || tbSummary.Checked) {
                if (IsComputed) {
                    if (Notifier.ShowConfirm("Payroll has already been computed. Recomputations will erase previously saved data. Are you sure you want to continue?", "Confirm Recompute") == System.Windows.Forms.DialogResult.No) {
                        return;
                    }
                }
                //Move to payroll tabs first.
                if(tbPayroll.Checked == false)
                    tbPayroll.PerformClick();
                IsComputed = false;
                RefreshRecords();
            }
        }

        //Handle the edit ending of the datagrid.
        private void RecordsDataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            //On summary tab.
            if (tbSummary.Checked) {
                UpdateSummaryNetPay(e.RowIndex);
            }
        }
  
        //Handle the data error event of the datagrid
        private void RecordsDataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e) {
            //On summary tab
            if (tbSummary.Checked) {
                //Show message on parse error.
                if ((e.Context & DataGridViewDataErrorContexts.Parsing) == DataGridViewDataErrorContexts.Parsing) {
                    Notifier.ShowError("The input is in an invalid format. Please enter numeric values only.");
                }
            }
        }

        //Handle update uniform button click (summary view only)
        //Update: Handle update for all additional deductions (Uniform, SA, SSS, PhilHealth, Pag-IBIG).
        private void btnUpdateUniform_Click(object sender, EventArgs e) {
            if (tbSummary.Checked) {
                if (RecordsDataGrid.SelectedRows.Count > 0) {
                    //Old: Focus on the cell to trigger edit mode.
                    //RecordsDataGrid.CurrentCell = RecordsDataGrid.SelectedRows[0].Cells["Uniform"];
                    //RecordsDataGrid.BeginEdit(true);
                    //New: Since a lot of fields has been added, update deductions via dialog.

                    //End datagrid editiing
                    if (RecordsDataGrid.EndEdit()) {

                        using (SummaryDeductionsDialog dlg = new SummaryDeductionsDialog()) {
                            //Current row index.
                            int ind = RecordsDataGrid.SelectedRows[0].Index;

                            //Retrieve the current values.
                            double _officeCA = 0.0, _uniform = 0.0, _sss = 0.0, _ph = 0.0, _pi = 0.0;
                            if (!Double.TryParse(RecordsDataGrid["Office CA", ind].Value.ToString(), out _officeCA)) _officeCA = 0.00;
                            if (!Double.TryParse(RecordsDataGrid["Uniform", ind].Value.ToString(), out _uniform)) _uniform = 0.00;
                            if (!Double.TryParse(RecordsDataGrid["SSS", ind].Value.ToString(), out _sss)) _sss = 0.00;
                            if (!Double.TryParse(RecordsDataGrid["PagIbig", ind].Value.ToString(), out _pi)) _pi = 0.00;
                            if (!Double.TryParse(RecordsDataGrid["PhilHealth", ind].Value.ToString(), out _ph)) _ph = 0.00;

                            dlg.txtOfficeCA.Text = _officeCA.ToString("#,##0.00");
                            dlg.txtUniform.Text = _uniform.ToString("#,##0.00");
                            dlg.txtSSS.Text = _sss.ToString("#,##0.00");
                            dlg.txtPI.Text = _pi.ToString("#,##0.00");
                            dlg.txtPH.Text = _ph.ToString("#,##0.00");

                            dlg.txtOfficeCA.SelectionStart = 0;

                            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                                //Retrieve the field values.
                                double officeCA, uniform, sss, philhealth, pagibig;
                                officeCA = Convert.ToDouble(dlg.txtOfficeCA.Text);
                                uniform = Convert.ToDouble(dlg.txtUniform.Text);
                                sss = Convert.ToDouble(dlg.txtSSS.Text);
                                philhealth = Convert.ToDouble(dlg.txtPH.Text);
                                pagibig = Convert.ToDouble(dlg.txtPI.Text);

                                //Insert the update into the fields
                                RecordsDataGrid["Office CA", ind].Value = officeCA;
                                RecordsDataGrid["Uniform", ind].Value = uniform;
                                RecordsDataGrid["SSS", ind].Value = sss;
                                RecordsDataGrid["PhilHealth", ind].Value = philhealth;
                                RecordsDataGrid["PagIbig", ind].Value = pagibig;

                                //Update row.
                                UpdateSummaryNetPay(ind);
                            }
                        }
                    }
                }
            }
        }

        //Handle keypress event on Records DataGridView
        private void RecordsDataGrid_KeyDown(object sender, KeyEventArgs e) {
            //TODO: Trigger item viewer on ENTER
            if (e.KeyCode == Keys.Enter) {
                btnViewCR.PerformClick();
            }
            if (e.KeyCode == Keys.F2) {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        //Handle double-click of records datagridview.
        private void RecordsDataGrid_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (RecordsDataGrid.HitTest(e.X, e.Y).Type == DataGridViewHitTestType.Cell) {
                btnViewCR.PerformClick(); //Trigger View Command on double-click5
            }
        }

        //Special: View/Review Cash Report on cash report tab only.
        private void btnViewCR_Click(object sender, EventArgs e) {
            if (btnViewCR.Visible && btnViewCR.Enabled && tbCashReports.Checked) {
                if (RecordsDataGrid.SelectedRows.Count <= 0) return;
                //Allow viewing of Cash Report on Cash Report Tab only.

                //Query Info about the Cash Report
                String query = "SELECT * FROM CashReport WHERE CRID = @id";
                OleDbParameter[] p = new OleDbParameter[1];
                p[0] = new OleDbParameter("@id", "{" + Cell("CRITEMID") + "}");

                DataTable crinfo = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

                //            String query = "SELECT A.ID, A.PAYROLLID, A.TransactionID, B.TransportDate, B.PlateNo, B.Destination, B.Rate, B.DRNO, B.Driver AS [DriverID], (C.LastName + ', ' + C.FirstName) AS [Driver], B.DriverRate, B.Helper AS [HelperID] , (D.LastName + ', ' + D.FirstName) AS [Helper], B.HelperRate, B.IsBackLoad FROM ((PayrollTransportLog AS A ";

                //Set working values.
                if (crinfo != null) {
                    if (crinfo.Rows.Count > 0) {
                        CashReportEditorPage pg = new CashReportEditorPage();
                        MainWindow wnd = (MainWindow)ParentForm;
                        pg.WorkingCashReportID = crinfo.Rows[0]["CRID"].ToString();
                        pg.CashReportTitle = crinfo.Rows[0]["Title"].ToString();
                        pg.CashReportStartDate = DateTime.Parse(crinfo.Rows[0]["DateStart"].ToString()).ToString("MM/dd/yy");
                        pg.CashReportEndDate = DateTime.Parse(crinfo.Rows[0]["DateEnd"].ToString()).ToString("MM/dd/yy");
                        pg.CashReportLocked = Convert.ToBoolean(crinfo.Rows[0]["IsFinal"].ToString());
                        Visible = Enabled = false;
                        if (wnd != null) {
                            pg.Text = crinfo.Rows[0]["Title"].ToString();
                            pg.CashReportPrevInstance = this;
                            wnd.LoadPageNext(pg);
                        }
                    } else {
                        Notifier.ShowError("Failed to load the selected Cash Report.");
                    }
                }
                crinfo.Dispose();
            }
        }

        //Change displayed records if payroll tab is active
        private void cboType_SelectedIndexChanged(object sender, EventArgs e) {
            if (tbPayroll.Checked) DisplayPayroll();
        }

        //Handle event when add button is clicked.
        private void btnAdd_Click(object sender, EventArgs e) {
            if (IsComputed) {
                if(Notifier.ShowConfirm("Payroll has already been computed. If you're going to include a new item to this payroll, previous computations and values will be erased or overwritten. Add new items anyway?", "Payroll Recomputation") == System.Windows.Forms.DialogResult.No){
                    return;
                }
            }
            if (tbCashReports.Checked) {
                ImportCashReport();     //If Cash Report tab is active, import cash reports.
            } else if (tbTruckingLogs.Checked) {
                ImportTruckingLog();    //Import trucking log.
            }
        }

        //Handle event when Exclude button is clicked.
        private void btnDelete_Click(object sender, EventArgs e) {
            if (IsComputed) {
                if (Notifier.ShowConfirm("Payroll has already been computed. If you're going to exclude items from this payroll, previous computations and values will be erased or overwritten. Continue anyway?", "Payroll Recomputation") == System.Windows.Forms.DialogResult.No) {
                    return;
                }
            }
            if (tbCashReports.Checked) {
                RemoveCashReport(); //Remove cash report from the payroll
            } else if (tbTruckingLogs.Checked) {
                ExcludeTruckingLog();   //Remove trucking log from the payroll.
            }
        }

        //Check if what tab is active, then perform corresponding refresh.
        private void btnRefresh_Click(object sender, EventArgs e) {
            RefreshRecords();   //Refresh datagridview records.
        }

        //Handle back button click.
        private void btnBack_Click(object sender, EventArgs e) {
            if (PreviousPage != null) {
                if (ParentForm is MainWindow) {
                    ((MainWindow)ParentForm).LoadPage(PreviousPage);
                    ((PayrollPage)PreviousPage).SetSelectedRow(PreviousSelectedRow);
                    PreviousPage.Enabled = true;
                }
            }
        }

        //Show status of payroll on hover.
        private void lblPayrollTitle_MouseHover(object sender, EventArgs e) {
            if (IsComputed) {
                lblPayrollTitle.Text = "Payroll has been computed.";
            } else {
                lblPayrollTitle.Text = "Payroll has not yet been computed. Please view payroll or summary page to start the computation.";
            }
        }

        //Shows title of payroll on mouse leave.
        private void lblPayrollTitle_MouseLeave(object sender, EventArgs e) {
            lblPayrollTitle.Text = PayrollTitle;
            if (PayrollFinal)
                lblPayrollTitle.Text += " [Locked]";
        }


        #endregion

        #region Shortcut key event handler

        //Include button
        private void includeToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnAdd.Enabled && btnAdd.Visible) btnAdd.PerformClick();
        }

        //Exclude button
        private void excludeeToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnDelete.Enabled && btnDelete.Visible) btnDelete.PerformClick();
        }

        //Recompute button
        private void recomputeToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnRecompute.Enabled && btnRecompute.Visible) btnRecompute.PerformClick();
        }

        //Refresh
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnRefresh.Enabled && btnRefresh.Visible) btnRefresh.PerformClick();
        }

        //View CR
        private void viewCashReportToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnViewCR.Enabled && btnViewCR.Visible) btnViewCR.PerformClick();
        }

        //Edit Unifor
        private void editUniformToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnUpdateUniform.Enabled && btnUpdateUniform.Visible) btnUpdateUniform.PerformClick();
        }
                
        //Generate Payslip
        private void generatePayslipToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnCreatePayslip.Enabled && btnCreatePayslip.Visible) btnCreatePayslip.PerformClick();
        }

        #endregion
                
        #region Validation
        #endregion

    }

}