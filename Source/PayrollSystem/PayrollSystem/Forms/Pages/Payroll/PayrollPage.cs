﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using ClosedXML;
using ClosedXML.Excel;

namespace PayrollSystem {

    class PayrollPage : SystemPage {

        #region Constructor

        //Constructor prepare all stuff.
        public PayrollPage() {
            
            //Call form designer initializations.
            InitializeComponent();

            //Setup commands.
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.View | PageToolbarButtons.Edit| PageToolbarButtons.MarkFinal | PageToolbarButtons.Delete | PageToolbarButtons.Archive | PageToolbarButtons.Export | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.MonthYearDatePicker);

            //Disable mutiselect
            SetRecordsDataGridMultiselect(false);
        }

        //Form designer initializations
        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // dpStart
            // 
            this.dpStart.ValueChanged += new System.EventHandler(this.dpStart_ValueChanged);
            // 
            // PayrollPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.FinalRecordsColumnName = "IsFinal";
            this.Name = "PayrollPage";
            this.Text = "Payroll";
            this.OnAdd += new System.EventHandler(this.PayrollPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.PayrollPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.PayrollPage_OnDelete);
            this.OnArchive += new System.EventHandler(this.PayrollPage_OnArchive);
            this.OnExport += new System.EventHandler(this.PayrollPage_OnExport);
            this.OnSearch += new System.EventHandler(this.PayrollPage_OnSearch);
            this.OnFinalize += new System.EventHandler(this.PayrollPage_OnFinalize);
            this.OnRefresh += new System.EventHandler(this.PayrollPage_OnRefresh);
            this.OnRecordDoubleClicked += new System.EventHandler(this.PayrollPage_OnRecordDoubleClicked);
            this.OnRecordsSelectionChanged += new System.EventHandler(this.PayrollPage_OnRecordsSelectionChanged);
            this.Load += new System.EventHandler(this.PayrollPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Local Variables

        DataTable dt_payrolls = null;
        Image lockicon = Properties.Resources._lock;
        Image star_shaded = Properties.Resources.star_shaded;
        Image star_unshaded = Properties.Resources.star_unshaded;

        #endregion

        #region Public Functions

        //Manually set the selected record in the datagrid.
        public void SetSelectedRow(int index) {
            if(index > -1 && index < GetRecordsDataGridView().Rows.Count)
                GetRecordsDataGridView().Rows[index].Selected = true;
        }

        #endregion

        #region Local Functions

        //Loads all existing title from the payroll table.
        List<String> GetPayrollTitles() {
            List<String> outp = new List<String>();
            String query = "SELECT Title FROM Payroll";
            DataTable res = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false);
            foreach (DataRow r in res.Rows) {
                outp.Add(r["Title"].ToString().Trim());
            }
            return outp;
        }

        #endregion

        #region Exporting Excel Functions

        //Export necesssary payroll details into spreadsheet.
        void ExportPayrollInfo(string filename) {
            //What spreadsheets are needed:
            //Summary Payroll
            //Payroll
            //Cash Reports
            //Trucking Logs Included
            //Cash Reports & Expenses sheet.

            //Start workbook.
            using (XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled)) {

                //First, create Payroll Sheet.
                InsertPayrollSheet(wb);

                //Insert summary payroll sheet.
                InsertSummarySheet(wb);

                //Insert included trucking logs.
                InsertIncludedTruckingLogs(wb);

                //Insert Included Cash reports and breakdown.
                InsertIncludedCashReports(wb);

                //Try saving workbook.
                try {
                    //Save workbook.
                    wb.SaveAs(filename);
                    Notifier.ShowInformation("Payroll was successfully saved as Excel spreadsheet document.", "Export Payroll");
                } catch (Exception ex) {
                    Notifier.ShowError("Failed to export payroll as Excel spreadsheet document.\n\n" + ex.Message);
                    Trace.WriteLine("PayrollPage@ExportPayrollInfo(): " + ex.Message);
                }
            }

        }

        #endregion

        #region Worksheet Insert Functions

        //Function to create all included cash reports as worksheets (1 for cash report and another 1 for expense breakdown)
        void InsertIncludedCashReports(XLWorkbook wb) {
            //Query string.
            String query = "SELECT A.CRITEMID, B.Title FROM (PayrollCashReportItem AS A) INNER JOIN CashReport AS B ON A.CRITEMID = B.CRID ";
            query += "WHERE PAYROLLID = @id ";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

            //Datarow IEnumerable.
            IEnumerable<DataRow> crtable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p).Rows.Cast<DataRow>();

            //Enumerate all included cash reports.
            int crCTR = 0;
            foreach (DataRow crs in crtable) {
                String crid = "{" + crs["CRITEMID"].ToString() + "}";

                //Now do here insert spreadsheets for cash reports./////////////////////
                //Get the selected cash report's ID.

                //Retrieve all sections that belongs to the selected Cash Report
                query = "SELECT A.SECID, SS.EndingBalance AS [StartingBalance], SS.Title AS [StartingBalanceTitle], SSCR.DateEnd AS [StartingBalanceDate] ,A.Title, B.Particular, B.PlateNo, B.ItemDate, B.IsExpense, B.Amount, B.Fuel, B.Violations, B.Toll, B.TruckingExpenses, B.RMVehicle, B.ServiceFee, B.Others, (DRV.FirstName + ' ' + DRV.LastName) AS [Driver], B.DriverCA, (HLP.FirstName + ' ' + HLP.LastName) AS [Helper], B.HelperCA FROM ((((CashReportSections AS A INNER JOIN CashReportItems AS B ON A.SECID = B.SECID) LEFT JOIN Employee AS DRV ON B.Driver = DRV.EmployeeID) LEFT JOIN Employee AS HLP ON B.Helper = HLP.EmployeeID) LEFT JOIN CashReportSections AS SS ON A.StartingBalance = SS.SECID) LEFT JOIN CashReport AS SSCR ON SS.CRID = SSCR.CRID  ";
                query += "WHERE A.CRID = @id ";
                query += "ORDER BY B.ItemDate, B.PlateNo ";

                //SQL Parameter.
                OleDbParameter[] q = new OleDbParameter[1];
                q[0] = new OleDbParameter("@id", crid);

                DataTable dataT = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, q);

                //Create IEnumerable for LINQing
                IEnumerable<DataRow> dataR = dataT.Rows.Cast<DataRow>();

                //Group items by section.
                IEnumerable<IGrouping<String, DataRow>> sectionGroup = dataR.GroupBy(x => x["SECID"].ToString());

                //Prepare sheet here.
                try {
                    /////////////////WORKSHEET ONE: CASH FLOW STATEMENT///////////////////////////////////////////////////
                    IXLWorksheet ws = wb.Worksheets.Add("Cash Report (" + crCTR + ")");
                    //Define default styles here.
                    ws.Style.Font.SetFontName("Arial").Font.SetFontSize(8);
                    ws.SetShowGridLines(false).PageSetup.SetPagesWide(1); //Create new tab/worksheet.
                    ws.ColumnWidth = 11;
                    ws.RowHeight = 16;

                    //Set formats
                    ws.Style.DateFormat.SetFormat("MM/dd/yy");
                    ws.Style.NumberFormat.SetFormat("#,##0.00;(#,##0.00);-;@");

                    //Define column widths here.
                    ws.Columns("C").Width = 38;
                    ws.Columns("D,E").Width = 14;

                    ////REPORT HEADER////

                    //Header main
                    ws.Range("A1:E1").Merge().SetValue("CASH REPORT: " + crs["Title"]).Style
                        .Font.SetBold()
                        .Font.SetFontSize(10)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Bottom)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                    //Subheader
                    ws.Range("A2:E2").Merge().SetValue("Period Covered: " + DateTime.Parse(Cell("DateStart")).ToString("MMM dd") + " - " + DateTime.Parse(Cell("DateEnd")).ToString("MMM dd, yyyy")).Style
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Top)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                    
                    //Item columns
                    List<Object[]> icols = new List<Object[]>();
                    icols.Add(new Object[] {"DATE", "PLATE NO", "PARTICULAR", "DEBIT", "CREDIT" });

                    //Iterate through sections (keys).
                    int startRow = 3;
                    foreach (String key in sectionGroup.Select(x => x.Key)) {
                        //Section Title
                        string sectionTitle = sectionGroup.Where(x => x.Key == key).SelectMany(x => x).First()["Title"].ToString();

                        //Section header  
                        ws.Range(startRow, 1, startRow, 5).Merge().SetValue(sectionTitle).Style
                            .Font.SetBold()
                            .Fill.SetBackgroundColor(XLColor.Black)
                            .Font.SetFontColor(XLColor.White)
                            .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        startRow++;

                        //Prepare item collections here.
                        List<Object[]> income = new List<Object[]>();
                        List<Object[]> expenses = new List<Object[]>();

                        //Totals
                        double incomeTotal = 0.0, expenseTotal = 0.0, endingBalance = 0.0, startingBalance = 0.0;

                        //Iterate through each items.
                        foreach (DataRow row in sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {

                            //WHAT IS NEEDED:
                            //DATE, PLATE NO, PARTICULAR, AMOUNT
                            //Write in corresponding expense or income.
                            string date, plateno, particular;
                            double amount;

                            date = DateTime.Parse(row["ItemDate"].ToString()).ToString("MM/dd/yy");
                            plateno = row["PlateNo"].ToString();
                            particular = row["Particular"].ToString();
                            amount = Convert.ToDouble(row["Amount"].ToString());

                            if ((bool)row["IsExpense"] == true) {
                                //add to expense collection
                                List<Object> col_expenses = new List<Object>();
                                col_expenses.Add(date);
                                col_expenses.Add(plateno);
                                col_expenses.Add(particular);
                                if (amount >= 0) {
                                    col_expenses.Add(amount);
                                    col_expenses.Add("");           //Space to move to depit column
                                } else {
                                    col_expenses.Add("");           //Space to move to credit column
                                    col_expenses.Add(amount);
                                }
                                expenses.Add(col_expenses.ToArray());
                            } else {
                                //add to income collection
                                List<Object> col_income = new List<Object>();
                                col_income.Add(date);
                                col_income.Add(plateno);
                                col_income.Add(particular);
                                if (amount >= 0) {
                                    col_income.Add("");            //Space to move to credit column
                                    col_income.Add(amount);
                                } else {
                                    col_income.Add(amount);
                                    col_income.Add("");            //Space to move to debit column
                                }
                                income.Add(col_income.ToArray());
                            }

                        }

                        //Get starting balance.
                        startingBalance = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToDbl(y["StartingBalance"]))).First();

                        /////////// INCOME //////////////
                        if (income.Count > 0 || startingBalance != 0) {
                            //Insert rows of each Income
                            ws.Range(startRow, 1, startRow, 5).Merge().SetValue("ADD (INCOME)").Style
                                 .Font.SetBold()
                                 .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                                 .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            startRow++;

                            //Write item colummns header.
                            ws.Cell(startRow, 1).InsertData(icols);
                            ws.Range(startRow, 1, startRow, 5).Style.Font.SetBold();
                            ws.Range(startRow, 4, startRow, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right).Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            startRow++;
                            
                            //SPECIAL: IF STARTING BALANCE IS AVAILABLE, INSERT IT HERE
                            if (startingBalance != 0) {
                                string sb_title = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToStr(y["StartingBalanceTitle"]))).First();
                                string sb_date = DateTime.Parse(sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToStr(y["StartingBalanceDate"]))).First()).ToShortDateString();

                                if (incomeTotal >= 0) {
                                    ws.Cell(startRow, 5).SetValue(startingBalance);
                                } else {
                                    ws.Cell(startRow, 4).SetValue(startingBalance);
                                }

                                ws.Cell(startRow, 1).SetValue(sb_date);
                                ws.Cell(startRow, 3).SetValue(sb_title);

                                //italicize starting balance.
                                ws.Range(startRow, 1, startRow, 3).Style.Font.SetItalic();

                                startRow++;
                            }

                            //NORMAL ITEMS:
                            ws.Cell(startRow, 1).InsertData(income);
                            startRow += income.Count;

                            //Income totals.
                            incomeTotal = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Where(y => Convert.ToBoolean(y["IsExpense"]) == false).Select(y => Convert.ToDouble(y["Amount"]))).Sum();
                            incomeTotal += startingBalance;
                            if (incomeTotal >= 0)
                                ws.Cell(startRow, 5).SetValue(incomeTotal);
                            else
                                ws.Cell(startRow, 4).SetValue(incomeTotal);
                            ws.Cell(startRow, 1).SetValue("TOTAL");


                            //Border totals separator
                            ws.Range(startRow, 1, startRow, 5).Style
                                .Border.SetTopBorder(XLBorderStyleValues.Thin)
                                .Font.SetBold();
                                                       
                            startRow += 2;  //Add space after INCOME subsection.
                        }
                        /////////// INCOME //////////////

                        /////////// EXPENSE //////////////
                        if (expenses.Count > 0) {
                            //Insert rows of each Income
                            ws.Range(startRow, 1, startRow, 5).Merge().SetValue("LESS (EXPENSES)").Style
                              .Font.SetBold()
                              .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            startRow++;

                            //Write item colummns header.
                            ws.Cell(startRow, 1).InsertData(icols);
                            ws.Range(startRow, 1, startRow, 5).Style.Font.SetBold();
                            ws.Range(startRow, 4, startRow, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right).Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            startRow++;

                            //Write expenses
                            ws.Cell(startRow, 1).InsertData(expenses);
                            startRow += expenses.Count;

                            //Write total expenses
                            expenseTotal = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Where(y => Convert.ToBoolean(y["IsExpense"]) == true).Select(y => Convert.ToDouble(y["Amount"]))).Sum();
                            if (expenseTotal >= 0)
                                ws.Cell(startRow, 4).SetValue(expenseTotal);
                            else
                                ws.Cell(startRow, 5).SetValue(expenseTotal);
                            ws.Cell(startRow, 1).SetValue("TOTAL");

                            //Border line
                            ws.Range(startRow, 1, startRow, 5).Style
                                .Border.SetTopBorder(XLBorderStyleValues.Thin)
                                .Font.SetBold();

                            startRow++;
                        }
                        /////////// EXPENSE //////////////

                        /////// ENDING BALANCE ///////////
                        endingBalance = incomeTotal - expenseTotal;
                        if (endingBalance >= 0)
                            ws.Cell(startRow, 5).SetValue(Math.Abs(endingBalance));
                        else
                            ws.Cell(startRow, 4).SetValue(Math.Abs(endingBalance));
                        ws.Cell(startRow, 1).SetValue("ENDING BALANCE");
                        ws.Cell(startRow, 3).SetValue(sectionTitle);
                        ws.Range(startRow, 1, startRow, 5).Style.Font.SetBold();
                        startRow++;
                        /////// ENDING BALANCE ///////////

                        startRow += 2; //Add space for next section
                    }

                    /////////////////END OF WORKSHEET ONE: CASH FLOW STATEMENT/////////////////////////////////////////////

                    ///////////////////////////// WORKSHEET TWO: EXPENSE BREAKDOWN ///////////////////////////////////////

                    IXLWorksheet ws2 = wb.Worksheets.Add("Expenses Breakdown (" + crCTR + ")");
                    ws2.SetShowGridLines(false).PageSetup.SetPageOrientation(XLPageOrientation.Landscape).SetPagesWide(1); //Create new tab/worksheet.
                    ws2.Style.NumberFormat.SetFormat("#,##0.00;(#,##0.00);-;@");
                    ws2.Style.Font.SetFontName("Arial").Font.SetFontSize(8);

                    //Set defualt worksheet row and col height.
                    ws2.RowHeight = 16;
                    ws2.ColumnWidth = 12;

                    //Define column widths here.
                    ws2.Columns("N,O").Width = 23;
                    ws2.Columns("C").Width = 22;
                    ws2.Columns("A,B").Width = 10.5;
                    //wb.ActiveWorksheet.Columns("H").Width = 16;

                    ////REPORT HEADER////

                    //Header main
                    ws2.Range("A1:M1").Merge().SetValue("EXPENSES BREAKDOWN: " + crs["Title"]).Style
                        .Font.SetFontSize(10)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Bottom)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
                        .Font.SetBold();
                    
                    //Subheader 2
                    ws2.Range("A2:M2").Merge().SetValue("Period Covered: " + DateTime.Parse(Cell("DateStart")).ToString("MMM dd") + " - " + DateTime.Parse(Cell("DateEnd")).ToString("MMM dd, yyyy")).Style
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Top)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                    //Item columns
                    icols.Clear();
                    icols.Add(new Object[] {"DATE", "PLATE NO", "PARTICULAR", "AMOUNT",
                                "FUEL", "VIOLATIONS", "TOLL", "TRUCKING EXP", "RMV", "SERVICE FEE",
                                "OTHERS", "CA DRIVER", "CA HELPER", "DRIVER", "HELPER"});

                    //Group items by section.
                    sectionGroup = dataR.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).OrderBy(x => x["PlateNo"].ToString()).ThenBy(x => DateTime.Parse(x["ItemDate"].ToString())).GroupBy(x => x["PlateNo"].ToString());

                    //Grand totals.
                    double gtotalAmount = 0.0, gtotalFuel = 0.0, gtotalViolations = 0.0, gtotalToll = 0.0,
                               gtotalTExpenses = 0.0, gtotalRMV = 0.0, gtotalSF = 0.0, gtotalOthers = 0.0,
                               gtotalDriverCA = 0.0, gtotalHelperCA = 0.0;

                    //Iterate through plate nos. (keys)
                    startRow = 4;
                    foreach (String key in sectionGroup.Select(x => x.Key)) {

                        //Write item colummns header.
                        ws2.Cell(startRow, 1).InsertData(icols);
                        ws2.Range(startRow, 1, startRow, 15).Style.Font.SetBold().Border.SetBottomBorder(XLBorderStyleValues.Thin);
                        ws2.Range(startRow, 4, startRow, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right).Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);
                        startRow++;

                        //Totals
                        double totalAmount = 0.0, totalFuel = 0.0, totalViolations = 0.0, totalToll = 0.0,
                               totalTExpenses = 0.0, totalRMV = 0.0, totalSF = 0.0, totalOthers = 0.0,
                               totalDriverCA = 0.0, totalHelperCA = 0.0;

                        //Iterate through each items.
                        foreach (DataRow row in sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {
                            //WHAT IS NEEDED:
                            //DATE, PLATE NO, PARTICULAR, AMOUNT
                            //Write in corresponding expense or income.
                            string date, plateno, particular, driver, helper;
                            double amount, fuel, violations, toll, texpenses, rmv, sf, others, driverca, helperca;

                            date = DateTime.Parse(row["ItemDate"].ToString()).ToString("MM/dd/yy");
                            plateno = row["PlateNo"].ToString();
                            particular = row["Particular"].ToString();

                            driver = row["Driver"].ToString(); 
                            helper = row["Helper"].ToString();

                            totalAmount += amount = Convert.ToDouble(row["Amount"].ToString());
                            totalFuel += fuel = Convert.ToDouble(InputValidators.DBNullToDbl(row["Amount"]));
                            totalViolations += violations = Convert.ToDouble(InputValidators.DBNullToDbl(row["Violations"]));
                            totalToll += toll = Convert.ToDouble(InputValidators.DBNullToDbl(row["Toll"]));
                            totalTExpenses += texpenses = Convert.ToDouble(InputValidators.DBNullToDbl(row["TruckingExpenses"]));
                            totalRMV += rmv = Convert.ToDouble(InputValidators.DBNullToDbl(row["RMVehicle"]));
                            totalSF += sf = Convert.ToDouble(InputValidators.DBNullToDbl(row["ServiceFee"]));
                            totalOthers += others = Convert.ToDouble(InputValidators.DBNullToDbl(row["Others"]));
                            totalDriverCA += driverca = Convert.ToDouble(InputValidators.DBNullToDbl(row["DriverCA"]));
                            totalHelperCA += helperca = Convert.ToDouble(InputValidators.DBNullToDbl(row["HelperCA"]));
                            
                            ws2.Cell(startRow, 1).InsertData(new Object[] { new Object[] {date, plateno, particular, amount, fuel, violations, toll, texpenses, rmv, sf, others, driverca, helperca, driver, helper} });
    
                            startRow++;
                        }

                        //Add border separator.
                        ws2.Range(startRow, 1, startRow, 15).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                        //Insert totals for each section.
                        ws2.Cell(startRow, 4).InsertData(new Object[] { new Object[] { totalAmount, totalFuel, totalViolations, totalToll, totalTExpenses, totalRMV, totalSF, totalOthers, totalDriverCA, totalHelperCA } });
                        ws2.Range(startRow, 1, startRow, 16).Style.Font.SetBold();
                        startRow++;

                        //add to grand totals.
                        gtotalAmount += totalAmount;
                        gtotalFuel += totalFuel;
                        gtotalViolations += totalViolations;
                        gtotalToll += totalToll;
                        gtotalTExpenses += totalTExpenses;
                        gtotalRMV += totalRMV;
                        gtotalSF += totalSF;
                        gtotalOthers += totalOthers;
                        gtotalDriverCA += totalDriverCA;
                        gtotalHelperCA += totalHelperCA;

                        startRow += 2;  //Add some space for the next plate no category.
                    }

                    //Insert Grand totals
                    ws2.Cell(startRow, 1).InsertData(new Object[] { new Object[] { "TOTALS", "", "", gtotalAmount, gtotalFuel, gtotalViolations, gtotalToll, gtotalTExpenses, gtotalRMV, gtotalSF, gtotalOthers, gtotalDriverCA, gtotalHelperCA } });
                    ws2.Range(startRow, 1, startRow, 16).Style.Font.SetBold();

                    /////////////////////////// END WORKSHEET TWO: EXPENSE BREAKDOWN /////////////////////////////////////
                    ws2.Columns("A:O").AdjustToContents();   //Compact columns.

                    ///////////////////////////////////// SAVE WORKBOOK ///////////////////////////////////////////////////
                    dataT.Dispose();            //Dispose data table to save space.

                } catch (Exception ex) {
                    Trace.WriteLine(ex.Message);
                }

                crCTR++;
                ////////////////////////////////////////////////////////////////////////
            }

        }
        
        //Function to create payroll worksheet.
        void InsertPayrollSheet(XLWorkbook wb) {
            //Create worksheet.
            IXLWorksheet ws = wb.Worksheets.Add("Payroll");

            //Set style
            ws.ShowGridLines = false;
            ws.Style.Font.SetFontName("Arial").Font.SetFontSize(8);
            ws.PageSetup.PagesWide = 1;
            ws.Style.NumberFormat.SetFormat("#,##0.00;(#,##0.00);-;@");
            ws.Column("A").Style.NumberFormat.SetFormat("@");
            
            //Records collection.
            List<Object[]> insert_item = new List<Object[]>();

            //Insert header texts.
            insert_item.Add(new Object[] { "PAYROLL RECORDS" });
            insert_item.Add(new Object[] { "Period Covered: " + DateTime.Parse(Cell("DateStart")).ToString("MMM dd") + " - " + DateTime.Parse(Cell("DateEnd")).ToString("MMM dd, yyyy") });

            //Insert header details.
            ws.Cell(1, 1).InsertData(insert_item);

            //Format some header cells.
            ws.Range("A1:I1").Merge().Style.Font.SetBold().Font.SetFontSize(10).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left).Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);
            ws.Range("A2:I2").Merge().Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left).Alignment.SetVertical(XLAlignmentVerticalValues.Top);


            //Retrieve payroll records from database.
            String query = "SELECT A.ID, A.PID, (B.LastName + ', ' + B.FirstName) AS [Employee], A.[Position], A.PlateNo, A.Rate, A.Tax, A.CF, A.SA, A.TotalDeductions, A.GrossPay FROM PayrollEntry AS A INNER JOIN Employee AS B ON A.Employee = B.EmployeeID ";
            query += "WHERE A.PID = @id ";
            query += "ORDER BY A.PlateNo, (B.LastName + ', ' + B.FirstName)";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

            //IEnumerable of payroll entries.
            IEnumerable<DataRow> PayrollTable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p).Rows.Cast<DataRow>();

            //IEnumerable payroll by employee type
            IEnumerable<DataRow> DriverTable = PayrollTable.Where(x => x["Position"].ToString().Equals("Driver", StringComparison.CurrentCultureIgnoreCase));
            IEnumerable<DataRow> HelperTable = PayrollTable.Where(x => x["Position"].ToString().Equals("Helper", StringComparison.CurrentCultureIgnoreCase));

            //Start row:
            int startRow = 4; //1: heading, 2: date, 3: column headers.

            //Insert column header.
            insert_item.Clear();
            insert_item.Add(new Object[] { "DRIVER", "PLATE NO.", "GROSS", "TAX (2%)", "CF (5%)", "SA", "DEDUCTIONS", "NET"});
            ws.Cell(3, 2).InsertData(insert_item).Style.Font.SetBold();
            ws.Range(3, 4, 3, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            //Grand totals
            double tRate = 0.0, tTax = 0.0, tCF = 0.0, tSA = 0.0, tTotalDeductions = 0.0, tGross = 0.0;

            //Iterate through all DRIVER payroll entries.
            insert_item.Clear();        //We'll use this to insert the records into the worksheet.
            int counter = 1;
            foreach (DataRow row in DriverTable) {
                //Get values.
                string employee = row["Employee"].ToString();
                string plateNo = row["PlateNo"].ToString();
                double rate = Convert.ToDouble(row["Rate"]);
                double tax = Convert.ToDouble(row["Tax"]);
                double cf = Convert.ToDouble(row["CF"]);
                double sa = Convert.ToDouble(row["SA"]);
                double totalDeductions = Convert.ToDouble(row["TotalDeductions"]);
                double gross = Convert.ToDouble(row["GrossPay"]);

                //add to grand totals
                tRate += rate; tTax += tax; tCF += cf; tSA += sa; tTotalDeductions += totalDeductions; tGross += gross;

                //Insert items to array.
                insert_item.Add(new Object[] { counter, employee, plateNo, rate, tax, cf, sa, totalDeductions, gross });
                counter++;

                //Trace. (uncomment for debug)
                //Trace.WriteLine(row["Employee"] + " : " + row["Rate"]);
            }

            //Add grand totals.
            insert_item.Add(new Object[] { "", "TOTALS", "", tRate, tTax, tCF, tSA, tTotalDeductions, tGross });

            //Insert driver records here.
            ws.Cell(startRow, 1).InsertData(insert_item);
            ws.Range(counter + 3, 1, counter + 3, 9).Style.Font.SetBold();     //Bold totals row.

            //Add borders.
            ws.Range(startRow - 1, 1, counter + 3, 9).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            
            //Iterate through all HELPER payroll entries.
            insert_item.Clear();        //Clear again array insert container.
            startRow += counter + 2;        //Move start row to how many inserted rows (drivers) + 2 to add some spaces.
            counter = 1;

            //Insert column header.
            insert_item.Clear();
            insert_item.Add(new Object[] { "HELPER", "PLATE NO.", "GROSS", "TAX (2%)", "CF (5%)", "SA", "DEDUCTIONS", "NET" });
            ws.Cell(startRow - 1, 2).InsertData(insert_item).Style.Font.SetBold();
            ws.Range(startRow - 1, 4, startRow - 1, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            //Grand totals
            tRate = tTax = tCF = tSA = tTotalDeductions = tGross = 0.0;

            insert_item.Clear();
            foreach (DataRow row in HelperTable) {
                //Get values.
                string employee = row["Employee"].ToString();
                string plateNo = row["PlateNo"].ToString();
                double rate = Convert.ToDouble(row["Rate"]);
                double tax = Convert.ToDouble(row["Tax"]);
                double cf = Convert.ToDouble(row["CF"]);
                double sa = Convert.ToDouble(row["SA"]);
                double totalDeductions = Convert.ToDouble(row["TotalDeductions"]);
                double gross = Convert.ToDouble(row["GrossPay"]);

                //add to grand totals
                tRate += rate; tTax += tax; tCF += cf; tSA += sa; tTotalDeductions += totalDeductions; tGross += gross;

                //Insert items to array.
                insert_item.Add(new Object[] { counter, employee, plateNo, rate, tax, cf, sa, totalDeductions, gross });
                counter++;
            }

            //Add grand totals.
            insert_item.Add(new Object[] { "", "TOTALS", "", tRate, tTax, tCF, tSA, tTotalDeductions, tGross });

            //Insert helper records here.
            ws.Cell(startRow, 1).InsertData(insert_item);
            ws.Range((startRow - 1) + (counter), 1, (startRow - 1) + (counter), 9).Style.Font.SetBold();     //Bold totals row.

            //Add borders.
            ws.Range(startRow - 1, 1, (startRow - 1) + (counter), 9).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            //Adjust row height to 16. (selecting because setting default RowHeight just dont work!!!)
            ws.Rows(1, startRow + counter).Height = 16;
            ws.Columns("C:I").Width = 11.5;
            ws.Columns("B").Width = 23;
            ws.Columns("A").Width = 2;
        }

        //Function to create payroll summary worksheet.
        void InsertSummarySheet(XLWorkbook wb) {
            //Create worksheet
            IXLWorksheet ws = wb.Worksheets.Add("Summary");

            //Set style
            ws.ShowGridLines = false;
            ws.Style.Font.SetFontName("Arial").Font.SetFontSize(8);
            ws.PageSetup.PagesWide = 1;
            ws.Style.NumberFormat.SetFormat("#,##0.00;(#,##0.00);-;@");
            ws.Column("A").Style.NumberFormat.SetFormat("@");

            //Data cache.
            List<Object[]> data = new List<Object[]>();

            //Columns expended: A:E
            //[#] [Driver/Helper] [Amount] [CA] [Net Pay]
            data.Add(new Object[] { "PAYROLL SUMMARY" });
            data.Add(new Object[] { "Payroll Period: " + DateTime.Parse(Cell("DateStart")).ToString("MMM dd") + " - " + DateTime.Parse(Cell("DateEnd")).ToString("MMM dd, yyyy") });
            data.Add(new Object[] { "", "DRIVER/HELPER", "AMOUNT", "CA", "NET PAY" });
            ws.Cell(1, 1).InsertData(data);

            data.Clear();   //Clear for next batch of insertion.

            //Insert summary data here.  
            //Retrieve payroll records from database.
            String query = "SELECT A.ID, A.PID, (B.LastName + ', ' + B.FirstName) AS [Employee], A.GrossPay, A.CA, A.NetPay FROM PayrollSummary AS A INNER JOIN Employee AS B ON A.Employee = B.EmployeeID ";
            query += "WHERE A.PID = @id ";
            query += "ORDER BY (B.LastName + ', ' + B.FirstName)";

            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

            //IEnumerable of payroll entries.
            IEnumerable<DataRow> SummaryTable = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p).Rows.Cast<DataRow>();

            //Global totals.
            double tGross, tCA, tNet;
            tGross = tCA = tNet = 0.00;

            //Loop data.
            int counter = 1;
            foreach (DataRow row in SummaryTable) {
                //Retrieve from row
                string employee = row["Employee"].ToString();
                double gross = Convert.ToDouble(row["GrossPay"]);
                double ca = Convert.ToDouble(row["CA"]);
                double net = Convert.ToDouble(row["NetPay"]);

                //Add record to insert queue.
                data.Add(new Object[] { counter, employee, gross, ca, net });

                //Add to totals.
                tGross += gross; tCA += ca; tNet += net;

                //Increment counter variable.
                counter++;
            }

            //Add totals
            data.Add(new Object[] { "", "TOTALS", tGross, tCA, tNet });

            //Insert data.
            ws.Cell(4, 1).InsertData(data);

            /////////////// Format spreadsheet ////////////////
            ws.Rows(1, counter + 5).Height = 17;
            ws.Columns("C:E").Width = 11;
            ws.Column("B").Width = 45;
            ws.Columns("A").Width = 2;

            //Bold column headers
            ws.Range("A3:E3").Style.Font.SetBold();
            ws.Range("C3:E3").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
            IXLRange header = ws.Range("A1:E1").Merge();
            header.Style.Font.SetBold().Font.SetFontSize(10).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left).Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);
            IXLRange title = ws.Range("A2:E2").Merge();
            title.Style.Font.SetFontSize(8).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left).Alignment.SetVertical(XLAlignmentVerticalValues.Top);
            ws.Rows(1,2).Height = 16;
            ws.Range(counter + 3, 1, counter + 3, 5).Style.Font.SetBold();
            ws.Range(3, 1, counter + 3, 5).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);
        }

        //Function to create included trucking log worksheet.
        void InsertIncludedTruckingLogs(XLWorkbook wb) {
            //Query string.
            String query = "SELECT A.TransportDate AS [Date], A.PlateNo AS [Plate No], (A.Destination + IIF(A.IsBackLoad = true, ' (Backload)', '')) AS [Destination], A.DRNo AS [DR No], A.Rate AS [Trucking Rate], A.DriverRate AS [Driver Rate], A.HelperRate AS [Helper Rate], (B.FirstName + ' ' + B.LastName) AS [Driver], (C.FirstName + ' ' + C.LastName) AS [Helper] FROM ((TransportLog AS A LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID) INNER JOIN PayrollTransportLog AS PLog ON A.ID = PLog.TransactionID ";
            query += "WHERE (PLog.PAYROLLID = @id) ";
            query += "ORDER BY PlateNo ASC, TransportDate ASC, Destination ASC";
            
            //SQL Parameter.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@id", "{" + Cell("PID") + "}");

            //Datarow IEnumerable.
            //Group entries by plate number..
            DataTable table = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            table.Columns["DR No"].DataType = typeof(string);

            IEnumerable<IGrouping<String, DataRow>> col = table.Rows.Cast<DataRow>().GroupBy(x => x["Plate No"].ToString());

            //Create new worksheet.
            IXLWorksheet xml = wb.Worksheets.Add("Trucking Log");
            xml.ShowGridLines = false;
            xml.PageSetup.FitToPages(1, 0);
            xml.PageSetup.PageOrientation = XLPageOrientation.Landscape;
            xml.Style.DateFormat.SetFormat("dd-mmm");
            xml.Style.NumberFormat.SetFormat("#,##0.00");
            xml.Style.Font.SetFontSize(8).Font.SetFontName("Arial");

            //Set column size.
            xml.Columns("A,B").Width = 11;
            xml.Columns("D,E,F,G").Width = 16;
            xml.Columns("C,H,I").Width = 25;

            //Define default border style.

            //Worksheet Header
            xml.Range("A1:I1").Merge().SetValue("TRUCKING LOG").Style
                .Font.SetFontSize(10)
                .Font.SetBold()
                .Alignment.SetVertical(XLAlignmentVerticalValues.Bottom).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            
            //Date info
            xml.Range("A2:I2").Merge().SetValue("Period Covered: " + DateTime.Parse(Cell("DateStart")).ToString("MMM dd") + " - " + DateTime.Parse(Cell("DateEnd")).ToString("MMM dd, yyyy")).Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Top).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            
            //Control row heights on header.
            xml.Rows(1, 2).Height = 16;

            //Insert every record.
            int currRow = 3;
            List<Object[]> data = new List<Object[]>();
            //Grand total variables.
            double truckingRateGTotal = 0.0, driverRateGTotal = 0.0, helperRateGTotal = 0.0;
            foreach (String key in col.Select(x => x.Key)) {

                //Write column header.
                List<Object[]> cellCols = new List<Object[]>();
                List<Object> cols = new List<Object>();
                foreach (DataColumn co in table.Columns)
                    cols.Add(co.ColumnName);
                cellCols.Add(cols.ToArray());
                xml.Cell(currRow, 1).InsertData(cellCols);
                currRow++;

                //Enumerate items by grouped plate number.    
                data.Clear();
                foreach (DataRow r in col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {
                    XLCellDataCollection dataCol = new XLCellDataCollection();

                    //insert data per column.
                    data.Add(new Object[] { DateTime.Parse(r["Date"].ToString()).ToShortDateString(),
                                            r["Plate No"],
                                            r["Destination"],
                                            r["DR No"].ToString(),
                                            r["Trucking Rate"],
                                            r["Driver Rate"],
                                            r["Helper Rate"],
                                            InputValidators.DBNullToStr(r["Driver"]),
                                            InputValidators.DBNullToStr(r["Helper"]) });
                }

                double truckingRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Trucking Rate"]));
                double driverRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Driver Rate"]));
                double helperRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Helper Rate"]));
                
                //Insert group totals.
                data.Add(new Object[] {"", "", "", "", truckingRateSum,
                                        driverRateSum,
                                        helperRateSum });

                //set borders around cell.
                xml.Range(currRow - 1, 1, currRow + (data.Count - 1), 9).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                //Insert values
                xml.Cell(currRow, 1).InsertData(data);

                //Add to grand totals.
                truckingRateGTotal += truckingRateSum;
                driverRateGTotal += driverRateSum;
                helperRateGTotal += helperRateSum;

                //Set group column headers and totals bold.
                xml.Range(currRow - 1, 1, currRow - 1, 9).Style.Font.SetBold();
                xml.Range(currRow + data.Count - 1, 1, currRow + data.Count - 1, 9).Style.Font.SetBold();

                currRow += data.Count + 2;  //Add space to next set of records.
            }

            //Write the grand totals.
            data.Clear();
            data.Add(new Object[] {"", "", "", "", truckingRateGTotal,
                                        driverRateGTotal,
                                        helperRateGTotal });

            xml.Cell(currRow, 1).InsertData(data);  //Insert grand total

            //set grand totals bold.
            xml.Range(currRow, 1, currRow, 9).Style.Font.SetBold();

            //Readjust rows.
            xml.Rows(1, currRow + 5).Height = 16;

            table.Dispose();
        }

        #endregion

        #region CRUD

        //Function when page is refreshed.
        private void PayrollPage_OnRefresh(object sender, EventArgs e) {
            //Query all Cash Reports

            String query = "SELECT PID, Title, DateStart, DateEnd, Author, IsArchived, IsFinal, IsComputed FROM Payroll ";
            query += "WHERE IsArchived = false AND (MONTH(DateStart) = @startmonth AND YEAR(DateStart) = @startyear) AND (Title LIKE '%' & @search & '%') ";
            query += "ORDER BY DateStart, Title";

            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@startmonth", GetStartDate().Month);
            p[1] = new OleDbParameter("@startyear", GetStartDate().Year);
            p[2] = new OleDbParameter("@search", GetSearchText());

            if (dt_payrolls != null) dt_payrolls.Dispose();
            dt_payrolls = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

            //Move ifFinal at beginning of columns.
            dt_payrolls.Columns["IsFinal"].SetOrdinal(0);
            dt_payrolls.Columns["IsComputed"].SetOrdinal(1);

            //Insert image representations of locked/final records.
            DataColumn icol = dt_payrolls.Columns.Add("IMG", typeof(Image));
            DataColumn icompute = dt_payrolls.Columns.Add("IMGCOMPUTED", typeof(Image));
            icompute.SetOrdinal(0);
            icol.SetOrdinal(0);
            foreach (DataRow r in dt_payrolls.Rows) {
                if ((bool)r["IsFinal"]) r[0] = lockicon;
            }
            foreach (DataRow r in dt_payrolls.Rows) {
                if ((bool)r["IsComputed"]) r[1] = star_shaded;
                else r[1] = star_unshaded;
            }
            
            //Set datasource of datagrSet
            SetRecordsDataSource(dt_payrolls);

            //Hide unnecessary columns.
            HideColumn("PID");
            HideColumn("IsArchived");
            HideColumn("IsFinal");
            HideColumn("IsComputed");

            //Set column user-friendly title.
            SetColumnHeader("DateStart", "Start Date");
            SetColumnHeader("DateEnd", "End Date");

            //Customize the lock image column.
            DataGridViewColumn finalcol = GetColumn("IMG");
            finalcol.Width = 30;
            finalcol.Resizable = DataGridViewTriState.False;
            finalcol.DefaultCellStyle.NullValue = null;
            finalcol.ToolTipText = "Finalized";

            //Customize the computed image column.
            DataGridViewColumn finalcol2 = GetColumn("IMGCOMPUTED");
            finalcol2.Width = 30;
            finalcol2.Resizable = DataGridViewTriState.False;
            finalcol2.DefaultCellStyle.NullValue = null;
            finalcol2.ToolTipText = "Computed";

            //Remove the column header text of the lock indicator.
            SetColumnHeader("IMG", "");
            SetColumnHeader("IMGCOMPUTED", "");

            //Autofit specific columns.
            RecordsDGVFitColumnToContent("Title");
            RecordsDGVFitColumnToContent("Author");
        }

        //Function when export button is pressed.
        private void PayrollPage_OnExport(object sender, EventArgs e) {
            if (Convert.ToBoolean(Cell("IsComputed"))) {
                using (SaveFileDialog dlg = new SaveFileDialog()) {
                    dlg.Filter = "Excel Spreadsheet (*.xlsx)|*.xlsx";
                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        ExportPayrollInfo(dlg.FileName);
                }
            } else {
                Notifier.ShowWarning("Payroll must be computed first before exporting it as a spreadsheet document.\n\nTo compute the payroll, you must go to the 'Payroll' or 'Summary' tab of this record.", "Export Payroll");
            }
        }

        //Function when archive command is pressed.
        private void PayrollPage_OnArchive(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                String title = Cell("Title");
                String period = DateTime.Parse(Cell("DateStart")).ToShortDateString() + " - " + DateTime.Parse(Cell("DateEnd")).ToShortDateString();
                if (Notifier.ShowConfirm("Are you sure you want to archive the selected payroll?\n\n" + title + "\n" + period, "Confirm Archive") == System.Windows.Forms.DialogResult.Yes) {
                    //Proceed to deleting the specified payroll.
                    String query = "UPDATE Payroll SET IsArchived = true WHERE PID = @pid";

                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@pid", "{" + Cell("PID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Payroll successfully moved to the system archives.", "Payroll Archived");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to archive the specified payroll.");
                }
            }
        }

        //Handle event when ADD button is clicked.
        private void PayrollPage_OnAdd(object sender, EventArgs e) {
            PayrollDialog dlg = new PayrollDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetStartingDate(GetStartDateString());
            dlg.SetExistingtitles(GetPayrollTitles());
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //Retrieve inputs.
                string title, dstart, dend;
                dlg.GetFieldValues(out title, out dstart, out dend);

                //Insert new payroll file to the database.
                String query = "INSERT INTO Payroll(Title, DateStart, DateEnd, Author) ";
                query += "VALUES(@title, @dstart, @end, @auth)";

                OleDbParameter[] p = new OleDbParameter[4];
                p[0] = new OleDbParameter("@title", title);
                p[1] = new OleDbParameter("@dstart", dstart);
                p[2] = new OleDbParameter("@dend", dend);
                p[3] = new OleDbParameter("@auth", UserManager.ACCOUNT_NAME);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("A new payroll record was successfully created.", "New Payroll");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Oops! An error has occurred while creating a new payroll record.");
            }
        }

        //Handle event when EDIT button is clicked (on unlocked records only)
        private void PayrollPage_OnEdit(object sender, EventArgs e) {
            PayrollDialog dlg = new PayrollDialog();
            dlg.SetStartingDate(GetStartDateString());
            dlg.SetExistingtitles(GetPayrollTitles());
            dlg.SetFieldValues(Cell("Title"), Cell("DateStart"), Cell("DateEnd"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //Retrieve inputs.
                string title, dstart, dend;
                dlg.GetFieldValues(out title, out dstart, out dend);

                //Insert new payroll file to the database.
                String query = "UPDATE Payroll SET Title = @title, DateStart = @dstart, DateEnd = @end ";
                query += "WHERE PID = @id";

                OleDbParameter[] p = new OleDbParameter[4];
                p[0] = new OleDbParameter("@title", title);
                p[1] = new OleDbParameter("@dstart", dstart);
                p[2] = new OleDbParameter("@dend", dend);
                p[3] = new OleDbParameter("@pid", "{" + Cell("PID") + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("Successfully updated payroll", "Update Payroll");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to update payroll.");
            }
        }

        //Handle event when search button is clicked.
        private void PayrollPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();   //Refresh records.
        }

        //Handle delete payroll command.
        private void PayrollPage_OnDelete(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                String title = Cell("Title");
                String period = DateTime.Parse(Cell("DateStart")).ToShortDateString() + " - " + DateTime.Parse(Cell("DateEnd")).ToShortDateString();
                if (Notifier.ShowConfirm("Are you sure you want to delete the selected payroll?\n\n" + title + "\n" + period, "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    //Proceed to deleting the specified payroll.
                    String query = "DELETE FROM Payroll WHERE PID = @pid";

                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@pid", "{" + Cell("PID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Payroll successfully deleted.", "Payroll Deleted");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to delete the specified payroll.");
                }
            }
        }

        //Handle event when "Mark Final" button is clicked.
        private void PayrollPage_OnFinalize(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                if (Convert.ToBoolean(Cell("IsComputed")) == false) {
                    Notifier.ShowWarning("You should compute the payroll first by viewing it before you can mark it as final.", "Lock Payroll");
                    return;
                }
                String title = Cell("Title");
                String period = DateTime.Parse(Cell("DateStart")).ToShortDateString() + " - " + DateTime.Parse(Cell("DateEnd")).ToShortDateString();
                if (Notifier.ShowConfirm("You are about to mark the selected payroll as final that will lock the record from further modifications. Are you sure you want to continue?\n\n" + title + "\n" + period, "Confirm Finalize") == System.Windows.Forms.DialogResult.Yes) {
                    //Proceed to deleting the specified payroll.
                    String query = "UPDATE Payroll SET IsFinal = true WHERE PID = @pid";

                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@pid", "{" + Cell("PID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Payroll was successfully locked.", "Payroll Final");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to lock the specified payroll.");
                }
            }
        }
        
        //Handles event when a record is opened.
        private void PayrollPage_OnRecordDoubleClicked(object sender, EventArgs e) {
            PayrollEditorPage pg = new PayrollEditorPage();
            pg.Text = Cell("Title");
            pg.SetPayrollInfo(Cell("PID"), Cell("Title"), DateTime.Parse(Cell("DateStart")).ToShortDateString(), DateTime.Parse(Cell("DateEnd")).ToShortDateString(), Convert.ToBoolean(Cell("IsFinal")));
            Enabled = false;
            if (ParentForm is MainWindow) {
                MainWindow m = (MainWindow)ParentForm;
                pg.SetPreviousPage(this, GetRecordsDataGridView().SelectedRows[0].Index);
                m.LoadPageNext(pg);
            } else {
                pg.Dispose();
            }
        }

        #endregion

        #region Window Event handlers

        //Handle on load events.
        private void PayrollPage_Load(object sender, EventArgs e) {
            SetStartDate(GlobalVariables.PayrollPage_Date.ToShortDateString());
        }

        #endregion

        #region Control Event handlers

        //Handle event when the selected record was changed.
        private void PayrollPage_OnRecordsSelectionChanged(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Cell("IsFinal").Equals("True")) {
                    SetEditButtonVisible(false);
                    SetDeleteButtonVisible(false);
                    SetFinalButtonVisible(false);
                    SetArchiveButtonVisible(true);
                } else {
                    SetEditButtonVisible(true);
                    SetDeleteButtonVisible(true);
                    SetFinalButtonVisible(true);
                    SetArchiveButtonVisible(false);
                }
            }
        }

        //Remember the date filter.
        private void dpStart_ValueChanged(object sender, EventArgs e) {
            GlobalVariables.PayrollPage_Date = GetStartDate();
        }

        #endregion
        
    }

}
