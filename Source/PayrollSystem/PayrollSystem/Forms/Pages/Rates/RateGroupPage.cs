﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class RateGroupPage : SystemPage {

        #region local data

        DataTable dt;

        #endregion

        #region Constructor

        public RateGroupPage() {
            Text = "Rate Groups";
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // RateGroupPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "RateGroupPage";
            this.OnAdd += new System.EventHandler(this.RateGroupPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.RateGroupPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.RateGroupPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.RateGroupPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.RateGroupPage_OnRefresh);
            this.Load += new System.EventHandler(this.RateGroupPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Window Eventhandlers

        private void RateGroupPage_Load(object sender, EventArgs e) {
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
            RefreshRecords();
        }

        #endregion

        #region Public Functions

        public static DataTable QueryRateGroups(string vehicle_type) {
            DataTable dat = new DataTable();
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@vt", vehicle_type);
            dat = DatabaseManager.GetMainDatabase().ExecuteQuery("SELECT ID, Title FROM RateGroup WHERE VehicleType=@vt", false, p);
            return dat;
        }

        public static DataTable QueryRateGroupsAll(string vehicle_type) {
            DataTable dat = new DataTable();
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@vt", vehicle_type);
            dat = DatabaseManager.GetMainDatabase().ExecuteQuery("SELECT ID, Title, DriverRate, HelperRate FROM RateGroup WHERE VehicleType=@vt", false, p);
            return dat;
        }

        #endregion

        #region CRUD

        private void RateGroupPage_OnRefresh(object sender, EventArgs e) {
            if (dt != null) dt.Dispose();
            //WHERE (Title Like '%' & [@search] & '%') And (VehicleType Like '%' & [@vtype] & '%') ORDER BY Title;
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@vtype", "");
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_RateGroup", true, p);
            SetRecordsDataSource(dt);
            HideColumn("ID");
            SetColumnFormat("Driver Rate", "#,##0.00");
            SetColumnFormat("Helper Rate", "#,##0.00");
        }

        private void RateGroupPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void RateGroupPage_OnAdd(object sender, EventArgs e) {
            RateGroupDialog dlg = new RateGroupDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            ComboboxDataItem[] types = VehicleTypesPage.QueryVehicleTypes();
            if (types.Length <= 0) {
                Notifier.ShowError("No vehicle types found. You must add vehicle types first before adding rate groups.");
                if (Notifier.ShowConfirm("Do you want to add vehicle types?", "Add Vehicle Types") == System.Windows.Forms.DialogResult.Yes) {
                    if (ParentForm is MainWindow) {
                        MainWindow wnd = (MainWindow)ParentForm;
                        VehicleTypesPage p = new VehicleTypesPage();
                        wnd.LoadPage(p);
                        p.InvokeAdd();
                    }
                    return;
                } else {
                    return;
                }
            }
            dlg.SetVehicleTypeList(types);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //VALUES ([@title], [@vtype], [@drate], [@hrate]);
                string title = "", vtype = "", driver_rate = "", helper_rate = "";
                dlg.GetFieldValues(out title, out vtype, out driver_rate, out helper_rate); 
                
                OleDbParameter[] p = new OleDbParameter[4];
                p[0] = new OleDbParameter("@title", title);
                p[1] = new OleDbParameter("@vtype", vtype);
                p[2] = new OleDbParameter("@drate", driver_rate);
                p[2].DbType = DbType.Double;
                p[3] = new OleDbParameter("@hrate", helper_rate);
                p[3].DbType = DbType.Double;
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_RateGroup", true, p);
                if (ok) {
                    Notifier.ShowInformation("Successfully added new rate group.", "New Rate Group");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to add new rate group.");
                }
            }
        }

        private void RateGroupPage_OnDelete(object sender, EventArgs e) {
            if(GetRecordsSelectedRows().Count == 0) return;
            string msg = "";
            int rcount = GetRecordsSelectedRows().Count; 
            if (rcount == GetRecordsCount())
                msg = "Are you sure you want to delete ALL records?";
            else if (rcount > 1)
                msg = "Are you sure you want to delete the selected " + rcount + " records?";
            else
                msg = "Are you sure you want to delete the selected record?";
            if (Notifier.ShowConfirm(msg, "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                //WHERE ID=[@id];
                bool success = true;
                int fail_count = 0;
                int ok_count = 0;
                foreach (DataGridViewRow row in GetRecordsSelectedRows()) {
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", row.Cells["ID"].Value.ToString());
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_RateGroup", true, p);
                    if (ok) {
                        ok_count++;
                    } else {
                        success = false;
                        fail_count++;
                    }
                }
                if (rcount == 1) {
                    if (success) {
                        Notifier.ShowInformation("The selected rate group was successfully deleted.", "Delete Rate Group");
                        RefreshRecords();
                    } else {
                        Notifier.ShowInformation("Failed to delete the selected rate group.", "Delete Rate Group");
                        RefreshRecords();
                    }
                } else if (rcount > 1) {
                    if (success) {
                        Notifier.ShowInformation("All selected rate groups were successfully deleted.", "Delete Rate Group");
                        RefreshRecords();
                    } else {
                        if (ok_count == 0) {
                            Notifier.ShowInformation("The delete operation failed for all selected rate groups.", "Delete Rate Group");
                        } else {
                            Notifier.ShowInformation("The delete operation completed successfully, but failed on some records.\n\nSuccessful: " + ok_count + "\nFailed: " + fail_count, "Delete Rate Group");
                            RefreshRecords();
                        }
                    }
                }
            }
        }

        private void RateGroupPage_OnEdit(object sender, EventArgs e) {
            RateGroupDialog dlg = new RateGroupDialog();
            ComboboxDataItem[] types = VehicleTypesPage.QueryVehicleTypes();
            if (types.Length <= 0) {
                Notifier.ShowError("No vehicle types found. You must add vehicle types first before adding rate groups.");
                if (Notifier.ShowConfirm("Do you want to add vehicle types?", "Add Vehicle Types") == System.Windows.Forms.DialogResult.Yes) {
                    if (ParentForm is MainWindow) {
                        MainWindow wnd = (MainWindow)ParentForm;
                        VehicleTypesPage p = new VehicleTypesPage();
                        wnd.LoadPage(p);
                        p.InvokeAdd();
                    }
                    return;
                } else {
                    return;
                }
            }
            dlg.SetVehicleTypeList(types);
            dlg.SetFieldValues(Cell("Title"), Cell("Vehicle Type"), Convert.ToDouble(Cell("Driver Rate")).ToString("0.00"), Convert.ToDouble(Cell("Helper Rate")).ToString("0.00"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //SET Title = [@title], VehicleType = [@vtype], DriverRate = [@drate], HelperRate = [@hrate] WHERE ID=[@id];
                string title = "", vtype = "", driver_rate = "", helper_rate = "";
                string id = Cell("ID");
                dlg.GetFieldValues(out title, out vtype, out driver_rate, out helper_rate);

                OleDbParameter[] p = new OleDbParameter[5];
                p[0] = new OleDbParameter("@title", title);
                p[1] = new OleDbParameter("@vtype", vtype);
                p[2] = new OleDbParameter("@drate", driver_rate);
                p[2].DbType = DbType.Double;
                p[3] = new OleDbParameter("@hrate", helper_rate);
                p[3].DbType = DbType.Double;
                p[4] = new OleDbParameter("@id", "{" + id + "}");
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_RateGroup", true, p);
                if (ok) {
                    Notifier.ShowInformation("Successfully updated rate group.", "Update Rate Group");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to update specified rate group.");
                }
            }
        }

        #endregion

    }

}
