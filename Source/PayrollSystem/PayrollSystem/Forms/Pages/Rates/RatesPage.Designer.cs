﻿namespace PayrollSystem {
    partial class RatesPage {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TabStripMain = new System.Windows.Forms.ToolStrip();
            this.ContentPanel = new PayrollSystem.BufferedPanel();
            this.NoticePanel = new PayrollSystem.BufferedPanel();
            this.lblNoticeBody = new System.Windows.Forms.Label();
            this.lnkNoticeLink = new System.Windows.Forms.LinkLabel();
            this.lblNoticeHeader = new System.Windows.Forms.Label();
            this.GroupInfoPanel = new PayrollSystem.BufferedPanel();
            this.cboRateGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblHelperRate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDriverRate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bufferedPanel2 = new PayrollSystem.BufferedPanel();
            this.ContentPanel.SuspendLayout();
            this.NoticePanel.SuspendLayout();
            this.GroupInfoPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabStripMain
            // 
            this.TabStripMain.AllowItemReorder = true;
            this.TabStripMain.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TabStripMain.Location = new System.Drawing.Point(0, 0);
            this.TabStripMain.Name = "TabStripMain";
            this.TabStripMain.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.TabStripMain.Size = new System.Drawing.Size(727, 25);
            this.TabStripMain.TabIndex = 1;
            this.TabStripMain.Text = "toolStrip1";
            // 
            // ContentPanel
            // 
            this.ContentPanel.Controls.Add(this.NoticePanel);
            this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentPanel.Location = new System.Drawing.Point(0, 55);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(727, 367);
            this.ContentPanel.TabIndex = 3;
            // 
            // NoticePanel
            // 
            this.NoticePanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NoticePanel.BackColor = System.Drawing.Color.White;
            this.NoticePanel.Controls.Add(this.lblNoticeBody);
            this.NoticePanel.Controls.Add(this.lnkNoticeLink);
            this.NoticePanel.Controls.Add(this.lblNoticeHeader);
            this.NoticePanel.Location = new System.Drawing.Point(81, 61);
            this.NoticePanel.Name = "NoticePanel";
            this.NoticePanel.Size = new System.Drawing.Size(564, 244);
            this.NoticePanel.TabIndex = 1;
            this.NoticePanel.Visible = false;
            // 
            // lblNoticeBody
            // 
            this.lblNoticeBody.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNoticeBody.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoticeBody.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblNoticeBody.Location = new System.Drawing.Point(29, 113);
            this.lblNoticeBody.Name = "lblNoticeBody";
            this.lblNoticeBody.Size = new System.Drawing.Size(506, 22);
            this.lblNoticeBody.TabIndex = 2;
            this.lblNoticeBody.Text = "To add rates, you must specify vehicle types first.";
            this.lblNoticeBody.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lnkNoticeLink
            // 
            this.lnkNoticeLink.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkNoticeLink.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lnkNoticeLink.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkNoticeLink.Location = new System.Drawing.Point(32, 137);
            this.lnkNoticeLink.Name = "lnkNoticeLink";
            this.lnkNoticeLink.Size = new System.Drawing.Size(501, 21);
            this.lnkNoticeLink.TabIndex = 1;
            this.lnkNoticeLink.TabStop = true;
            this.lnkNoticeLink.Text = "Click here to add vehicle types";
            this.lnkNoticeLink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lnkNoticeLink.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(109)))), ((int)(((byte)(198)))));
            this.lnkNoticeLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNoticeLink_LinkClicked);
            // 
            // lblNoticeHeader
            // 
            this.lblNoticeHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNoticeHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoticeHeader.Location = new System.Drawing.Point(31, 82);
            this.lblNoticeHeader.Name = "lblNoticeHeader";
            this.lblNoticeHeader.Size = new System.Drawing.Size(503, 27);
            this.lblNoticeHeader.TabIndex = 0;
            this.lblNoticeHeader.Text = "No vehicle types found.";
            this.lblNoticeHeader.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // GroupInfoPanel
            // 
            this.GroupInfoPanel.Controls.Add(this.cboRateGroup);
            this.GroupInfoPanel.Controls.Add(this.label1);
            this.GroupInfoPanel.Controls.Add(this.flowLayoutPanel1);
            this.GroupInfoPanel.Controls.Add(this.bufferedPanel2);
            this.GroupInfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupInfoPanel.Location = new System.Drawing.Point(0, 25);
            this.GroupInfoPanel.Name = "GroupInfoPanel";
            this.GroupInfoPanel.Size = new System.Drawing.Size(727, 30);
            this.GroupInfoPanel.TabIndex = 2;
            // 
            // cboRateGroup
            // 
            this.cboRateGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboRateGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRateGroup.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRateGroup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.cboRateGroup.FormattingEnabled = true;
            this.cboRateGroup.Location = new System.Drawing.Point(90, 5);
            this.cboRateGroup.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cboRateGroup.MaximumSize = new System.Drawing.Size(180, 0);
            this.cboRateGroup.MinimumSize = new System.Drawing.Size(100, 0);
            this.cboRateGroup.Name = "cboRateGroup";
            this.cboRateGroup.Size = new System.Drawing.Size(180, 21);
            this.cboRateGroup.TabIndex = 3;
            this.cboRateGroup.SelectedIndexChanged += new System.EventHandler(this.cboRateGroup_SelectedIndexChanged);
            this.cboRateGroup.DropDownClosed += new System.EventHandler(this.cboRateGroup_DropDownClosed);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rate Group:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lblHelperRate);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.lblDriverRate);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(402, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(325, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // lblHelperRate
            // 
            this.lblHelperRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHelperRate.AutoSize = true;
            this.lblHelperRate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelperRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblHelperRate.Location = new System.Drawing.Point(259, 8);
            this.lblHelperRate.Margin = new System.Windows.Forms.Padding(0, 5, 3, 5);
            this.lblHelperRate.Name = "lblHelperRate";
            this.lblHelperRate.Size = new System.Drawing.Size(63, 13);
            this.lblHelperRate.TabIndex = 6;
            this.lblHelperRate.Text = "Php 0.00";
            this.lblHelperRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label5.Location = new System.Drawing.Point(210, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Helper:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDriverRate
            // 
            this.lblDriverRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDriverRate.AutoSize = true;
            this.lblDriverRate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblDriverRate.Location = new System.Drawing.Point(144, 8);
            this.lblDriverRate.Margin = new System.Windows.Forms.Padding(0, 5, 3, 5);
            this.lblDriverRate.Name = "lblDriverRate";
            this.lblDriverRate.Size = new System.Drawing.Size(63, 13);
            this.lblDriverRate.TabIndex = 8;
            this.lblDriverRate.Text = "Php 0.00";
            this.lblDriverRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label2.Location = new System.Drawing.Point(96, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Driver:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bufferedPanel2
            // 
            this.bufferedPanel2.BackColor = System.Drawing.Color.Gainsboro;
            this.bufferedPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel2.Location = new System.Drawing.Point(0, 29);
            this.bufferedPanel2.Name = "bufferedPanel2";
            this.bufferedPanel2.Size = new System.Drawing.Size(727, 1);
            this.bufferedPanel2.TabIndex = 4;
            // 
            // RatesPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(727, 422);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.GroupInfoPanel);
            this.Controls.Add(this.TabStripMain);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RatesPage";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rates and Destinations";
            this.Load += new System.EventHandler(this.RatesPage_Load);
            this.Resize += new System.EventHandler(this.RatesPage_Resize);
            this.ContentPanel.ResumeLayout(false);
            this.NoticePanel.ResumeLayout(false);
            this.GroupInfoPanel.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TabStripMain;
        private BufferedPanel GroupInfoPanel;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cboRateGroup;
        private BufferedPanel ContentPanel;
        private BufferedPanel bufferedPanel2;
        private System.Windows.Forms.Label lblHelperRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDriverRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblNoticeHeader;
        private BufferedPanel NoticePanel;
        private System.Windows.Forms.Label lblNoticeBody;
        private System.Windows.Forms.LinkLabel lnkNoticeLink;
    }
}