﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Windows.Forms;

namespace PayrollSystem {

    public partial class RatesPage : Form {

        #region Local Variables

        SystemPage DataViewer = new SystemPage();

        BindingList<ComboboxDataItem> RateGroupList = new BindingList<ComboboxDataItem>();
        DataTable RateGroupListTable;

        string VTypeID = "";

        #endregion

        #region Constructor

        public RatesPage() {
            InitializeComponent();

            //Initialize DataViewer
            DataViewer.TopLevel = false;
            DataViewer.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            DataViewer.ShowInTaskbar = false;
            DataViewer.ShowIcon = false;
            DataViewer.Dock = DockStyle.Fill;
            DataViewer.SetToolbarButtons(SystemPage.PageToolbarButtons.Add | SystemPage.PageToolbarButtons.Edit | SystemPage.PageToolbarButtons.Delete | SystemPage.PageToolbarButtons.Refresh | SystemPage.PageToolbarButtons.Search);
            ContentPanel.Controls.Add(DataViewer);

            TabStripMain.Renderer = new LightTabStripRenderer();
            DataViewer.OnRefresh += DataViewer_OnRefresh;
            DataViewer.OnAdd += DataViewer_OnAdd;
            DataViewer.OnSearch += DataViewer_OnSearch;
            DataViewer.OnDelete += DataViewer_OnDelete;
            DataViewer.OnEdit += DataViewer_OnEdit;
            DataViewer.OnRecordsSelectionChanged += DataViewer_OnRecordsSelectionChanged;
            DataViewer.SetRecordsDataGridMultiselect(false);
            HideControls();
        }

        #endregion

        #region CRUD

        void DataViewer_OnSearch(object sender, EventArgs e) {
            DataViewer.RefreshRecords();
        }

        void DataViewer_OnEdit(object sender, EventArgs e) {
            if (cboRateGroup.SelectedIndex > -1) {
                //SET Destination = [@dest], Rate = [@rate], RateGroup = [@rg] WHERE ID=[@id];
                RateDialog dlg = new RateDialog();
                dlg.SetExistingDestinations(GetExistingDestinationsArray());
                dlg.SetFieldValues(DataViewer.Cell("Destination"), Convert.ToDouble(DataViewer.Cell("Rate")).ToString("0.00"));
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    string dest = "", rate = "", rg = "", id;
                    dlg.GetFieldValues(out dest, out rate);
                    rg = cboRateGroup.SelectedValue.ToString();
                    id = DataViewer.Cell("ID");
                    OleDbParameter[] p = new OleDbParameter[4];
                    p[0] = new OleDbParameter("@dest", dest);
                    p[1] = new OleDbParameter("@rate", rate);
                    p[1].DbType = DbType.Double;
                    p[2] = new OleDbParameter("@rg", "{" + rg + "}");
                    p[3] = new OleDbParameter("@id", "{" + id + "}");
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_Rates", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Rate is successfully updated.", "Update Rate");
                        DataViewer.RefreshRecords();
                    } else {
                        Notifier.ShowInformation("Failed to update rate.", "Update Rate");
                    }
                }
            }
        }

        void DataViewer_OnDelete(object sender, EventArgs e) {
            if (DataViewer.GetRecordsSelectedRows().Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to delete the selected rate?", "Delete Rate") == System.Windows.Forms.DialogResult.Yes) {
                    //WHERE ID=[@id];
                    OleDbParameter[] p = new OleDbParameter[1];
                    string ID = DataViewer.GetRecordsSelectedRow().Cells["ID"].Value.ToString();
                    p[0] = new OleDbParameter("@id", ID);
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_Rates", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Successfully deleted selected rate.", "Delete Rate");
                        DataViewer.RefreshRecords();
                    } else {
                        Notifier.ShowInformation("Failed to delete rate.", "Delete Rate");
                    }
                }
            }
        }

        void DataViewer_OnAdd(object sender, EventArgs e) {
            if (cboRateGroup.SelectedIndex > -1) {
                //VALUES ([@dest], [@rate], [@rg]);
                RateDialog dlg = new RateDialog();
                dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
                dlg.SetExistingDestinations(GetExistingDestinationsArray());
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    string dest = "", rate = "", rg = "";
                    dlg.GetFieldValues(out dest, out rate);
                    rg = cboRateGroup.SelectedValue.ToString();
                    OleDbParameter[] p = new OleDbParameter[3];
                    p[0] = new OleDbParameter("@dest", dest);
                    p[1] = new OleDbParameter("@rate", rate);
                    p[1].DbType = DbType.Double;
                    p[2] = new OleDbParameter("@rg", "{" + rg + "}");
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_Rates", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Successfully added new rate for this rate group.", "Add New Rate");
                        DataViewer.RefreshRecords();
                    } else {
                        Notifier.ShowInformation("Failed to add new rate.", "Add New Rate");
                    }
                }
            }
        }

        void DataViewer_OnRefresh(object sender, EventArgs e) {
            //WHERE (Destination Like '%' & [@search] & '%') And (RateGroup = [@rategroup] & '%')
            if (DataViewer.IsSearching == false) {
                if (cboRateGroup.Items.Count > 0) {
                    if (cboRateGroup.SelectedIndex < 0) cboRateGroup.SelectedIndex = 0;
                }
                if (cboRateGroup.SelectedIndex > -1) {
                    OleDbParameter[] p = new OleDbParameter[2];
                    p[0] = new OleDbParameter("@search", DataViewer.GetSearchText());
                    p[1] = new OleDbParameter("@rategroup", "{" + cboRateGroup.SelectedValue.ToString() + "}");

                    DataTable dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Rates", true, p);
                    DataViewer.SetRecordsDataSource(dt);
                    DataViewer.HideColumn("ID");
                    DataViewer.HideColumn("Rate Group");
                    DataViewer.SetColumnFormat("Rate", "#,##0.00");
                }
            } else {
                DataTable res = QueryDestinationsByVehicleType("{" + VTypeID + "}", DataViewer.GetSearchText());
                DataViewer.SetRecordsDataSource(res);
                DataViewer.HideColumn("ID");
                DataViewer.HideColumn("RateGroup");
                DataViewer.SetColumnFormat("Rate", "#,##0.00");
            }
        }

        void DataViewer_OnRecordsSelectionChanged(object sender, EventArgs e) {
            if (DataViewer.IsSearching) {
                cboRateGroup.SelectedValue = DataViewer.Cell("RateGroup");
            }
        }

        #endregion

        #region Public Fuctions

        public static ComboboxRateItem[] QueryRates(string vtype_id) {
            List<ComboboxRateItem> lst = new List<ComboboxRateItem>();
            DataTable dat = new DataTable();
            string query = "SELECT A.ID, A.Destination, A.Rate, A.RateGroup, B.DriverRate, B.HelperRate FROM Rates AS A INNER JOIN RateGroup AS B ON A.RateGroup = B.ID WHERE (B.VehicleType = @vtype) ORDER BY A.Destination";
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@vtype", vtype_id);
            dat = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            foreach (DataRow r in dat.Rows)
                lst.Add(new ComboboxRateItem(r["ID"].ToString(), r["Destination"].ToString(), Convert.ToDouble(r["Rate"].ToString()), r["RateGroup"].ToString(),  Convert.ToDouble(r["DriverRate"].ToString()),  Convert.ToDouble(r["HelperRate"].ToString())));
            return lst.ToArray();
        }

        public DataTable QueryDestinationsByVehicleType(string vehicle_type, string search) {
            DataTable dat = new DataTable();
            string query = "SELECT A.ID, A.Destination, A.Rate, A.RateGroup FROM Rates AS A INNER JOIN RateGroup AS B ON A.RateGroup = B.ID WHERE (B.VehicleType = @vtype) AND (Destination LIKE '%' + @search + '%') ORDER BY A.Destination";
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@vtype", vehicle_type);
            p[1] = new OleDbParameter("@search", search);
            dat = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            return dat;
        }

        public DataTable QueryDestinationsByVehicleType(string vehicle_type) {
            DataTable dat = new DataTable();
            string query = "SELECT A.ID, A.Destination, A.Rate, A.RateGroup FROM Rates AS A INNER JOIN RateGroup AS B ON A.RateGroup = B.ID WHERE B.VehicleType = @vtype ORDER BY A.Destination";
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@vtype", vehicle_type);
            dat = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            return dat;
        }

        #endregion

        #region Local Functions

        string[] GetExistingDestinationsArray() {
            DataTable t = QueryDestinationsByVehicleType("{" + VTypeID + "}");
            List<String> existing = new List<string>();
            foreach (DataRow r in t.Rows) {
                existing.Add(r["Destination"].ToString());
            }
            return existing.ToArray();
            //return DataViewer.GetRecordsValueArray("Destination");
        }

        void OnTabChanged(TabButton sender) {
            //Load Rate Groups for the selected vehicle type.
            VTypeID = sender.Key;
            RateGroupListTable = RateGroupPage.QueryRateGroupsAll("{" + VTypeID + "}");
            RateGroupList.Clear();
            if (RateGroupListTable.Rows.Count == 0) {
                SetNotice(1);
                ClearRates();
                cboRateGroup.SelectedIndex = -1;
            } else {
                HideNotice();
                foreach (DataRow row in RateGroupListTable.Rows) {
                    RateGroupList.Add(new ComboboxDataItem(row["ID"].ToString(), row["Title"].ToString()));
                }
                cboRateGroup.DisplayMember = "Value";
                cboRateGroup.ValueMember = "ID";
                cboRateGroup.DataSource = RateGroupList;
                GetRateGroupInfo();
            }
        }

        void HideControls() {
            TabStripMain.Visible = false;
            GroupInfoPanel.Visible = false;
        }

        void ShowControls() {
            TabStripMain.Visible = true;
            GroupInfoPanel.Visible = true;
        }

        void UncheckAllTabs() {
            foreach (ToolStripButton b in TabStripMain.Items) {
                b.Checked = false;
            }
        }

        void RearrangeActiveTab() {
            if (ParentForm.WindowState != FormWindowState.Minimized) {
                List<ToolStripButton> buttons = new List<ToolStripButton>();
                foreach (ToolStripButton b in TabStripMain.Items) {
                    if (b.Checked && b.IsOnOverflow) {
                        buttons.Add(b);
                    }
                }
                foreach (ToolStripButton b in buttons) {
                    TabStripMain.Items.Insert(0, b);
                }
            }
        }

        void AttachTabItemEventhandler(object item) {
            if (item is ToolStripButton) {
                ToolStripButton b = (ToolStripButton)item;
                b.Margin = new Padding(0);
                b.Click -= TabStripItem_Click;
                b.Click += TabStripItem_Click;
            }
        }

        void AddNewTab(string ID, string Value) {
            TabButton itm = new TabButton(ID, Value);
            TabStripMain.Items.Add(itm);
            AttachTabItemEventhandler(itm);
        }

        TabButton GetActiveTab() {
            foreach (TabButton b in TabStripMain.Items) {
                if (b.Checked) {
                    return b;
                }
            }
            return null;
        }

        void SetActiveTab(int index) {
            try {
                object o = TabStripMain.Items[index];
                if (o is TabButton) {
                    TabButton b = (TabButton)o;
                    b.Checked = true;
                    OnTabChanged(b);
                }
            } catch { }
        }

        private void TabStripItem_Click(object sender, EventArgs e) {
            ToolStripButton b = (ToolStripButton)sender;
            if (b.Checked == false) {
                if (b.IsOnOverflow) {
                    TabStripMain.Items.Insert(0, b);
                }
                UncheckAllTabs();
                b.Checked = true;
                if (b is TabButton) {
                    TabButton a = (TabButton)b;
                    OnTabChanged(a);
                }
            }
        }

        #endregion

        #region Local Data Functions

        void LoadVehicleTypes() {

        }

        void SetNoticeContents(string header, string body, string link_text) {
            lblNoticeHeader.Text = header;
            lblNoticeBody.Text = body;
            lnkNoticeLink.Text = link_text;
        }

        //0 - no vehicle types
        //1 - no rate groups.
        int NoticeType = -1;
        void SetNotice(int val) {
            if (val == 0) {
                SetNoticeContents("No Vehicle Types Found", "To add rates, you must specify vehicle types first.", "Click here to manage vehicle type");
                ShowNotice();
            } else if (val == 1) {
                SetNoticeContents("No Rate Groups Found", "There are no rate groups available for this vehicle type.", "Click here to manage rate groups");
                ShowNotice();
            } else {
                HideNotice();
            }
            NoticeType = val;
        }

        //Notice link event handler
        private void lnkNoticeLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (NoticeType == 0) {
                if (ParentForm is MainWindow) {
                    MainWindow w = (MainWindow)ParentForm;
                    VehicleTypesPage p = new VehicleTypesPage();
                    w.LoadPage(p);
                    p.InvokeAdd();
                }
            } else if (NoticeType == 1) {
                MainWindow w = (MainWindow)ParentForm;
                RateGroupPage p = new RateGroupPage();
                w.LoadPage(p);
                p.InvokeAdd();
            }
        }

        void ShowNotice() {
            NoticePanel.Visible = true;
            NoticePanel.BringToFront();
            NoticePanel.Dock = DockStyle.Fill;
            if (DataViewer != null) {
                DataViewer.Enabled = false;
                DataViewer.Visible = false;
            }
        }

        void HideNotice() {
            NoticePanel.Visible = false;
            NoticePanel.SendToBack();
            if (DataViewer != null) {
                DataViewer.Enabled = true;
                DataViewer.Visible = true;
            }
        }

        void GetRateGroupInfo() {
            if (cboRateGroup.SelectedIndex > -1 && RateGroupListTable.Rows.Count > 0) {
                string group_id = cboRateGroup.SelectedValue.ToString();
                if (RateGroupListTable != null) {
                    foreach (DataRow row in RateGroupListTable.Rows) {
                        if (row["ID"].ToString().Equals(group_id)) {
                            lblDriverRate.Text = "Php " + Convert.ToDouble(row["DriverRate"].ToString()).ToString("#,##0.00");
                            lblHelperRate.Text = "Php " + Convert.ToDouble(row["HelperRate"].ToString()).ToString("#,##0.00");
                            if(DataViewer.IsSearching == false) DataViewer.RefreshRecords();
                            break;
                        }
                    }
                }
            }
        }

        void ClearRates() {
            lblDriverRate.Text = lblHelperRate.Text = "Php 0.00";
        }

        #endregion

        #region Window Eventhandlers

        private void RatesPage_Resize(object sender, EventArgs e) {
            RearrangeActiveTab();
        }

        private void RatesPage_Load(object sender, EventArgs e) {
            ComboboxDataItem[] vehicle_type = VehicleTypesPage.QueryVehicleTypes();
            if (vehicle_type.Length <= 0) {
                SetNotice(0);
            }

            bool hasTypes = false;
            foreach (ComboboxDataItem i in vehicle_type) {
                AddNewTab(i.ID, i.Value);
                if (hasTypes == false) hasTypes = true;
            }
            if (hasTypes) {
                SetActiveTab(0);
                ShowControls();
            }

            //add the data view on the page
            DataViewer.Show();
        }

        #endregion

        #region Control Event handlers

        private void cboRateGroup_DropDownClosed(object sender, EventArgs e) {
            if (cboRateGroup.Items.Count > 0) {
                if (DataViewer.IsSearching) {
                    DataViewer.IsSearching = false;
                    DataViewer.ForceHideSearchBox();
                    DataViewer.RefreshRecords();
                }
            }
        }

        private void cboRateGroup_SelectedIndexChanged(object sender, EventArgs e) {
            GetRateGroupInfo();
        }

        #endregion

    }

}
