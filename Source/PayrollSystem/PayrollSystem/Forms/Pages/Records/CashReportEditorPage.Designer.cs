﻿namespace PayrollSystem {
    partial class CashReportEditorPage {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashReportEditorPage));
            this.SplitContainerMain = new System.Windows.Forms.SplitContainer();
            this.SectionsDataGrid = new System.Windows.Forms.DataGridView();
            this.SectionsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.SectionsToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnSectionAdd = new System.Windows.Forms.ToolStripButton();
            this.btnSectionDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.lblSearch = new System.Windows.Forms.ToolStripLabel();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnBack = new System.Windows.Forms.ToolStripButton();
            this.btnExport = new System.Windows.Forms.ToolStripButton();
            this.btnImportCSV = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btnSearch = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearRefreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ColumnsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.freezeUnfreezeColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditorPanel = new PayrollSystem.BufferedPanel();
            this.ParticularsSplitPanel = new System.Windows.Forms.SplitContainer();
            this.ParticularsDataGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartingBalancePanel = new PayrollSystem.BufferedPanel();
            this.lblStartingBal = new System.Windows.Forms.Label();
            this.bufferedPanel6 = new PayrollSystem.BufferedPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.TotalsDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalsHeaderPanel = new PayrollSystem.BufferedPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.bufferedPanel5 = new PayrollSystem.BufferedPanel();
            this.EditorToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.btnAddParticular = new System.Windows.Forms.ToolStripButton();
            this.btnEditParticular = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteParticular = new System.Windows.Forms.ToolStripButton();
            this.btnAddStartingBal = new System.Windows.Forms.ToolStripButton();
            this.bufferedPanel2 = new PayrollSystem.BufferedPanel();
            this.rdExpenses = new System.Windows.Forms.RadioButton();
            this.rdIncome = new System.Windows.Forms.RadioButton();
            this.bufferedPanel3 = new PayrollSystem.BufferedPanel();
            this.InfoPanel = new PayrollSystem.BufferedPanel();
            this.bufferedPanel1 = new PayrollSystem.BufferedPanel();
            this.TotalsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTotalIncome = new System.Windows.Forms.Label();
            this.lblTotalExpenses = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblEndingBal = new System.Windows.Forms.Label();
            this.CRInfoPanel = new PayrollSystem.BufferedPanel();
            this.lblCashReportTitle = new System.Windows.Forms.Label();
            this.lblDatePeriod = new System.Windows.Forms.Label();
            this.lblSectionTitle = new System.Windows.Forms.Label();
            this.pnlToggleHeader = new System.Windows.Forms.Panel();
            this.btnToggleHeader = new System.Windows.Forms.RadioButton();
            this.NoSectionNoticePanel = new PayrollSystem.BufferedPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNoSections = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMain)).BeginInit();
            this.SplitContainerMain.Panel1.SuspendLayout();
            this.SplitContainerMain.Panel2.SuspendLayout();
            this.SplitContainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SectionsDataGrid)).BeginInit();
            this.SectionsContextMenuStrip.SuspendLayout();
            this.SectionsToolStrip.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.ColumnsContextMenuStrip.SuspendLayout();
            this.EditorPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParticularsSplitPanel)).BeginInit();
            this.ParticularsSplitPanel.Panel1.SuspendLayout();
            this.ParticularsSplitPanel.Panel2.SuspendLayout();
            this.ParticularsSplitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParticularsDataGrid)).BeginInit();
            this.StartingBalancePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalsDataGrid)).BeginInit();
            this.TotalsHeaderPanel.SuspendLayout();
            this.EditorToolStrip.SuspendLayout();
            this.bufferedPanel2.SuspendLayout();
            this.InfoPanel.SuspendLayout();
            this.TotalsPanel.SuspendLayout();
            this.CRInfoPanel.SuspendLayout();
            this.pnlToggleHeader.SuspendLayout();
            this.NoSectionNoticePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitContainerMain
            // 
            this.SplitContainerMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.SplitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainerMain.Location = new System.Drawing.Point(0, 37);
            this.SplitContainerMain.Name = "SplitContainerMain";
            // 
            // SplitContainerMain.Panel1
            // 
            this.SplitContainerMain.Panel1.Controls.Add(this.SectionsDataGrid);
            this.SplitContainerMain.Panel1.Controls.Add(this.label3);
            this.SplitContainerMain.Panel1.Controls.Add(this.SectionsToolStrip);
            this.SplitContainerMain.Panel1MinSize = 130;
            // 
            // SplitContainerMain.Panel2
            // 
            this.SplitContainerMain.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.SplitContainerMain.Panel2.Controls.Add(this.EditorPanel);
            this.SplitContainerMain.Panel2.Controls.Add(this.NoSectionNoticePanel);
            this.SplitContainerMain.Size = new System.Drawing.Size(733, 412);
            this.SplitContainerMain.SplitterDistance = 157;
            this.SplitContainerMain.SplitterWidth = 1;
            this.SplitContainerMain.TabIndex = 0;
            // 
            // SectionsDataGrid
            // 
            this.SectionsDataGrid.AllowUserToAddRows = false;
            this.SectionsDataGrid.AllowUserToDeleteRows = false;
            this.SectionsDataGrid.AllowUserToResizeColumns = false;
            this.SectionsDataGrid.AllowUserToResizeRows = false;
            this.SectionsDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.SectionsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SectionsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.SectionsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SectionsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SectionsDataGrid.ColumnHeadersHeight = 28;
            this.SectionsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SectionsDataGrid.ColumnHeadersVisible = false;
            this.SectionsDataGrid.ContextMenuStrip = this.SectionsContextMenuStrip;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(131)))), ((int)(((byte)(137)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SectionsDataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.SectionsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SectionsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.SectionsDataGrid.Location = new System.Drawing.Point(0, 32);
            this.SectionsDataGrid.MultiSelect = false;
            this.SectionsDataGrid.Name = "SectionsDataGrid";
            this.SectionsDataGrid.ReadOnly = true;
            this.SectionsDataGrid.RowHeadersVisible = false;
            this.SectionsDataGrid.RowTemplate.Height = 32;
            this.SectionsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SectionsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SectionsDataGrid.ShowCellErrors = false;
            this.SectionsDataGrid.ShowEditingIcon = false;
            this.SectionsDataGrid.Size = new System.Drawing.Size(157, 355);
            this.SectionsDataGrid.TabIndex = 7;
            this.SectionsDataGrid.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.SectionsDataGrid_RowPrePaint);
            this.SectionsDataGrid.SelectionChanged += new System.EventHandler(this.SectionsDataGrid_SelectionChanged);
            this.SectionsDataGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SectionsDataGrid_MouseDown);
            // 
            // SectionsContextMenuStrip
            // 
            this.SectionsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSectionToolStripMenuItem,
            this.deleteSectionToolStripMenuItem});
            this.SectionsContextMenuStrip.Name = "SectionsContextMenuStrip";
            this.SectionsContextMenuStrip.Size = new System.Drawing.Size(150, 48);
            this.SectionsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.SectionsContextMenuStrip_Opening);
            // 
            // addSectionToolStripMenuItem
            // 
            this.addSectionToolStripMenuItem.Name = "addSectionToolStripMenuItem";
            this.addSectionToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.addSectionToolStripMenuItem.Text = "Add Section...";
            this.addSectionToolStripMenuItem.Click += new System.EventHandler(this.addSectionToolStripMenuItem_Click);
            // 
            // deleteSectionToolStripMenuItem
            // 
            this.deleteSectionToolStripMenuItem.Name = "deleteSectionToolStripMenuItem";
            this.deleteSectionToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteSectionToolStripMenuItem.Text = "Delete Section";
            this.deleteSectionToolStripMenuItem.Click += new System.EventHandler(this.deleteSectionToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(157, 32);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sections";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SectionsToolStrip
            // 
            this.SectionsToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SectionsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.SectionsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSectionAdd,
            this.btnSectionDelete});
            this.SectionsToolStrip.Location = new System.Drawing.Point(0, 387);
            this.SectionsToolStrip.Name = "SectionsToolStrip";
            this.SectionsToolStrip.Padding = new System.Windows.Forms.Padding(3, 0, 1, 0);
            this.SectionsToolStrip.Size = new System.Drawing.Size(157, 25);
            this.SectionsToolStrip.TabIndex = 9;
            this.SectionsToolStrip.Text = "toolStrip1";
            // 
            // btnSectionAdd
            // 
            this.btnSectionAdd.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSectionAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSectionAdd.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSectionAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSectionAdd.Image = global::PayrollSystem.Properties.Resources.add02;
            this.btnSectionAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSectionAdd.Name = "btnSectionAdd";
            this.btnSectionAdd.Size = new System.Drawing.Size(23, 22);
            this.btnSectionAdd.Text = "Add Section";
            this.btnSectionAdd.Click += new System.EventHandler(this.btnSectionAdd_Click);
            // 
            // btnSectionDelete
            // 
            this.btnSectionDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSectionDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSectionDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnSectionDelete.Image = global::PayrollSystem.Properties.Resources.delete02;
            this.btnSectionDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSectionDelete.Name = "btnSectionDelete";
            this.btnSectionDelete.Size = new System.Drawing.Size(23, 22);
            this.btnSectionDelete.Text = "Delete Section";
            this.btnSectionDelete.Click += new System.EventHandler(this.btnSectionDelete_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 33);
            // 
            // txtSearch
            // 
            this.txtSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.txtSearch.AutoSize = false;
            this.txtSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.txtSearch.Margin = new System.Windows.Forms.Padding(0);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.txtSearch.Padding = new System.Windows.Forms.Padding(2);
            this.txtSearch.Size = new System.Drawing.Size(72, 21);
            this.txtSearch.Visible = false;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // lblSearch
            // 
            this.lblSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblSearch.AutoSize = false;
            this.lblSearch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.lblSearch.Size = new System.Drawing.Size(52, 24);
            this.lblSearch.Text = "Search:";
            this.lblSearch.ToolTipText = "Search (Ctrl + F)";
            this.lblSearch.Visible = false;
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBack,
            this.toolStripSeparator1,
            this.btnExport,
            this.btnImportCSV,
            this.btnRefresh,
            this.toolStripButton1,
            this.btnSearch,
            this.txtSearch,
            this.lblSearch});
            this.MainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Padding = new System.Windows.Forms.Padding(5, 2, 1, 2);
            this.MainToolStrip.Size = new System.Drawing.Size(733, 37);
            this.MainToolStrip.TabIndex = 1;
            this.MainToolStrip.Text = "MainToolStrip";
            // 
            // btnBack
            // 
            this.btnBack.AutoToolTip = false;
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnBack.Image = global::PayrollSystem.Properties.Resources.back;
            this.btnBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBack.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(49, 30);
            this.btnBack.Text = "Back";
            this.btnBack.ToolTipText = "Back to Cash Reports";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnExport
            // 
            this.btnExport.AutoToolTip = false;
            this.btnExport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnExport.Image = global::PayrollSystem.Properties.Resources.save;
            this.btnExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExport.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(59, 30);
            this.btnExport.Text = "Export";
            this.btnExport.ToolTipText = "Export (Ctrl + S)";
            this.btnExport.Visible = false;
            // 
            // btnImportCSV
            // 
            this.btnImportCSV.AutoToolTip = false;
            this.btnImportCSV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportCSV.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnImportCSV.Image = global::PayrollSystem.Properties.Resources.import;
            this.btnImportCSV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImportCSV.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnImportCSV.Name = "btnImportCSV";
            this.btnImportCSV.Size = new System.Drawing.Size(59, 30);
            this.btnImportCSV.Text = "Import";
            this.btnImportCSV.ToolTipText = "Import (Ctrl + I)";
            this.btnImportCSV.Click += new System.EventHandler(this.btnImportCSV_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AutoToolTip = false;
            this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnRefresh.Image = global::PayrollSystem.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 30);
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.ToolTipText = "Refresh (F5 or Shift + F5)";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripButton1.Enabled = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(1, 30);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // btnSearch
            // 
            this.btnSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSearch.AutoSize = false;
            this.btnSearch.AutoToolTip = false;
            this.btnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSearch.Image = global::PayrollSystem.Properties.Resources.search;
            this.btnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(23, 28);
            this.btnSearch.Text = "Search";
            this.btnSearch.ToolTipText = "Search (Ctrl + F)";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.clearRefreshToolStripMenuItem,
            this.importToolStripMenuItem,
            this.controlsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(733, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // clearRefreshToolStripMenuItem
            // 
            this.clearRefreshToolStripMenuItem.Name = "clearRefreshToolStripMenuItem";
            this.clearRefreshToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
            this.clearRefreshToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.clearRefreshToolStripMenuItem.Text = "Clear Refresh";
            this.clearRefreshToolStripMenuItem.Click += new System.EventHandler(this.clearRefreshToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.importToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.btnImportCSV_Click);
            // 
            // controlsToolStripMenuItem
            // 
            this.controlsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.controlsToolStripMenuItem.Name = "controlsToolStripMenuItem";
            this.controlsToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.controlsToolStripMenuItem.Text = "Controls";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.addToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.editToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // ColumnsContextMenuStrip
            // 
            this.ColumnsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.freezeUnfreezeColumnToolStripMenuItem});
            this.ColumnsContextMenuStrip.Name = "ColumnsContextMenuStrip";
            this.ColumnsContextMenuStrip.Size = new System.Drawing.Size(205, 26);
            // 
            // freezeUnfreezeColumnToolStripMenuItem
            // 
            this.freezeUnfreezeColumnToolStripMenuItem.Name = "freezeUnfreezeColumnToolStripMenuItem";
            this.freezeUnfreezeColumnToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.freezeUnfreezeColumnToolStripMenuItem.Text = "Freeze/Unfreeze Column";
            this.freezeUnfreezeColumnToolStripMenuItem.Click += new System.EventHandler(this.freezeUnfreezeColumnToolStripMenuItem_Click);
            // 
            // EditorPanel
            // 
            this.EditorPanel.BorderColor = System.Drawing.Color.Black;
            this.EditorPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.EditorPanel.Controls.Add(this.ParticularsSplitPanel);
            this.EditorPanel.Controls.Add(this.EditorToolStrip);
            this.EditorPanel.Controls.Add(this.bufferedPanel2);
            this.EditorPanel.Controls.Add(this.InfoPanel);
            this.EditorPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorPanel.Location = new System.Drawing.Point(0, 0);
            this.EditorPanel.Name = "EditorPanel";
            this.EditorPanel.Size = new System.Drawing.Size(575, 412);
            this.EditorPanel.TabIndex = 0;
            this.EditorPanel.Visible = false;
            // 
            // ParticularsSplitPanel
            // 
            this.ParticularsSplitPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.ParticularsSplitPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParticularsSplitPanel.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.ParticularsSplitPanel.Location = new System.Drawing.Point(0, 137);
            this.ParticularsSplitPanel.Name = "ParticularsSplitPanel";
            // 
            // ParticularsSplitPanel.Panel1
            // 
            this.ParticularsSplitPanel.Panel1.Controls.Add(this.ParticularsDataGrid);
            this.ParticularsSplitPanel.Panel1.Controls.Add(this.StartingBalancePanel);
            // 
            // ParticularsSplitPanel.Panel2
            // 
            this.ParticularsSplitPanel.Panel2.BackColor = System.Drawing.Color.White;
            this.ParticularsSplitPanel.Panel2.Controls.Add(this.TotalsDataGrid);
            this.ParticularsSplitPanel.Panel2.Controls.Add(this.TotalsHeaderPanel);
            this.ParticularsSplitPanel.Panel2MinSize = 140;
            this.ParticularsSplitPanel.Size = new System.Drawing.Size(575, 275);
            this.ParticularsSplitPanel.SplitterDistance = 372;
            this.ParticularsSplitPanel.SplitterWidth = 1;
            this.ParticularsSplitPanel.TabIndex = 18;
            // 
            // ParticularsDataGrid
            // 
            this.ParticularsDataGrid.AllowUserToAddRows = false;
            this.ParticularsDataGrid.AllowUserToDeleteRows = false;
            this.ParticularsDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.ParticularsDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.ParticularsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.ParticularsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ParticularsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.ParticularsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(254)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ParticularsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.ParticularsDataGrid.ColumnHeadersHeight = 28;
            this.ParticularsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ParticularsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 8F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ParticularsDataGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this.ParticularsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParticularsDataGrid.EnableHeadersVisualStyles = false;
            this.ParticularsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.ParticularsDataGrid.Location = new System.Drawing.Point(0, 28);
            this.ParticularsDataGrid.MultiSelect = false;
            this.ParticularsDataGrid.Name = "ParticularsDataGrid";
            this.ParticularsDataGrid.ReadOnly = true;
            this.ParticularsDataGrid.RowHeadersVisible = false;
            this.ParticularsDataGrid.RowTemplate.Height = 28;
            this.ParticularsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ParticularsDataGrid.ShowCellErrors = false;
            this.ParticularsDataGrid.ShowEditingIcon = false;
            this.ParticularsDataGrid.Size = new System.Drawing.Size(372, 247);
            this.ParticularsDataGrid.TabIndex = 14;
            this.ParticularsDataGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.ParticularsDataGrid_CellPainting);
            this.ParticularsDataGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ParticularsDataGrid_ColumnHeaderMouseClick);
            this.ParticularsDataGrid.ColumnStateChanged += new System.Windows.Forms.DataGridViewColumnStateChangedEventHandler(this.ParticularsDataGrid_ColumnStateChanged);
            this.ParticularsDataGrid.SelectionChanged += new System.EventHandler(this.ParticularsDataGrid_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 139;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 139;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 139;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // StartingBalancePanel
            // 
            this.StartingBalancePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.StartingBalancePanel.BackgroundImage = global::PayrollSystem.Properties.Resources.panel_bg;
            this.StartingBalancePanel.BorderColor = System.Drawing.Color.Black;
            this.StartingBalancePanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.StartingBalancePanel.Controls.Add(this.lblStartingBal);
            this.StartingBalancePanel.Controls.Add(this.bufferedPanel6);
            this.StartingBalancePanel.Controls.Add(this.label7);
            this.StartingBalancePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.StartingBalancePanel.Location = new System.Drawing.Point(0, 0);
            this.StartingBalancePanel.Name = "StartingBalancePanel";
            this.StartingBalancePanel.Size = new System.Drawing.Size(372, 28);
            this.StartingBalancePanel.TabIndex = 15;
            // 
            // lblStartingBal
            // 
            this.lblStartingBal.AutoEllipsis = true;
            this.lblStartingBal.BackColor = System.Drawing.Color.Transparent;
            this.lblStartingBal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblStartingBal.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartingBal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblStartingBal.Location = new System.Drawing.Point(217, 0);
            this.lblStartingBal.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.lblStartingBal.Name = "lblStartingBal";
            this.lblStartingBal.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblStartingBal.Size = new System.Drawing.Size(155, 27);
            this.lblStartingBal.TabIndex = 14;
            this.lblStartingBal.Text = "Php 999,999,999.00";
            this.lblStartingBal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bufferedPanel6
            // 
            this.bufferedPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.bufferedPanel6.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel6.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel6.Location = new System.Drawing.Point(0, 27);
            this.bufferedPanel6.Name = "bufferedPanel6";
            this.bufferedPanel6.Size = new System.Drawing.Size(372, 1);
            this.bufferedPanel6.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label7.Location = new System.Drawing.Point(3, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Starting Balance:";
            // 
            // TotalsDataGrid
            // 
            this.TotalsDataGrid.AllowUserToAddRows = false;
            this.TotalsDataGrid.AllowUserToDeleteRows = false;
            this.TotalsDataGrid.AllowUserToResizeRows = false;
            this.TotalsDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.TotalsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TotalsDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.TotalsDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TotalsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.TotalsDataGrid.ColumnHeadersHeight = 28;
            this.TotalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TotalsDataGrid.ColumnHeadersVisible = false;
            this.TotalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle7.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TotalsDataGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.TotalsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TotalsDataGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.TotalsDataGrid.Location = new System.Drawing.Point(0, 31);
            this.TotalsDataGrid.MultiSelect = false;
            this.TotalsDataGrid.Name = "TotalsDataGrid";
            this.TotalsDataGrid.ReadOnly = true;
            this.TotalsDataGrid.RowHeadersVisible = false;
            this.TotalsDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TotalsDataGrid.ShowCellErrors = false;
            this.TotalsDataGrid.ShowEditingIcon = false;
            this.TotalsDataGrid.Size = new System.Drawing.Size(202, 244);
            this.TotalsDataGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Totals";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // TotalsHeaderPanel
            // 
            this.TotalsHeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.TotalsHeaderPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.TotalsHeaderPanel.Controls.Add(this.label4);
            this.TotalsHeaderPanel.Controls.Add(this.bufferedPanel5);
            this.TotalsHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TotalsHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.TotalsHeaderPanel.Name = "TotalsHeaderPanel";
            this.TotalsHeaderPanel.Size = new System.Drawing.Size(202, 31);
            this.TotalsHeaderPanel.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(202, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "Totals";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bufferedPanel5
            // 
            this.bufferedPanel5.BackColor = System.Drawing.Color.Gainsboro;
            this.bufferedPanel5.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel5.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel5.Location = new System.Drawing.Point(0, 30);
            this.bufferedPanel5.Name = "bufferedPanel5";
            this.bufferedPanel5.Size = new System.Drawing.Size(202, 1);
            this.bufferedPanel5.TabIndex = 0;
            // 
            // EditorToolStrip
            // 
            this.EditorToolStrip.AutoSize = false;
            this.EditorToolStrip.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditorToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.EditorToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5,
            this.btnAddParticular,
            this.btnEditParticular,
            this.btnDeleteParticular,
            this.btnAddStartingBal});
            this.EditorToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.EditorToolStrip.Location = new System.Drawing.Point(0, 109);
            this.EditorToolStrip.Name = "EditorToolStrip";
            this.EditorToolStrip.Padding = new System.Windows.Forms.Padding(5, 0, 1, 0);
            this.EditorToolStrip.Size = new System.Drawing.Size(575, 28);
            this.EditorToolStrip.TabIndex = 17;
            this.EditorToolStrip.Text = "toolStrip1";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton5.AutoSize = false;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripButton5.Enabled = false;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(1, 30);
            this.toolStripButton5.Text = "toolStripButton1";
            // 
            // btnAddParticular
            // 
            this.btnAddParticular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnAddParticular.Image = global::PayrollSystem.Properties.Resources.add02;
            this.btnAddParticular.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddParticular.Name = "btnAddParticular";
            this.btnAddParticular.Size = new System.Drawing.Size(49, 25);
            this.btnAddParticular.Text = "Add";
            this.btnAddParticular.Click += new System.EventHandler(this.btnAddParticular_Click);
            // 
            // btnEditParticular
            // 
            this.btnEditParticular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnEditParticular.Image = global::PayrollSystem.Properties.Resources.edit;
            this.btnEditParticular.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditParticular.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.btnEditParticular.Name = "btnEditParticular";
            this.btnEditParticular.Size = new System.Drawing.Size(48, 25);
            this.btnEditParticular.Text = "Edit";
            this.btnEditParticular.Click += new System.EventHandler(this.btnEditParticular_Click);
            // 
            // btnDeleteParticular
            // 
            this.btnDeleteParticular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnDeleteParticular.Image = global::PayrollSystem.Properties.Resources.delete02;
            this.btnDeleteParticular.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteParticular.Name = "btnDeleteParticular";
            this.btnDeleteParticular.Size = new System.Drawing.Size(64, 25);
            this.btnDeleteParticular.Text = "Delete";
            this.btnDeleteParticular.Click += new System.EventHandler(this.btnDeleteParticular_Click);
            // 
            // btnAddStartingBal
            // 
            this.btnAddStartingBal.AutoToolTip = false;
            this.btnAddStartingBal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.btnAddStartingBal.Image = global::PayrollSystem.Properties.Resources.add_folder;
            this.btnAddStartingBal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddStartingBal.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.btnAddStartingBal.Name = "btnAddStartingBal";
            this.btnAddStartingBal.Size = new System.Drawing.Size(121, 25);
            this.btnAddStartingBal.Text = "Starting Balance";
            this.btnAddStartingBal.ToolTipText = "Specifies the starting balance based from previous cash reports.";
            this.btnAddStartingBal.Click += new System.EventHandler(this.btnAddStartingBal_Click);
            // 
            // bufferedPanel2
            // 
            this.bufferedPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.bufferedPanel2.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel2.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel2.Controls.Add(this.rdExpenses);
            this.bufferedPanel2.Controls.Add(this.rdIncome);
            this.bufferedPanel2.Controls.Add(this.bufferedPanel3);
            this.bufferedPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bufferedPanel2.Location = new System.Drawing.Point(0, 75);
            this.bufferedPanel2.Name = "bufferedPanel2";
            this.bufferedPanel2.Size = new System.Drawing.Size(575, 34);
            this.bufferedPanel2.TabIndex = 16;
            // 
            // rdExpenses
            // 
            this.rdExpenses.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rdExpenses.Appearance = System.Windows.Forms.Appearance.Button;
            this.rdExpenses.FlatAppearance.BorderSize = 0;
            this.rdExpenses.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.rdExpenses.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.rdExpenses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdExpenses.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdExpenses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.rdExpenses.Location = new System.Drawing.Point(289, 3);
            this.rdExpenses.Name = "rdExpenses";
            this.rdExpenses.Size = new System.Drawing.Size(137, 29);
            this.rdExpenses.TabIndex = 5;
            this.rdExpenses.TabStop = true;
            this.rdExpenses.Text = "Expenses";
            this.rdExpenses.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdExpenses.UseVisualStyleBackColor = true;
            this.rdExpenses.CheckedChanged += new System.EventHandler(this.NavRadio_CheckedChanged);
            // 
            // rdIncome
            // 
            this.rdIncome.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rdIncome.Appearance = System.Windows.Forms.Appearance.Button;
            this.rdIncome.FlatAppearance.BorderSize = 0;
            this.rdIncome.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.rdIncome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.rdIncome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdIncome.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdIncome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.rdIncome.Location = new System.Drawing.Point(149, 3);
            this.rdIncome.Name = "rdIncome";
            this.rdIncome.Size = new System.Drawing.Size(137, 29);
            this.rdIncome.TabIndex = 4;
            this.rdIncome.TabStop = true;
            this.rdIncome.Text = "Fund Transfer";
            this.rdIncome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdIncome.UseVisualStyleBackColor = true;
            this.rdIncome.CheckedChanged += new System.EventHandler(this.NavRadio_CheckedChanged);
            // 
            // bufferedPanel3
            // 
            this.bufferedPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.bufferedPanel3.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel3.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel3.Location = new System.Drawing.Point(0, 33);
            this.bufferedPanel3.Name = "bufferedPanel3";
            this.bufferedPanel3.Size = new System.Drawing.Size(575, 1);
            this.bufferedPanel3.TabIndex = 1;
            // 
            // InfoPanel
            // 
            this.InfoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.InfoPanel.BorderColor = System.Drawing.Color.Black;
            this.InfoPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.InfoPanel.Controls.Add(this.bufferedPanel1);
            this.InfoPanel.Controls.Add(this.TotalsPanel);
            this.InfoPanel.Controls.Add(this.CRInfoPanel);
            this.InfoPanel.Controls.Add(this.pnlToggleHeader);
            this.InfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(575, 75);
            this.InfoPanel.TabIndex = 15;
            // 
            // bufferedPanel1
            // 
            this.bufferedPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.bufferedPanel1.BorderColor = System.Drawing.Color.Black;
            this.bufferedPanel1.Borders = System.Windows.Forms.AnchorStyles.None;
            this.bufferedPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bufferedPanel1.Location = new System.Drawing.Point(0, 74);
            this.bufferedPanel1.Name = "bufferedPanel1";
            this.bufferedPanel1.Size = new System.Drawing.Size(575, 1);
            this.bufferedPanel1.TabIndex = 0;
            // 
            // TotalsPanel
            // 
            this.TotalsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TotalsPanel.ColumnCount = 2;
            this.TotalsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.02985F));
            this.TotalsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.97015F));
            this.TotalsPanel.Controls.Add(this.label9, 0, 2);
            this.TotalsPanel.Controls.Add(this.lblTotalIncome, 0, 1);
            this.TotalsPanel.Controls.Add(this.lblTotalExpenses, 0, 2);
            this.TotalsPanel.Controls.Add(this.label6, 0, 0);
            this.TotalsPanel.Controls.Add(this.label5, 0, 1);
            this.TotalsPanel.Controls.Add(this.lblEndingBal, 0, 0);
            this.TotalsPanel.Location = new System.Drawing.Point(284, 9);
            this.TotalsPanel.Name = "TotalsPanel";
            this.TotalsPanel.RowCount = 3;
            this.TotalsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TotalsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TotalsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TotalsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TotalsPanel.Size = new System.Drawing.Size(261, 62);
            this.TotalsPanel.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label9.Location = new System.Drawing.Point(1, 43);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "Expenses:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalIncome
            // 
            this.lblTotalIncome.AutoEllipsis = true;
            this.lblTotalIncome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalIncome.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalIncome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblTotalIncome.Location = new System.Drawing.Point(115, 22);
            this.lblTotalIncome.Margin = new System.Windows.Forms.Padding(1);
            this.lblTotalIncome.Name = "lblTotalIncome";
            this.lblTotalIncome.Size = new System.Drawing.Size(145, 19);
            this.lblTotalIncome.TabIndex = 16;
            this.lblTotalIncome.Text = "Php 999,999,999.00";
            this.lblTotalIncome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalExpenses
            // 
            this.lblTotalExpenses.AutoEllipsis = true;
            this.lblTotalExpenses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalExpenses.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalExpenses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblTotalExpenses.Location = new System.Drawing.Point(115, 43);
            this.lblTotalExpenses.Margin = new System.Windows.Forms.Padding(1);
            this.lblTotalExpenses.Name = "lblTotalExpenses";
            this.lblTotalExpenses.Size = new System.Drawing.Size(145, 18);
            this.lblTotalExpenses.TabIndex = 15;
            this.lblTotalExpenses.Text = "Php 999,999,999.00";
            this.lblTotalExpenses.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label6.Location = new System.Drawing.Point(1, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 19);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ending Balance:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label5.Location = new System.Drawing.Point(1, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 19);
            this.label5.TabIndex = 13;
            this.label5.Text = "Fund Transfer:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblEndingBal
            // 
            this.lblEndingBal.AutoEllipsis = true;
            this.lblEndingBal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEndingBal.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndingBal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblEndingBal.Location = new System.Drawing.Point(115, 1);
            this.lblEndingBal.Margin = new System.Windows.Forms.Padding(1);
            this.lblEndingBal.Name = "lblEndingBal";
            this.lblEndingBal.Size = new System.Drawing.Size(145, 19);
            this.lblEndingBal.TabIndex = 12;
            this.lblEndingBal.Text = "Php 999,999,999.00";
            this.lblEndingBal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CRInfoPanel
            // 
            this.CRInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CRInfoPanel.BorderColor = System.Drawing.Color.Black;
            this.CRInfoPanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.CRInfoPanel.Controls.Add(this.lblCashReportTitle);
            this.CRInfoPanel.Controls.Add(this.lblDatePeriod);
            this.CRInfoPanel.Controls.Add(this.lblSectionTitle);
            this.CRInfoPanel.Location = new System.Drawing.Point(5, 9);
            this.CRInfoPanel.Name = "CRInfoPanel";
            this.CRInfoPanel.Size = new System.Drawing.Size(274, 62);
            this.CRInfoPanel.TabIndex = 13;
            // 
            // lblCashReportTitle
            // 
            this.lblCashReportTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashReportTitle.AutoEllipsis = true;
            this.lblCashReportTitle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashReportTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblCashReportTitle.Location = new System.Drawing.Point(6, 5);
            this.lblCashReportTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.lblCashReportTitle.Name = "lblCashReportTitle";
            this.lblCashReportTitle.Size = new System.Drawing.Size(240, 18);
            this.lblCashReportTitle.TabIndex = 10;
            this.lblCashReportTitle.Text = "Cash Report Title Goes Here";
            // 
            // lblDatePeriod
            // 
            this.lblDatePeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDatePeriod.AutoEllipsis = true;
            this.lblDatePeriod.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatePeriod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.lblDatePeriod.Location = new System.Drawing.Point(6, 23);
            this.lblDatePeriod.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.lblDatePeriod.Name = "lblDatePeriod";
            this.lblDatePeriod.Size = new System.Drawing.Size(255, 13);
            this.lblDatePeriod.TabIndex = 12;
            this.lblDatePeriod.Text = "Jan 1, 2016 - Dec 31, 3000";
            // 
            // lblSectionTitle
            // 
            this.lblSectionTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSectionTitle.AutoEllipsis = true;
            this.lblSectionTitle.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSectionTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.lblSectionTitle.Location = new System.Drawing.Point(6, 45);
            this.lblSectionTitle.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.lblSectionTitle.Name = "lblSectionTitle";
            this.lblSectionTitle.Size = new System.Drawing.Size(255, 13);
            this.lblSectionTitle.TabIndex = 11;
            this.lblSectionTitle.Text = "Section Title Goes Here";
            // 
            // pnlToggleHeader
            // 
            this.pnlToggleHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToggleHeader.Controls.Add(this.btnToggleHeader);
            this.pnlToggleHeader.Location = new System.Drawing.Point(549, 13);
            this.pnlToggleHeader.Name = "pnlToggleHeader";
            this.pnlToggleHeader.Size = new System.Drawing.Size(20, 20);
            this.pnlToggleHeader.TabIndex = 15;
            // 
            // btnToggleHeader
            // 
            this.btnToggleHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnToggleHeader.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnToggleHeader.AutoCheck = false;
            this.btnToggleHeader.FlatAppearance.BorderSize = 0;
            this.btnToggleHeader.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnToggleHeader.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.btnToggleHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToggleHeader.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToggleHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.btnToggleHeader.Location = new System.Drawing.Point(-7, -5);
            this.btnToggleHeader.Name = "btnToggleHeader";
            this.btnToggleHeader.Size = new System.Drawing.Size(36, 35);
            this.btnToggleHeader.TabIndex = 15;
            this.btnToggleHeader.TabStop = true;
            this.btnToggleHeader.Text = "p";
            this.btnToggleHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.btnToggleHeader, "Toggle header view");
            this.btnToggleHeader.UseVisualStyleBackColor = true;
            this.btnToggleHeader.Click += new System.EventHandler(this.btnToggleHeader_Click);
            // 
            // NoSectionNoticePanel
            // 
            this.NoSectionNoticePanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NoSectionNoticePanel.BorderColor = System.Drawing.Color.Black;
            this.NoSectionNoticePanel.Borders = System.Windows.Forms.AnchorStyles.None;
            this.NoSectionNoticePanel.Controls.Add(this.label2);
            this.NoSectionNoticePanel.Controls.Add(this.lblNoSections);
            this.NoSectionNoticePanel.Controls.Add(this.pictureBox1);
            this.NoSectionNoticePanel.Controls.Add(this.label1);
            this.NoSectionNoticePanel.Location = new System.Drawing.Point(138, 114);
            this.NoSectionNoticePanel.Name = "NoSectionNoticePanel";
            this.NoSectionNoticePanel.Size = new System.Drawing.Size(328, 148);
            this.NoSectionNoticePanel.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label2.Location = new System.Drawing.Point(104, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "on the sidebar to begin.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNoSections
            // 
            this.lblNoSections.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoSections.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.lblNoSections.Location = new System.Drawing.Point(23, 40);
            this.lblNoSections.Name = "lblNoSections";
            this.lblNoSections.Size = new System.Drawing.Size(282, 26);
            this.lblNoSections.TabIndex = 0;
            this.lblNoSections.Text = "No Sections Found";
            this.lblNoSections.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PayrollSystem.Properties.Resources.add02;
            this.pictureBox1.Location = new System.Drawing.Point(86, 93);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.label1.Location = new System.Drawing.Point(44, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Add sections to this cash report. Press";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CashReportEditorPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(733, 449);
            this.Controls.Add(this.SplitContainerMain);
            this.Controls.Add(this.MainToolStrip);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "CashReportEditorPage";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Edit Cash Report";
            this.Load += new System.EventHandler(this.CashReportEditorPage_Load);
            this.Resize += new System.EventHandler(this.CashReportEditorPage_Resize);
            this.SplitContainerMain.Panel1.ResumeLayout(false);
            this.SplitContainerMain.Panel1.PerformLayout();
            this.SplitContainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMain)).EndInit();
            this.SplitContainerMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SectionsDataGrid)).EndInit();
            this.SectionsContextMenuStrip.ResumeLayout(false);
            this.SectionsToolStrip.ResumeLayout(false);
            this.SectionsToolStrip.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ColumnsContextMenuStrip.ResumeLayout(false);
            this.EditorPanel.ResumeLayout(false);
            this.ParticularsSplitPanel.Panel1.ResumeLayout(false);
            this.ParticularsSplitPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParticularsSplitPanel)).EndInit();
            this.ParticularsSplitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParticularsDataGrid)).EndInit();
            this.StartingBalancePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TotalsDataGrid)).EndInit();
            this.TotalsHeaderPanel.ResumeLayout(false);
            this.EditorToolStrip.ResumeLayout(false);
            this.EditorToolStrip.PerformLayout();
            this.bufferedPanel2.ResumeLayout(false);
            this.InfoPanel.ResumeLayout(false);
            this.TotalsPanel.ResumeLayout(false);
            this.CRInfoPanel.ResumeLayout(false);
            this.pnlToggleHeader.ResumeLayout(false);
            this.NoSectionNoticePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer SplitContainerMain;
        internal System.Windows.Forms.DataGridView SectionsDataGrid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStrip SectionsToolStrip;
        private System.Windows.Forms.ToolStripButton btnSectionAdd;
        private System.Windows.Forms.ToolStripButton btnSectionDelete;
        private System.Windows.Forms.ContextMenuStrip SectionsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addSectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSectionToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblNoSections;
        private System.Windows.Forms.Label label1;
        private BufferedPanel EditorPanel;
        private BufferedPanel NoSectionNoticePanel;
        private System.Windows.Forms.ToolStripButton btnBack;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnExport;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton btnSearch;
        protected internal System.Windows.Forms.ToolStripTextBox txtSearch;
        private System.Windows.Forms.ToolStripLabel lblSearch;
        public System.Windows.Forms.ToolStrip MainToolStrip;
        private BufferedPanel InfoPanel;
        private System.Windows.Forms.Label lblDatePeriod;
        private System.Windows.Forms.Label lblSectionTitle;
        private System.Windows.Forms.Label lblCashReportTitle;
        private BufferedPanel bufferedPanel1;
        protected internal System.Windows.Forms.DataGridView ParticularsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private BufferedPanel bufferedPanel2;
        private BufferedPanel bufferedPanel3;
        private System.Windows.Forms.RadioButton rdIncome;
        private System.Windows.Forms.RadioButton rdExpenses;
        public System.Windows.Forms.ToolStrip EditorToolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton btnAddParticular;
        private System.Windows.Forms.ToolStripButton btnEditParticular;
        private System.Windows.Forms.ToolStripButton btnDeleteParticular;
        private BufferedPanel CRInfoPanel;
        private System.Windows.Forms.TableLayoutPanel TotalsPanel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblEndingBal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTotalIncome;
        private System.Windows.Forms.Label lblTotalExpenses;
        private System.Windows.Forms.SplitContainer ParticularsSplitPanel;
        internal System.Windows.Forms.DataGridView TotalsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private BufferedPanel TotalsHeaderPanel;
        private System.Windows.Forms.Label label4;
        private BufferedPanel bufferedPanel5;
        private System.Windows.Forms.ToolStripButton btnAddStartingBal;
        private BufferedPanel StartingBalancePanel;
        private System.Windows.Forms.Label label7;
        private BufferedPanel bufferedPanel6;
        private System.Windows.Forms.Label lblStartingBal;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearRefreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnImportCSV;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.RadioButton btnToggleHeader;
        private System.Windows.Forms.Panel pnlToggleHeader;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip ColumnsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem freezeUnfreezeColumnToolStripMenuItem;
    }
}