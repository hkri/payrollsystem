﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

namespace PayrollSystem  {

    public partial class CashReportEditorPage : Form {

        #region Constructor

        //Constructor.
        public CashReportEditorPage() {
            //Designer component design initialization.
            InitializeComponent();

            //Set renderers
            //Set custom toolbar renderer.
            EditorToolStrip.Renderer = ColumnsContextMenuStrip.Renderer = MainToolStrip.Renderer = SectionsToolStrip.Renderer = SectionsContextMenuStrip.Renderer = GlobalVariables.renderer;
        }

        #endregion

        #region Public Variables

        public Form CashReportPrevInstance = null;

        #endregion

        #region Cash Report Settings

        public string CashReportTitle = "";
        public string CashReportStartDate = "";
        public string CashReportEndDate = "";
        public string LastWorkingDate = "";
        public bool   CashReportLocked = false;

        #endregion

        #region Local variables

        //Sections datatable datasource.
        DataTable dt_sections;
        
        //Income datatable datasource.
        DataTable dt_income;

        //Starting Balance value
        Double StartingBalance = 0.0;

        //Search string.
        String skey = "";
        
        //right-clicked column index
        int ActiveColumn = -1;

        #endregion

        #region Public Properties

        string _workingid = "";
        //The cash report currently targeted.
        public string WorkingCashReportID {
            get { return _workingid; }
            set { _workingid = value; }
        }

        string _workingsectionid = "";
        //The section currently being edited.
        public string WorkingSectionID {
            get { return _workingsectionid; }
            set { _workingsectionid = value; }
        }
        


        #endregion

        #region local functions

        //Enables or disables the edit/delete commands depending on the number of selected rows.
        void SetEditCommandsEnabled() {
            if (ParticularsDataGrid.SelectedRows.Count > 0) {
                btnEditParticular.Enabled = true;
                btnDeleteParticular.Enabled = true;
            } else {
                btnEditParticular.Enabled = false;
                btnDeleteParticular.Enabled = false;
            }
        }

        //Gets the summary header view toggle state
        void GetHeaderState() {
            if (GlobalVariables.CREPage_SummaryHeaderExpanded) {
                btnToggleHeader.Text = "\u25B4";
                InfoPanel.Height = 85;
                TotalsPanel.Height = 62;
                lblDatePeriod.Visible = true;
            } else {
                btnToggleHeader.Text = "\u25BE";
                InfoPanel.Height = 40;
                TotalsPanel.Height = 75;
                lblDatePeriod.Visible = false;
            }
        }

        //Toggles the summary header view.
        void ToggleHeader() {
            GlobalVariables.CREPage_SummaryHeaderExpanded = !GlobalVariables.CREPage_SummaryHeaderExpanded;
            GetHeaderState();
        }

        //Perform general refresh.
        void RefreshRecords() {
            if (btnRefresh.Enabled && btnRefresh.Visible) btnRefresh_Click(btnRefresh, new EventArgs());
        }

        //Sets the visibility status of the "No section" notice labels.
        void SetNoSectionNoticePanelVisible(bool visibility) {
            NoSectionNoticePanel.Visible = visibility;
            EditorPanel.Visible = !visibility;
            if (EditorPanel.Dock != DockStyle.Fill && EditorPanel.Visible)
                EditorPanel.Dock = DockStyle.Fill;
        }

        //Event when the selected section has changed.
        void OnSelectedSectionChanged() {
            lblSectionTitle.Text = Cell(SectionsDataGrid, "Title").ToString();

            //Get starting balance cash report section.
            GetSectionStartingBalance();

            if (rdIncome.Checked)
                RefreshIncome();
            else if (rdExpenses.Checked)
                RefreshExpenses();
        }

        //Gets the sections starting balance, if any.
        void GetSectionStartingBalance() {
            if (Cell(SectionsDataGrid, "Starting Balance") != null) {
                string startingbal = Cell(SectionsDataGrid, "Starting Balance").ToString();
                if (String.IsNullOrEmpty(startingbal.Trim())) {
                    StartingBalancePanel.Visible = false;
                    StartingBalance = 0.0;
                } else {
                    if (rdIncome.Checked) {
                        StartingBalancePanel.Visible = true;
                        StartingBalance = Convert.ToDouble(startingbal);
                        lblStartingBal.Text = "Php " + StartingBalance.ToString("#,##0.00");
                    }
                }
            }
        }

        //Computes for the total income, total expenses, and ending balance
        //of the current section.
        void GetSectionTotals() {
            //Query, get all amounts.
            string join = "LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID";
            string query = "SELECT ITEMID, SECID, Particular, PlateNo, ItemDate, IsExpense, Amount, PetronSucatDiesel, Fuel, Violations, Toll, TruckingExpenses, RMVehicle, ServiceFee, Others, (B.LastName + ', ' + B.FirstName) AS Driver, DriverCA, (C.LastName + ', ' + C.FirstName) AS Helper, HelperCA FROM (CashReportItems AS A " + join + " WHERE (SECID = @secid) ";
            skey = skey.Trim(); //Remove whitespaces.
            query += "AND (A.Particular LIKE '%' + @skey + '%' ";

            //Search by driver
            query += "OR (B.LastName + ', ' + B.FirstName) LIKE '%' + @skey + '%' ";
            query += "OR (B.FirstName + ' ' + B.LastName) LIKE '%' + @skey + '%' ";
            query += "OR (B.FirstName + ' ' + B.MiddleName + ' ' + B.FirstName) LIKE '%' + @skey + '%' ";

            //Search by helper
            query += "OR (C.LastName + ', ' + C.FirstName) LIKE '%' + @skey + '%' ";
            query += "OR (C.FirstName + ' ' + C.LastName) LIKE '%' + @skey + '%' ";
            query += "OR (C.FirstName + ' ' + C.MiddleName + ' ' + C.FirstName) LIKE '%' + @skey + '%' ";
            
            query += ")";
            
            //Query for Cash Report-wide total
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
            p[1] = new OleDbParameter("@skey", skey);
            
            //Get the query results and save to the DataTable.
            DataTable amounts = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            IEnumerable<DataRow> rows = amounts.Rows.Cast <DataRow>();  //Perform LINQ on this IEnumerable.

            amounts.Dispose();

            //Create a DataTable of totals (for "Totals" pane)
            DataTable totalsTable = new DataTable();
            DataRow r;  //Row to insert on the totals table.
            totalsTable.Columns.Add("Item");
            totalsTable.Columns.Add("Value", typeof(double));


            //Compute total income.
            double tIncome = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == false).Sum(a => Convert.ToDouble(a["Amount"])) + StartingBalance;
            lblTotalIncome.Text = "Php " + tIncome.ToString("#,##0.00");
            
            //Compute total expense.
            double tExpense = -1 * rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["Amount"]));
            lblTotalExpenses.Text = "Php " + tExpense.ToString("#,##0.00");

            //Compute ending balance.
            double endingBal = tIncome + tExpense;
            lblEndingBal.Text = "Php " + endingBal.ToString("#,##0.00");

            //Add type specific totals
            if (rdExpenses.Checked) {
                //Perform totals on expenses.
                r = totalsTable.NewRow();
                r["Item"] = "Expenses";
                r["Value"] = -1 * tExpense;
                totalsTable.Rows.Add(r);

                //Space
                r = totalsTable.NewRow();
                r["Item"] = "";
                r["Value"] = DBNull.Value;
                totalsTable.Rows.Add(r);

                //Total Sucat Diesel.
                double tSucat = 0.00;
                try {
                    tSucat = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["PetronSucatDiesel"]));
                } catch (Exception ex) {
                    //System.Diagnostics.Debug.WriteLine("DB Null.");
                }
                r = totalsTable.NewRow();
                r["Item"] = "Petron Sucat Diesel";
                r["Value"] = tSucat;
                totalsTable.Rows.Add(r);

                //Total Fuel.
                double tFuel = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["Fuel"]));
                r = totalsTable.NewRow();
                r["Item"] = "Other Diesel";
                r["Value"] = tFuel;
                totalsTable.Rows.Add(r);

                //Total Violations.
                double tViolations = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["Violations"]));
                r = totalsTable.NewRow();
                r["Item"] = "Violations";
                r["Value"] = tViolations;
                totalsTable.Rows.Add(r);

                //Total Toll.
                double tToll = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["Toll"]));
                r = totalsTable.NewRow();
                r["Item"] = "Toll";
                r["Value"] = tToll;
                totalsTable.Rows.Add(r);

                //Total Trucking Expenses.
                double tTX = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["TruckingExpenses"]));
                r = totalsTable.NewRow();
                r["Item"] = "Trucking Expenses";
                r["Value"] = tTX;
                totalsTable.Rows.Add(r);

                //Total RMV
                double tRMV = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["RMVehicle"]));
                r = totalsTable.NewRow();
                r["Item"] = "RM Vehicle";
                r["Value"] = tRMV;
                totalsTable.Rows.Add(r);

                //Total SF
                double tSF = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["ServiceFee"]));
                r = totalsTable.NewRow();
                r["Item"] = "Service Fee";
                r["Value"] = tSF;
                totalsTable.Rows.Add(r);

                //Total Others
                double tOth = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["Others"]));
                r = totalsTable.NewRow();
                r["Item"] = "Other Expenses";
                r["Value"] = tOth;
                totalsTable.Rows.Add(r);

                //Total Driver CA
                double tDCA = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["DriverCA"]));
                r = totalsTable.NewRow();
                r["Item"] = "Driver CA";
                r["Value"] = tDCA;
                totalsTable.Rows.Add(r);

                //Total Helper CA
                double tHCA = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["HelperCA"]));
                r = totalsTable.NewRow();
                r["Item"] = "Helper CA";
                r["Value"] = tHCA;
                totalsTable.Rows.Add(r);

                //Space
                r = totalsTable.NewRow();
                r["Item"] = "";
                r["Value"] = DBNull.Value;
                totalsTable.Rows.Add(r);

                //Totals by Driver CA
                //double tHCA = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).Sum(a => Convert.ToDouble(a["HelperCA"]));
                //return (from c in rows group c by c.Cells[group_by].Value.ToString() 
                //into a orderby a.Key select new TotalsItem(a.Key.ToString(), a.Sum(x => Convert.ToDouble(x.Cells[column].Value))));

                //Title:
                r = totalsTable.NewRow();
                r["Item"] = "By Driver";
                r["Value"] = DBNull.Value;
                totalsTable.Rows.Add(r);

                IEnumerable<TotalsItem> driverCAs = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).GroupBy(x => x["Driver"].ToString()).OrderBy(x => x.Key.ToString()).Select(x => new TotalsItem(x.Key.ToString(), x.Sum(y => Convert.ToDouble(y["DriverCA"]))));
                foreach (TotalsItem i in driverCAs) {
                    if (i.Item.Trim().Equals("") == false) {
                        r = totalsTable.NewRow();
                        r["Item"] = i.Item;
                        r["Value"] = i.Value;
                        totalsTable.Rows.Add(r);
                    }
                }

                //Space
                r = totalsTable.NewRow();
                r["Item"] = "";
                r["Value"] = DBNull.Value;
                totalsTable.Rows.Add(r);

                //Title:
                r = totalsTable.NewRow();
                r["Item"] = "By Helper";
                r["Value"] = DBNull.Value;
                totalsTable.Rows.Add(r);

                IEnumerable<TotalsItem> helperCAs = rows.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).GroupBy(x => x["Helper"].ToString()).OrderBy(x => x.Key.ToString()).Select(x => new TotalsItem(x.Key.ToString(), x.Sum(y => Convert.ToDouble(y["HelperCA"]))));
                foreach (TotalsItem i in helperCAs) {
                    if (i.Item.Trim().Equals("") == false) {
                        r = totalsTable.NewRow();
                        r["Item"] = i.Item;
                        r["Value"] = i.Value;
                        totalsTable.Rows.Add(r);
                    }
                }

            } else if (rdIncome.Checked) {
                //Perform totals on income.
                r = totalsTable.NewRow();
                r["Item"] = "Fund Transfer";
                r["Value"] = tIncome;
                totalsTable.Rows.Add(r);
            }

            //Attach totals table on the "Totals" pane datagrid
            TotalsDataGrid.Columns.Clear();
            TotalsDataGrid.DataSource = totalsTable;
            TotalsDataGrid.Columns["Value"].DefaultCellStyle.Format = "#,##0.00";
            TotalsDataGrid.Columns["Value"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //Store ending balance on sections' Ending Balance column.
            query = "UPDATE CashReportSections SET EndingBalance = @ending WHERE SECID = @secid";

            OleDbParameter[] q = new OleDbParameter[2];
            q[0] = new OleDbParameter("@ending", endingBal);
            q[1] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
            DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, q);

            SetEditCommandsEnabled();
        }

        //Shorthand decimal formatted - string conversion.
        double Dbl(object value){
            if (value != DBNull.Value && value.ToString().Trim() != "")
                return Convert.ToDouble(value.ToString());
            else return 0.00;
        }

        #endregion

        #region DataGridView Functions

        //Sets the minimum width of the specified column.
        void ColMinWidth(DataGridView datagrid, string colname,int minwidth) {
            datagrid.Columns[colname].MinimumWidth = minwidth;
        }

        //Sets the column of the specified column of the given datagrid.
        void ColWidth(DataGridView datagrid, string colname, int width) {
            datagrid.Columns[colname].Width = width;
        }

        //Hides the specified column in the given datagrid.
        void HideCol(DataGridView datagrid, string columnname) {
            datagrid.Columns[columnname].Visible = false;
        }

        //Returns the selected cell of the specified column in the given datagrid.
        object Cell(DataGridView datagrid, string column) {
            if (datagrid.SelectedRows.Count <= 0)
                return null;
            return datagrid.SelectedRows[0].Cells[column].Value;
        }

        //Gets data from the income datagridview.
        string Particular(string column) {
            if (ParticularsDataGrid.SelectedRows.Count <= 0)
                return null;
            return ParticularsDataGrid.SelectedRows[0].Cells[column].Value.ToString();
        }

        //Sets the AutoSize of specified column of the given datagrid to fill.
        void FillCol(DataGridView datagrid, string columnname) {
            datagrid.Columns[columnname].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        //Sets the formatting of the specified column.
        void FormatCol(DataGridView datagrid, string colname, string format) {
            datagrid.Columns[colname].DefaultCellStyle.Format = format;
        }

        //Returns array of objects in a given column of the given datagrid.
        List<Object> ItemArray(DataGridView datagrid, string column) {
            List<Object> lst = new List<Object>();
            foreach (DataGridViewRow r in datagrid.Rows) {
                lst.Add(r.Cells[column].Value);
            }
            return lst;
        }

        #endregion

        #region Toolbar Eventhandlers

        //Freeze columns
        private void freezeUnfreezeColumnToolStripMenuItem_Click(object sender, EventArgs e) {
            if (ActiveColumn >= 0) {
                ParticularsDataGrid.Columns[ActiveColumn].Frozen = !ParticularsDataGrid.Columns[ActiveColumn].Frozen;
            }
        }

        //Trigger search btn
        private void txtSearch_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                e.Handled = true;
                e.SuppressKeyPress = true;
                btnSearch.PerformClick();
            } else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Back) {
                txtSearch.Text = "";
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        
        //Search hotkey.
        private void searchToolStripMenuItem_Click(object sender, EventArgs e) {
            if (btnSearch.Visible && btnSearch.Enabled)
                btnSearch.PerformClick();
        }

        //Refresh shortcut key
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e) {
            RefreshRecords();
        }

        //Full refresh shortcut key
        private void clearRefreshToolStripMenuItem_Click(object sender, EventArgs e) {
            txtSearch.Text = "";
            if (btnSearch.Visible && btnSearch.Enabled)
                btnSearch.PerformClick();
        }
        
        //Perform search refresh.
        private void btnSearch_Click(object sender, EventArgs e) {
            if (lblSearch.Visible == false && txtSearch.Visible == false) {
                //When search button is triggered, show search box.
                lblSearch.Visible = txtSearch.Visible = true;
                txtSearch.Text = "";
                txtSearch.Focus();
            } else {
                //If search textbox is already visible, perform search.
                if (txtSearch.Text.Trim().Length > 0) {
                    //If there is content in the searchbox, search.
                    skey = txtSearch.Text;
                    RefreshRecords();
                } else {
                    //If there aren't any content in the searchbox, hide searchbox.
                    lblSearch.Visible = txtSearch.Visible = false;
                    skey = "";
                    RefreshRecords();
                }
            }
        }

        //Go back to the cash reports page.
        private void btnBack_Click(object sender, EventArgs e) {
            if (ParentForm is MainWindow) {
                MainWindow wnd = (MainWindow)ParentForm;
                if (wnd != null) {
                    if (CashReportPrevInstance != null) {
                        CashReportPrevInstance.Enabled = true;
                        wnd.LoadPage(CashReportPrevInstance);
                    }
                }
            }
        }

        #endregion

        #region Section DataGridView CRUD

        //Function to load all sections of the cash report.
        void RefreshCashReportSections() {
            //WHERE CRID=[@crid] ORDER BY Title;
            //Set Sections Database DataSource.
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@crid", "{" + WorkingCashReportID + "}");

            if (dt_sections != null) dt_sections.Dispose();

            string query = "SELECT A.StartingBalance AS [SBID], A.SECID AS [ID], A.CRID AS [CRID], A.EndingBalance AS [Ending Balance], A.Title, B.Title AS [SBTitle], B.EndingBalance AS [Starting Balance], C.Title AS [SBCashReport], C.DateStart, C.DateEnd FROM (CashReportSections AS A LEFT JOIN CashReportSections AS B ON A.StartingBalance = B.SECID) LEFT JOIN CashReport AS C ON B.CRID = C.CRID WHERE A.CRID=[@crid] ORDER BY A.Title";

            dt_sections = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
            SectionsDataGrid.DataSource = dt_sections;

            //Hide unnecessary columns.
            HideCol(SectionsDataGrid, "ID");
            HideCol(SectionsDataGrid, "CRID");
            HideCol(SectionsDataGrid, "SBID");
            HideCol(SectionsDataGrid, "Ending Balance");
            HideCol(SectionsDataGrid, "SBTitle");
            HideCol(SectionsDataGrid, "Starting Balance");
            HideCol(SectionsDataGrid, "SBCashReport");
            HideCol(SectionsDataGrid, "DateStart");
            HideCol(SectionsDataGrid, "DateEnd");
            SectionsDataGrid.Columns["Title"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //Hide or show notice of no sections.
            if (dt_sections.Rows.Count <= 0)
                SetNoSectionNoticePanelVisible(true);
            else
                SetNoSectionNoticePanelVisible(false);
        }

        #endregion

        #region Income DataGridView CRUD

        //Function to retrive all income records.
        void RefreshIncome() {
            /*
             * WHERE (PlateNo Like '%' & [@search] & '%' Or Particular Like '%' & [@search]
             * & '%' OR (B.FirstName + ' ' + B.LastName) Like '%' & [@search] & '%' OR 
             * (C.FirstName + ' ' + C.LastName) Like '%' & [@search] & '%') 
             * And IsExpense=IIf([@isexpense]="y",True,IIf([@isexpense]="n",False,False)) 
             * And SECID=[@secid]
             * ORDER BY ItemDate, Particular;
             */
            //@search
            //@isexpense
            //@secid
            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@search", skey);
            p[1] = new OleDbParameter("@isexpense", "n");
            p[2] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");

            //Load from database incomes.
            if (dt_income == null) dt_income = new DataTable();
            else dt_income.Clear();

            DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReportItems", true, dt_income, p);
            
            if (ParticularsDataGrid.DataSource == null) {
                //Perform some initial column reorder.
                dt_income.Columns["Date"].SetOrdinal(0);

                //Clear all columns first.
                ParticularsDataGrid.Columns.Clear();

                //Income table DataBinding
                ParticularsDataGrid.DataSource = dt_income;

                //Hide unnecessary columns.
                //SELECT ITEMID AS ID, Particular, PlateNo AS [Plate Number], ItemDate AS [Date], 
                //Amount, Fuel, Violations, Toll, TruckingExpenses AS [Trucking Expenses], 
                //RMVehicle AS [RM Vehicle], ServiceFee AS [Service Fee], Others, 
                //(B.FirstName + ' ' + B.LastName) AS Driver, DriverCA AS [CA Driver], 
                //(C.FirstName + ' ' + C.LastName) AS Helper, HelperCA AS [CA Helper]
                HideCol(ParticularsDataGrid, "ID");
                HideCol(ParticularsDataGrid, "SECID");
                HideCol(ParticularsDataGrid, "Fuel");
                HideCol(ParticularsDataGrid, "Violations");
                HideCol(ParticularsDataGrid, "Service Fee");
                HideCol(ParticularsDataGrid, "Others");
                HideCol(ParticularsDataGrid, "Driver");
                HideCol(ParticularsDataGrid, "CA Driver");
                HideCol(ParticularsDataGrid, "Helper");
                HideCol(ParticularsDataGrid, "CA Helper");
                HideCol(ParticularsDataGrid, "Toll");
                HideCol(ParticularsDataGrid, "Trucking Expenses");
                HideCol(ParticularsDataGrid, "RM Vehicle");
                HideCol(ParticularsDataGrid, "HID");
                HideCol(ParticularsDataGrid, "DID");

                //Setup column widths.
                ColWidth(ParticularsDataGrid, "Date", 120);
                ColWidth(ParticularsDataGrid, "Particular", 250);
                ColWidth(ParticularsDataGrid, "Plate Number", 120);
                ColWidth(ParticularsDataGrid, "Amount", 140);
                ColMinWidth(ParticularsDataGrid, "Particular", 180);
                //FillCol(ParticularsDataGrid, "Amount");
                //ParticularsDataGrid.AllowUserToResizeColumns = false;

                //Add spacer column.
                DataGridViewTextBoxColumn spacer = new DataGridViewTextBoxColumn();
                ParticularsDataGrid.Columns.Add(spacer);
                spacer.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                //Format column.
                FormatCol(ParticularsDataGrid, "Amount", "#,##0.00");
            }

            //Try to get the starting balance of the section.
            GetSectionStartingBalance();

            //Get totals
            GetSectionTotals();
        }

        //Add new income.
        void AddIncome() {
            IncomeExpenseDialog dlg = new IncomeExpenseDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetupInputs(0); //Income mode
            dlg.SetDateLimits(CashReportStartDate, CashReportEndDate);
            dlg.SetPlateNo(ManageVehiclesPage.QueryVehicles());
            dlg.SetLastDate(LastWorkingDate);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string particular, date, plateno;
                double amount = 0.0;
                dlg.GetFieldValuesIncome(out particular, out date, out plateno, out amount);

                string query = "INSERT INTO CashReportItems(SECID, Particular, PlateNo, ItemDate, Amount, IsExpense) VALUES(@secid, @particular, @pn, @date, @amount, false)";


                OleDbParameter[] p = new OleDbParameter[5];
                p[0] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
                p[1] = new OleDbParameter("@particular", particular);
                p[2] = new OleDbParameter("@pn", plateno);
                p[3] = new OleDbParameter("@date", date);
                p[4] = new OleDbParameter("@amount", amount);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("New fund transfer successfully added to the cash report.", "New Income");
                    RefreshIncome();
                    //Set working date.
                    LastWorkingDate = dlg.GetDate();
                } else
                    Notifier.ShowError("Failed to add new fund transfer to the cash report. Please refresh and try again.");

            }
        }

        //Edit selected income.
        void EditIncome() {
            IncomeExpenseDialog dlg = new IncomeExpenseDialog();
            dlg.SetupInputs(0); //Income mode
            dlg.SetDateLimits(CashReportStartDate, CashReportEndDate);
            dlg.SetPlateNo(ManageVehiclesPage.QueryVehicles());
            dlg.SetLastDate(LastWorkingDate);

            //Set the initial field values.
            dlg.SetFieldValuesIncome(Particular("Particular"), Particular("Date"), Particular("Plate Number"), Convert.ToDouble(Particular("Amount")).ToString("0.00"));

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string particular, date, plateno;
                double amount = 0.0;
                dlg.GetFieldValuesIncome(out particular, out date, out plateno, out amount);

                string query = "UPDATE CashReportItems SET Particular = @particular, PlateNo = @pn, ItemDate = @date, Amount = @amount WHERE ITEMID = @id";

                OleDbParameter[] p = new OleDbParameter[5];
                p[0] = new OleDbParameter("@particular", particular);
                p[1] = new OleDbParameter("@pn", plateno);
                p[2] = new OleDbParameter("@date", date);
                p[3] = new OleDbParameter("@amount", amount);
                p[4] = new OleDbParameter("@id", "{" + Particular("ID") + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("The selected particular was successfully updated.", "Update Fund Transfer");
                    RefreshIncome();
                    //Set working date.
                    LastWorkingDate = dlg.GetDate();
                } else
                    Notifier.ShowError("Failed to update selected record. Please refresh and try again.");

            }
        }

        //Delete the selected income.
        void DeleteIncome() {
            if (ParticularsDataGrid.SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected particular?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    //Delete the selected item.
                    //FROM CashReportItems WHERE ITEMID=[@itemid];
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@itemid", "{" + Cell(ParticularsDataGrid, "ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_CashReportItems", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Item \"" + Cell(ParticularsDataGrid, "Particular") + "\" was successfully deleted.", "Item Deleted");
                        RefreshIncome();
                    } else
                        Notifier.ShowError("Failed to delete the selected item. Please refresh and try again.");
                }
            }
        }

        #endregion

        #region Expenses DataGridView CRUD

        //Loads the expenses items.
        void RefreshExpenses() {
            /*
            * WHERE (PlateNo Like '%' & [@search] & '%' Or Particular Like '%' & [@search]
            * & '%' OR (B.FirstName + ' ' + B.LastName) Like '%' & [@search] & '%' OR 
            * (C.FirstName + ' ' + C.LastName) Like '%' & [@search] & '%') 
            * And IsExpense=IIf([@isexpense]="y",True,IIf([@isexpense]="n",False,False)) 
            * And SECID=[@secid]
            * ORDER BY ItemDate, Particular;
            */
            //@search
            //@isexpense
            //@secid

            /*
             * SELECT ITEMID AS ID, SECID, Particular, PlateNo AS [Plate Number], ItemDate AS [Date], Amount,
             * Fuel, Violations, Toll, TruckingExpenses AS [Trucking Expenses], RMVehicle AS [RM Vehicle], 
             * ServiceFee AS [Service Fee], Others, (B.FirstName + ' ' + B.LastName) AS Driver, 
             * DriverCA AS [CA Driver], (C.FirstName + ' ' + C.LastName) AS Helper, HelperCA AS [CA Helper], Driver AS DID, Helper AS HID 
             * FROM (CashReportItems AS A LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID
             * WHERE (PlateNo Like '%' & [@search] & '%' Or Particular Like '%' & [@search] & '%' OR (B.FirstName + ' ' + B.LastName) Like '%' & [@search] & '%'
             * OR (C.FirstName + ' ' + C.LastName) Like '%' & [@search] & '%') 
             * And IsExpense=IIf([@isexpense]="y",True,IIf([@isexpense]="n",False,False)) 
             * And SECID=[@secid]
             * ORDER BY ItemDate, Particular;

             */
            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@search", skey);
            p[1] = new OleDbParameter("@isexpense", "y");
            p[2] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
            //Load from database incomes.
            //if (dt_income == null) dt_income = new DataTable();
            //else dt_income.Clear();

            dt_income = new DataTable();

            //Hard-coded SQL query (instead of stored procedure).
            StringBuilder nqry = new StringBuilder();
            nqry.Append("SELECT ITEMID AS ID, SECID, Particular, PlateNo AS [Plate Number], ItemDate AS [Date], Amount, ");
            nqry.Append("[PetronSucatDiesel] AS [Petron Sucat Diesel], [Fuel] AS [Other Diesel], Violations, Toll, TruckingExpenses AS [Trucking Expenses], RMVehicle AS [RM Vehicle], ");
            nqry.Append("ServiceFee AS [Service Fee], Others, (B.FirstName + ' ' + B.LastName) AS Driver, ");
            nqry.Append("DriverCA AS [CA Driver], (C.FirstName + ' ' + C.LastName) AS Helper, HelperCA AS [CA Helper], Driver AS DID, Helper AS HID ");
            nqry.Append("FROM (CashReportItems AS A LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID ");
            nqry.Append("WHERE (PlateNo Like '%' & [@search] & '%' Or Particular Like '%' & [@search] & '%' OR (B.FirstName + ' ' + B.LastName) Like '%' & [@search] & '%' ");
            nqry.Append("OR (C.FirstName + ' ' + C.LastName) Like '%' & [@search] & '%') ");
            nqry.Append("And IsExpense=IIf([@isexpense]=\"y\",True,IIf([@isexpense]=\"n\",False,False)) ");
            nqry.Append("And SECID=[@secid] ");
            nqry.Append("ORDER BY ItemDate, Particular;");
            
            //DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReportItems", true, dt_income, p);    //stored proc
            DatabaseManager.GetMainDatabase().ExecuteQuery(nqry.ToString(), false, dt_income, p);
            
            //Perform some initial column reorder.
            dt_income.Columns["Date"].SetOrdinal(0);

            //Clear all columns first.
            ParticularsDataGrid.Columns.Clear();

            //Income table DataBinding
            ParticularsDataGrid.DataSource = dt_income;

            //Hide unnecessary columns.
            //SELECT ITEMID AS ID, Particular, PlateNo AS [Plate Number], ItemDate AS [Date], 
            //Amount, Fuel, Violations, Toll, TruckingExpenses AS [Trucking Expenses], 
            //RMVehicle AS [RM Vehicle], ServiceFee AS [Service Fee], Others, 
            //(B.FirstName + ' ' + B.LastName) AS Driver, DriverCA AS [CA Driver], 
            //(C.FirstName + ' ' + C.LastName) AS Helper, HelperCA AS [CA Helper]
            HideCol(ParticularsDataGrid, "ID");
            HideCol(ParticularsDataGrid, "SECID");
            HideCol(ParticularsDataGrid, "HID");
            HideCol(ParticularsDataGrid, "DID");
            //HideCol(ParticularsDataGrid, "Fuel");
            //HideCol(ParticularsDataGrid, "Violations");
            //HideCol(ParticularsDataGrid, "Service Fee");
            //HideCol(ParticularsDataGrid, "Others");
            //HideCol(ParticularsDataGrid, "Driver");
            //HideCol(ParticularsDataGrid, "CA Driver");
            //HideCol(ParticularsDataGrid, "Helper");
            //HideCol(ParticularsDataGrid, "CA Helper");
            //HideCol(ParticularsDataGrid, "Toll");
            //HideCol(ParticularsDataGrid, "Trucking Expenses");
            //HideCol(ParticularsDataGrid, "RM Vehicle");

            //Setup column widths.
            ColWidth(ParticularsDataGrid, "Date", 80);
            ColWidth(ParticularsDataGrid, "Particular", 250);
            ColWidth(ParticularsDataGrid, "Plate Number", 120);
            ColWidth(ParticularsDataGrid, "Amount", 110);
            ColMinWidth(ParticularsDataGrid, "Particular", 180);
            ColWidth(ParticularsDataGrid, "Petron Sucat Diesel", 150);
            ColWidth(ParticularsDataGrid, "Other Diesel", 130);
            ColWidth(ParticularsDataGrid, "Driver", 170);
            ColWidth(ParticularsDataGrid, "Helper", 170);
            //FillCol(ParticularsDataGrid, "Amount");
            //ParticularsDataGrid.AllowUserToResizeColumns = false;

            //Add spacer column.
            DataGridViewTextBoxColumn spacer = new DataGridViewTextBoxColumn();
            ParticularsDataGrid.Columns.Add(spacer);
            spacer.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //Format column.
            FormatCol(ParticularsDataGrid, "Amount", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Other Diesel", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Petron Sucat Diesel", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Violations", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Service Fee", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Others", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Driver", "#,##0.00");
            FormatCol(ParticularsDataGrid, "CA Driver", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Helper", "#,##0.00");
            FormatCol(ParticularsDataGrid, "CA Helper", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Toll", "#,##0.00");
            FormatCol(ParticularsDataGrid, "Trucking Expenses", "#,##0.00");
            FormatCol(ParticularsDataGrid, "RM Vehicle", "#,##0.00");

            //Get totals
            GetSectionTotals();
        }

        //Add new expense.
        void AddExpense() {
            IncomeExpenseDialog dlg = new IncomeExpenseDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetupInputs(1); //Expense mode
            dlg.SetDateLimits(CashReportStartDate, CashReportEndDate);
            dlg.SetPlateNo(ManageVehiclesPage.QueryVehicles());
            dlg.SetEmployees(EmployeesPage.QueryEmployeeForComboBox());
            dlg.SetLastDate(LastWorkingDate);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                //@fuel, @violations, @toll, @texpenses, @rmv, 
                //@sf, @others, @driver, @driverCA, @helper, @helperCA

                string particular, date, plateno, driver, helper;
                double amount = 0.0, fuel, violations, toll, texpenses, rmv, sf, others, driverCA, helperCA;
                double petronDiesel;
                dlg.GetFieldValuesExpense(out particular, out date, out plateno, out amount, out fuel, out violations
                                            , out toll, out texpenses, out rmv, out sf, out others, out driver, out driverCA,
                                            out helper, out helperCA, out petronDiesel);

                string query = "INSERT INTO CashReportItems(SECID, Particular, PlateNo, ItemDate, Amount, IsExpense, Fuel, Violations, Toll, TruckingExpenses, RMVehicle, ServiceFee, Others, Driver, DriverCA, Helper, HelperCA, PetronSucatDiesel) VALUES(@secid, @particular, @pn, @date, @amount, true, @fuel, @violations, @toll, @texpenses, @rmv, @sf, @others, @driver, @driverCA, @helper, @helperCA, @sucatdsel)";

                OleDbParameter[] p = new OleDbParameter[17];
                p[0] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
                p[1] = new OleDbParameter("@particular", particular);
                p[2] = new OleDbParameter("@pn", plateno);
                p[3] = new OleDbParameter("@date", date);
                p[4] = new OleDbParameter("@amount", amount);

                p[5] = new OleDbParameter("@fuel", fuel);
                p[6] = new OleDbParameter("@violations", violations);
                p[7] = new OleDbParameter("@toll", toll);
                p[8] = new OleDbParameter("@texpenses", texpenses);
                p[9] = new OleDbParameter("@rmv", rmv);
                p[10] = new OleDbParameter("@sf", sf);
                p[11] = new OleDbParameter("@others", others);
                p[12] = new OleDbParameter("@driver", "{" + driver + "}");
                p[13] = new OleDbParameter("@driverCA", driverCA);
                p[14] = new OleDbParameter("@helper", "{" + helper + "}");
                p[15] = new OleDbParameter("@helperCA", helperCA);
                p[16] = new OleDbParameter("@sucatdsel", petronDiesel);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("New Expense successfully added to the cash report.", "New Expense");
                    RefreshExpenses();
                    //Set working date.
                    LastWorkingDate = dlg.GetDate();
                } else
                    Notifier.ShowError("Failed to add new expense to the cash report. Please refresh and try again.");

            }
        }

        //Edit selected expense.
        void EditExpense() {
            IncomeExpenseDialog dlg = new IncomeExpenseDialog();
            dlg.SetupInputs(1); //Expense mode
            dlg.SetDateLimits(CashReportStartDate, CashReportEndDate);
            dlg.SetPlateNo(ManageVehiclesPage.QueryVehicles());
            dlg.SetEmployees(EmployeesPage.QueryEmployeeForComboBox());
            dlg.SetLastDate(LastWorkingDate);
            dlg.SetFieldValuesExpense(Particular("Particular"), Particular("Date"), Particular("Plate Number"), Dbl(Particular("Amount")),
                            Dbl(Particular("Other Diesel")), Dbl(Particular("Violations")), Dbl(Particular("Toll")), Dbl(Particular("Trucking Expenses")),
                            Dbl(Particular("RM Vehicle")), Dbl(Particular("Service Fee")), Dbl(Particular("Others")), Particular("DID"),
                            Dbl(Particular("CA Driver")), Particular("HID"), Dbl(Particular("CA Helper")), Dbl(Particular("Petron Sucat Diesel")));


            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                //@fuel, @violations, @toll, @texpenses, @rmv, 
                //@sf, @others, @driver, @driverCA, @helper, @helperCA

                string particular, date, plateno, driver, helper;
                double amount = 0.0, fuel, violations, toll, texpenses, rmv, sf, others, driverCA, helperCA;
                double sucatDiesel;
                dlg.GetFieldValuesExpense(out particular, out date, out plateno, out amount, out fuel, out violations
                                            , out toll, out texpenses, out rmv, out sf, out others, out driver, out driverCA,
                                            out helper, out helperCA, out sucatDiesel);

                string query = "UPDATE CashReportItems SET Particular = @particular, PlateNo = @pn, ItemDate = @date, Amount = @amount, Fuel = @fuel, Violations = @violations, Toll = @toll, TruckingExpenses = @texpenses, RMVehicle = @rmv, ServiceFee = @sf, Others = @others, Driver = @driver, DriverCA = @driverCA, Helper = @helper, HelperCA = @helperCA, [PetronSucatDiesel] = @sucat WHERE ITEMID = @id";

                OleDbParameter[] p = new OleDbParameter[17];
                p[0] = new OleDbParameter("@particular", particular);
                p[1] = new OleDbParameter("@pn", plateno);
                p[2] = new OleDbParameter("@date", date);
                p[3] = new OleDbParameter("@amount", amount);

                p[4] = new OleDbParameter("@fuel", fuel);
                p[5] = new OleDbParameter("@violations", violations);
                p[6] = new OleDbParameter("@toll", toll);
                p[7] = new OleDbParameter("@texpenses", texpenses);
                p[8] = new OleDbParameter("@rmv", rmv);
                p[9] = new OleDbParameter("@sf", sf);
                p[10] = new OleDbParameter("@others", others);
                p[11] = new OleDbParameter("@driver", "{" + driver + "}");
                p[12] = new OleDbParameter("@driverCA", driverCA);
                p[13] = new OleDbParameter("@helper", "{" + helper + "}");
                p[14] = new OleDbParameter("@helperCA", helperCA);
                p[15] = new OleDbParameter("@sucat", sucatDiesel);

                p[16] = new OleDbParameter("@helperCA", "{" + Particular("ID") + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                if (ok) {
                    Notifier.ShowInformation("The selected expense entry was successfully updated.", "Expense Updated");
                    RefreshExpenses();
                    //Set working date.
                    LastWorkingDate = dlg.GetDate();
                } else
                    Notifier.ShowError("Failed to update the specified expense. Please refresh and try again.");
            }
        }

        //Delete the selected expense.
        void DeleteExpense() {
            if (ParticularsDataGrid.SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected particular?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    //Delete the selected item.
                    //FROM CashReportItems WHERE ITEMID=[@itemid];
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@itemid", "{" + Cell(ParticularsDataGrid, "ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_CashReportItems", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Item \"" + Cell(ParticularsDataGrid, "Particular") + "\" was successfully deleted.", "Item Deleted");
                        RefreshExpenses();
                    } else
                        Notifier.ShowError("Failed to delete the selected item. Please refresh and try again.");
                }
            }
        }

        #endregion

        #region Income/Expense Command Eventhandlers.

        //Add Income/Expense button.
        private void btnAddParticular_Click(object sender, EventArgs e) {
            if (btnAddParticular.Enabled && btnAddParticular.Visible) {
                if (rdIncome.Checked)
                    AddIncome();
                else if (rdExpenses.Checked)
                    AddExpense();
            }
        }

        //Edit Income/Expense button.
        private void btnEditParticular_Click(object sender, EventArgs e) {
            if (ParticularsDataGrid.SelectedRows.Count > 0) {
                if (btnEditParticular.Enabled && btnEditParticular.Visible) {
                    if (rdIncome.Checked)
                        EditIncome();
                    else if (rdExpenses.Checked)
                        EditExpense();
                }
            }
        }

        //Delete Income/Expense button.
        private void btnDeleteParticular_Click(object sender, EventArgs e) {
            if (ParticularsDataGrid.SelectedRows.Count > 0) {
                if (btnDeleteParticular.Enabled && btnDeleteParticular.Visible) {
                    if (rdIncome.Checked)
                        DeleteIncome();
                    else if (rdExpenses.Checked)
                        DeleteExpense();
                }
            }
        }

        #endregion

        #region Window eventhandlers

        //Eventhandler when window is resized.
        private void CashReportEditorPage_Resize(object sender, EventArgs e) {
            SuspendLayout();
            if (Width < 650) {
                SplitContainerMain.Panel1Collapsed = true;
                ParticularsSplitPanel.Panel2Collapsed = true;
            } else {
                SplitContainerMain.Panel1Collapsed = false;
                ParticularsSplitPanel.Panel2Collapsed = false;
            }
            if (Height < 500)
                InfoPanel.Visible = false;
            else
                InfoPanel.Visible = true;
            ResumeLayout();
        }

        //Eventhandler on page load.
        private void CashReportEditorPage_Load(object sender, EventArgs e) {

            SuspendLayout();

            // Toggle button -> expanded (up arrow)
            GetHeaderState();

            //Refresh the sections sidebar.
            RefreshCashReportSections();

            //Refresh the incomes panel.
            RefreshIncome();

            //Set the cash report details.
            lblCashReportTitle.Text = CashReportTitle;
            lblDatePeriod.Text = DateTime.Parse(CashReportStartDate).ToString("MMM dd, yyyy") + " - " + DateTime.Parse(CashReportEndDate).ToString("MMM dd, yyyy");
            if (CashReportLocked) lblCashReportTitle.Text += " [LOCKED]";

            //Set incomes first.
            rdIncome.Checked = true;

            //Hide command buttons if locked.
            if (CashReportLocked) {
                SectionsToolStrip.Visible = false;
                EditorToolStrip.Visible = false;
                SectionsDataGrid.ContextMenuStrip = null;
                btnImportCSV.Visible = btnImportCSV.Enabled = false;
            }

            //Set working date.
            LastWorkingDate = CashReportStartDate;

            ParticularsSplitPanel.SplitterWidth = 1;

            ResumeLayout();
        }

        #endregion

        #region Section Controls EventHandler

        //Toggles the collapse/expanded view of the summary header.
        private void btnToggleHeader_Click(object sender, EventArgs e) {
            ToggleHeader();
        }

        //Select the corresponding section item, even on right click.
        private void SectionsDataGrid_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == System.Windows.Forms.MouseButtons.Right) {
                int rowindex = SectionsDataGrid.HitTest(e.X, e.Y).RowIndex;
                if (rowindex > -1)
                    SectionsDataGrid.Rows[rowindex].Selected = true;
            }
        }

        //hide the delete section command when there's nothing selected.
        private void SectionsContextMenuStrip_Opening(object sender, CancelEventArgs e) {
            if (SectionsDataGrid.SelectedRows.Count <= 0)
                deleteSectionToolStripMenuItem.Visible = false;
            else
                deleteSectionToolStripMenuItem.Visible = true;
        }

        //Add Section context menu.
        private void addSectionToolStripMenuItem_Click(object sender, EventArgs e) {
            btnSectionAdd.PerformClick();
        }

        //Delete section context menu.
        private void deleteSectionToolStripMenuItem_Click(object sender, EventArgs e) {
            btnSectionDelete.PerformClick();
        }

        //When selected row has changed.
        private void SectionsDataGrid_SelectionChanged(object sender, EventArgs e) {
            if (SectionsDataGrid.SelectedRows.Count > 0) {
                string nSectionIDGet = Cell(SectionsDataGrid, "ID").ToString();
                if (nSectionIDGet.Equals(WorkingSectionID) == false) {
                    WorkingSectionID = Cell(SectionsDataGrid, "ID").ToString();
                    OnSelectedSectionChanged();
                }
            }
        }

        //Style rows to gradient.
        Brush b;
        Pen p;
        private void SectionsDataGrid_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e) {
            if ((e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected) {
                b = new LinearGradientBrush(Point.Empty, new Point(0, e.RowBounds.Height), Color.FromArgb(63, 136, 245), Color.FromArgb(57, 115, 221));
                e.Graphics.FillRectangle(b, e.RowBounds);
                b = new SolidBrush(Color.FromArgb(30, Color.Black));
                p = new Pen(b);
                e.Graphics.DrawLine(p, new Point(0, e.RowBounds.Top), new Point(e.RowBounds.Width, e.RowBounds.Top));
                e.Graphics.DrawLine(p, new Point(0, e.RowBounds.Top + e.RowBounds.Height - 1), new Point(e.RowBounds.Width, e.RowBounds.Top + e.RowBounds.Height - 1));
                e.PaintCellsContent(e.ClipBounds);
                e.Handled = true;
            }
        }

        //Delete Section Eventhandler
        private void btnSectionDelete_Click(object sender, EventArgs e) {
            if (SectionsDataGrid.SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected section and all its contents?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    //FROM CashReportSections WHERE SECID=[@sid]; //Insert to SQL
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", "{" + Cell(SectionsDataGrid, "ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_CashReportSections", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Section has been permanently deleted.", "Section Deleted");
                        RefreshCashReportSections();
                    } else {
                        Notifier.ShowError("Failed to delete section.");
                    }
                }
            }
        }

        //Add Section Event Handler
        private void btnSectionAdd_Click(object sender, EventArgs e) {
            if (WorkingCashReportID.Trim().Equals("")) return;
            CashReportSectionDialog dlg = new CashReportSectionDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetExistingSections(new List<String>(ItemArray(SectionsDataGrid, "Title").Cast<String>()));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //VALUES ([@crid], [@endingbalance], [@title]);
                string crid = WorkingCashReportID, title;
                double endingbalance = 0.00;
                dlg.GetFieldValues(out title);

                //Insert to SQL
                OleDbParameter[] p = new OleDbParameter[3];
                p[0] = new OleDbParameter("@crid", crid);
                p[1] = new OleDbParameter("@endingbalance", endingbalance);
                p[1].DbType = DbType.Double;
                p[2] = new OleDbParameter("@title", title);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_CashReportSections", true, p);
                if (ok) {
                    Notifier.ShowInformation("New section has been added to this cash report.", "Section Added");
                    RefreshCashReportSections();
                } else {
                    Notifier.ShowError("Failed to add new section.");
                }
            }
        }

        #endregion

        #region Eventhandler for Income Expense Data viewer        
        
        //shows or hide the edit buttons.
        private void ParticularsDataGrid_SelectionChanged(object sender, EventArgs e) {
            SetEditCommandsEnabled();
        }

        //Set the backgroundcolor of frozen column to black.
        private void ParticularsDataGrid_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e) {
            if (e.StateChanged == DataGridViewElementStates.Frozen) {
                if (e.Column.Frozen) {
                    e.Column.HeaderCell.Style.BackColor = Color.FromArgb(55, 55, 55);
                    e.Column.HeaderCell.Style.ForeColor = Color.FromArgb(250, 250, 250);
                } else {
                    e.Column.HeaderCell.Style.BackColor = Color.FromArgb(253, 253, 253);
                    e.Column.HeaderCell.Style.ForeColor = Color.FromArgb(47, 47, 47);
                }
            }
        }

        //draw a custom column header border.
        private void ParticularsDataGrid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e) {
            if (e.RowIndex == -1 && e.ColumnIndex > -1) {
                e.Handled = true;

                e.PaintBackground(e.ClipBounds, true);

                if (!ParticularsDataGrid.Columns[e.ColumnIndex].Frozen) {
                    using (SolidBrush br = new SolidBrush(Color.FromArgb(235, 235, 235))) {
                        using (Pen pn = new Pen(br)) {
                            e.Graphics.DrawLine(pn, new Point(e.CellBounds.Right - 1, 0), new Point(e.CellBounds.Right - 1, e.CellBounds.Height - 1));
                        }
                    }
                }

                e.PaintContent(e.ClipBounds);
            }
        }

        //Add Starting balance event handler.
        private void btnAddStartingBal_Click(object sender, EventArgs e) {
            if (rdIncome.Checked) {
                StartingBalanceDialog dlg = new StartingBalanceDialog();
           
                //If the starting balance ID is nothing, add mode
                //else, load it to the dialog.
                if (Cell(SectionsDataGrid, "Starting Balance") != null) {
                    string startingbal = Cell(SectionsDataGrid, "Starting Balance").ToString();
                    if (String.IsNullOrEmpty(startingbal.Trim()) == false) {
                        //there is a loaded starting balance, set it.
                        dlg.SetSectionIDStartingBalance(Cell(SectionsDataGrid, "SBID").ToString());
                    }
                }

                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    string sid = dlg.GetSectionIDStartingBalance(); //Retrieve the section id of starting balance.
                    string tid = "{" + Cell(SectionsDataGrid, "ID").ToString() + "}";   //Gets the active section id.
                    //Insert the section id to the current sections "Starting Balance" column.
                    string query = "UPDATE CashReportSections SET StartingBalance = @sid WHERE SECID = @id";
                    OleDbParameter[] p = new OleDbParameter[2];
                    p[0] = new OleDbParameter("@sid", sid);
                    p[1] = new OleDbParameter("@id", tid);

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                    if (ok) {
                        Notifier.ShowInformation("Starting balance successfully saved.", "Starting Balance");
                        int selectedIndex = SectionsDataGrid.SelectedRows[0].Index;
                        RefreshCashReportSections();
                        SectionsDataGrid.Rows[selectedIndex].Selected = true;
                        if (rdExpenses.Checked)
                            RefreshExpenses();
                        else if (rdIncome.Checked)
                            RefreshIncome();
                    } else
                        Notifier.ShowError("Failed to set the starting balance for this section.");
                }
            }
        }

        //Switch for Income or Expense
        private void NavRadio_CheckedChanged(object sender, EventArgs e) {
            RadioButton chk = (RadioButton)sender;
            if (chk.Checked) {
                chk.Font = new Font(chk.Font, FontStyle.Bold);
                ParticularsDataGrid.DataSource = null;
                if (chk == rdIncome) {
                    RefreshIncome();
                    btnAddStartingBal.Visible = true;
                }
                if (chk == rdExpenses) {
                    RefreshExpenses();
                    btnAddStartingBal.Visible = StartingBalancePanel.Visible = false;
                }
            } else {
                chk.Font = new Font(chk.Font, FontStyle.Regular);
            }
        }

        //Shows a context menu if user wants to freeze/unfreeze column headers.
        private void ParticularsDataGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e) {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.ColumnIndex >= 0) {
                ActiveColumn = e.ColumnIndex;
                bool frozenstate = ParticularsDataGrid.Columns[ActiveColumn].Frozen;
                string col_title = ParticularsDataGrid.Columns[ActiveColumn].HeaderText;
                if (!col_title.Trim().Equals("")) {
                    if (frozenstate) freezeUnfreezeColumnToolStripMenuItem.Text = "Unfreeze \"" + col_title + "\"";
                    else freezeUnfreezeColumnToolStripMenuItem.Text = "Freeze \"" + col_title + "\"";
                    ColumnsContextMenuStrip.Show(MousePosition);

                }
            }
        }

        #endregion

        #region Main Toolbar Eventhandlers

        //Refreshes Income/Expense datagrid.
        private void btnRefresh_Click(object sender, EventArgs e) {
            SuspendLayout();
            if (rdIncome.Checked) RefreshIncome();
            else if (rdExpenses.Checked) RefreshExpenses();
            ResumeLayout();
        }

        //Creates cash report values through imported CSV
        private void btnImportCSV_Click(object sender, EventArgs e) {
            if (btnImportCSV.Enabled && btnImportCSV.Visible) {
                if (CashReportLocked == false) {
                    ImportCRDialog dlg = new ImportCRDialog();
                    DateTime _sd, _ed;
                    DateTime.TryParse(CashReportStartDate, out _sd);
                    DateTime.TryParse(CashReportEndDate, out _ed);
                    dlg._STARTDATE = _sd;
                    dlg._ENDDATE = _ed;
                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                        string type = "";

                        //Iterate through all records.
                        foreach (DataRow row in dlg._DATASRC.Rows) {

                            //Get type
                            type = row["FT/EX"].ToString().ToUpper().Trim();

                            if (type == "FT") {
                                //FUND TRANSFER
                                string particular, date, plateno;
                                double amount = 0.0;

                                //Basic info
                                particular = row["Particular"].ToString();
                                date = row["Date"].ToString();
                                plateno = row["Plate No"].ToString();
                                Double.TryParse(row["Amount"].ToString(), out amount);

                                string query = "INSERT INTO CashReportItems(SECID, Particular, PlateNo, ItemDate, Amount, IsExpense) VALUES(@secid, @particular, @pn, @date, @amount, false)";
                                
                                OleDbParameter[] p = new OleDbParameter[5];
                                p[0] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
                                p[1] = new OleDbParameter("@particular", particular);
                                p[2] = new OleDbParameter("@pn", plateno);
                                p[3] = new OleDbParameter("@date", date);
                                p[4] = new OleDbParameter("@amount", amount);

                                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                            } else if (type == "EX") {
                                //EXPENSES
                                //@fuel, @violations, @toll, @texpenses, @rmv, 
                                //@sf, @others, @driver, @driverCA, @helper, @helperCA

                                string particular, date, plateno, driver, helper;
                                double amount = 0.0, sucat, fuel, violations, toll, texpenses, rmv, sf, others, driverCA, helperCA;

                                //Basic info
                                particular = row["Particular"].ToString();
                                date = row["Date"].ToString();
                                plateno = row["Plate No"].ToString();
                                Double.TryParse(row["Amount"].ToString(), out amount);

                                //Details
                                Double.TryParse(row["Petron Sucat Diesel"].ToString(), out sucat);
                                Double.TryParse(row["Other Diesel"].ToString(), out fuel);
                                Double.TryParse(row["Violations"].ToString(), out violations);
                                Double.TryParse(row["Toll"].ToString(), out toll);
                                Double.TryParse(row["Trucking Expenses"].ToString(), out texpenses);
                                Double.TryParse(row["RMV"].ToString(), out rmv);
                                Double.TryParse(row["Service Fee"].ToString(), out sf);
                                Double.TryParse(row["Others"].ToString(), out others);
                                Double.TryParse(row["Driver CA"].ToString(), out driverCA);
                                Double.TryParse(row["Helper CA"].ToString(), out helperCA);

                                //Employee
                                driver = row["Driver ID"].ToString();
                                helper = row["Helper ID"].ToString();

                                string query = "INSERT INTO CashReportItems(SECID, Particular, PlateNo, ItemDate, Amount, IsExpense, Fuel, Violations, Toll, TruckingExpenses, RMVehicle, ServiceFee, Others, Driver, DriverCA, Helper, HelperCA, PetronSucatDiesel) VALUES(@secid, @particular, @pn, @date, @amount, true, @fuel, @violations, @toll, @texpenses, @rmv, @sf, @others, @driver, @driverCA, @helper, @helperCA, @sucat)";

                                OleDbParameter[] p = new OleDbParameter[17];
                                p[0] = new OleDbParameter("@secid", "{" + WorkingSectionID + "}");
                                p[1] = new OleDbParameter("@particular", particular);
                                p[2] = new OleDbParameter("@pn", plateno);
                                p[3] = new OleDbParameter("@date", date);
                                p[4] = new OleDbParameter("@amount", amount);

                                p[5] = new OleDbParameter("@fuel", fuel);
                                p[6] = new OleDbParameter("@violations", violations);
                                p[7] = new OleDbParameter("@toll", toll);
                                p[8] = new OleDbParameter("@texpenses", texpenses);
                                p[9] = new OleDbParameter("@rmv", rmv);
                                p[10] = new OleDbParameter("@sf", sf);
                                p[11] = new OleDbParameter("@others", others);
                                p[12] = new OleDbParameter("@driver", "{" + driver + "}");
                                p[13] = new OleDbParameter("@driverCA", driverCA);
                                p[14] = new OleDbParameter("@helper", "{" + helper + "}");
                                p[15] = new OleDbParameter("@helperCA", helperCA);
                                p[16] = new OleDbParameter("@sucat", sucat);

                                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                            }
                        }
                        Notifier.ShowInformation("Records successfully imported to current cash report.", "Import");
                        RefreshRecords();
                    }
                } else {
                    Notifier.ShowWarning("This cash report has been marked final.", "Import CSV");
                }
            }
        }

        #endregion

        #region accelerator eventhandlers

        private void addToolStripMenuItem_Click(object sender, EventArgs e) {
            btnAddParticular.PerformClick();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e) {
            btnEditParticular.PerformClick();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
            btnDeleteParticular.PerformClick();
        }

        #endregion
       
    }

}
