﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using ClosedXML;
using ClosedXML.Excel;

namespace PayrollSystem {

    public class CashReportPage : SystemPage {

        #region Constructor

        //Constructor of the page.
        public CashReportPage() {
            //Design-time initialization.
            InitializeComponent();
            
            //Initialize page controls and filters whatnot.
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Export | PageToolbarButtons.MarkFinal | PageToolbarButtons.Delete | PageToolbarButtons.Archive | PageToolbarButtons.Refresh | PageToolbarButtons.View | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.SingleDatePeriod | FilterControls.FinalCheckbox);

            //Set the first date picker to custom date.
            DateTimePicker p = GetDatePicker1();
            p.Format = DateTimePickerFormat.Custom;
            p.CustomFormat = "yyyy";
            p.ShowUpDown = true;

            //Show final records by default.
            SetShowFinalCheckboxChecked(true);

            //Disallow Multiselect.
            SetRecordsDataGridMultiselect(false);
        }

        //Designer-generated settings.
        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // dpStart
            // 
            this.dpStart.ValueChanged += new System.EventHandler(this.dpStart_ValueChanged);
            // 
            // CashReportPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "CashReportPage";
            this.Text = "Cash Reports";
            this.OnAdd += new System.EventHandler(this.CashReportPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.CashReportPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.CashReportPage_OnDelete);
            this.OnArchive += new System.EventHandler(this.CashReportPage_OnArchive);
            this.OnExport += new System.EventHandler(this.CashReportPage_OnExport);
            this.OnSearch += new System.EventHandler(this.CashReportPage_OnSearch);
            this.OnFinalize += new System.EventHandler(this.CashReportPage_OnFinalize);
            this.OnRefresh += new System.EventHandler(this.CashReportPage_OnRefresh);
            this.OnRecordDoubleClicked += new System.EventHandler(this.CashReportPage_OnRecordDoubleClicked);
            this.OnRecordsSelectionChanged += new System.EventHandler(this.CashReportPage_OnRecordsSelectionChanged);
            this.OnShowFinalizeChecked += new System.EventHandler(this.CashReportPage_OnShowFinalizeChecked);
            this.Load += new System.EventHandler(this.CashReportPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Local Variables

        DataTable dt = null;           //Records database cache.
        Image lockicon = Properties.Resources._lock;

        #endregion

        #region Public Functions

        #endregion

        #region Local Functions

        //Gets array of existing CR titles.
        string[] GetExistingCRList() {
            List<String> lst = new List<String>();
            DataTable exst = DatabaseManager.GetMainDatabase().ExecuteQuery("SELECT Title FROM CashReport", false);
            foreach (DataRow r in exst.Rows) {
                lst.Add(r["Title"].ToString().Trim());
            }
            return lst.ToArray();
        }

        //Hide cash reports that are not finalized.
        void SetFinalRecordsVisible(bool visible) {

            foreach (DataGridViewRow r in GetRecordsDataGridView().Rows) {
                if (r.Cells["IsFinal"].Value.Equals(true)) {
                    CurrencyManager cm = (CurrencyManager)GetRecordsDataGridView().BindingContext[GetRecordsDataGridView().DataSource];
                    if (r.Selected) {
                        cm.SuspendBinding();
                        GetRecordsDataGridView().ClearSelection();
                    }
                    r.Visible = visible;
                    cm.ResumeBinding();
                }
            }
        }

        #endregion

        #region CRUD

        //Handle export cash report event.
        private void CashReportPage_OnExport(object sender, EventArgs e) {
            using (SaveFileDialog dlg = new SaveFileDialog()) {
                dlg.Filter = "Excel Spreadsheet (*.xlsx)|*.xlsx";
                dlg.Title = "Export Cash Report";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    //Get the selected cash report's ID.
                    String CRID = "{" + Cell("ID") + "}";

                    //Retrieve all sections that belongs to the selected Cash Report
                    String query = "SELECT A.SECID, SS.EndingBalance AS [StartingBalance], SS.Title AS [StartingBalanceTitle], SSCR.DateEnd AS [StartingBalanceDate] ,A.Title, B.Particular, B.PlateNo, B.ItemDate, B.IsExpense, B.Amount, B.PetronSucatDiesel, B.Fuel, B.Violations, B.Toll, B.TruckingExpenses, B.RMVehicle, B.ServiceFee, B.Others, (DRV.FirstName + ' ' + DRV.LastName) AS [Driver], B.DriverCA, (HLP.FirstName + ' ' + HLP.LastName) AS [Helper], B.HelperCA FROM ((((CashReportSections AS A INNER JOIN CashReportItems AS B ON A.SECID = B.SECID) LEFT JOIN Employee AS DRV ON B.Driver = DRV.EmployeeID) LEFT JOIN Employee AS HLP ON B.Helper = HLP.EmployeeID) LEFT JOIN CashReportSections AS SS ON A.StartingBalance = SS.SECID) LEFT JOIN CashReport AS SSCR ON SS.CRID = SSCR.CRID  ";
                    query += "WHERE A.CRID = @id ";
                    query += "ORDER BY B.ItemDate, B.PlateNo ";

                    //SQL Parameter.
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", CRID);

                    DataTable dataT = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);

                    //Create IEnumerable for LINQing
                    IEnumerable<DataRow> dataR = dataT.Rows.Cast<DataRow>();

                    //Check.
                    /*
                    foreach (DataRow row in dataR) {
                        foreach (object o in row.ItemArray)
                            Trace.Write(o + " : ");
                        Trace.WriteLine("");
                    }
                    */

                    //Group items by section.
                    IEnumerable<IGrouping<String, DataRow>> sectionGroup = dataR.GroupBy(x => x["SECID"].ToString());

                    //Prepare workbook here.
                    try {
                        ClosedXMLEditor wb = new ClosedXMLEditor();

                        //Define default styles here.
                        wb.FontName = "Calibri";
                        wb.FontSize = 11;

                        /////////////////WORKSHEET ONE: CASH FLOW STATEMENT///////////////////////////////////////////////////
                        wb.SetWorksheet("Cash Flow Statement").SetShowGridLines(false).PageSetup.SetPagesWide(1); //Create new tab/worksheet.

                        //Set defualt worksheet row and col height.
                        wb.ActiveWorksheet.RowHeight = 16;
                        wb.ActiveWorksheet.ColumnWidth = 11;

                        //Define column widths here.
                        wb.ActiveWorksheet.Column("C").Width = 38;
                        wb.ActiveWorksheet.Columns("D,E").Width = 14;

                        ////REPORT HEADER////

                        //Header main
                        wb.ActiveWorksheet.Row(1).Height = 35;
                        wb.SetAlignment(ClosedXMLEditor.TextAlignments.BottomCenter).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetFontSize(16);
                        wb.SetMerge("A1:E1", new XLCellData("CASH REPORT", XLCellValues.Text));

                        //Subheader
                        wb.SetFontSize(10).SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.TopCenter);
                        wb.SetMerge("A2:E2", new XLCellData("DATE PERIOD: " + DateTime.Parse(Cell("Date Start")).ToString("MMM dd, yyyy") + " - " + DateTime.Parse(Cell("Date End")).ToString("MMM dd, yyyy")));

                        //Subheader 2
                        wb.SetFontSize(8).SetFG(XLColor.Gray).SetFormatting(ClosedXMLEditor.CellFormats.Italic).SetAlignment(ClosedXMLEditor.TextAlignments.TopCenter);
                        wb.SetMerge("A3:E3", new XLCellData("SYSTEM GENERATED REPORT: " + DateTime.Now.ToString("MMMM dd, yyyy")));
                        ////////

                        //Item columns
                        XLCellDataCollection icols = new XLCellDataCollection();
                        icols.Add(new XLCellData("DATE"));
                        icols.Add(new XLCellData("PLATE NO"));
                        icols.Add(new XLCellData("PARTICULAR"));
                        icols.Add(new XLCellData("DEBIT"));
                        icols.Add(new XLCellData("CREDIT"));

                        //Iterate through sections (keys).
                        int startRow = 5;
                        foreach (String key in sectionGroup.Select(x => x.Key)) {
                            //Section Title
                            string sectionTitle = sectionGroup.Where(x => x.Key == key).SelectMany(x => x).First()["Title"].ToString();

                            //Section header
                            wb.SetFontSize(10).SetFG(XLColor.White).SetBG(XLColor.Black).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleCenter);
                            wb.SetMerge(startRow, 1, startRow, 5, new XLCellData(sectionTitle));
                            startRow++;

                            //Prepare item collections here.
                            List<XLCellDataCollection> income = new List<XLCellDataCollection>();
                            List<XLCellDataCollection> expenses = new List<XLCellDataCollection>();

                            //Totals
                            double incomeTotal = 0.0, expenseTotal = 0.0, endingBalance = 0.0, startingBalance = 0.0;

                            //Iterate through each items.
                            foreach (DataRow row in sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {

                                //WHAT IS NEEDED:
                                //DATE, PLATE NO, PARTICULAR, AMOUNT
                                //Write in corresponding expense or income.
                                string date, plateno, particular;
                                double amount;

                                date = DateTime.Parse(row["ItemDate"].ToString()).ToString("MM/dd/yy");
                                plateno = row["PlateNo"].ToString();
                                particular = row["Particular"].ToString();
                                amount = Convert.ToDouble(row["Amount"].ToString());

                                if ((bool)row["IsExpense"] == true) {
                                    //add to expense collection
                                    XLCellDataCollection col_expenses = new XLCellDataCollection();
                                    col_expenses.Add(new XLCellData(date, XLCellValues.DateTime));
                                    col_expenses.Add(new XLCellData(plateno, XLCellValues.Text));
                                    col_expenses.Add(new XLCellData(particular, XLCellValues.Text));
                                    if (amount >= 0) {
                                        col_expenses.Add(new XLCellData(amount, XLCellValues.Number));
                                        col_expenses.Add(new XLCellData("", XLCellValues.Text));           //Space to move to depit column
                                    } else {
                                        col_expenses.Add(new XLCellData("", XLCellValues.Text));           //Space to move to credit column
                                        col_expenses.Add(new XLCellData(amount, XLCellValues.Number));
                                    }
                                    expenses.Add(col_expenses);
                                } else {
                                    //add to income collection
                                    XLCellDataCollection col_income = new XLCellDataCollection();
                                    col_income.Add(new XLCellData(date, XLCellValues.DateTime));
                                    col_income.Add(new XLCellData(plateno, XLCellValues.Text));
                                    col_income.Add(new XLCellData(particular, XLCellValues.Text));
                                    if (amount >= 0) {
                                        col_income.Add(new XLCellData("", XLCellValues.Text));            //Space to move to credit column
                                        col_income.Add(new XLCellData(amount, XLCellValues.Number));
                                    } else {
                                        col_income.Add(new XLCellData(amount, XLCellValues.Number));
                                        col_income.Add(new XLCellData("", XLCellValues.Text));            //Space to move to debit column
                                    }
                                    income.Add(col_income);
                                }

                            }

                            //Set formats
                            wb.SetDateFormat("MM/dd/yy");
                            wb.SetNumberFormat("#,##0.00;(#,##0.00);-;@");

                            //Get starting balance.
                            //Trace.WriteLine(sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToDbl(y["StartingBalance"]))).Count());
                            startingBalance = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToDbl(y["StartingBalance"]))).First();

                            /////////// INCOME //////////////
                            if (income.Count > 0 || startingBalance != 0) {
                                //Insert rows of each Income
                                wb.SetBG(XLColor.NoColor).SetFG(XLColor.Black).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                wb.SetMerge(startRow, 1, startRow, 5, new XLCellData("ADD (FUND TRANSFER)"));
                                wb.ActiveWorksheet.Row(startRow).Height = 18;
                                startRow++;

                                //Write item colummns header.
                                int i = 1;
                                foreach (XLCellData colHeader in icols) {
                                    if (i >= 4) wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleRight);
                                    else wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                    wb.Set(startRow, i, colHeader.Value);
                                    i++;
                                }
                                startRow++;

                                //Write incomes
                                wb.SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);

                                //SPECIAL: IF STARTING BALANCE IS AVAILABLE, INSERT IT HERE
                                if (startingBalance != 0) {
                                    wb.SetFormatting(ClosedXMLEditor.CellFormats.Italic);
                                    string sb_title = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToStr(y["StartingBalanceTitle"]))).First();
                                    string sb_date = DateTime.Parse(sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => InputValidators.DBNullToStr(y["StartingBalanceDate"]))).First()).ToShortDateString();

                                    if (incomeTotal >= 0) {
                                        wb.Set(startRow, 5, startingBalance, XLCellValues.Number);
                                    } else {
                                        wb.Set(startRow, 4, startingBalance, XLCellValues.Number);
                                    }
                                    wb.Set(startRow, 1, sb_date, XLCellValues.DateTime);
                                    wb.Set(startRow, 3, sb_title, XLCellValues.Text);
                                    startRow++;
                                }
                                //NORMAL ITEMS:
                                wb.SetFormatting(ClosedXMLEditor.CellFormats.None);
                                foreach (XLCellDataCollection items in income) {
                                    wb.SetCols(1, startRow, items);
                                    wb.ActiveWorksheet.Row(startRow).Height = 16;
                                    startRow++;
                                }

                                //Income totals.
                                wb.SetFormatting(ClosedXMLEditor.CellFormats.Bold);
                                incomeTotal = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Where(y => Convert.ToBoolean(y["IsExpense"]) == false).Select(y => Convert.ToDouble(y["Amount"]))).Sum();
                                incomeTotal += startingBalance;
                                if (incomeTotal >= 0)
                                    wb.Set(startRow, 5, incomeTotal, XLCellValues.Number);
                                else
                                    wb.Set(startRow, 4, incomeTotal, XLCellValues.Number);
                                wb.Set(startRow, 1, "TOTAL", XLCellValues.Text);
                                wb.ActiveWorksheet.Range(startRow, 1, startRow, 5).Style.Border.SetTopBorder(XLBorderStyleValues.Thin).Border.SetTopBorderColor(XLColor.Black);

                                startRow += 2;  //Add space after INCOME subsection.
                            }
                            /////////// INCOME //////////////

                            /////////// EXPENSE //////////////
                            if (expenses.Count > 0) {
                                //Insert rows of each Income
                                wb.SetBG(XLColor.NoColor).SetFG(XLColor.Black).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                wb.SetMerge(startRow, 1, startRow, 5, new XLCellData("LESS (EXPENSES)"));
                                wb.ActiveWorksheet.Row(startRow).Height = 18;
                                startRow++;

                                //Write item colummns header.
                                int i = 1;
                                foreach (XLCellData colHeader in icols) {
                                    if (i >= 4) wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleRight);
                                    else wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                    wb.Set(startRow, i, colHeader.Value);
                                    i++;
                                }
                                startRow++;

                                //Write expenses
                                wb.SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                foreach (XLCellDataCollection items in expenses) {
                                    wb.SetCols(1, startRow, items);
                                    wb.ActiveWorksheet.Row(startRow).Height = 16;
                                    startRow++;
                                }

                                //Write total expenses
                                wb.SetFormatting(ClosedXMLEditor.CellFormats.Bold);
                                expenseTotal = sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Where(y => Convert.ToBoolean(y["IsExpense"]) == true).Select(y => Convert.ToDouble(y["Amount"]))).Sum();
                                if (expenseTotal >= 0)
                                    wb.Set(startRow, 4, expenseTotal, XLCellValues.Number);
                                else
                                    wb.Set(startRow, 5, expenseTotal, XLCellValues.Number);
                                wb.Set(startRow, 1, "TOTAL", XLCellValues.Text);
                                wb.ActiveWorksheet.Range(startRow, 1, startRow, 5).Style.Border.SetTopBorder(XLBorderStyleValues.Thin).Border.SetTopBorderColor(XLColor.Black);
                                startRow++;
                            }
                            /////////// EXPENSE //////////////

                            /////// ENDING BALANCE ///////////
                            endingBalance = incomeTotal - expenseTotal;
                            if (endingBalance >= 0)
                                wb.Set(startRow, 5, Math.Abs(endingBalance), XLCellValues.Number);
                            else
                                wb.Set(startRow, 4, Math.Abs(endingBalance), XLCellValues.Number);
                            wb.Set(startRow, 1, "ENDING BALANCE", XLCellValues.Text);
                            wb.Set(startRow, 3, sectionTitle, XLCellValues.Text);
                            startRow++;
                            /////// ENDING BALANCE ///////////
                            
                            startRow += 2; //Add space for next section
                        }

                        /////////////////END OF WORKSHEET ONE: CASH FLOW STATEMENT/////////////////////////////////////////////
                        
                        ///////////////////////////// WORKSHEET TWO: EXPENSE BREAKDOWN ///////////////////////////////////////

                        wb.SetWorksheet("Expenses Breakdown").SetShowGridLines(false).PageSetup.SetPageOrientation(XLPageOrientation.Landscape).SetPagesWide(1); //Create new tab/worksheet.

                        //Set defualt worksheet row and col height.
                        wb.ActiveWorksheet.RowHeight = 16;
                        wb.ActiveWorksheet.ColumnWidth = 12;

                        //Define column widths here.
                        wb.ActiveWorksheet.Columns("N,O").Width = 23;
                        wb.ActiveWorksheet.Columns("C").Width = 22;
                        wb.ActiveWorksheet.Columns("A,B").Width = 10.5;
                        //wb.ActiveWorksheet.Columns("H").Width = 16;

                        ////REPORT HEADER////

                        //Header main
                        wb.ActiveWorksheet.Row(1).Height = 35;
                        wb.SetAlignment(ClosedXMLEditor.TextAlignments.BottomLeft).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetFontSize(16);
                        wb.SetMerge("A1:M1", new XLCellData("EXPENSES BREAKDOWN", XLCellValues.Text));

                        //Subheader 1
                        wb.SetFontSize(11).SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.BottomLeft);
                        wb.SetMerge("A2:M2", new XLCellData(Cell("Title")));

                        //Subheader 2
                        wb.SetFontSize(10).SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.TopLeft);
                        wb.SetMerge("A3:M3", new XLCellData("DATE PERIOD: " + DateTime.Parse(Cell("Date Start")).ToString("MMM dd, yyyy") + " - " + DateTime.Parse(Cell("Date End")).ToString("MMM dd, yyyy")));
                        wb.ActiveWorksheet.Row(3).Height = 20;

                        //Item columns
                        icols.Clear();
                        icols = new XLCellDataCollection();
                        icols.Add(new XLCellData("DATE"));
                        icols.Add(new XLCellData("PLATE NO"));
                        icols.Add(new XLCellData("PARTICULAR"));
                        icols.Add(new XLCellData("AMOUNT"));
                        icols.Add(new XLCellData("PETRON SUCAT DIESEL"));
                        icols.Add(new XLCellData("OTHER DIESEL"));
                        icols.Add(new XLCellData("VIOLATIONS"));
                        icols.Add(new XLCellData("TOLL"));
                        icols.Add(new XLCellData("TRUCKING EXP."));
                        icols.Add(new XLCellData("RM VEHICLE"));
                        icols.Add(new XLCellData("SERVICE FEE"));
                        icols.Add(new XLCellData("OTHERS"));
                        icols.Add(new XLCellData("CA DRIVER"));
                        icols.Add(new XLCellData("CA HELPER"));
                        icols.Add(new XLCellData("DRIVER"));
                        icols.Add(new XLCellData("HELPER"));

                        //Group items by section.
                        sectionGroup = dataR.Where(x => Convert.ToBoolean(x["IsExpense"]) == true).OrderBy(x =>  x["PlateNo"].ToString()).ThenBy(x => DateTime.Parse(x["ItemDate"].ToString())).GroupBy(x => x["PlateNo"].ToString());

                        //Grand totals.
                        double gtotalAmount = 0.0, gtotalFuel = 0.0, gtotalViolations = 0.0, gtotalToll = 0.0,
                                   gtotalTExpenses = 0.0, gtotalRMV = 0.0, gtotalSF = 0.0, gtotalOthers = 0.0,
                                   gtotalDriverCA = 0.0, gtotalHelperCA = 0.0, gtotalPetronSucat = 0.0;

                        //Iterate through plate nos. (keys)
                        startRow = 4;
                        wb.SetNumberFormat("#,##0.00;(#,##0.00);-;@");
                        foreach (String key in sectionGroup.Select(x => x.Key)) {
                            //Write item colummns header.
                            int i = 1;
                            wb.SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetBorders(ClosedXMLEditor.CellBorders.Bottom).SetBorderStyle(XLBorderStyleValues.Thin);
                            foreach (XLCellData colHeader in icols) {
                                if (i >= 4 && i <= 13) wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleRight);
                                else wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                                wb.Set(startRow, i, colHeader.Value);
                                i++;
                            }
                            startRow++;

                            //Totals
                            double totalAmount = 0.0, totalFuel = 0.0, totalViolations = 0.0, totalToll = 0.0,
                                   totalTExpenses = 0.0, totalRMV = 0.0, totalSF = 0.0, totalOthers = 0.0,
                                   totalDriverCA = 0.0, totalHelperCA = 0.0, totalPetronSucat = 0.0;

                            //Iterate through each items.
                            wb.SetFormatting(ClosedXMLEditor.CellFormats.None).SetBorders(ClosedXMLEditor.CellBorders.None);
                            foreach (DataRow row in sectionGroup.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {
                                /*
                                 icols.Add(new XLCellData("AMOUNT"));
                                  icols.Add(new XLCellData("FUEL"));
                                  icols.Add(new XLCellData("VIOLATIONS"));
                                  icols.Add(new XLCellData("TOLL"));
                                  icols.Add(new XLCellData("TRUCKING EXPENSES"));
                                  icols.Add(new XLCellData("RM VEHICLE"));
                                  icols.Add(new XLCellData("SERVICE FEE"));
                                  icols.Add(new XLCellData("OTHERS"));
                                  icols.Add(new XLCellData("CA DRIVER"));
                                  icols.Add(new XLCellData("CA HELPER"));
                                  icols.Add(new XLCellData("DRIVER"));
                                  icols.Add(new XLCellData("HELPER"));
                               */

                                //WHAT IS NEEDED:
                                //DATE, PLATE NO, PARTICULAR, AMOUNT
                                //Write in corresponding expense or income.
                                string date, plateno, particular, driver, helper;
                                double amount, sucatd, fuel, violations, toll, texpenses, rmv, sf, others, driverca, helperca;

                                date = DateTime.Parse(row["ItemDate"].ToString()).ToString("MM/dd/yy");
                                plateno = row["PlateNo"].ToString();
                                particular = row["Particular"].ToString();

                                driver = row["Driver"].ToString();
                                helper = row["Helper"].ToString();

                                totalAmount     += amount = Convert.ToDouble(row["Amount"].ToString());
                                totalPetronSucat+= sucatd = Convert.ToDouble(InputValidators.DBNullToDbl(row["PetronSucatDiesel"]));
                                totalFuel       += fuel = Convert.ToDouble(InputValidators.DBNullToDbl(row["Fuel"]));
                                totalViolations += violations = Convert.ToDouble(InputValidators.DBNullToDbl(row["Violations"]));
                                totalToll       += toll = Convert.ToDouble(InputValidators.DBNullToDbl(row["Toll"]));
                                totalTExpenses  += texpenses = Convert.ToDouble(InputValidators.DBNullToDbl(row["TruckingExpenses"]));
                                totalRMV        += rmv = Convert.ToDouble(InputValidators.DBNullToDbl(row["RMVehicle"]));
                                totalSF         += sf = Convert.ToDouble(InputValidators.DBNullToDbl(row["ServiceFee"]));
                                totalOthers     += others = Convert.ToDouble(InputValidators.DBNullToDbl(row["Others"]));
                                totalDriverCA   += driverca = Convert.ToDouble(InputValidators.DBNullToDbl(row["DriverCA"]));
                                totalHelperCA   += helperca = Convert.ToDouble(InputValidators.DBNullToDbl(row["HelperCA"]));

                                wb.Set(startRow, 1, date, XLCellValues.DateTime);
                                wb.Set(startRow, 2, plateno, XLCellValues.Text);
                                wb.Set(startRow, 3, particular, XLCellValues.Text);

                                wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleRight);

                                wb.Set(startRow, 4, amount, XLCellValues.Number);

                                wb.Set(startRow, 5, sucatd, XLCellValues.Number);
                                wb.Set(startRow, 6, fuel, XLCellValues.Number);
                                wb.Set(startRow, 7, violations, XLCellValues.Number);
                                wb.Set(startRow, 8, toll, XLCellValues.Number);
                                wb.Set(startRow, 9, texpenses, XLCellValues.Number);
                                wb.Set(startRow, 10, rmv, XLCellValues.Number);
                                wb.Set(startRow, 11, sf, XLCellValues.Number);
                                wb.Set(startRow, 12, others, XLCellValues.Number);
                                wb.Set(startRow, 13, driverca, XLCellValues.Number);
                                wb.Set(startRow, 14, helperca, XLCellValues.Number);

                                wb.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);

                                wb.Set(startRow, 15, driver, XLCellValues.Text);
                                wb.Set(startRow, 16, helper, XLCellValues.Text);

                                startRow++;
                            }

                            //Add border separator.
                            wb.ActiveWorksheet.Range(startRow, 1, startRow, 16).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                            //Insert totals for each section.
                            wb.SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetBorders(ClosedXMLEditor.CellBorders.None);
                            wb.Set(startRow, 4, totalAmount, XLCellValues.Number);
                            wb.Set(startRow, 5, totalPetronSucat, XLCellValues.Number);
                            wb.Set(startRow, 6, totalFuel, XLCellValues.Number);
                            wb.Set(startRow, 7, totalViolations, XLCellValues.Number);
                            wb.Set(startRow, 8, totalToll, XLCellValues.Number);
                            wb.Set(startRow, 9, totalTExpenses, XLCellValues.Number);
                            wb.Set(startRow, 10, totalRMV, XLCellValues.Number);
                            wb.Set(startRow, 11, totalSF, XLCellValues.Number);
                            wb.Set(startRow, 12, totalOthers, XLCellValues.Number);
                            wb.Set(startRow, 13, totalDriverCA, XLCellValues.Number);
                            wb.Set(startRow, 14, totalHelperCA, XLCellValues.Number);
                            startRow++;

                            //add to grand totals.
                            gtotalAmount        += totalAmount;
                            gtotalPetronSucat   += totalPetronSucat;
                            gtotalFuel          += totalFuel;
                            gtotalViolations    += totalViolations;
                            gtotalToll          += totalToll;
                            gtotalTExpenses     += totalTExpenses;
                            gtotalRMV           += totalRMV;
                            gtotalSF            += totalSF;
                            gtotalOthers        += totalOthers;
                            gtotalDriverCA      += totalDriverCA;
                            gtotalHelperCA      += totalHelperCA;

                            startRow += 2;  //Add some space for the next plate no category.
                        }

                        //Insert Grand totals
                        wb.SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetBorders(ClosedXMLEditor.CellBorders.None);
                        wb.Set(startRow, 1,"TOTALS", XLCellValues.Text);
                        wb.Set(startRow, 4, gtotalAmount, XLCellValues.Number);
                        wb.Set(startRow, 5, gtotalPetronSucat, XLCellValues.Number);
                        wb.Set(startRow, 6, gtotalFuel, XLCellValues.Number);
                        wb.Set(startRow, 7, gtotalViolations, XLCellValues.Number);
                        wb.Set(startRow, 8, gtotalToll, XLCellValues.Number);
                        wb.Set(startRow, 9, gtotalTExpenses, XLCellValues.Number);
                        wb.Set(startRow, 10, gtotalRMV, XLCellValues.Number);
                        wb.Set(startRow, 11, gtotalSF, XLCellValues.Number);
                        wb.Set(startRow, 12, gtotalOthers, XLCellValues.Number);
                        wb.Set(startRow, 13, gtotalDriverCA, XLCellValues.Number);
                        wb.Set(startRow, 14, gtotalHelperCA, XLCellValues.Number);

                        /////////////////////////// END WORKSHEET TWO: EXPENSE BREAKDOWN /////////////////////////////////////

                        wb.ActiveWorksheet.Columns("A:O").AdjustToContents();   //Compact columns.

                        ///////////////////////////////////// SAVE WORKBOOK ///////////////////////////////////////////////////
                        dataT.Dispose();            //Dispose data table to save space.

                        if (wb.Save(dlg.FileName))    //Save workbook.
                            Notifier.ShowInformation("Cash Report was successfully exported as Excel spreadsheet.", "Export Complete");
                        else {
                            Notifier.ShowError("Failed to export cash report.\n\nIt may be that file is inaccessible, drive is unavailable, or file is opened in another application.");
                        }
                        wb.Dispose();
                    } catch (Exception ex) {
                        Notifier.ShowError("Failed to export cash report.\n\n" + ex.Message);
                    }
                }
            }
        }

        //Event handler when user is deleting the current cash report.
        private void CashReportPage_OnDelete(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                if (Cell("IsFinal").Equals("False")) {
                    if (Notifier.ShowConfirm("Are you sure you want to permanently delete the selected cash report?\n\n"
                        + Cell("Title") + "\n" + Convert.ToDateTime(Cell("Date Start")).ToShortDateString()
                        + " to " + Convert.ToDateTime(Cell("Date End")).ToShortDateString(), "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                        
                        //Since no sproc for delete, define SQL here.
                        string query = "DELETE FROM CashReport WHERE CRID = @id";
                        
                        //IIF([@archived]='y',True,IIF([@archived]='n',False,False))
                        //WHERE CRID=[@id];
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        if (ok) {
                            Notifier.ShowInformation("Cash report was permanently deleted.", "Deleted Cash Report");
                            RefreshRecords();
                        } else
                            Notifier.ShowError("Failed to delete this cash report. Please refresh and try again.");
                    }
                }
            }
        }

        //Eventhandler when user is archiving the final cash report.
        private void CashReportPage_OnArchive(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                if (Cell("IsFinal").Equals("True")) {
                    if (Notifier.ShowConfirm("Are you sure you want to archive the selected cash report?", "Confirm Archive") == System.Windows.Forms.DialogResult.Yes) {
                        //IIF([@archived]='y',True,IIF([@archived]='n',False,False))
                        //WHERE CRID=[@id];
                        OleDbParameter[] p = new OleDbParameter[2];
                        p[0] = new OleDbParameter("@isarchived", "y");
                        p[1] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Archive_CashReport", true, p);
                        if (ok) {
                            Notifier.ShowInformation("Cash report was successfully moved to the archives section.", "Archive Cash Report");
                            RefreshRecords();
                        } else
                            Notifier.ShowError("Failed to archive this cash report. Please refresh and try again.");
                    }
                }
            }
        }

        //Eventhandler when user is accessing the selected record.
        private void CashReportPage_OnRecordDoubleClicked(object sender, EventArgs e) {
            CashReportEditorPage pg = new CashReportEditorPage();
            MainWindow wnd = (MainWindow)ParentForm;
            
            //Set working values.
            pg.WorkingCashReportID = Cell("ID");
            pg.CashReportTitle = Cell("Title");
            pg.CashReportStartDate = DateTime.Parse(Cell("Date Start")).ToString("MM/dd/yy");
            pg.CashReportEndDate = DateTime.Parse(Cell("Date End")).ToString("MM/dd/yy");
            pg.CashReportLocked = Convert.ToBoolean(Cell("isFinal"));

            Visible = Enabled = false;
            if (wnd != null) {
                pg.Text = Cell("Title");
                pg.CashReportPrevInstance = this;
                wnd.LoadPageNext(pg);
            }
        }
        
        //Eventhandler on Mark Final button clicked.
        private void CashReportPage_OnFinalize(object sender, EventArgs e) {
            if (Cell("IsFinal").Equals("False")) {
                if (Notifier.ShowConfirm("Making a cash report statement final will lock it, thus preventing further modifications. Marking it final also allows the cash report to be included in your payroll computations.\n\nAre you sure you want to make the selected cash report final?", "Finalize Cash Report") == System.Windows.Forms.DialogResult.Yes) {
                    //SET IsFinal = IIF([@isfinal]="y", TRUE, IIF([@isfinal] = "n", FALSE, FALSE))
                    //WHERE CRID=[@id];
                    OleDbParameter[] p = new OleDbParameter[2];
                    p[0] = new OleDbParameter("@isfinal", "y");
                    p[1] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Final_CashReport", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Cash report was successfully marked as final.", "Finalize Cash Report");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("Failed to mark this cash report as final. Please refresh and try again.");
                }
            }
        }

        //Eventhandler on add button clicked.
        private void CashReportPage_OnAdd(object sender, EventArgs e) {
            CashReportDialog dlg = new CashReportDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetExistingCR(GetExistingCRList());
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //VALUES ([@dstart], [@dend], [@title], [@dcreated], [@author]);
                string dstart, dend, title, dcreated, author;
                dlg.GetFieldValues(out title, out dstart, out dend);
                author = UserManager.ACCOUNT_NAME;
                dcreated = DateTime.Now.ToString("MM/dd/yy HH:mm");
                OleDbParameter[] p = new OleDbParameter[5];
                p[0] = new OleDbParameter("@dstart", dstart);
                p[1] = new OleDbParameter("@dend", dend);
                p[2] = new OleDbParameter("@title", title);
                p[3] = new OleDbParameter("@dcreated", dcreated);
                p[4] = new OleDbParameter("@author", author);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_CashReport", true, p);
                if (ok) {
                    Notifier.ShowInformation("A new cash report has been successfully created.", "New Cash Report");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to create a new cash report.");
            }
        }

        //Eventhanlder on refresh button clicked
        private void CashReportPage_OnRefresh(object sender, EventArgs e) {
            //WHERE (Year([DateStart])=IIf([@year]='' Or IsNull([@year]),Year([DateStart]),[@year])) And 
            //(([Title] Like '%' & [@search] & '%') Or ([Author] Like '%' & [@search] & '%')) And 
            //IsArchived=IIf([@isarchived]="y",True,IIf([@isarchived]="n",False,False))

            //@year
            //@searh
            //@isarchived
            SuspendLayout();    //Suspend form layouting while we're loading from the database.

            OleDbParameter[] p = new OleDbParameter[3];
            p[0] = new OleDbParameter("@year", GetStartDate().Year);
            p[0].DbType = DbType.Int16;
            p[1] = new OleDbParameter("@search", GetSearchText());
            p[2] = new OleDbParameter("@isarchived", "n");

            //Get data from database.
            if (dt == null) {
                dt = new DataTable();
                DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReport", true, dt, p);

                //Insert image representations of locked/final records.
                DataColumn icol = dt.Columns.Add("IMG", typeof(Image));
                icol.SetOrdinal(0);

            } else {
                dt.Clear();
                DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReport", true, dt, p);
            }

            //dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_CashReport", true, p);

            //Add lock icon if record is marked final.
            foreach (DataRow r in dt.Rows) {
                if ((bool)r["IsFinal"]) r[0] = lockicon;
            }

            //Set DataSource of the datagrid if not set.
            if (GetRecordsDataSource() == null) {
                SetRecordsDataSource(dt);

                //Customize the lock image column.
                DataGridViewColumn finalcol = GetColumn("IMG");
                finalcol.Width = 30;
                finalcol.Resizable = DataGridViewTriState.False;
                finalcol.DefaultCellStyle.NullValue = null;
                finalcol.ToolTipText = "Finalized";

                //Remove the column header text of the lock indicator.
                SetColumnHeader("IMG", "");
                SetColumnHeader("Created", "Date Created");

                //Hide unnecessary columns.
                HideColumn("ID");
                HideColumn("IsFinal");
                HideColumn("IsArchived");

                //Resize some columns
                finalcol = GetColumn("Title");
                finalcol.Width = 250;
                finalcol = GetColumn("Created");
                finalcol.Width = 150;
                finalcol = GetColumn("Author");
                finalcol.Width = 200;

                //Sort by start date.
                GetRecordsDataGridView().Sort(GetRecordsDataGridView().Columns["Date Start"], System.ComponentModel.ListSortDirection.Descending);

                //Show or hide the final records based on checkbox.
                SetFinalRecordsVisible(GetShowFinal());
            }

            UpdateInternalIDs();

            ResumeLayout(); //Resume form layouting.
        }

        //Eventhandler on editing CashReport.
        private void CashReportPage_OnEdit(object sender, EventArgs e) {
            CashReportDialog dlg = new CashReportDialog();
            dlg.SetExistingCR(GetExistingCRList());
            dlg.SetFieldValues(Cell("Title"), Cell("Date Start"), Cell("Date End"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                // SET Title = [@title] WHERE CRID=[@crid];
                string dstart, dend, title;
                dlg.GetFieldValues(out title, out dstart, out dend);
                OleDbParameter[] p = new OleDbParameter[2];
                p[0] = new OleDbParameter("@title", title);
                p[1] = new OleDbParameter("@crid", "{" + Cell("ID") + "}");
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_CashReport", true, p);
                if (ok) {
                    Notifier.ShowInformation("Cash report successfully renamed.", "Update Cash Report");
                    RefreshRecords();
                } else
                    Notifier.ShowError("Failed to update cash report.");
            }
        }

        //Eventhandler when search is triggered.
        private void CashReportPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        //Eventhandler when the "Show Final Records" is ticked.
        private void CashReportPage_OnShowFinalizeChecked(object sender, EventArgs e) {
            SetFinalRecordsVisible(GetShowFinal());
        }

        #endregion

        #region Window Eventhandlers

        //Load the last date filter.
        private void CashReportPage_Load(object sender, EventArgs e) {
            SuspendLayout();
            SetStartDate(GlobalVariables.CRPage_Date.ToString());
            ResumeLayout();
        }

        #endregion

        #region Control Eventhandlers

        //event handler when user changed selected records.
        private void CashReportPage_OnRecordsSelectionChanged(object sender, EventArgs e) {
            if (Cell("IsFinal").Equals("True")) {
                SetDeleteButtonVisible(false);
                SetFinalButtonVisible(false);
                SetEditButtonVisible(false);
            } else {
                SetArchiveButtonVisible(false);
            }
        }

        //Remember the last date filter set.
        private void dpStart_ValueChanged(object sender, EventArgs e) {
            GlobalVariables.CRPage_Date = GetStartDate();
        }

        #endregion
    }

}
