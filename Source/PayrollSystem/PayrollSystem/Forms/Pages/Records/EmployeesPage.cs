﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    public class EmployeesPage : SystemPage {

        #region Controls

        ToolStripButton btnToggleNameView = new ToolStripButton("Surname First", Properties.Resources.rearrange);

        #endregion

        #region Constructor

        public EmployeesPage() {
            InitializeComponent();

            btnToggleNameView.Click += btnToggleNameView_Click;
            btnToggleNameView.AutoToolTip = false;
            btnToggleNameView.ToolTipText = "Click to toggle surname/first name first view";
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // EmployeesPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "EmployeesPage";
            this.Text = "Employees";
            this.OnAdd += new System.EventHandler(this.EmployeesPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.EmployeesPage_OnEdit);
            this.OnArchive += new System.EventHandler(this.EmployeesPage_OnArchive);
            this.OnSearch += new System.EventHandler(this.EmployeesPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.EmployeesPage_OnRefresh);
            this.Load += new System.EventHandler(this.EmployeesPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Control Eventhandlers

        void btnToggleNameView_Click(object sender, EventArgs e) {
            name_view = (name_view == 1) ? 0 : 1;
            if (name_view == 1) {
                btnToggleNameView.Text = "First name first";
            } else if (name_view == 0) {
                btnToggleNameView.Text = "Surname first";
            }
            RefreshRecords();
        }

        #endregion

        #region Local Variables

        DataTable dt = null;
        int name_view = 0;  // 0 - last name first; 1 - first name first

        #endregion

        #region Public Functions

        #endregion

        #region Window eventhandlers

        private void EmployeesPage_Load(object sender, EventArgs e) {
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Archive | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
            AddCustomToolbarButton(btnToggleNameView);
            SetRecordsDataGridMultiselect(false);
        }

        #endregion

        #region Public Functions

        public static List<Employee> QueryEmployeesList() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "n");

            List<Employee> dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p).AsEnumerable().Select(x => new Employee(x["First Name"].ToString(), x["Middle Name"].ToString(), x["Last Name"].ToString(), "", x["EmployeeID"].ToString())).ToList();
            return dta;
        }

        public static DataTable QueryEmployees() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "n");

            DataTable dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            return dta;
        }

        public static DataTable QueryEmployeesArchived() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "y");

            DataTable dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            return dta;
        }

        public static List<String> QueryEmployeeNames() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "n");

            DataTable dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            List<String> names = new List<String>();
            foreach (DataRow row in dta.Rows) {
                names.Add(row["FullName2"].ToString());
            }
            return names;
        }

        public static List<String> QueryEmployeeNamesArchived() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "y");

            DataTable dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            List<String> names = new List<String>();
            foreach (DataRow row in dta.Rows) {
                names.Add(row["FullName2"].ToString());
            }
            return names;
        }

        public static List<ComboboxDataItem> QueryEmployeeForComboBox() {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@isarchived", "n");

            DataTable dta = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            List<ComboboxDataItem> names = new List<ComboboxDataItem>();
            foreach (DataRow row in dta.Rows) {
                names.Add(new ComboboxDataItem(row["EmployeeID"].ToString(), row["FullName2"].ToString()));
            }
            return names.OrderBy(x => x.Value).ToList();
        }

        #endregion

        #region CRUD

        private void EmployeesPage_OnAdd(object sender, EventArgs e) {
            EmployeeDialog dlg = new EmployeeDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetExistingNames(QueryEmployeeNames());
            dlg.SetExistingNamesArchived(QueryEmployeeNamesArchived());
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //VALUES ([@fn], [@mn], [@ln], [@dhired]);
                string fn, mn, ln, dhired;
                dlg.GetFieldValues(out fn, out mn, out ln, out dhired);
                OleDbParameter[] p = new OleDbParameter[4];
                p[0] = new OleDbParameter("@fn", fn);
                p[1] = new OleDbParameter("@mn", mn);
                if (mn.Trim() == "") p[1].Value = DBNull.Value;
                p[2] = new OleDbParameter("@ln", ln);
                p[3] = new OleDbParameter("@dhired", dhired);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_Employee", true, p);
                if (ok) {
                    Notifier.ShowInformation("New employee successfully added.", "Add Employee");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to add new employee record.");
                }
            }
        }

        private void EmployeesPage_OnRefresh(object sender, EventArgs e) {
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@isarchived", "n");

            if (dt != null) dt.Dispose();
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Employee", true, p);
            SetRecordsDataSource(dt);
            HideColumn("EmployeeID");
            HideColumn("First Name");
            HideColumn("Middle Name");
            HideColumn("Last Name");
            if (name_view == 0) {
                HideColumn("FullName2");
            } else {
                HideColumn("FullName1");
            }
            SetColumnHeader("FullName1", "Employee");
            SetColumnHeader("FullName2", "Employee");
            RecordsDGVFitColumnToContent("FullName1");
            RecordsDGVFitColumnToContent("FullName2");
        }

        private void EmployeesPage_OnEdit(object sender, EventArgs e) {
            EmployeeDialog dlg = new EmployeeDialog();
            dlg.SetFieldValues(Cell("First Name"), Cell("Middle Name"), Cell("Last Name"), Cell("Date Hired"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //SET FirstName = [@fn], MiddleName = [@mn], LastName = [@ln], DateHired = [@hired] WHERE EmployeeID=[@id];
                string fn, mn, ln, dhired;
                dlg.GetFieldValues(out fn, out mn, out ln, out dhired);
                OleDbParameter[] p = new OleDbParameter[5];
                p[0] = new OleDbParameter("@fn", fn);
                p[1] = new OleDbParameter("@mn", mn);
                if (mn.Trim() == "") p[1].Value = DBNull.Value;
                p[2] = new OleDbParameter("@ln", ln);
                p[3] = new OleDbParameter("@dhired", dhired);
                p[4] = new OleDbParameter("@id", "{" + Cell("EmployeeID") + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_Employee", true, p);
                if (ok) {
                    Notifier.ShowInformation("Employee record successfully updated.", "Update Employee Record");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("There was a problem updating the selected employee record.");
                }
            }
        }

        private void EmployeesPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void EmployeesPage_OnArchive(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to archive the selected employee record?", "Confirm Archive") == System.Windows.Forms.DialogResult.Yes) {
                    // SET IsArchived = IIf([@archived]='y',True,IIf([@archived]='n',False,False)) WHERE (((Employee.[EmployeeID])=[@id]));
                    OleDbParameter[] p = new OleDbParameter[2];
                    p[0] = new OleDbParameter("@archived", "y");
                    p[1] = new OleDbParameter("@id","{" + Cell("EmployeeID") + "}");
                    bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Archive_Employee", true, p);
                    if (ok) {
                        Notifier.ShowInformation("Employee record successfully archived. You can find it on the Employee Archives page.", "Archive Record");
                        RefreshRecords();
                    } else {
                        Notifier.ShowError("Failed to perform archive operation on selected record.");
                    }
                }
            }
        }

        #endregion

    }

}
