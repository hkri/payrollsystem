﻿
/* NET Namespaces */
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

/* ClosedXML Namespaces */
using ClosedXML;
using ClosedXML.Excel;

namespace PayrollSystem {

    class TruckingLogPage : SystemPage {

        #region local data

        DataTable dt;
        Image lockicon = (Image)Properties.Resources._lock;

        #endregion

        #region Constructor

        //Constructor
        public TruckingLogPage() {
            //Initialize design-time components
            InitializeComponent();

            //Setup visible toolbar and filter bar buttons whatnot.
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Import | PageToolbarButtons.Archive | PageToolbarButtons.MarkFinal | PageToolbarButtons.Export | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.RangeDatePeriod | FilterControls.FinalCheckbox);
            SetShowFinalCheckboxChecked(true);
           
            SetTotalsButtonVisible(true);
            SetRecordsDataGridMultiselect(true);

            //Setup for totals datagrid view.
            SetTotalsVisible(true);
            InitializeTotals();

            //Test totals content.
            //AddTotalInfo("Test1", 1241);
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // TruckingLogPage
            // 
            this.AllowsArchiveOfUnlockedRecords = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(706, 474);
            this.Name = "TruckingLogPage";
            this.Text = "Trucking and Transport Log";
            this.OnAdd += new System.EventHandler(this.TruckingLogPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.TruckingLogPage_OnEdit);
            this.OnArchive += new System.EventHandler(this.TruckingLogPage_OnArchive);
            this.OnImport += new System.EventHandler(this.TruckingLogPage_OnImport);
            this.OnExport += new System.EventHandler(this.TruckingLogPage_OnExport);
            this.OnSearch += new System.EventHandler(this.TruckingLogPage_OnSearch);
            this.OnFinalize += new System.EventHandler(this.TruckingLogPage_OnFinalize);
            this.OnRefresh += new System.EventHandler(this.TruckingLogPage_OnRefresh);
            this.OnRecordsSelectionChanged += new System.EventHandler(this.TruckingLogPage_OnRecordsSelectionChanged);
            this.OnShowFinalizeChecked += new System.EventHandler(this.TruckingLogPage_OnShowFinalizeChecked);
            this.Load += new System.EventHandler(this.TruckingLogPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region CRUD

        //Imports trucking log from CSV file.
        private void TruckingLogPage_OnImport(object sender, EventArgs e) {
            //Call the import trucking log CSV dialog to verify data integrity before inserting to database.
            using (ImportTruckingLogDialog dlg = new ImportTruckingLogDialog()) {
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                    bool lockComplete = false;  //Flag if records with complete fields will automatically be locked.

                    //Ask user if lock complete.
                    if (Notifier.ShowConfirm("Do you want to lock complete records from the imported CSV?", "Lock Records") == System.Windows.Forms.DialogResult.Yes)
                        lockComplete = true;

                    //Get list of valid entries.
                    List<TruckingLogRecord> recs = dlg.GetFieldValues();

                    //Insert one by one.
                    bool success = true;
                    foreach (TruckingLogRecord rec in recs) {
                        //Finalize record.
                        bool finalize = false;

                        //If lock on complete = true, then check each field if complete.
                        if (lockComplete) {
                            if (rec.Date.Trim().Equals("") == false &&
                               rec.PlateNumber.Trim().Equals("") == false &&
                               rec.Destination.Trim().Equals("") == false &&
                               rec.DRNo.Trim().Equals("") == false &&
                               rec.Driver.Trim().Equals("") == false &&
                               rec.Helper.Trim().Equals("") == false)
                                finalize = true;
                        }

                        OleDbParameter[] p = new OleDbParameter[11];
                        p[0] = new OleDbParameter("@tdate", rec.Date);
                        p[0].DbType = DbType.DateTime;
                        p[1] = new OleDbParameter("@pn", rec.PlateNumber);
                        p[2] = new OleDbParameter("@dest", rec.Destination);
                        p[3] = new OleDbParameter("@rate", rec.Rate);
                        p[4] = new OleDbParameter("@dr", rec.DRNo);
                        p[5] = new OleDbParameter("@driver", rec.Driver);
                        p[6] = new OleDbParameter("@driverrate", rec.DriverRate);
                        p[7] = new OleDbParameter("@helper", rec.Helper);
                        p[8] = new OleDbParameter("@helperrate", rec.HelperRate);
                        p[9] = new OleDbParameter("@isfinal", finalize);
                        p[9].DbType = DbType.Boolean;
                        p[10] = new OleDbParameter("@backload", rec.IsBackload);

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_TransportLog", true, p);
                        success &= ok;
                    } 
                    
                    if (success) {
                        Notifier.ShowInformation("Trucking logs successfully imported.", "Import Trucking Log");
                        RefreshRecords();
                    } else {
                        Notifier.ShowError("Failed to add new trucking log.");
                    }
                }
            }
        }

        //Exports the active trucking log as excel workbook.
        private void TruckingLogPage_OnExport(object sender, EventArgs e) {
            bool includeUnfinalized = false;    //Flag if IsFinal = false records will be included.

            //Create save file dialog.
            using (SaveFileDialog dlg = new SaveFileDialog()) {
                dlg.Filter = "Excel Spreadsheet (*.xlsx)|*.xlsx";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    //Filename is specified, continue saving

                    //Ask if user wants to include Unfinalized records.
                    DialogResult confirm = Notifier.ShowConfirm("Do you like to export unfinished records as well?", "Export Trucking Log", true);
                    if (confirm == System.Windows.Forms.DialogResult.Yes)
                        includeUnfinalized = true;

                    if (confirm != System.Windows.Forms.DialogResult.Cancel) {
                        //Show waiting dialog.
                        PleaseWaitDialog dlgwait = new PleaseWaitDialog();
                        dlgwait.Show(this);
                        dlgwait.Refresh();

                        //Query string.
                        String query = "SELECT TransportDate AS [Date], PlateNo AS [Plate No], (A.Destination + IIF(A.IsBackLoad = true, ' (Backload)', '')) AS [Destination], DRNo AS [DR No], Rate AS [Trucking Rate], DriverRate AS [Driver Rate], HelperRate AS [Helper Rate], (B.FirstName + ' ' + B.LastName) AS [Driver], (C.FirstName + ' ' + C.LastName) AS [Helper] FROM (TransportLog AS A LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID ";
                        query += "WHERE (A.IsArchived = false) AND (TransportDate >= @startdate AND TransportDate <= @enddate) AND (IsFinal = true ";
                        if (includeUnfinalized) query += "OR IsFinal = false";
                        query += ") ";
                        query += "ORDER BY PlateNo ASC, TransportDate ASC, Destination ASC";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[2];
                        p[0] = new OleDbParameter("@startdate", GetStartDateString());
                        p[0].DbType = DbType.Date;
                        p[1] = new OleDbParameter("@enddate", GetEndDateString());
                        p[1].DbType = DbType.Date;

                        //Datarow IEnumerable.
                        //Group entries by plate number..
                        DataTable table = DatabaseManager.GetMainDatabase().ExecuteQuery(query, false, p);
                        table.Columns["DR No"].DataType = typeof(string);

                        IEnumerable<IGrouping<String, DataRow>> col = table.Rows.Cast<DataRow>().GroupBy(x => x["Plate No"].ToString());

                        //Create new ClosedXML writer.
                        using (ClosedXMLEditor xml = new ClosedXMLEditor()) {

                            //Create new worksheet.
                            xml.SetWorksheet("Trucking Log").ShowGridLines = false;
                            xml.ActiveWorksheet.PageSetup.FitToPages(1, 0);

                            //Set column size.
                            xml.SetColsWidth("A,B", 11);
                            xml.SetColsWidth("D,E,F,G", 16);
                            xml.SetColsWidth("C,H,I", 25);

                            //Define default border style.
                            xml.SetBorderStyle(XLBorderStyleValues.Thin);

                            //Worksheet Header
                            xml.SetBorders(ClosedXMLEditor.CellBorders.None);
                            xml.SetFontSize(16).SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetAlignment(ClosedXMLEditor.TextAlignments.BottomCenter);
                            xml.Format(xml.SetMerge("A1:I1", new XLCellData("5SZIAN TRUCKING SERVICES")));
                            xml.SetRowsHeight("1", 30);

                            //Worksheet subheader
                            xml.SetFontSize(11).SetFormatting(ClosedXMLEditor.CellFormats.None).SetAlignment(ClosedXMLEditor.TextAlignments.TopCenter);
                            xml.Format(xml.SetMerge("A2:I2", new XLCellData("TRUCKING LOG")));

                            //Date info
                            xml.SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft);
                            xml.Format(xml.SetMerge("A3:I3", new XLCellData("DATE COVERED: " + dpStart.Value.ToShortDateString() + " - " + dpEnd.Value.ToShortDateString())));
                            xml.SetRowsHeight("3", 25);

                            //Insert every record.
                            xml.SetDateFormat("dd-mmm");
                            xml.SetNumberFormat("#,##0.00");
                            xml.SetBG(XLColor.White).SetBorders(ClosedXMLEditor.CellBorders.All).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft).SetFormatting(ClosedXMLEditor.CellFormats.None);
                            int currRow = 4;
                            //Grand total variables.
                            double truckingRateGTotal = 0.0, driverRateGTotal = 0.0, helperRateGTotal = 0.0;
                            foreach (String key in col.Select(x => x.Key)) {

                                //Write column header.
                                xml.SetBorders(ClosedXMLEditor.CellBorders.All);
                                xml.SetFormatting(ClosedXMLEditor.CellFormats.Bold).SetBG(XLColor.FromArgb(245, 245, 245));
                                XLCellDataCollection cellCols = new XLCellDataCollection();
                                foreach (DataColumn co in table.Columns)
                                    cellCols.Add(new XLCellData(co.ColumnName));
                                xml.SetCols(1, currRow, cellCols);
                                currRow++;

                                //Enumerate items by grouped plate number.      
                                xml.SetDateFormat("dd-mmm");
                                xml.SetNumberFormat("#,##0.00");
                                xml.SetBG(XLColor.White).SetBorders(ClosedXMLEditor.CellBorders.All).SetAlignment(ClosedXMLEditor.TextAlignments.MiddleLeft).SetFormatting(ClosedXMLEditor.CellFormats.None);
                                foreach (DataRow r in col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y))) {
                                    XLCellDataCollection dataCol = new XLCellDataCollection();

                                    //insert data per column.
                                    dataCol.Add(new XLCellData(DateTime.Parse(r["Date"].ToString()).ToShortDateString(), XLCellValues.DateTime));
                                    dataCol.Add(new XLCellData(r["Plate No"], XLCellValues.Text));
                                    dataCol.Add(new XLCellData(r["Destination"], XLCellValues.Text));
                                    dataCol.Add(new XLCellData(r["DR No"].ToString(), XLCellValues.Text));
                                    dataCol.Add(new XLCellData(r["Trucking Rate"], XLCellValues.Number));
                                    dataCol.Add(new XLCellData(r["Driver Rate"], XLCellValues.Number));
                                    dataCol.Add(new XLCellData(r["Helper Rate"], XLCellValues.Number));
                                    dataCol.Add(new XLCellData(InputValidators.DBNullToStr(r["Driver"]), XLCellValues.Text));
                                    dataCol.Add(new XLCellData(InputValidators.DBNullToStr(r["Helper"]), XLCellValues.Text));
                                    xml.SetCols(1, currRow, dataCol);

                                    currRow++;
                                }

                                //Get sum of each group
                                XLCellDataCollection dataCol2 = new XLCellDataCollection();
                                double truckingRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Trucking Rate"]));
                                double driverRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Driver Rate"]));
                                double helperRateSum = col.Where(x => x.Key == key).SelectMany(x => x.Select(y => y)).Sum(x => Convert.ToDouble(x["Helper Rate"]));
                                dataCol2.Add(new XLCellData(truckingRateSum, XLCellValues.Number));
                                dataCol2.Add(new XLCellData(driverRateSum, XLCellValues.Number));
                                dataCol2.Add(new XLCellData(helperRateSum, XLCellValues.Number));
                                xml.SetCols(5, currRow, dataCol2);   //Insert totals.

                                //Add to grand totals.
                                truckingRateGTotal += truckingRateSum;
                                driverRateGTotal += driverRateSum;
                                helperRateGTotal += helperRateSum;

                                currRow += 2;  //Add space to next set of records.
                            }

                            //Write the grand totals.
                            XLCellDataCollection dataCol3 = new XLCellDataCollection();
                            dataCol3.Add(new XLCellData(truckingRateGTotal, XLCellValues.Number));
                            dataCol3.Add(new XLCellData(driverRateGTotal, XLCellValues.Number));
                            dataCol3.Add(new XLCellData(helperRateGTotal, XLCellValues.Number));
                            xml.SetCols(5, currRow, dataCol3);

                            //Save file.
                            if (xml.Save(dlg.FileName)) {
                                Notifier.ShowInformation("Successfully exported trucking logs as Excel spreadsheet.", "Export Trucking Log");
                            } else {
                                Notifier.ShowError("Failed to export transport logs.\n\nIt may be that file is inaccessible, drive is unavailable, or file is opened in another application.");
                            }
                        }
                        table.Dispose();
                        dlgwait.Close();
                    }
                }
            }
        }

        bool multiple_selected = false; //Do not recalculate when there is no multiple selection.
        private void TruckingLogPage_OnRecordsSelectionChanged(object sender, EventArgs e) {
            if (Cell("IsFinal").Equals("true", StringComparison.CurrentCultureIgnoreCase)) {
                DisableEditingControls();
            }
            if (IsTotalsVisible()) {
                GetTotalsDataGridView().SuspendLayout();
                if (GetRecordsSelectedRows().Count <= 1 && multiple_selected) {
                    multiple_selected = false;
                    GetRecordsTotals();
                } else if (GetRecordsSelectedRows().Count > 1) {
                    multiple_selected = true;
                    GetSelectedRecordsTotals();
                }
                GetTotalsDataGridView().ResumeLayout();
            }
        }

        private void TruckingLogPage_OnRefresh(object sender, EventArgs e) {
            //WHERE (TransportDate>=[@datestart] And TransportDate<=[@dateend]) And
            //((PlateNo Like '%' & [@search] & '%') Or (Destination Like '%' & [@search] & '%') Or 
            //(Helper Like '%' & [@search] & '%') Or (Driver Like '%' & [@search] & '%')) And 
            //(IsArchived=IIf([@isarchived]="y",True,IIf([@isarchived]="n",False,False)));
            //@datestart
            //@dateend
            //@search
            //@isarchived
            //@isfinal
            //Query.
            OleDbParameter[] p = new OleDbParameter[4];
            p[0] = new OleDbParameter("@datestart", GetStartDateString());
            p[1] = new OleDbParameter("@dateend", GetEndDateString());
            p[2] = new OleDbParameter("@search", InputValidators.EscapeSquareBrackets(GetSearchText()));
            p[3] = new OleDbParameter("@isarchived", "n");

            String qry= "SELECT A.ID, TransportDate AS [Date], PlateNo AS [Plate Number], (A.Destination + IIF(A.IsBackLoad = true, ' (Backload)', '')) AS Destination, Rate, DRNo AS [DR No], (B.[FirstName]+IIf(IsNull(B.[MiddleName]),'',' '+B.[MiddleName])+' '+B.[LastName]) AS Driver, DriverRate AS [Driver Rate], (C.[FirstName]+IIf(IsNull(C.[MiddleName]),'',' '+C.[MiddleName])+' '+C.[LastName]) AS Helper, HelperRate AS [Helper Rate], A.IsArchived, A.IsFinal, A.IsBackLoad, A.Destination AS Store ";
            qry += "FROM (TransportLog AS A LEFT JOIN Employee AS B ON A.Driver = B.EmployeeID) LEFT JOIN Employee AS C ON A.Helper = C.EmployeeID ";
            qry += "WHERE (TransportDate>=[@datestart] And TransportDate<=[@dateend]) And ((PlateNo Like '%' & [@search] & '%') Or (Destination Like '%' & [@search] & '%') Or ( (C.[FirstName]+IIf(IsNull(C.[MiddleName]),'',' '+C.[MiddleName])+' '+C.[LastName]) Like '%' & [@search] & '%') Or ( (B.[FirstName]+IIf(IsNull(B.[MiddleName]),'',' '+B.[MiddleName])+' '+B.[LastName]) Like '%' & [@search] & '%')) And (A.IsArchived=IIf([@isarchived]='y',True,IIf([@isarchived]='n',False,False)));";

            if (dt != null) dt.Dispose();
            //dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_TransportLog", true, p);
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery(qry, false, p);

            //Move IsFinal at the beginning of the table.
            dt.Columns["IsFinal"].SetOrdinal(0);

            //insert an image column to the datatable.
            DataColumn icol = dt.Columns.Add("IMG", typeof(Image));
            icol.SetOrdinal(0);
            foreach (DataRow r in dt.Rows) {
                if ((bool)r["IsFinal"]) {
                    r[0] = lockicon;
                    if (!GetShowFinal()) {
                        r.Delete();
                    }
                }
            }

            SetRecordsDataSource(dt);

            HideColumn("IsArchived");
            HideColumn("IsFinal");
            HideColumn("ID");
            HideColumn("IsBackLoad");
            HideColumn("Store");

            DataGridViewColumn finalcol = GetColumn("IMG");
            finalcol.Width = 30;
            finalcol.Resizable = DataGridViewTriState.False;
            finalcol.DefaultCellStyle.NullValue = null;
            finalcol.ToolTipText = "Finalized";
            SetColumnHeader("IMG", "");

            //Set column formatting.
            SetColumnFormat("Rate", "#,##0.00");
            SetColumnFormat("Driver Rate", "#,##0.00");
            SetColumnFormat("Helper Rate", "#,##0.00");

            //Order fields.
            GetRecordsDataGridView().Sort(GetRecordsDataGridView().Columns["Date"], System.ComponentModel.ListSortDirection.Descending);

            if(IsTotalsVisible())
                GetRecordsTotals(); //Compute sums of all records.

            //Autofit some columns.
            RecordsDGVFitColumnToContent("Destination");
            RecordsDGVFitColumnToContent("Driver");
            RecordsDGVFitColumnToContent("Helper");
        }

        //Just refresh when the show finalize checkbox is ticked.
        private void TruckingLogPage_OnShowFinalizeChecked(object sender, EventArgs e) {
            //RefreshRecords();
        }

        private void TruckingLogPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void TruckingLogPage_OnAdd(object sender, EventArgs e) {
            TruckingLogDialog dlg = new TruckingLogDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            
            //Setup date limits
            dlg.SetDatePickerDate(GetStartDateString());
            dlg.SetDatePickerMin(GetStartDateString());
            dlg.SetDatePickerMax(GetEndDateString());

            //Setup vehicle plates.
            ComboboxDataItem[] dd = ManageVehiclesPage.QueryVehiclesPlateNoAndType();
            if (dd.Length <= 0) {
                Notifier.ShowError("You must register vehicles in the database first before adding trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupPlateNo(dd);

            //Setup vehicle types
            ComboboxDataItem[] vt = VehicleTypesPage.QueryVehicleTypes();
            if (vt.Length <= 0) {
                Notifier.ShowError("You must add vehicle types in the database first before you can add trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupVehicleTypes(vt);

            //Load all employees
            List<ComboboxDataItem> employees = EmployeesPage.QueryEmployeeForComboBox();
            if (employees.Count <= 0) {
                Notifier.ShowError("You must register employees first in the database before you can add trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupEmployeesSelection(employees);

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string date = "", plateno = "", drno = "", destination = "", rate = "", driver = "", driverRate = "",
                       helper = "", helperRate = "";
                bool backload = false;
                dlg.GetFieldValues(out date, out plateno, out drno, out destination, out rate, out driver, out driverRate, out helper, out helperRate, out backload);
                //VALUES ([@tdate], [@pn], [@dest], [@rate], [@dr], [@driver], [@driverrate], [@helper], [@helperrate]);
                OleDbParameter[] p = new OleDbParameter[11];
                p[0] = new OleDbParameter("@tdate", date);
                p[0].DbType = DbType.DateTime;
                p[1] = new OleDbParameter("@pn", plateno);
                p[2] = new OleDbParameter("@dest", destination);
                p[3] = new OleDbParameter("@rate", rate);
                p[4] = new OleDbParameter("@dr", drno);
                p[5] = new OleDbParameter("@driver", driver);
                p[6] = new OleDbParameter("@driverrate", driverRate);
                p[7] = new OleDbParameter("@helper", helper);
                p[8] = new OleDbParameter("@helperrate", helperRate);
                p[9] = new OleDbParameter("@isfinal", dlg.IsFinalizing());
                p[10] = new OleDbParameter("@backload", backload);

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_TransportLog", true, p);
                if (ok) {
                    Notifier.ShowInformation("New trucking log successfully created.", "New Trucking Log");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to add new trucking log.");
                }

            }
        }

        private void TruckingLogPage_OnEdit(object sender, EventArgs e) {
            TruckingLogDialog dlg = new TruckingLogDialog();

            //Setup date limits
            dlg.SetDatePickerDate(GetStartDateString());
            dlg.SetDatePickerMin(GetStartDateString());
            dlg.SetDatePickerMax(GetEndDateString());

            //Setup vehicle plates.
            ComboboxDataItem[] dd = ManageVehiclesPage.QueryVehiclesPlateNoAndType();
            if (dd.Length <= 0) {
                Notifier.ShowError("You must register vehicles in the database first before adding trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupPlateNo(dd);

            //Setup vehicle types
            ComboboxDataItem[] vt = VehicleTypesPage.QueryVehicleTypes();
            if (vt.Length <= 0) {
                Notifier.ShowError("You must add vehicle types in the database first before you can add trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupVehicleTypes(vt);

            //Load all employees
            List<ComboboxDataItem> employees = EmployeesPage.QueryEmployeeForComboBox();
            if (employees.Count <= 0) {
                Notifier.ShowError("You must register employees first in the database before you can add trucking logs.");
                dlg.Dispose();
                return;
            }
            dlg.SetupEmployeesSelection(employees);

            //Set current values.
            dlg.SetFieldValues(Cell("Date"), Cell("Plate Number"), Cell("DR No"), Cell("Store"), Cell("Rate"), Cell("Driver"), Cell("Driver Rate"), Cell("Helper"), Cell("Helper Rate"), Cell("IsBackLoad"));

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string date = "", plateno = "", drno = "", destination = "", rate = "", driver = "", driverRate = "",
                       helper = "", helperRate = "";
                bool backload = false;
                dlg.GetFieldValues(out date, out plateno, out drno, out destination, out rate, out driver, out driverRate, out helper, out helperRate, out backload);
                //SET TransportDate = [@date], PlateNo = [@pn], Destination = [@dest], Rate = [@rate], 
                //DRNo = [@dr], Driver = [@driver], DriverRate = [@driverrate], Helper = [@helper], HelperRate = [@helperrate], IsFinal = @final
                //WHERE ID=[@id];
                OleDbParameter[] p = new OleDbParameter[12];
                p[0] = new OleDbParameter("@date", date);
                p[0].DbType = DbType.DateTime;
                p[1] = new OleDbParameter("@pn", plateno);
                p[2] = new OleDbParameter("@dest", destination);
                p[3] = new OleDbParameter("@rate", rate);
                p[4] = new OleDbParameter("@dr", drno);
                p[5] = new OleDbParameter("@driver", driver);
                p[6] = new OleDbParameter("@driverrate", driverRate);
                p[7] = new OleDbParameter("@helper", helper);
                p[8] = new OleDbParameter("@helperrate", helperRate);
                p[9] = new OleDbParameter("@final", dlg.IsFinalizing());
                p[10] = new OleDbParameter("@final", backload);
                p[11] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_TransportLog", true, p);
                if (ok) {
                    Notifier.ShowInformation("Trucking log successfully updated.", "Updated Trucking Log");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to update trucking log.");
                }

            }
        }

        private void TruckingLogPage_OnFinalize(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count == 1) {
                if (Cell("DR No").Trim() != "" && Cell("Destination").Trim() != "" &&
                    Cell("Rate").Trim() != "" && Cell("Plate Number").Trim() != "") {
                    if (Notifier.ShowConfirm("Finalizing a trucking log will prevent further modifications and makes it available for payroll computation. Proceed only if all details in this log is final.\n\nMark this record as final?", "Finalize Record") == System.Windows.Forms.DialogResult.Yes) {
                        String query = "UPDATE TransportLog SET IsFinal = true ";
                        query += "WHERE ID = @id";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", "{" + Cell("ID") + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        if (ok) {
                            Notifier.ShowInformation("Trucking log successfully locked from further modifications.", "Finalize Record");
                            RefreshRecords();
                        } else {
                            Notifier.ShowError("Failed to finalize trucking log.");
                        }
                    }
                } else {
                    string missing = "";
                    if (Cell("Plate Number").Trim().Equals("")) missing += "\u2022 Plate Number\n";
                    if (Cell("Destination").Trim().Equals("")) missing += "\u2022 Destination\n";
                    if (Cell("DR No").Trim().Equals("")) missing += "\u2022 DR No\n";
                    if (Cell("Rate").Trim().Equals("")) missing += "\u2022 Rate\n";
                    Notifier.ShowWarning("You can only finalize a trucking log if all fields are complete.\n\nComplete the following fields first:\n\n" + missing, "Finalize Trucking Log");
                    return;
                }
            } else {
                //This portion will batch finalize records.
                DialogResult conf = Notifier.ShowConfirm("This action will finalize all valid selected records. Do you want to proceed?", "Finalize Records");
                if (conf == System.Windows.Forms.DialogResult.No) return;
                int locked = 0, succ = 0, failed = 0;
                for (int i = 0; i < GetRecordsSelectedRows().Count; i++) {
                    //Skip final.
                    if (Convert.ToBoolean(Cell("IsFinal", i))) {
                        locked++;
                        continue;
                    }
                    if (Cell("DR No", i).Trim() != "" && Cell("Destination", i).Trim() != "" &&
                    Cell("Rate", i).Trim() != "" && Cell("Plate Number", i).Trim() != "") {
                        String query = "UPDATE TransportLog SET IsFinal = true ";
                        query += "WHERE ID = @id";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", "{" + Cell("ID", i) + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        if (ok) succ++;
                        else failed++;
                    } else {
                        failed++;
                    }
                }
                if (failed == 0) Notifier.ShowInformation("All records have been successfully locked.", "Lock records");
                else if (failed > 0 && succ <= 0) Notifier.ShowError("All selected records have insufficient data. Please complete them first.");
                else if(failed > 0 && succ > 0) Notifier.ShowWarning(succ + " records has been successfully locked but there are " + failed + " record/s that must be completed first.", "Lock records");
                RefreshRecords();
            }
        }

        private void TruckingLogPage_OnArchive(object sender, EventArgs e) {
            if (Notifier.ShowConfirm("Archived records will not be included in payroll computations.\nAre you sure you want to archive the selected trucking log?", "Confirm Archive") == System.Windows.Forms.DialogResult.Yes) {
                //SET IsArchived = IIf([@archived]='y',True,IIf([@archived]='n',False,False)) WHERE ID=[@id];
                bool ok = true;
                int failctr = 0, okctr = 0;
                for(int i = 0; i < GetRecordsSelectedRows().Count; i++) {

                    OleDbParameter[] p = new OleDbParameter[2];
                    p[0] = new OleDbParameter("@archived", "y");
                    p[1] = new OleDbParameter("@id", "{" + Cell("ID", i) + "}");

                    bool exec = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Archive_TransportLog", true, p);
                    if (!exec) {
                        ok = exec;   //Overwrite if execution failed.
                        failctr++;   //Count failed saves.
                    } else okctr++;  //Count success saves.

                }
                if (ok) {
                    Notifier.ShowInformation("You have successfully archived " + okctr + " record/s.", "Archive Successful");
                    RefreshRecords();
                } else {
                    if (okctr > 0) {
                        Notifier.ShowWarning("You have successfully archived " + okctr + " record/s. However, " + failctr + " failed due to some issues. Please try again.", "Archive Records");
                    } else {
                        Notifier.ShowError("You have failed to archive the selected records. Please try again.");
                    }
                }
            }
        }

        #endregion

        #region Window Eventhandler

        private void TruckingLogPage_Load(object sender, EventArgs e) {

        }

        #endregion

        #region Local Functions

        void GetRecordsTotals() {
            //Suspend layout of the totals datagrid view while computing.
            GetTotalsDataGridView().SuspendLayout();

            //Compute totals
            ClearAllTotal();

            //Totals of all
            AddTotalInfoHeader("Totals of all records");
            AddTotalInfo("Rate", GetColumnTotal("Rate"));
            AddTotalInfo("Driver Rate", GetColumnTotal("Driver Rate"));
            AddTotalInfo("Helper Rate", GetColumnTotal("Helper Rate"));

            //Totals of locked/final records
            DataRow subheader = AddTotalInfoHeader("Totals of all locked records");
            AddTotalInfo("Rate", GetColumnTotal("Rate", "IsFinal", true));
            AddTotalInfo("Driver Rate", GetColumnTotal("Driver Rate", "IsFinal", true));
            AddTotalInfo("Helper Rate", GetColumnTotal("Helper Rate", "IsFinal", true));
            EditTotalInfoHeader(subheader, "Totals of all locked records (" + GetRowCount("IsFinal", true) + ")");

            //Totals of unlocked records.
            subheader = AddTotalInfoHeader("Totals of all unlocked records");
            AddTotalInfo("Rate", GetColumnTotal("Rate", "IsFinal", false));
            AddTotalInfo("Driver Rate", GetColumnTotal("Driver Rate", "IsFinal", false));
            AddTotalInfo("Helper Rate", GetColumnTotal("Helper Rate", "IsFinal", false));
            EditTotalInfoHeader(subheader, "Totals of all unlocked records (" + GetRowCount("IsFinal", false) + ")");

            //AddTotalInfo("Final/Locked Records", GetRowCount("IsFinal", true).ToString());
            //AddTotalInfo("Unlocked Records", GetRowCount("IsFinal", false).ToString() + " (Will not be included in the payroll computation)");


            //AddTotalInfo("Driver Rate (Locked Records)", GetColumnTotal("Driver Rate", "IsFinal", true));
            //AddTotalInfo("Driver Rate (Unlocked Records)", GetColumnTotal("Driver Rate", "IsFinal", false));

            //Get driver rate totals
            subheader = AddTotalInfoHeader("Totals By Driver");
            IEnumerable<TotalsItem> q = GetColumnTotalGroupedBy("Driver Rate", "Driver");
            foreach (TotalsItem o in q)
                AddTotalInfo(o.Item, o.Value);
            EditTotalInfoHeader(subheader, "Totals By Driver (" + q.Where(x => x.Item.ToString().Trim() != "").Count() + ")");

            //Get helper rate totals.
            subheader = AddTotalInfoHeader("Totals By Helper");
            q = GetColumnTotalGroupedBy("Helper Rate", "Helper");
            foreach (TotalsItem o in q)
                AddTotalInfo(o.Item, o.Value);
            EditTotalInfoHeader(subheader, "Totals By Helper (" + q.Where(x => x.Item.ToString().Trim() != "").Count() + ")");

            //Make each columns in the totals datagrid fit contents.
            AutoFitTotalsDataGridItems();

            //Resume layout.
            GetTotalsDataGridView().ResumeLayout();
        }

        void GetSelectedRecordsTotals() {
            //Compute totals
            ClearAllTotal();

            //Totals of all
            AddTotalInfoHeader("Totals of selected records");
            AddTotalInfo("Rate", GetColumnTotalSelectedRows("Rate"));
            AddTotalInfo("Driver Rate", GetColumnTotalSelectedRows("Driver Rate"));
            AddTotalInfo("Helper Rate", GetColumnTotalSelectedRows("Helper Rate"));

            //Totals of locked/final records
            DataRow subheader = AddTotalInfoHeader("Totals of all locked records");
            AddTotalInfo("Rate", GetColumnTotalSelectedRows("Rate", "IsFinal", true));
            AddTotalInfo("Driver Rate", GetColumnTotalSelectedRows("Driver Rate", "IsFinal", true));
            AddTotalInfo("Helper Rate", GetColumnTotalSelectedRows("Helper Rate", "IsFinal", true));
            EditTotalInfoHeader(subheader, "Totals of all locked records (" + GetSelectedRowCount("IsFinal", true) + ")");

            //Totals of unlocked records.
            subheader = AddTotalInfoHeader("Totals of all unlocked records");
            AddTotalInfo("Rate", GetColumnTotalSelectedRows("Rate", "IsFinal", false));
            AddTotalInfo("Driver Rate", GetColumnTotalSelectedRows("Driver Rate", "IsFinal", false));
            AddTotalInfo("Helper Rate", GetColumnTotalSelectedRows("Helper Rate", "IsFinal", false));
            EditTotalInfoHeader(subheader, "Totals of all unlocked records (" + GetSelectedRowCount("IsFinal", false) + ")");

            //Get driver rate totals
            subheader = AddTotalInfoHeader("Totals By Driver");
            IEnumerable<TotalsItem> q = GetColumnTotalGroupedBySelectedRows("Driver Rate", "Driver");
            foreach (TotalsItem o in q)
                AddTotalInfo(o.Item, o.Value);
            EditTotalInfoHeader(subheader, "Totals By Driver (" + q.Where(x => x.Item.ToString().Trim() != "").Count() + ")");

            //Get helper rate totals.
            subheader = AddTotalInfoHeader("Totals By Helper");
            q = GetColumnTotalGroupedBySelectedRows("Helper Rate", "Helper");
            foreach (TotalsItem o in q)
                AddTotalInfo(o.Item, o.Value);
            EditTotalInfoHeader(subheader, "Totals By Helper (" + q.Where(x => x.Item.ToString().Trim() != "").Count() + ")");

            //Make each columns in the totals datagrid fit contents.
            AutoFitTotalsDataGridItems();
        }

        #endregion


    }

}
