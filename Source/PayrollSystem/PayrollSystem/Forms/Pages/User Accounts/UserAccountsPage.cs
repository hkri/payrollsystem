﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Collections.Generic;

namespace PayrollSystem {

    class UserAccountsPage : SystemPage {

        #region Local Variables

        DataTable src = new DataTable();

        #endregion

        #region Constructor

        public UserAccountsPage() {
            Text = "User Accounts";
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // UserAccountsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "UserAccountsPage";
            this.OnAdd += new System.EventHandler(this.UserAccountsPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.UserAccountsPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.UserAccountsPage_OnDelete);
            this.OnRefresh += new System.EventHandler(this.UserAccountsPage_OnRefresh);
            this.Load += new System.EventHandler(this.UserAccountsPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Data CRUD

        private void UserAccountsPage_OnAdd(object sender, EventArgs e) {
            UserAccountDialog dlg = new UserAccountDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            string[] a_un = GetRecordsValueArray("Username");
            dlg.SetExistingUN(a_un);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //@un, @pw, @an, @typ)
                string un = "", pw = "", an = "", typ = "";
                dlg.GetFieldValues(out un, out an, out typ, out pw);
                SqlCeParameter[] p = new SqlCeParameter[4];
                p[0] = new SqlCeParameter("@un", un);
                p[1] = new SqlCeParameter("@pw", pw);
                p[2] = new SqlCeParameter("@an", an);
                p[3] = new SqlCeParameter("@typ", typ);
                bool success = DatabaseManager.GetLocalDatabase().ExecuteNonQuery(QueryStrings.LocalDatabaseTables.Users.Add_Users, false, p);
                if (success) {
                    Notifier.ShowInformation("New account successfully added.", "New Account");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("There was a problem creating a new user account.");
                }
            }
        }

        private void UserAccountsPage_OnRefresh(object sender, EventArgs e) {
            if (src != null) src.Dispose();
            src = DatabaseManager.GetLocalDatabase().ExecuteQuery(QueryStrings.LocalDatabaseTables.Users.Query_Users, false);
            SetRecordsDataSource(src);
            HideColumn("ID");
            HideColumn("Password");
            RecordsDGVFitColumnToContent("Account Name");
        }

        private void UserAccountsPage_OnDelete(object sender, EventArgs e) {
            if (GetRecordsSelectedRowValues("Username")[0] == UserManager.USERNAME) {
                Notifier.ShowWarning("You cannot delete your own account.", "Delete Account");
                return;
            }
            if (GetRecordsCount() == 1) {
                Notifier.ShowWarning("There must be at least one (1) user account in the system.", "Delete Account");
                return;
            }
            int deleting_admins = GetRecordsSelectedRowValuesCount("ID", "Type", "Administrator");
            int all_admins = GetRecordsCount("Type", "Administrator");
            if ((all_admins - deleting_admins) == 0) {
                Notifier.ShowWarning("There must be at least one (1) administrator account in the system.", "Delete Account");
                return;
            }
            if (GetRecordsSelectedRows().Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to delete the selected user account?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    string[] targets = GetRecordsSelectedRowValues("ID");
                    bool success = false;
                    foreach (string s in targets) {
                        SqlCeParameter[] p = new SqlCeParameter[1];
                        p[0] = new SqlCeParameter("@id", s);
                        success = DatabaseManager.GetLocalDatabase().ExecuteNonQuery(QueryStrings.LocalDatabaseTables.Users.Delete_Users, false, p);
                    }
                    if (success) {
                        Notifier.ShowInformation("Successfully deleted user account.", "Account Deleted");
                        RefreshRecords();
                    } else
                        Notifier.ShowError("There was a problem deleting the specified user account.");
                }
            }
        }

        private void UserAccountsPage_OnEdit(object sender, EventArgs e) {
            UserAccountDialog dlg = new UserAccountDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Editing;
            string[] a_un = GetRecordsValueArray("Username");
            dlg.SetExistingUN(a_un);
            dlg.SetAdminUserCount(GetRecordsCount("Type", "Administrator"));
       
            DataGridViewRow r = GetRecordsSelectedRow();

            string cid = Cell("ID");
            dlg.SetFieldValues(Cell("Username"), Cell("Account Name"), Cell("Type"), Cell("Password"));
            //string cid = cols["ID"];
            //dlg.SetFieldValues(cols["Username"], cols["Account Name"], cols["Type"], cols["Password"]);
            //string cid = GetColumn(r, "ID");
            //dlg.SetFieldValues(GetColumn(r, "Username"), GetColumn(r, "Account Name"), GetColumn(r, "Type"), GetColumn(r, "Password"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //SET Username = @un, Password = @pw, AccountName = @an, Type = @typ WHERE ID = @id"
                string un = "", pw = "", an = "", typ = "";
                dlg.GetFieldValues(out un, out an, out typ, out pw);
                SqlCeParameter[] p = new SqlCeParameter[5];
                p[0] = new SqlCeParameter("@un", un);
                p[1] = new SqlCeParameter("@pw", pw);
                p[2] = new SqlCeParameter("@an", an);
                p[3] = new SqlCeParameter("@typ", typ);
                p[4] = new SqlCeParameter("@id", cid);
                bool success = DatabaseManager.GetLocalDatabase().ExecuteNonQuery(QueryStrings.LocalDatabaseTables.Users.Edit_Users, false, p);
                if (success) {
                    Notifier.ShowInformation("Account successfully updated.", "Edit Account");
                    RefreshRecords();
                    if (UserManager.USERID == cid) {
                        UserManager.USERID = cid;
                        UserManager.USERNAME = un;
                        UserManager.ACCOUNT_TYPE = Convert.ToInt32(typ);
                        UserManager.ACCOUNT_NAME = an;
                    }
                } else {
                    Notifier.ShowError("There was a problem updating the selected user account.");
                }
            }
        }

        #endregion

        #region Window event handlers

        private void UserAccountsPage_Load(object sender, EventArgs e) {
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Refresh);
            SetRecordsDataGridMultiselect(false);

        }

        #endregion

    }

}
