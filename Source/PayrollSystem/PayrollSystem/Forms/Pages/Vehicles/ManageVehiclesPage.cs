﻿using System;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PayrollSystem {

    class ManageVehiclesPage : SystemPage {

        DataTable dt = null;

        #region Constructor

        public ManageVehiclesPage() {
            Text = "Manage Vehicle Records";
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // ManageVehiclesPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "ManageVehiclesPage";
            this.OnAdd += new System.EventHandler(this.ManageVehiclesPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.ManageVehiclesPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.ManageVehiclesPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.ManageVehiclesPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.ManageVehiclesPage_OnRefresh);
            this.Load += new System.EventHandler(this.ManageVehiclesPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Window eventhandlers

        private void ManageVehiclesPage_Load(object sender, EventArgs e) {
            RefreshRecords();
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
        }

        private void ManageVehiclesPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        #endregion

        #region CRUD

        private void ManageVehiclesPage_OnRefresh(object sender, EventArgs e) {
            if (dt != null) dt.Dispose();
            //WHERE (PlateNo LIKE '%' & [@search] & '%') AND (VehicleType LIKE '%' & [@vtype] & '%');
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@vtype", "");
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Vehicles", true, p);
            SetRecordsDataGridMultiselect(false);
            SetRecordsDataSource(dt);
            HideColumn("ID");
            RecordsDGVFitColumnToContent("Type");
        }

        private void ManageVehiclesPage_OnAdd(object sender, EventArgs e) {
            VehicleDialog dlg = new VehicleDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetExistingPlateNumbers(GetRecordsValueArray("Plate Number"));
            dlg.SetVehicleTypes(VehicleTypesPage.QueryVehicleTypes());
            if(dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK){
                /*INSERT INTO Vehicles ( PlateNo, VehicleType ) VALUES ([@pn], [@vtype]);*/
                string pn = "", vtype = "";
                dlg.GetFieldValues(out pn, out vtype);
                OleDbParameter[] p = new OleDbParameter[2];
                p[0] = new OleDbParameter("@pn", pn);
                p[1] = new OleDbParameter("@vtype", "{" + vtype + "}");
                bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_Vehicles", true, p);
                if(succ){
                    Notifier.ShowInformation("New vehicle record successfully added.", "Add Vehicle");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to add new vehicles.");
                }
            }
        }

        private void ManageVehiclesPage_OnEdit(object sender, EventArgs e) {
            VehicleDialog dlg = new VehicleDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Editing;
            dlg.SetExistingPlateNumbers(GetRecordsValueArray("Plate Number"));
            dlg.SetVehicleTypes(VehicleTypesPage.QueryVehicleTypes());
            dlg.SetFieldValues(Cell("Plate Number"), Cell("Type"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //T PlateNo = [@pn], VehicleType = [@vtype]WHERE ID=[@id];
                string pn = "", vtype = "";
                dlg.GetFieldValues(out pn, out vtype);
                OleDbParameter[] p = new OleDbParameter[3];
                p[0] = new OleDbParameter("@pn", pn);
                p[1] = new OleDbParameter("@vtype", "{" + vtype + "}");
                p[2] = new OleDbParameter("@id", "{" + Cell("ID") + "}");
                bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_Vehicles", true, p);
                if (succ) {
                    Notifier.ShowInformation("New vehicle record successfully updated.", "Update Vehicle");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to uupdate vehicle record.");
                }
            }
        }

        private void ManageVehiclesPage_OnDelete(object sender, EventArgs e) {
            if (GetRecordsSelectedRows().Count > 0) {
                if (Notifier.ShowConfirm("You are about to delete a vehicle record from the database, which will affect all other records that reference to this vehicle (i.e. Status Reports). Do you wish to proceed?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                    OleDbParameter[] p = new OleDbParameter[1];
                    p[0] = new OleDbParameter("@id", Cell("ID"));
                    bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_Vehicles", true, p);
                    if (succ) {
                        Notifier.ShowInformation("Vehicle record successfully deleted.", "Delete Vehicle");
                        RefreshRecords();
                    } else {
                        Notifier.ShowError("Failed to delete vehicle record.");
                    }
                }
            }
        }

        #endregion

        #region Public Functions

        internal static ComboboxDataItem[] QueryVehicles() {
            List<ComboboxDataItem> lst = new List<ComboboxDataItem>();
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@vtype", "");
            DataView dd = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_Vehicles", true, p).AsDataView();
            
            foreach (DataRow drow in dd.Table.Rows) {
                lst.Add(new ComboboxDataItem(drow["ID"].ToString(), drow["Plate Number"].ToString()));
            }
            return lst.OrderBy(x => x.Value).ToArray();
        }

        internal static ComboboxDataItem[] QueryVehiclesPlateNoAndType() {
            List<ComboboxDataItem> lst = new List<ComboboxDataItem>();
            OleDbParameter[] p = new OleDbParameter[2];
            p[0] = new OleDbParameter("@search", "");
            p[1] = new OleDbParameter("@vtype", "");
            DataView dd = DatabaseManager.GetMainDatabase().ExecuteQuery("SELECT * FROM Vehicles AS A INNER JOIN VehicleType AS B ON A.VehicleType = B.ID ORDER BY A.PlateNo", false, p).AsDataView();
            foreach (DataRow drow in dd.Table.Rows) {
                lst.Add(new ComboboxDataItem(drow["VehicleType"].ToString(), drow["PlateNo"].ToString()));
            }
            return lst.ToArray();
        }

        #endregion

    }

}
