﻿//.NET Framework imports.
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Collections.Generic;

//ClosedXML imports.
using ClosedXML;
using ClosedXML.Excel;

namespace PayrollSystem {

    class StatusReportPage : SystemPage {

        #region Local Objects

        //Datasource of the table.
        DataTable dt = null;

        #endregion

        #region Constructor

        public StatusReportPage() {
            InitializeComponent();
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Export | PageToolbarButtons.Archive | PageToolbarButtons.Refresh | PageToolbarButtons.Export | PageToolbarButtons.Search);
            SetFilterControls(FilterControls.MonthYearDatePicker);
            SetStartDate(DateTime.Now.ToShortDateString());
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // StatusReportPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "StatusReportPage";
            this.Text = "Status Report";
            this.OnAdd += new System.EventHandler(this.StatusReportPage_OnAdd);
            this.OnArchive += new System.EventHandler(this.StatusReportPage_OnArchive);
            this.OnExport += new System.EventHandler(this.StatusReportPage_OnExport);
            this.OnSearch += new System.EventHandler(this.StatusReportPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.StatusReportPage_OnRefresh);
            this.Load += new System.EventHandler(this.StatusReportPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Window eventhandlers

        private void StatusReportPage_OnDatePeriodChanged(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void StatusReportPage_Load(object sender, EventArgs e) {
            RefreshRecords();
        }

        #endregion

        #region Export Function

        //Function to create worksheet of status report from an existing workbook.
        void ExportToSpreadsheet(XLWorkbook wb) {
            //Create worksheet.
            IXLWorksheet ws = wb.Worksheets.Add("Status Report");

            //Set initial styles.
            ws.PageSetup.PagesWide = 1;
            ws.ShowGridLines = false;
            ws.Style.Font.SetFontName("Arial").Font.SetFontSize(9).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left).Alignment.SetVertical(XLAlignmentVerticalValues.Center);

            //List to hold data cache.
            List<Object[]> data = new List<Object[]>();

            //Write heading.
            data.Add(new Object[] { "Status Report" });
            data.Add(new Object[] { "Date period: " + GetStartDate().ToString("MMM yyyy") });
            data.Add(new Object[] { "Date", "Plate No.", "Status Update", "Author" });

            //Iterate through the datagridview records.
            IEnumerable<DataRow> eRows = dt.Rows.Cast<DataRow>();
            eRows = eRows.OrderBy(x => x["Plate Number"]).ThenByDescending(x => x["Date"]);
            foreach (DataRow row in eRows) {
                data.Add(new Object[] { DateTime.Parse(row["Date"].ToString()).ToString("MM/dd/yyyy"), row["Plate Number"].ToString(), row["Status"].ToString(), row["Author"].ToString() });
            }

            //Insert records.
            ws.Cell(1, 1).InsertData(data);

            //Format.
            //Set all rows to 16.
            ws.Rows(1, data.Count + 5).Height = 16.5;

            //Adjust columns width
            ws.Columns("A,B").Width = 10;
            ws.Column("C").Width = 45;
            ws.Column("D").Width = 30;

            //Main header
            ws.Range("A1:D1").Merge().Style
                .Font.SetBold()
                .Font.SetFontSize(10)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Bottom);

            //Date
            ws.Range("A2:D2").Merge().Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Top);
            ws.Row(2).Height = 30;

            //Column headers.
            ws.Range("A3:D3").Style
                .Font.SetBold()
                .Border.SetBottomBorder(XLBorderStyleValues.Thin);
        }

        #endregion

        #region CRUD

        //Saves records in a spreadsheet document.
        private void StatusReportPage_OnExport(object sender, EventArgs e) {
            //Export if records is not empty.
            if (GetRecordsDataGridView().Rows.Count > 0) {
                using (SaveFileDialog dlg = new SaveFileDialog()) {
                    dlg.Filter = "Excel spreadsheet document (*.xlsx)|*.xlsx";
                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                        using (XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled)) {
                            ExportToSpreadsheet(wb);
                            try {
                                wb.SaveAs(dlg.FileName);
                                Notifier.ShowInformation("Status reports successfully saved as spreadsheet document.", "Export Status Report");
                            } catch (Exception ex) {
                                Notifier.ShowError(ex.Message);
                            }
                        }
                    }
                }
            } else
                Notifier.ShowWarning("No records to be exported. Please check your filters or add records first.", "Export Records");
        }

        private void StatusReportPage_OnArchive(object sender, EventArgs e) {
            if (GetRecordsDataGridView().SelectedRows.Count > 0) {
                if (Notifier.ShowConfirm("Are you sure you want to move the selected status report to the archives?", "Archive Status Report") == System.Windows.Forms.DialogResult.Yes) {
                    bool success = true;
                    foreach (DataGridViewRow row in GetRecordsSelectedRows()) {
                        String query = "UPDATE StatusReport SET IsArchived = true ";
                        query += "WHERE ID = @id";

                        //SQL Parameter.
                        OleDbParameter[] p = new OleDbParameter[1];
                        p[0] = new OleDbParameter("@id", "{" + row.Cells["ID"].Value.ToString() + "}");

                        bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery(query, false, p);
                        success &= ok;
                    } 
                    if (success) {
                        Notifier.ShowInformation("Status report was moved to the archives.", "Archive Status Report");
                        RefreshRecords();
                    } else {
                        Notifier.ShowError("There was an error when trying to archive the selected status report.");
                    }
                }
            }
        }

        private void StatusReportPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void StatusReportPage_OnAdd(object sender, EventArgs e) {
            StatusReportDialog dlg = new StatusReportDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //VALUES ([@pn], [@rdate], [@status], [@author]);
                string status = "", pn = "", date;
                dlg.GetFieldValues(out pn, out status, out date);
                OleDbParameter[] p = new OleDbParameter[4];
                p[0] = new OleDbParameter("@pn", pn);
                p[1] = new OleDbParameter("@rdate", date);
                p[2] = new OleDbParameter("@status", status);
                p[3] = new OleDbParameter("@author", UserManager.ACCOUNT_NAME);
                bool ok = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_StatusReport", true, p);
                if (ok) {
                    Notifier.ShowInformation("Successfully created new status report for vehicle " + pn + ".", "Status Report");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to create new status report. Please try again, or contact a system administrator.");
                }
            }
        }

        private void StatusReportPage_OnRefresh(object sender, EventArgs e) {
            if (dt != null) dt.Dispose();
            /*
             *  WHERE ((StatusReport.PlateNo Like '%' & [@search] & '%') Or (StatusReport.Status Like '%' & [@search] & '%')
             *  Or (Author Like '%' & [@search] & '%')) And (Month(ReportDate)=IIf([@month]='' Or
             *  IsNull([@month]),Month(ReportDate),[@month])) And (Year(ReportDate)=IIf([@year]='' Or
             *  IsNull([@year]),Year(ReportDate),[@year])) And (IsArchived=IIf([@isarchived]="y",True,IIf([@isarchived]="n",False,False)))
                ORDER BY ReportDate DESC;
             */
            OleDbParameter[] p = new OleDbParameter[4];
            p[0] = new OleDbParameter("@search", GetSearchText());
            p[1] = new OleDbParameter("@month", GetPickerFromMonth());
            p[1].DbType = DbType.Int32;
            p[2] = new OleDbParameter("@year", GetPickerFromYear());
            p[2].DbType = DbType.Int32;
            p[3] = new OleDbParameter("@isarchived", "n");
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_StatusReport", true, p);
            SetRecordsDataSource(dt);
            HideColumn("ID");
            HideColumn("IsArchived");
            RecordsDGVFitColumnToContent("Status");
            RecordsDGVFitColumnToContent("Author");
        }

        #endregion

    }

}
