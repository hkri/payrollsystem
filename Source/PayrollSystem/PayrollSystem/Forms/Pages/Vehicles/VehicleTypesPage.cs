﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Data.OleDb;
using System.ComponentModel;

namespace PayrollSystem {

    class VehicleTypesPage : SystemPage {

        DataTable dt = null;

        #region constructor

        public VehicleTypesPage() {
            Text = "Vehicle Types";
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Refresh);
            InitializeComponent();
        }

        private void InitializeComponent() {
            this.SuspendLayout();
            // 
            // VehicleTypesPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(875, 448);
            this.Name = "VehicleTypesPage";
            this.OnAdd += new System.EventHandler(this.VehicleTypesPage_OnAdd);
            this.OnEdit += new System.EventHandler(this.VehicleTypesPage_OnEdit);
            this.OnDelete += new System.EventHandler(this.VehicleTypesPage_OnDelete);
            this.OnSearch += new System.EventHandler(this.VehicleTypesPage_OnSearch);
            this.OnRefresh += new System.EventHandler(this.VehicleTypesPage_OnRefresh);
            this.Load += new System.EventHandler(this.VehicleTypesPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Window eventhandlers

        private void VehicleTypesPage_Load(object sender, EventArgs e) {
            SetToolbarButtons(PageToolbarButtons.Add | PageToolbarButtons.Edit | PageToolbarButtons.Delete | PageToolbarButtons.Refresh | PageToolbarButtons.Search);
            RefreshRecords();
        }

        #endregion

        #region CRUD

        private void VehicleTypesPage_OnRefresh(object sender, EventArgs e) {
            if (dt != null) dt.Dispose();
            //WHERE Type Like '%'& [@search] &'%';
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@search", GetSearchText());
            dt = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_VehicleType", true, p);
            SetRecordsDataSource(dt);
            HideColumn("ID");
            SetRecordsDataGridMultiselect(false);
            RecordsDGVFitColumnToContent("Type");
        }

        private void VehicleTypesPage_OnSearch(object sender, EventArgs e) {
            RefreshRecords();
        }

        private void VehicleTypesPage_OnAdd(object sender, EventArgs e) {
            VehicleTypeDialog dlg = new VehicleTypeDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Add;
            dlg.SetExistingTypes(GetRecordsValueArray("Type"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string ntype = "";
                dlg.GetFieldValues(out ntype);
                //VALUES ([@type]);
                OleDbParameter[] p = new OleDbParameter[1];
                p[0] = new OleDbParameter("@type", ntype);
                bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Add_VehicleType", true, p);
                if (succ) {
                    Notifier.ShowInformation("Successfully added new vehicle type.", "Vehicle Type");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to add new vehicle type.");
                }
            }
        }

        private void VehicleTypesPage_OnDelete(object sender, EventArgs e) {
            if (Notifier.ShowConfirm("Deleting a vehicle type will make related rate groups and rates useless. Please continue if you are sure of your actions. Proceed to deleting this type?", "Confirm Delete") == System.Windows.Forms.DialogResult.Yes) {
                OleDbParameter[] p = new OleDbParameter[1];
                //WHERE ID=[@id];
                p[0] = new OleDbParameter("@id", Cell("ID"));
                bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Delete_VehicleType", true, p);
                if (succ) {
                    Notifier.ShowInformation("Successfully deleted vehicle type.", "Vehicle Type");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to delete vehicle type.");
                }
            }
        }

        private void VehicleTypesPage_OnEdit(object sender, EventArgs e) {
            VehicleTypeDialog dlg = new VehicleTypeDialog();
            dlg.FileMaintenanceMode = FileMaintenanceDialog.FileMaintenanceDialogMode.Editing;
            dlg.SetFieldValues(Cell("Type"));
            dlg.SetExistingTypes(GetRecordsValueArray("Type"));
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string ntype = "";
                dlg.GetFieldValues(out ntype);
                //SET Type = [@type] WHERE ID=[@id];
                OleDbParameter[] p = new OleDbParameter[2];
                p[0] = new OleDbParameter("@type", ntype);
                p[1] = new OleDbParameter("@id", "{" + Cell("ID") + "}");
                bool succ = DatabaseManager.GetMainDatabase().ExecuteNonQuery("Update_VehicleType", true, p);
                if (succ) {
                    Notifier.ShowInformation("Successfully updated vehicle type.", "Vehicle Type");
                    RefreshRecords();
                } else {
                    Notifier.ShowError("Failed to update vehicle type.");
                }
            }
        }

        #endregion

        #region Statuc Functions

        internal static ComboboxDataItem[] QueryVehicleTypes() {
            List<ComboboxDataItem> lst = new List<ComboboxDataItem>();
            OleDbParameter[] p = new OleDbParameter[1];
            p[0] = new OleDbParameter("@search", "");
            DataView dd = DatabaseManager.GetMainDatabase().ExecuteQuery("Query_VehicleType", true, p).AsDataView();
            foreach (DataRow drow in dd.Table.Rows) {
                lst.Add(new ComboboxDataItem(drow["ID"].ToString(), drow["Type"].ToString()));
            }
            return lst.ToArray();
        }


        #endregion

    }

}
