﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace PayrollSystem {
    public partial class BackgroundLoadingDataSample : Form {

        #region Ctor

        public BackgroundLoadingDataSample() {
            InitializeComponent();
        }

        #endregion

        #region Local Variables

        int ctr = 0;

        #endregion

        #region Local Functions

        void count(object o) {
            int limit = (int)o;
            ctr = 0;
            for (; ctr <= limit; ctr++) {
                if (!lblCtr.IsDisposed) {
                    lblCtr.Invoke((MethodInvoker)(() => 
                    {
                        if(!lblCtr.IsDisposed) { 
                            lblCtr.Text = ctr.ToString(); 
                        }
                    }));
                    Debug.WriteLine(ctr);
                    Thread.Sleep(1000);
                } else return;
            }
        }

        #endregion

        #region Thead-safe functions

        void setText(object control, string text) {

        }

        #endregion


        #region Window Eventhandlers

        private void btnStart_Click(object sender, EventArgs e) {
            Thread t = new Thread(new ParameterizedThreadStart(count));
            t.Start(100);
        }

        #endregion
    }
}
