﻿/*
 * Interface to allow other object to call update database status.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem.Interfaces {

    public interface IDatabaseStatusUpdater {

        /* Update Database Connection Status */
        void UpdateDBConnectivityStatus(bool isConnected);

        /* Update name of active database */
        void UpdateActiveDatabaseName(string db_name);

    }

}
