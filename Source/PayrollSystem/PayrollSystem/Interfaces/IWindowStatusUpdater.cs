﻿/*
 *  Interface to allow interfacing
 *  with windows that have status bars.
 *  
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayrollSystem.Interfaces {

    /// <summary>
    /// Interface to allow window status updating.
    /// </summary>
    interface IWindowStatusUpdater {

        /* Update status label */
        void UpdateWindowStatus(String status);

        /* Reset the status label (e.g. Ready or empty string) */
        void ResetWindowStatus();

        /* Update the selected records count status label */
        void UpdateSelectedRecordCountStatus(int selectedRecordCount);

        /* Update the total records count status label */
        void UpdateTotalRecordCountStatus(int totalRecordCount);

    }

}
