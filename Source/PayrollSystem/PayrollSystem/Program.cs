﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace PayrollSystem {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CreateApplicationDirectory();

            if (BuildLocalDBConnection()) {
                LoginWindow login = new LoginWindow();
                if (login.ShowDialog() == DialogResult.OK) {
                    Application.Run(new MainWindow());
                }
            }

        }

        static void CreateApplicationDirectory() {
            if (Directory.Exists(GlobalVariables.DB_DIR) == false) {
                try {
                    Directory.CreateDirectory(GlobalVariables.DB_DIR);
                } catch (Exception ex) {
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }
        }

        static bool BuildLocalDBConnection() {
            DatabaseManager.BuildLocalDBConnectionString();
            return DatabaseManager.CreateLocalDBConnection();
        }

    }
}
