﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("5SZian Trucking Services Payroll System")]
[assembly: AssemblyDescription("Payroll system for 5SZian Trucking Services.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mach 21 Studios")]
[assembly: AssemblyProduct("Payroll System")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c39d7951-1360-43e3-9d13-b464ee4bb347")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
//[assembly: AssemblyVersion("1.0.0.0")]
//[assembly: AssemblyFileVersion("0.1.*")]
[assembly: AssemblyVersion("2.0.17.0410")]