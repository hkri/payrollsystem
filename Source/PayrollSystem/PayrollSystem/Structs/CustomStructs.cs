﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace PayrollSystem {

    /// <summary>
    /// Struct to be used for batch insert of trucking log (CSV import)
    /// </summary>
    struct TruckingLogRecord {

        public string Date;
        public string PlateNumber;
        public string DRNo;
        public string Destination;
        public string Driver;
        public string Helper;

        public double Rate;
        public double DriverRate;
        public double HelperRate;

        public bool IsBackload;

        public TruckingLogRecord(string date, string plateNo, string drno, string destination, double rate, string driver, double driverRate, string helper, double helperRate, bool isBackload) {
            Date = date;
            PlateNumber = plateNo;
            DRNo = drno;
            Destination = destination;
            Driver = driver;
            Helper = helper;
            Rate = rate;
            DriverRate = driverRate;
            HelperRate = helperRate;
            IsBackload = isBackload;
        }

    }

}
