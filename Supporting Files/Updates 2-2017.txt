----------------------------------------------------------
Release Notes v.2.0.17.313
----------------------------------------------------------
[Database Updates]
- Added new fields for the payroll:
  > SSS
  > PhilHealth
  > Pag-IBIG

[Software Updates]

- Now remembers last date filter set on some pages.

- Column sorting now retained (Cash Report page) when
  records are refreshed.

- Collapse/Expand Cash Report summary header allows
  more room for viewing records. Adjusted spacings
  and location of summary info.

- Added column freezing in Cash Report editor. Frozen
  columns are indicated by a dark background. Freezing
  columns is accessible by right-clicking the target column.

- New Payroll summary column added.

- Payslip now adjusted to show the benefit deductions.

- Can now export individual payslip records each saved in
  PDF files. The payslips are saved in a folder where the
  payslip spreadsheet is saved.

- Totals panel can now be hidden/shown.

- Adjusted record filterings on Trucking Logs:
  Page will now only start loading records when page is
  Refreshed or "Set" button is pressed to avoid lags on
  large data.

- Added locked record filter on Trucking Log page.